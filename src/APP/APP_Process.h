//-----------------------------------------------------------------------------|
//	APP_Process.h
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __APP_PROCESS_H__
#define __APP_PROCESS_H__
#include "common.h"

//===================================================================
// Public Function Prototypes
//===================================================================
void APP_main(void);                        // Called at the end of the boot process, does not return
Schedule_Type APP_GetSchedule(void);        // returns the current schedule
void APP_SetSchedule(Schedule_Type sch);   // called to set the next schedule
void APP_IdSet_DA(uint8_t mode);

#endif // __APP_PROCESS_H__

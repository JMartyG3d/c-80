//-----------------------------------------------------------------------------|
//	APP_Process.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include "common.h"
#include "APP_Process.h"
#include "IOX_Control.h"
#include "PWR_Control.h"
#include "TMR_Control.h"
#include "SRC_Control.h"
#include "KEY_Process.h"
#include "DISP_Control.h"
#include "UI_Control.h"
#include "NVM_Control.h"
#include "NVM_Access.h"
#include "VOL_Control.h"
#include "OUT_Control.h"
#include "ADC_Process.h"
#include "IR_Control.h"
#include "TRIG_Control.h"
#include "RS232_Control.h"
#include "PIO_Config.h"
#include "UI_Main.h"
#include "DA_Config.h"
#include "PT_Control.h"
#include "TONE_Control.h"

static Schedule_Type __sched;
static uint8_t __DA_Type = DA_HDWR_DA1;

//==============================================================================
// Private Function Prototypes
//==============================================================================
static void app_inactive(void);   // schedule in "sleep/standby"
static void app_active(void);     // schedule when device is "on"
static void app_power_up(void);   // schedule for "off->on" transition
static void app_warm_up(void);    // schedule for "warm up period" after power up
static void app_power_down(void); // schedule for "on->off" transition

//==============================================================================
// Public Functions
//==============================================================================
void APP_main(void) // Called at the end of the boot process, does not return
{
    // first time entering main is always from AC power on
    __sched = E_SCHED_INIT;
    PWR_Initialize();

    // execute schedule
    for(;;) {
        switch (__sched) {
            case E_SCHED_INACTIVE:  app_inactive();     break;
            case E_SCHED_ACTIVE:    app_active();       break;
            case E_SCHED_POWERUP:   app_power_up();     break;
            case E_SCHED_WARMUP:    app_warm_up();      break;
            case E_SCHED_POWERDOWN: app_power_down();   break;
            case E_SCHED_INIT:
            default:
                break;
        }
        WDT->WDT_MR = WDT_MR_WDDIS;
        delay_ms(1);
    }
}

Schedule_Type APP_GetSchedule(void)        // returns the current schedule
{
    return __sched;
}

void APP_SetSchedule(Schedule_Type sch)   // called to set the next schedule
{
    __sched = sch;
}

void APP_IdSet_DA(uint8_t mode)
{
    __DA_Type = mode;
}

//==============================================================================
// Private Functions
//==============================================================================
static void app_inactive(void)
{   // loop (via APP_main) until another power command is received
    WDT->WDT_MR = WDT_MR_WDDIS; // refresh WDT
    RS232_Periodic(); // service RS232
    IR_Periodic();    // service IR
    KEY_Periodic();   // service button presses
    PWR_Periodic();   // check for power change, ac loss
    PT_Periodic();
    delay_ms(2);
}

static void app_active(void)
{
    DA_Periodic();
    PWR_Periodic(); // check for power change, ac loss, update audio detect

    if (TMR_Timeout(E_TMR_SRCMAKE)) { // Perform a source make following a break
        // service keys and UI before a source make
        KEY_Periodic(); // key process
        UI_Periodic();  // ui process
        SRC_Periodic();
    }

    if(TMR_Timeout(E_TMR_2MS)) { //  2ms tasks
        WDT->WDT_MR = WDT_MR_WDDIS; // refresh WDT
        ADC_Start();    // start adc conversion (this might be better off at a 10ms rate than 2ms)
        VOL_PeriodicRamp();
    }

    DA_Periodic();
    PWR_Periodic(); // check for power change, ac loss, update audio detect

    if(TMR_Timeout(E_TMR_20MS)) { // 20ms tasks
        PT_Periodic();
        VOL_PeriodicAccum();
        RS232_Periodic(); // service RS232
        IR_Periodic();  // service IR
        KEY_Periodic(); // key process
        OUT_Periodic(); // Check Headphone state when NOT Connected
        UI_Periodic();  // ui process
        NVM_Periodic(); // nvm periodic
    }

    if(TMR_Timeout(E_TMR_SRCDELAY)) {
        if(UI_GetMenu() == E_MENU_MAIN)
            SRC_PeriodicDelay();
    }

    DA_Periodic();
    PWR_Periodic(); // check for power change, ac loss, update audio detect
}

static void app_power_up(void)
{
    PWR_Powerup();                  // perform power up tasks

    // Complete Handshake prior to Src Change
    // Test for DA2 @115200 Baud
    if(__DA_Type == DA_HDWR_DA2){
        for(uint8_t i = 0; i < 25; i++) {
            DA_Periodic();
            delay_ms(70);

            if(DA_DspDelay())
                i = 0;
        }
    }
    // If not DA2 then test for DA1 @38400 Baud
    if(!DA_Installed()){
        DA_Config(3,DA_HDWR_DA1);
        for(uint8_t i = 0; i < 20; i++) {
            DA_Periodic();
            delay_ms(20);

            if(DA_DspDelay())
                i = 0;
        }
    }
    PWR_CheckFwDownload();
    TMR_Reset(E_TMR_WARMUP);        // reset power up timer
    SRC_Powerup();                  // trigger initial source change
    __sched = E_SCHED_WARMUP;       // set next schedule
}

static void app_warm_up(void)
{
    bool warm_up = FALSE;

    // loop until warm up timer expires or powerdown signal received
    while(!warm_up && (__sched == E_SCHED_WARMUP)) {
        PWR_Periodic();

        if(PT_Active() && ((REG_PIOA_PDSR & PIN23_PASS_IN))) {
            APP_SetSchedule(E_SCHED_POWERDOWN);
            PT_PoweringOffClr();
        }

        if (TMR_Timeout(E_TMR_2MS)) {//  2ms tasks
            WDT->WDT_MR = WDT_MR_WDDIS; // refresh WDT
            VOL_PeriodicRamp();
        }

        if (TMR_Timeout(E_TMR_SRCMAKE)) // Perform a source make following a break
            SRC_Periodic();

        // Service timer tasks
        if (TMR_Timeout(E_TMR_WARMUP)) { // warm up timer reached
            OUT_Warmup();           // enable outputs
            VOL_WakeupRamp();       // wake up ramp volume
            delay_ms(100);
            warm_up = TRUE;         // key exit of power up sequence
            TONE_Set();
        }

        PWR_Periodic(); // check for power change, ac loss
        delay_ms(1);
    }

    KEY_Init(); // clear pending key presses

    // if not cancelled by a power down signal
    if(__sched == E_SCHED_WARMUP) {
        __sched = E_SCHED_ACTIVE; // set next schedule
        KEY_PwrEnable();
    }
}

static void app_power_down(void)
{
    KEY_Init();                     // clear pending key presses
    PWR_Powerdown();                // perform power down sequence
    __sched = E_SCHED_INACTIVE;     // set schedule to INACTIVE
    KEY_PwrEnable();
}
/*
----------------------------------------------------------------------------|
-	conf_board.h
-	Abstract: Control which board configuration files are included

-	McIntosh Laboratories Inc.
----------------------------------------------------------------------------|
*/

#ifndef CONF_BOARD_H_INCLUDED
#define CONF_BOARD_H_INCLUDED

/* WDT enabled at startup. */
// #define CONF_BOARD_KEEP_WATCHDOG_AT_INIT

/* Configure PIO pins */
#define CONF_BOARD_PIO

/** Configure TWI pins */
#define CONF_BOARD_TWI0

/* UART Enable pins (Com Port). */
#define CONF_BOARD_UART_CONSOLE

/* ADC Configure pins */
//#define CONF_BOARD_ADC

/* USART pins are configured as basic serial port by default. */
/* Configure USART RXD, TXD, CTS, RTS and SCK pins            */
#define CONF_BOARD_USART
#define CONF_BOARD_USART_RXD
#define CONF_BOARD_USART_TXD
//#define CONF_BOARD_USART_CTS
//#define CONF_BOARD_USART_RTS
//#define CONF_BOARD_USART_SCK

/* SPI Configure pins */
#define CONF_BOARD_SPI
#define CONF_BOARD_SPI_NPCS0
//#define CONF_BOARD_SPI_NPCS1
//#define CONF_BOARD_SPI_NPCS2
//#define CONF_BOARD_SPI_NPCS3

/* Configure TC pins */
//#define CONF_BOARD_TC0_CH1

#endif /* CONF_BOARD_H_INCLUDED */

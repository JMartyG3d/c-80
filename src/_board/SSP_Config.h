//-----------------------------------------------------------------------------|
//	ssp_config.h
//	Abstract: contains definitions and configuration details for
//  features and devices that are available on the board, e.g., frequency and
//  startup time for an external crystal, external memory devices, LED and USART
//  pins.
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __SSP_CONFIG_H__
#define __SSP_CONFIG_H__

#include "compiler.h"
#include "system_sam4s.h"

#define BOARD_REV_A

//----------------------------------------------------------------------------//
/** Board oscillator settings */
#define BOARD_FREQ_SLCK_XTAL        (32768U)
#define BOARD_FREQ_SLCK_BYPASS      (32768U)
#define BOARD_FREQ_MAINCK_XTAL      (12000000U)
#define BOARD_FREQ_MAINCK_BYPASS    (12000000U)
#define BOARD_MCK                   CHIP_FREQ_CPU_MAX // Master Clock
#define BOARD_OSC_STARTUP_US        15625
//----------------------------------------------------------------------------//

#define PIN_SPARE       	(-1)
#define PIN_DEDICATED    	(-1)

// Pin Definitions				 		  // PIO       AD	Desc
//--------------------------------------  ------------------------------------//
#define PIN01_ADVREF		PIN_DEDICATED // -	  -    -    ADC, DAC and Analog Comparator 3.3V Reference
#define PIN02_GND           PIN_DEDICATED // -	  -    -    ADC Return
#define PIN03_AD4		    PIO_PB0X1_AD4 // -	  PB0  AD4
#define PIN04_AD5		    PIO_PB1X1_AD5 // -	  PB1  AD5
#define PIN05_AD6		 	PIO_PB2X1_AD6 // -	  PB2  AD6
#define PIN06_AD7		 	PIN_SPARE     // -	  PB3  AD7
#define PIN07_VDDIN         PIN_DEDICATED // -	  -	   -    Voltage Regulator Input, ADC, DAC and Analog Comparator Power Supply(1.62V to 3.6V)
#define PIN08_VDDOUT        PIN_DEDICATED // -	  -    -    Voltage Regulator Output(1.2V output Nominal)
#define PIN09_IR_CONTROL    PIO_PA17      // PA17 -    AD0  OUTPUT: IR Control for Data Ports
#define PIN10_IR_DATA_OUT   PIO_PA18      // PA18 -    AD1  OUTPUT: Converted IR Data going to Data Ports
#define PIN11_DAC_RXD       PIO_PA21      // PA21 -    AD8  Data Rx from Digital Audio Section
#define PIN12_VDDCORE		PIN_DEDICATED // -	  -
#define PIN13_PASS_LGT_CTL  PIO_PA19      // PA19 -    AD2  INPUT: Passthru Meterlight Control
#define PIN14_DAC_TXD		PIO_PA22      // PA22 -    AD9  Data Tx from Digital Audio Section
#define PIN15_IR_REAR 		PIO_PA23      // PA23 -    -
#define PIN16_IR_FRONT		PIO_PA20      // PA20 -    AD3
#define PIN17_GND	  		PIN_DEDICATED // -	  -    -
#define PIN18_VDDIO			PIN_DEDICATED // -	  -    -
#define PIN19_AC_DETECT		PIO_PA16      // PA16 -    -    INPUT: Monitored Power Supply signal
#define PIN20_POWER_KEY		PIO_PA15      // PA15 -    -    INPUT: Monitored On/Off Key Press
#define PIN21_SPI_SCL		PIO_PA14      // PA14 -    -    VFD SPI Clock
#define PIN22_SPI_SDA		PIO_PA13      // PA13 -    -    VFD SPI Data
#define PIN23_PASS_IN		PIO_PA24      // PA24 -    -    INPUT: Monitored Passthru input signal
#define PIN24_VDDCORE		PIN_DEDICATED // -	  -    -
#define PIN25_RTY_INPUT_A	PIO_PA25      // PA25 -    -
#define PIN26_RTY_INPUT_B	PIO_PA26      // PA26 -    -
#define PIN27_SPI_MISO		PIN_SPARE     // PA12 -    -
#define PIN28_VFD_CE		PIO_PA11      // PA11 -    -    VFD SPI CS
#define PIN29_DBU_TXD		PIO_PA10      // PA10 -    -
#define PIN30_DBU_RXD		PIO_PA9       // PA9  -    -
#define PIN31_RTY_VOL_A     PIO_PA8       // PA8  -    -
#define PIN32_RTY_VOL_B	    PIO_PA7       // PA7  -    -
#define PIN33_JTAG_TDI		PIN_DEDICATED // -    PB4  -
#define PIN34_DOCK_TXD		PIN_SPARE     // PA6  -    -
#define PIN35_DOCK_RXD		PIN_SPARE     // PA5  -    -
#define PIN36_TWI0_SCL		PIO_PA4       // PA4  -    -
#define PIN37_AUDIO_DET	    PIO_PA27      // PA27 -    -    INPUT: Monitored Audio signal for Auto-Off
#define PIN38_DA_RESET 		PIO_PA28      // PA28 -    -    OUTPUT: Reset DA Module
#define PIN39_RESET			PIN_DEDICATED // -	  -    -
#define PIN40_TST			PIN_DEDICATED // -	  -    -
#define PIN41_VOL_TONE_CLK  PIO_PA29      // PA29 -    -    SPI
#define PIN42_TONE_CS	    PIO_PA30      // PA30 -    -    SPI
#define PIN43_TWI0_SDA		PIO_PA3       // PA3  -    -
#define PIN44_VOL_TONE_DATA PIO_PA2       // PA2  -    -    SPI
#define PIN45_VDDIO			PIN_DEDICATED // -	  -    -
#define PIN46_GND			PIN_DEDICATED // -	  -    -
#define PIN47_SPARE			PIN_SPARE     // PA1  -    -
#define PIN48_SPARE			PIN_SPARE     // PA0  -    -
#define PIN49_TDO			PIN_DEDICATED // -    PB5  -
#define PIN50_JTAGSEL		PIN_DEDICATED // -	  -    -
#define PIN51_TMS			PIN_DEDICATED // -    PB6  -
#define PIN52_VOL_CS		PIO_PA31      // PA31 -	   -    SPI
#define PIN53_TCK			PIN_DEDICATED // -	  PB7  -
#define PIN54_VDDCORE		PIN_DEDICATED // -	  -    -
#define PIN55_ERASE			PIN_DEDICATED // -	  PB12 -
#define PIN56_DDM			PIN_DEDICATED // -	  PB10 -    USB-
#define PIN57_DDP			PIN_DEDICATED // -	  PB11 -    USB+
#define PIN58_VDDIO			PIN_DEDICATED // -	  -    -
#define PIN59_VDDFLASH		PIN_SPARE     // -	  PB13 -
#define PIN60_GND			PIN_DEDICATED // -	  -    -
#define PIN61_XOUT			PIN_DEDICATED // -	  PB8  -
#define PIN62_XIN			PIN_DEDICATED // -	  PB9  -
#define PIN63_SPARE			PIN_SPARE     // -	  PB14 DAC1
#define PIN64_VDDPLL		PIN_DEDICATED // -	  -    -
//----------------------------------------------------------------------------//

// pins that should not be deactivated during standy
#define C_PIO_POWER_PINS    (PIN20_POWER_KEY | PIN16_IR_FRONT | PIN15_IR_REAR | PIN19_AC_DETECT)//! TODO (PIN09_PWRCTRL_IN |  | PIN16_IR_FRONT | PIN29_TXD | PIN30_RXD)

// TWI0 ----------------------------------------------------------|
#define PINS_TWI0_DATA       PIO_PA3A_TWD0
#define PINS_TWI0_DATA_FLAGS (PIO_PERIPH_A | PIO_DEFAULT)
#define PINS_TWI0_CLK        PIO_PA4A_TWCK0
#define PINS_TWI0_CLK_FLAGS  (PIO_PERIPH_A | PIO_DEFAULT)

#define TWI_CLOCK            100000

#define BOARD_AT24C_TWI_INSTANCE  TWI0
#define BOARD_AT24C_ADDRESS       0x50
#define BOARD_CLK_TWI_EEPROM      PINS_TWI0_CLK
#define BOARD_CLK_TWI_MUX_EEPROM  IOPORT_MODE_MUX_A

// ADC  ----------------------------------------------------------|
#define C_ADC_CLOCK             2000000

// UART (RS232) --------------------------------------------------|
#define PINS_UART0				(PIO_PA9A_URXD0 | PIO_PA10A_UTXD0)
#define PINS_UART0_FLAGS		(PIO_PERIPH_A | PIO_DEFAULT)

#define PINS_UART0_MASK			(PIO_PA9A_URXD0|PIO_PA10A_UTXD0)
#define PINS_UART0_PIO			PIOA

#define CONSOLE_UART UART0
#define CONSOLE_ID   ID_UART0

#define UART0_BAUDRATE          115200

// USART1 (UAD) --------------------------------------------------|
#define PINS_USART1_RXD         PIO_PA21A_RXD1
#define PINS_USART1_RXD_FLAGS   (PIO_PERIPH_A | PIO_DEFAULT)

#define PINS_USART1_TXD         PIO_PA22A_TXD1
#define PINS_USART1_TXD_FLAGS   (PIO_PERIPH_A | PIO_DEFAULT)

#define PINS_USART1_PIO         PIOA

#define BOARD_USART1_BAUDRATE1   38400 // Used for DA1
#define BOARD_USART1_BAUDRATE2   115200// Used for DA2

// SPI -----------------------------------------------------------|
#define PINS_SPI_MISO           (PIO_PA12A_MISO)
#define PINS_SPI_MISO_FLAGS     (PIO_PERIPH_A | PIO_PULLUP)
#define PINS_SPI_MOSI           (PIO_PA13A_MOSI)
#define PINS_SPI_MOSI_FLAGS	    (PIO_PERIPH_A | PIO_PULLUP)
#define PINS_SPI_SPCK           (PIO_PA14A_SPCK)
#define PINS_SPI_SPCK_FLAGS     (PIO_PERIPH_A | PIO_PULLUP)

#define PINS_SPI_NPCS0          (PIO_PA11A_NPCS0)
#define PINS_SPI_NPCS0_FLAGS    (PIO_PERIPH_A | PIO_PULLUP)

#define PINS_SPI_PIO            PIOA
#define PINS_SPI_ID             ID_PIOA

#define CONFIG_SPI_CS           0
#define CONFIG_SPI_DLYBCS       0x0E
#define CONFIG_SPI_CPOL         1
#define CONFIG_SPI_CLK_PHASE    0
#define CONFIG_SPI_BITS_PER_TX  SPI_CSR_BITS_16_BIT
#define CONFIG_SPI_CLOCK        250000   // KHz
#define CONFIG_SPI_DLYBCT       2
#define CONFIG_SPI_DLYBS        0x5E

#endif // __SSP_CONFIG_H__

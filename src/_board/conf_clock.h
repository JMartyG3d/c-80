//-----------------------------------------------------------------------------|
//	config_clock.h
//	Abstract: McIntosh IR Remote Codes
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef CONF_CLOCK_H_INCLUDED
#define CONF_CLOCK_H_INCLUDED

// ===== System Clock (MCK) Master Clock Source Options
//#define CONFIG_SYSCLK_SOURCE        SYSCLK_SRC_SLCK_RC        // Internal 32kHz RC oscillator as master source clock
//#define CONFIG_SYSCLK_SOURCE        SYSCLK_SRC_SLCK_XTAL      // External 32kHz crystal oscillator as master source clock
//#define CONFIG_SYSCLK_SOURCE        SYSCLK_SRC_SLCK_BYPASS    // External 32kHz bypass oscillator as master source clock
//#define CONFIG_SYSCLK_SOURCE        SYSCLK_SRC_MAINCK_4M_RC   // Internal 4MHz RC oscillator as master source clock
//#define CONFIG_SYSCLK_SOURCE        SYSCLK_SRC_MAINCK_8M_RC   // Internal 8MHz RC oscillator as master source clock
//#define CONFIG_SYSCLK_SOURCE        SYSCLK_SRC_MAINCK_12M_RC  // Internal 12MHz RC oscillator as master source clock
//#define CONFIG_SYSCLK_SOURCE        SYSCLK_SRC_MAINCK_XTAL    // External crystal oscillator as master source clock
//#define CONFIG_SYSCLK_SOURCE        SYSCLK_SRC_MAINCK_BYPASS  // External bypass oscillator as master source clock
#define CONFIG_SYSCLK_SOURCE        SYSCLK_SRC_PLLACK           // Use PLLACK as master source clock
//#define CONFIG_SYSCLK_SOURCE        SYSCLK_SRC_PLLBCK         // Use PLLBCK as master source clock

// ===== System Clock (MCK) Prescaler Options   (Fmck = Fsys / (SYSCLK_PRES))
#define CONFIG_SYSCLK_PRES          SYSCLK_PRES_1
//#define CONFIG_SYSCLK_PRES          SYSCLK_PRES_2
//#define CONFIG_SYSCLK_PRES          SYSCLK_PRES_4
//#define CONFIG_SYSCLK_PRES          SYSCLK_PRES_8
//#define CONFIG_SYSCLK_PRES          SYSCLK_PRES_16
//#define CONFIG_SYSCLK_PRES          SYSCLK_PRES_32
//#define CONFIG_SYSCLK_PRES          SYSCLK_PRES_64
//#define CONFIG_SYSCLK_PRES          SYSCLK_PRES_3

// ===== PLL0 (A) Options   (Fpll = (Fclk * PLL_mul) / PLL_div)
// Use mul and div effective values here.
#define CONFIG_PLL0_SOURCE          PLL_SRC_MAINCK_XTAL // Config PLL0 to use ext RC oscillator as its source
#define CONFIG_PLL0_MUL             4         //
#define CONFIG_PLL0_DIV             1         // range [1-255]

// ===== PLL1 (B) Options   (Fpll = (Fclk * PLL_mul) / PLL_div)
// Use mul and div effective values here.
//#define CONFIG_PLL1_SOURCE          PLL_SRC_MAINCK_XTAL
//#define CONFIG_PLL1_MUL             1           // 5
//#define CONFIG_PLL1_DIV             2           // 1 range [1-255]

// ===== USB Clock Source Options   (Fusb = FpllX / USB_div)
// Use div effective value here.
#define CONFIG_USBCLK_SOURCE        USBCLK_SRC_PLL0
#define CONFIG_USBCLK_DIV           1

// ===== Target frequency (System clock)
// - XTAL frequency: 18.432 MHz
// - System clock source: PLLA
// - System clock prescaler: 2 (divided by 2)
// - PLLA source: XTAL
// - PLLA output: (XTAL * 13) / 1
// - System clock: (18,432,000 * 13)/ 1 / 2 = 120MHz
// ===== Target frequency (USB Clock)
// - USB clock source: PLLB
// - USB clock divider: 2 (divided by 2)
// - PLLB output: XTAL * 5 / 1
// - USB clock: (18,432,000 * 5) / 1 / 2 = 48MHz


#endif /* CONF_CLOCK_H_INCLUDED */

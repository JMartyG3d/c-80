//-----------------------------------------------------------------------------|
//	DISP_Control.c
//	Abstract: Display Processing functions
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include <string.h>
#include "DISP_Control.h"
#include "UI_Control.h"
#include "UI_Setup.h"
#include "PT_Control.h"
#include "IOX_Control.h"
#include "NVM_Access.h"
#include "DA_Config.h"
#include "APP_Process.h"
#include "VOL_Control.h"
#include "NVM_Access.h"
#include "NVM_Control.h"

// include correct header file
#ifdef C_DISP_IS_VFD
    #include "VFD_Driver.h"
#endif
#ifdef C_DISP_IS_OLED
    #include "OLED_Driver.h"
#endif

// display device driver function pointer typedefs
typedef struct {
    void (*init)(void);                         // initialization function
    void (*setstate)(uint8_t state);            // changes display control register
    void (*setpos)(uint8_t col, uint8_t row);   // set cursor position to column, row
    void (*print)(char c);                      // print c at current cursor position
    void (*mode)(uint8_t mode);                 // Sets the cursor mode (0=nothing, 1=blink, 2=cursor, 3=blinkcursor)
    void (*cgram)(uint8_t type);                // Fills the cgram with the specified type (0=Level, 1=Balance Left, 2=Balance Right)
#ifdef C_DISP_IS_VFD
    void (*brightness)(uint8_t opt);            // Set the brightness of VFD display (0=100%, 1=75%, 2=50%, 3=25%)
#endif
} disp_display_driver_funcs_t;


// function pointer configuration tables (update for project)
#ifdef C_DISP_IS_VFD
static disp_display_driver_funcs_t __dispf = {
    &VFD_Init,
    &VFD_SetState,
    &VFD_SetPosition,
    &VFD_PrintChar,
    &VFD_SetCursorMode,
    &VFD_FillCGRAM,
    &VFD_SetDutyCycle,
};
#else
static disp_display_driver_funcs_t __dispf = { // Based on MHA150/MXA80 - will need updates for future products
    &OLED_Init,
    &OLED_SetState,
    &OLED_SetPosition,
    &OLED_PrintChar,
    &OLED_SetBlinkCursor,
};
#endif

//===================================================================
// Private Function Prototypes
//===================================================================
static void disp_clearline(uint8_t line);

//===================================================================
// Public Functions
//===================================================================
void DISP_Initialize(void)
{
    __dispf.init();
}

void DISP_SpiInit(void)
{
    spi_disable(SPI);
	spi_reset(SPI);
	spi_set_master_mode(SPI);
    spi_set_variable_peripheral_select(SPI);
	spi_disable_mode_fault_detect(SPI);
	spi_set_peripheral_chip_select_value(SPI, CONFIG_SPI_CS);
	spi_set_delay_between_chip_select(SPI, CONFIG_SPI_DLYBCS);

    // CS0
	spi_set_clock_polarity(SPI, CONFIG_SPI_CS, CONFIG_SPI_CPOL);
	spi_set_clock_phase(SPI, CONFIG_SPI_CS, CONFIG_SPI_CLK_PHASE);
	spi_set_bits_per_transfer(SPI, CONFIG_SPI_CS, CONFIG_SPI_BITS_PER_TX);
	spi_set_baudrate_div(SPI, CONFIG_SPI_CS, ((sysclk_get_cpu_hz() / CONFIG_SPI_CLOCK)-1));
	spi_set_transfer_delay(SPI, CONFIG_SPI_CS, CONFIG_SPI_DLYBS, CONFIG_SPI_DLYBCT);

    spi_enable(SPI);
	pdc_enable_transfer(PDC_SPI, PERIPH_PTCR_RXTEN);
}

void DISP_SetState(uint8_t state)
{
    __dispf.setstate(state);
}

void DISP_ClearLine(uint8_t line)
{
    if (line == C_DISP_BOTHLINES){
        disp_clearline(C_DISP_LINE1);
        disp_clearline(C_DISP_LINE2);
    }
    else
        disp_clearline(line);
}

void DISP_Print(char * str, uint8_t line, uint8_t pos)
{
    uint32_t len = strlen(str);
    __dispf.setpos(pos, line);

    for(uint8_t i=0;i<len;i++) {
        if((i+pos)<C_DISP_LINELENGTH) // don't print off the screen
            __dispf.print(str[i]);
    }
}

void DISP_PrintLine(uint8_t line, char * str)
{
    uint32_t len = strlen(str);
    __dispf.setpos(0, line); // move cursor to start of line

    for(uint8_t i=0;i<C_DISP_LINELENGTH;i++) {
        if (i<len)
            __dispf.print(str[i]);
        else
            __dispf.print(' '); // fill rest of line with blanks
    }
}

void DISP_PrintCenter(uint8_t line, char * str) // str must be null terminated for this to work
{
    uint8_t len, pad, left, right;
    len = strlen(str);
    pad = C_DISP_LINELENGTH - len;

    if (pad&1==1) { // odd padding - add extra padding to the post side
        left = pad/2;
        right = left+1;
    }
    else // even
        left = right = pad/2;

    __dispf.setpos(0, line);     // move cursor to start of line

    for(uint8_t i=0;i<left;i++)  // print the left pad
        __dispf.print(' ');

    for(uint8_t i=0;i<len;i++)   // print the string
        __dispf.print(str[i]);

    for(uint8_t i=0;i<right;i++) // print the right pad
        __dispf.print(' ');
}

// prints over a specific section of text, filling in blanks - reduces re-draw
void DISP_PrintOver(uint8_t x, uint8_t y, uint8_t len, char * str)
{
    __dispf.setpos(x,y);

    for(uint32_t i=0;i<len;i++) {
        // don't print off the screen
        if ((i+x)<C_DISP_LINELENGTH) {
            if (str[i] != '\0')
                __dispf.print(str[i]);
            else
                __dispf.print(' '); // fill remaining length with blanks
        }
    }
}

void DISP_SetBlinkCursor(uint8_t setting)
{
    __dispf.mode(setting);
}

void DISP_FillCGRAM(uint8_t type)
{
    __dispf.cgram(type);
}

// display specific function
#ifdef C_DISP_IS_VFD
void DISP_SetDutyCycle(uint8_t cycle)
{
    __dispf.brightness(cycle);
}
#endif

void DISP_MainMenu(void)
{
    char ohm25_str[4] = {0x32,0x35,0x9A,'\0'};
    char ohm25_mono_str[21] = {'M','O','N','O',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',0x32,0x35,0x9A,'\0'};
    char ohm50_str[4] = {0x35,0x30,0x9A,'\0'};
    char ohm50_mono_str[21] = {'M','O','N','O',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',0x35,0x30,0x9A,'\0'};
    char ohm100_str[5] = {0x31,0x30,0x30,0x9A,'\0'};
    char ohm100_mono_str[21] = {'M','O','N','O',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',0x31,0x30,0x30,0x9A,'\0'};
    char ohm200_str[5] = {0x32,0x30,0x30,0x9A,'\0'};
    char ohm200_mono_str[21] = {'M','O','N','O',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',0x32,0x30,0x30,0x9A,'\0'};
    char ohm400_str[5] = {0x34,0x30,0x30,0x9A,'\0'};
    char ohm400_mono_str[21] = {'M','O','N','O',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',0x34,0x30,0x30,0x9A,'\0'};
    char ohm1K_str[6] = {0x31,0x30,0x30,0x30,0x9A,'\0'};
    char ohm1K_mono_str[21] = {'M','O','N','O',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',0x31,0x30,0x30,0x30,0x9A,'\0'};

    switch(NVM_GetSrc()) {
        case E_SRC_OPTI1:
        case E_SRC_OPTI2:
        case E_SRC_COAX1:
        case E_SRC_COAX2:
            if(DA_GetSpdifRate() == E_SPDIF_RATE_SILENT) {
                if(NVM_GetMono(NVM_GetSrc()))
                    DISP_PrintCenter(C_DISP_LINE2,"MONO          ------");
                else
                    DISP_PrintCenter(C_DISP_LINE2,"------\0");
            }
            break;
        case E_SRC_MCT:
            if(DA_GetMctRate() == E_MCT_RATE_SILENT) {
                if(NVM_GetMono(NVM_GetSrc()))
                    DISP_PrintCenter(C_DISP_LINE2,"MONO          ------");
                else
                    DISP_PrintCenter(C_DISP_LINE2, "------\0");
            }
            break;
        case E_SRC_USB:
            if(DA_GetUsbRate() == E_USB_RATE_SILENT) {
                if(NVM_GetMono(NVM_GetSrc()))
                    DISP_PrintCenter(C_DISP_LINE2,"MONO          ------");
                else
                    DISP_PrintCenter(C_DISP_LINE2, "------\0");
            }
            break;
        case E_SRC_HDMI:
            if(DA_GetHdmiRate() == E_HDMI_RATE_SILENT) {
                if(NVM_GetMono(NVM_GetSrc()))
                    DISP_PrintCenter(C_DISP_LINE2,"MONO          ------");
                else
                    DISP_PrintCenter(C_DISP_LINE2, "------\0");
            }
            break;
        case E_SRC_BAL1:
//        case E_SRC_BAL2:              // Removed in C80
        case E_SRC_UNBAL1:
        case E_SRC_UNBAL2:
//        case E_SRC_UNBAL3:            // Removed in C80
            DISP_ClearLine(C_DISP_LINE2);
            if(NVM_GetMono(NVM_GetSrc()))
                DISP_PrintCenter(C_DISP_LINE2,"MONO");
            break;
        case E_SRC_MC_PHONO:
            if(NVM_GetMono(NVM_GetSrc())) {
                switch(NVM_GetMcPhonoRes()) {
                    case E_PRES_25_OHMS:  DISP_PrintCenter(C_DISP_LINE2,ohm25_mono_str);  break;
                    case E_PRES_50_OHMS:  DISP_PrintCenter(C_DISP_LINE2,ohm50_mono_str);  break;
                    case E_PRES_100_OHMS: DISP_PrintCenter(C_DISP_LINE2,ohm100_mono_str); break;
                    case E_PRES_200_OHMS: DISP_PrintCenter(C_DISP_LINE2,ohm200_mono_str); break;
                    case E_PRES_400_OHMS: DISP_PrintCenter(C_DISP_LINE2,ohm400_mono_str); break;
                    case E_PRES_1K_OHMS:  DISP_PrintCenter(C_DISP_LINE2,ohm1K_mono_str);  break;
                }
            }
            else {
                switch(NVM_GetMcPhonoRes()) {
                    case E_PRES_25_OHMS:  DISP_PrintCenter(C_DISP_LINE2,ohm25_str);  break;
                    case E_PRES_50_OHMS:  DISP_PrintCenter(C_DISP_LINE2,ohm50_str);  break;
                    case E_PRES_100_OHMS: DISP_PrintCenter(C_DISP_LINE2,ohm100_str); break;
                    case E_PRES_200_OHMS: DISP_PrintCenter(C_DISP_LINE2,ohm200_str); break;
                    case E_PRES_400_OHMS: DISP_PrintCenter(C_DISP_LINE2,ohm400_str); break;
                    case E_PRES_1K_OHMS:  DISP_PrintCenter(C_DISP_LINE2,ohm1K_str);  break;
                }
            }
            break;
        case E_SRC_MM_PHONO:
            if(NVM_GetMono(NVM_GetSrc())) {
                switch(NVM_GetMmPhonoCap()) {
                    case E_PCAP_50_PF:  DISP_PrintCenter(C_DISP_LINE2,"MONO           50 pF"); break;
                    case E_PCAP_100_PF: DISP_PrintCenter(C_DISP_LINE2,"MONO          100 pF"); break;
                    case E_PCAP_150_PF: DISP_PrintCenter(C_DISP_LINE2,"MONO          150 pF"); break;
                    case E_PCAP_200_PF: DISP_PrintCenter(C_DISP_LINE2,"MONO          200 pF"); break;
                    case E_PCAP_250_PF: DISP_PrintCenter(C_DISP_LINE2,"MONO          250 pF"); break;
                    case E_PCAP_300_PF: DISP_PrintCenter(C_DISP_LINE2,"MONO          300 pF"); break;
                    case E_PCAP_350_PF: DISP_PrintCenter(C_DISP_LINE2,"MONO          350 pF"); break;
                    case E_PCAP_400_PF: DISP_PrintCenter(C_DISP_LINE2,"MONO          400 pF"); break;
                    case E_PCAP_450_PF: DISP_PrintCenter(C_DISP_LINE2,"MONO          450 pF"); break;
                    case E_PCAP_500_PF: DISP_PrintCenter(C_DISP_LINE2,"MONO          500 pF"); break;
                    case E_PCAP_550_PF: DISP_PrintCenter(C_DISP_LINE2,"MONO          550 pF"); break;
                    case E_PCAP_600_PF: DISP_PrintCenter(C_DISP_LINE2,"MONO          600 pF"); break;
                    case E_PCAP_650_PF: DISP_PrintCenter(C_DISP_LINE2,"MONO          650 pF"); break;
                    case E_PCAP_700_PF: DISP_PrintCenter(C_DISP_LINE2,"MONO          700 pF"); break;
                    case E_PCAP_750_PF: DISP_PrintCenter(C_DISP_LINE2,"MONO          750 pF"); break;
                    case E_PCAP_800_PF: DISP_PrintCenter(C_DISP_LINE2,"MONO          800 pF"); break;
                }
            }
            else {
                switch(NVM_GetMmPhonoCap()) {
                    case E_PCAP_50_PF:  DISP_PrintCenter(C_DISP_LINE2,"50 pF");  break;
                    case E_PCAP_100_PF: DISP_PrintCenter(C_DISP_LINE2,"100 pF"); break;
                    case E_PCAP_150_PF: DISP_PrintCenter(C_DISP_LINE2,"150 pF"); break;
                    case E_PCAP_200_PF: DISP_PrintCenter(C_DISP_LINE2,"200 pF"); break;
                    case E_PCAP_250_PF: DISP_PrintCenter(C_DISP_LINE2,"250 pF"); break;
                    case E_PCAP_300_PF: DISP_PrintCenter(C_DISP_LINE2,"300 pF"); break;
                    case E_PCAP_350_PF: DISP_PrintCenter(C_DISP_LINE2,"350 pF"); break;
                    case E_PCAP_400_PF: DISP_PrintCenter(C_DISP_LINE2,"400 pF"); break;
                    case E_PCAP_450_PF: DISP_PrintCenter(C_DISP_LINE2,"450 pF"); break;
                    case E_PCAP_500_PF: DISP_PrintCenter(C_DISP_LINE2,"500 pF"); break;
                    case E_PCAP_550_PF: DISP_PrintCenter(C_DISP_LINE2,"550 pF"); break;
                    case E_PCAP_600_PF: DISP_PrintCenter(C_DISP_LINE2,"600 pF"); break;
                    case E_PCAP_650_PF: DISP_PrintCenter(C_DISP_LINE2,"650 pF"); break;
                    case E_PCAP_700_PF: DISP_PrintCenter(C_DISP_LINE2,"700 pF"); break;
                    case E_PCAP_750_PF: DISP_PrintCenter(C_DISP_LINE2,"750 pF"); break;
                    case E_PCAP_800_PF: DISP_PrintCenter(C_DISP_LINE2,"800 pF"); break;
                }
            }
            break;
        default:
            break;
    }
}

void DISP_Src(void)
{
    char disp[13]; disp[12]=0;

    if(PT_Active() || PT_PoweringOff() )
        return;

    /* |0|1|2|3|4|5|6|7|8|9|A|B|C|D|E|F|
    |S|O|U|R|C|E|D|I|S|P|X|_|V|O|L|%| */
    snprintf(disp, 13, "%-11s \0", NVM_SavedNames[NVM_GetSrc()]);
    DISP_PrintLine(C_DISP_LINE1, disp);
    DISP_Vol();

    IOX_Mono(NVM_GetMono(NVM_GetSrc()));
    IOX_Tone(NVM_GetTone(NVM_GetSrc()));
//    IOX_ToneLed(NVM_GetTone(NVM_GetSrc()));  Removed for C80

    DISP_MainMenu();
}

void DISP_Vol(void)
{
    char disp[5];  disp[4]=0;

    if (VOL_GetMute() && !VOL_GetUnmuteReq())
        snprintf(disp, 5, "MUTE\0");
    else if (VOL_GetWakeup())
        snprintf(disp, 5, "%3u%%\0", VOL_Get());
    else
        snprintf(disp, 5, "%3u%%\0", pNVM_GetVol());

    /* |00|01|02|03|04|05|06|07|08|09|0A|0B|0C|0D|0E|0F|10|11|12|13|
    |S |O |U |R |C |E |D |I |S |P |X |  |  |  |  |  | V|O |L |% | */
    DISP_Print(disp, C_DISP_LINE1, 0x10);
}

//===================================================================
// Private Functions
//===================================================================
static void disp_clearline(uint8_t line)
{
    __dispf.setpos(0, line);    // move cursor to start of line

    for(uint8_t i=0;i<C_DISP_LINELENGTH;i++)
        __dispf.print(' '); // fill line with blanks
}

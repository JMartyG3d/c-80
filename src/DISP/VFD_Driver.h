//-----------------------------------------------------------------------------|
//	VFD_Driver.h
//	Abstract: Driver for Newhaven VFD: M0220MD-202MDAR1-S1
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __VFD_DRIVER_H__
#define __VFD_DRIVER_H__

//===================================================================
// Public Function Prototypes
//===================================================================
void VFD_Init(void);
void VFD_SetState(uint8_t state);
void VFD_SetPosition(uint8_t col, uint8_t row);
void VFD_PrintChar(char c);
void VFD_SetCursorMode(uint8_t mode);
void VFD_FillCGRAM(uint8_t type);
void VFD_SetDutyCycle(uint8_t cycle);


#endif /* __VFD_DRIVER_H__ */

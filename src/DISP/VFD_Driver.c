//-----------------------------------------------------------------------------|
//	VFD_Driver.c
//	Abstract: Driver for Newhaven VFD: M0220MD-202MDAR1-S1
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include <string.h>
#include "NVM_Access.h"
#include "VFD_Driver.h"
#include "DISP_Control.h"
#include "PT_Control.h"
#include "Common.h"

// VFD Defines /////////////////////////////////////////////////////////////////

// VFD Transmit packet ----------------------------------------------------
// The 2 byte data packet that is transmitted looks like the following:   |
// Start byte:            | '1' | '1' | '1' | '1' | '1' | R/W | RS  | '0' |
// Instruction/Data byte: | DB7 | DB6 | DB5 | DB4 | DB3 | DB2 | DB1 | DB0 |
// ------------------------------------------------------------------------

// Start byte ------------------------------------------------------------------
// Bits
#define C_VFD_RS    0x0200
#define C_VFD_RW    0x0400
#define C_VFD_START 0xF800
// Commands
#define C_VFD_ADDR_COUNTER_READ (C_VFD_RW) // Read contents of the address counter (unused by us)
#define C_VFD_WRITE             (C_VFD_RS) // Write to CGRAM or DDRAM
#define C_VFD_READ   (C_VFD_RS | C_VFD_RW) // Read value at current CGRAM or DDRAM location (unused by us)
//------------------------------------------------------------------------------

// Instruction/Data byte -------------------------------------------------------
//  DB0
#define C_VFD_CLEARDISPLAY      0x01  // clear the display
//  DB1
#define C_VFD_CURSORHOME        0x02  // set cursor to top left position
//  DB2
#define C_VFD_ENTRYMODE_CMD     0x04  // Command mask to set entry mode (OR in appropriate bits)
#define C_VFD_ENTRYMODE_INC     0x02  // OR with Entry mode command to set I/D bit (increment/decrement address counter)
#define C_VFD_ENTRYMODE_SHIFT   0x01  // OR with Entry mode command to enable shift function
//  DB3
#define C_VFD_DISP_CTRL_CMD     0x08  // Command mask for display control command
#define C_VFD_DISP_CTRL_ON      0x04  // OR with Disp ctrl to set display on
#define C_VFD_DISP_CTRL_CURSOR  0x02  // OR with Disp ctrl to set cursor on
#define C_VFD_DISP_CTRL_BLINK   0x01  // OR with Disp ctrl to set blink on
//  DB4 (unused by us)
#define C_VFD_SHIFT             0x10  // Command mask for cursor/display shift command (note: see datasheet for operation if you want to use this)
#define C_VFD_SHIFT_SC          0x08  // OR with Disp ctrl to set "shift character" bit
#define C_VFD_SHIFT_RL          0x04  // OR with Disp ctrl to set "right/left" bit
//  DB5
#define C_VFD_FUNCTION          0x20  // Command mask for function set command
#define C_VFD_FUNCTION_DL       0x10  // OR with disp ctrl to set data bus width to 8 bits
#define C_VFD_FUNCTION_N        0x08  // OR with disp ctlr to set number of lines to 2
#define C_VFD_FUNCTION_BR100    0x00  // OR with disp ctrl to set brightness to 100%
#define C_VFD_FUNCTION_BR75     0x01  // OR with disp ctrl to set brightness to 75%
#define C_VFD_FUNCTION_BR50     0x02  // OR with disp ctrl to set brightness to 50%
#define C_VFD_FUNCTION_BR25     0x03  // OR with disp ctrl to set brightness to 25%
#define C_VFD_FUNCTION_DEFAULT  (C_VFD_FUNCTION | C_VFD_FUNCTION_DL | C_VFD_FUNCTION_N) // default values - make sure to pass these in everytime function register is changed
//  DB6
#define C_VFD_CGRAM_ADDR_SET    0x40  // Command mask to set CGRAM address
//  DB7
#define C_VFD_DDRAM_ADDR_SET    0x80  // Command mask to set DDRAM address
//------------------------------------------------------------------------------

// Other defines ---------------------------------------------------------------
#define C_VFD_MAX_DUTY_CYCLE	3
#define C_VFD_DDRAM_ADDR_LINE2	0x40	// [0x40 - 0x53]


// CGRAM arrays ----------------------------------------------------------------
// NOTES:
// special char display includes 5 unique chars
//    1        2        3        4        5
// [    |]  [|    ]  [||   ]  [|| ||]  [   ||]
// [    |]  [|    ]  [||   ]  [|| ||]  [   ||]
// [    |]  [|    ]  [||   ]  [|| ||]  [   ||]
// [    |]  [|    ]  [||   ]  [|| ||]  [   ||]
// [    |]  [|    ]  [||   ]  [|| ||]  [   ||]
// [    |]  [|    ]  [||   ]  [|| ||]  [   ||]
// [    |]  [|    ]  [||   ]  [|| ||]  [   ||]
// [     ]  [     ]  [     ]  [     ]  [     ]  Bottom line always Blank
//
// Ex :Char 4 numeric value
//    4
// [|| ||]  -> [11011] -> 0x1B -> 27 decimal
// [|| ||]  -> [11011] -> 0x1B -> 27 decimal
// [|| ||]  -> [11011] -> 0x1B -> 27 decimal
// [|| ||]  -> [11011] -> 0x1B -> 27 decimal
// [|| ||]  -> [11011] -> 0x1B -> 27 decimal
// [|| ||]  -> [11011] -> 0x1B -> 27 decimal
// [|| ||]  -> [11011] -> 0x1B -> 27 decimal
// [     ]  -> [00000] -> 0x00 ->  0 decimal
//
// rotate 90deg and place values into CGRAMUI_Toggle_Eq array bottom line first
//              0,27,27,27,27,27,27,27,

static uint8_t const vfd_cgram_bal_l[48] = {
    0, 0, 0, 0, 0, 0, 0, 0,
    0,31,31,31,31,31,31,31,
    0,15,15,15,15,15,15,15,
    0, 7, 7, 7, 7, 7, 7, 7,
    0, 3, 3, 3, 3, 3, 3, 3,
    0, 1, 1, 1, 1, 1, 1, 1
};

static uint8_t const vfd_cgram_bal_r[56] = {
    0, 0, 0, 0, 0, 0, 0, 0,
    0,16,16,16,16,16,16,16,
    0,24,24,24,24,24,24,24,
    0,28,28,28,28,28,28,28,
    0,30,30,30,30,30,30,30,
    0,31,31,31,31,31,31,31,
    0, 1, 1, 1, 1, 1, 1, 1
};

// -----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////

//==============================================================================
// Private Functions
//==============================================================================
static void vfd_control(uint8_t instr);
static void vfd_write(uint8_t data);
//==============================================================================
// Public Function Defintions
//==============================================================================
void VFD_Init(void)
{
    // Initialization/reset sequence:
    // 1. VFD sets all DDRAM locations to 0x20 (space)
    // 2. VFD sets address counter to DDRAM 0x00 (top left)
    // 3. VFD sets DDRAM addresses and positions to non-shifted positions
    // 4. VFD sets Entry mode to AC incrementing, display shift disabled
    // 5. VFD sets Display mode to Display off, blink off, cursor off
    // 6. VFD sets function set to data length 8bit, lines 2, brightness 100%
    // 7. VFD waits for host to send a function set command
    //    ^-------- this is what we do
    //              afterwords we can set the remaining instructions

    // set the duty cycle for the desired brightness - this also enables the display after a reset
	VFD_SetDutyCycle(PT_Active() ? NVM_GetBrightnessPT() : NVM_GetBrightness());

    // set the entry mode to cursor incrementing
	vfd_control(C_VFD_ENTRYMODE_CMD | C_VFD_ENTRYMODE_INC);

    // set the display to be cleared
	vfd_control(C_VFD_CLEARDISPLAY);

    // set the cursor "home" -- used ddram address set to ensure next writes will be to ddram (after CGRAM fill)
    vfd_control(C_VFD_DDRAM_ADDR_SET);

    // turn the display on
	VFD_SetState(ON);
}

void VFD_SetState(uint8_t state) // only used to control the on/off state of the display
{
	if(state)
		vfd_control(C_VFD_DISP_CTRL_CMD | C_VFD_DISP_CTRL_ON);
	else
		vfd_control(C_VFD_DISP_CTRL_CMD);
}

void VFD_SetPosition(uint8_t col, uint8_t row)
{
    // DDRAM position table (hex)---------------------------------------------------------------------------
    // | 00 | 01 | 02 | 03 | 04 | 05 | 06 | 07 | 08 | 09 | 0A | 0B | 0C | 0D | 0E | 0F | 10 | 11 | 12 | 13 |
    // | 40 | 41 | 42 | 43 | 44 | 45 | 46 | 47 | 48 | 49 | 4A | 4B | 4C | 4D | 4E | 4F | 50 | 51 | 52 | 53 |
    // -----------------------------------------------------------------------------------------------------
    uint8_t position = (row == C_DISP_LINE1 ? col : C_VFD_DDRAM_ADDR_LINE2 + col);
	vfd_control((C_VFD_DDRAM_ADDR_SET | position));
}

void VFD_PrintChar(char c) // assumes position has already set by DISP_Control.c
{
    vfd_write(c); // pass on to write
}

void VFD_SetCursorMode(uint8_t mode) // 0 - nothing, 1 = blink, 2 = cursor, 3 = blink & cursor
{
    if (mode < 4) // validate mode
        vfd_control(C_VFD_DISP_CTRL_CMD | C_VFD_DISP_CTRL_ON | mode);
}

void VFD_FillCGRAM(uint8_t type)
{
    uint32_t i;
    switch(type) {
        case E_DISP_CGRAM_BAL_L:
            for(i = 0; i < sizeof(vfd_cgram_bal_l); i++) {
                vfd_control(C_VFD_CGRAM_ADDR_SET | i);
                vfd_write(vfd_cgram_bal_l[i]);
            }
            break;
        case E_DISP_CGRAM_BAL_R:
            for(i = 0; i < sizeof(vfd_cgram_bal_r); i++) {
                vfd_control(C_VFD_CGRAM_ADDR_SET | i);
                vfd_write(vfd_cgram_bal_r[i]);
            }
            break;
        default:
            break;
    }
}

void VFD_SetDutyCycle(uint8_t cycle)
{
    // validate and set duty cycle (brightness)
	if(cycle < (C_VFD_MAX_DUTY_CYCLE+1))
        vfd_control(C_VFD_FUNCTION_DEFAULT | cycle);
	else // else set to full brightness
        vfd_control(C_VFD_FUNCTION_DEFAULT | C_VFD_FUNCTION_BR100);
}

//===================================================================
// Private Functions
//===================================================================
static void vfd_control(uint8_t instr)
{
    spi_write(SPI, (C_VFD_START | instr), CONFIG_SPI_CS, FALSE);
    delay_us(25);
}

static void vfd_write(uint8_t data)
{
    spi_write(SPI, (C_VFD_START | C_VFD_WRITE | data), CONFIG_SPI_CS, FALSE);
    delay_us(25);
}

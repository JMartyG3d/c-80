//-----------------------------------------------------------------------------|
//	DISP_Control.h
//	Abstract: Display Processing functions
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __DISP_CONTROL_H__
#define __DISP_CONTROL_H__

// put defines here so as not to muck around in IAR configuration
#define C_DISP_IS_VFD 1
//#define C_DISP_IS_OLED 1

#ifdef C_DISP_IS_VFD
    #define C_DISP_LINELENGTH 20
#endif

#ifdef C_DISP_IS_OLED
    #define C_DISP_LINELENGTH 16
#endif

#define C_DISP_BOTHLINES 0
#define C_DISP_LINE1     1
#define C_DISP_LINE2     2

typedef enum {
    E_DISP_CGRAM_BAL_L,
    E_DISP_CGRAM_BAL_R,
} DISP_CGRAM_T;

//===================================================================
// Public Function Prototypes
//===================================================================
void DISP_Initialize(void); // initialize the display device
void DISP_SpiInit(void);    // initialize the spi bus
void DISP_SetState(uint8_t state);
void DISP_ClearLine(uint8_t line);
void DISP_Print(char * str, uint8_t line, uint8_t pos);
void DISP_PrintLine(uint8_t line, char * str);
void DISP_PrintCenter(uint8_t line, char * str); // str must be null terminated
void DISP_PrintOver(uint8_t x, uint8_t y, uint8_t len, char * str);
void DISP_SetBlinkCursor(uint8_t setting); // 0 = nothing, 1 = blink, 2 = cursor, 3 = both
void DISP_FillCGRAM(uint8_t type);
void DISP_MainMenu(void);
void DISP_Src(void);
void DISP_Vol(void);

// display specific function
#ifdef C_DISP_IS_VFD
void DISP_SetDutyCycle(uint8_t cycle);
#endif

#endif //DISP_PROCESS_H

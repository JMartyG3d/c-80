//-----------------------------------------------------------------------------|
//	DA_Queue.h
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __DA_QUEUE_H__
#define __DA_QUEUE_H__

#include "DA_Config.h"

#define	QUEUESIZE 20

typedef struct {
    Da_Msg_Type fifo[QUEUESIZE];
    uint8_t head;
    uint8_t tail;
    uint8_t size;
} Da_Msg_Queue_Type;

typedef struct {
    uint8_t data;
    uint8_t id;
} Debug_Msg_Type;

extern Debug_Msg_Type RxDebug[500];

//===================================================================
// Public Function Prototypes
//===================================================================
void DA_QueueInit(void);
bool DA_RxQueueDirty(void);
void DA_RxQueuePushMsg(Da_Msg_Type new_msg);
Da_Msg_Type DA_RxQueuePopMsg(void);

#endif // __DA_QUEUE_H__
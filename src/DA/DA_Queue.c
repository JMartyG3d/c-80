//-----------------------------------------------------------------------------|
//	DA_Queue.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include <string.h>
#include "DA_Queue.h"

static Da_Msg_Queue_Type RxQueue;

//===================================================================
// Private Function Prototypes
//===================================================================
static bool da_queuefull(Da_Msg_Queue_Type msg_queue);

//===================================================================
// Public Functions
//===================================================================
void DA_QueueInit(void)
{
	RxQueue.head = RxQueue.tail = RxQueue.size = 0;
}

bool DA_RxQueueDirty(void)
{
	return(RxQueue.size != 0);
}

void DA_RxQueuePushMsg(Da_Msg_Type new_msg)
{
	if(da_queuefull(RxQueue))
		return;
	else{
		RxQueue.tail = (RxQueue.tail == (QUEUESIZE-1)) ? 0 : RxQueue.tail + 1;
		RxQueue.fifo[RxQueue.tail] = new_msg;
        RxQueue.size++;
	}
}

Da_Msg_Type DA_RxQueuePopMsg(void)
{
    Da_Msg_Type rx = {.id = NULL};

	if(RxQueue.head != RxQueue.tail) {
		RxQueue.head = (RxQueue.head == (QUEUESIZE-1)) ? 0 : RxQueue.head + 1;
        rx = RxQueue.fifo[RxQueue.head];
        RxQueue.size--;
	}
    return rx;
}

//===================================================================
// Private Functions
//===================================================================
static bool da_queuefull(Da_Msg_Queue_Type msg_queue)
{
    return(msg_queue.size == QUEUESIZE-1);
}

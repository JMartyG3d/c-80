//-----------------------------------------------------------------------------|
//	DA_ProcessTx.h
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __DA_PROCESSTX_H__
#define __DA_PROCESSTX_H__

#include "DA_Config.h"
#include "Common.h"

//===================================================================
// Public Function Prototypes
//===================================================================
void DA_TxReqFwVerMajor(void);
void DA_TxReqFwVerMinor(void);
void DA_TxReqHwVer(void);
void DA_TxInputSelect(Da_Input_Type input);
void DA_TxSambaMode(void);
void DA_TxUsbAutomute(bool state);
void DA_TxHdmiCecState(bool state);
void DA_TxHdmiCecPower(bool state);
void DA_TxLipSyncMode(Lip_Mode_Type mode);
void DA_TxLipSyncDelay(Lip_Time_Type time);
void DA_TxMute(bool state);
void DA_TxVolume(uint8_t vol);
void DA_TxDigGain(Dig_Gain_Type input, uint8_t gain);
void DA_TxOsdName(void);

#endif // __DA_PROCESSTX_H__

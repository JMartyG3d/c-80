//-----------------------------------------------------------------------------|
//	DA_Display.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include <string.h>
#include "NVM_Access.h"
#include "DISP_Control.h"
#include "VOL_Control.h"
#include "PWR_Control.h"
#include "DA_Display.h"
#include "DA_ProcessRx.h"
#include "DA_Queue.h"
#include "DA_Config.h"
#include "DA_ProcessTx.h"
#include "SRC_Control.h"
#include "UI_Control.h"
#include "UI_Function.h"

//===================================================================
// Public Functions
//===================================================================
void DA_DisplayUsbRate(Usb_Rate_Type disp)
{
    static uint8_t periodic;
    static Usb_Rate_Type prev_disp = (Usb_Rate_Type) 0xFF;

    if(!SRC_IsDigital(NVM_GetSrc()) || SRC_ChangeInProgress())
        return;

    if(prev_disp != disp || periodic++ > 5) {
        if(NVM_GetMono(NVM_GetSrc())) {
            switch(disp) {
                case E_USB_RATE_SILENT:     DISP_PrintCenter(C_DISP_LINE2,"MONO          ------"); break;
                case E_USB_RATE_32KHZ:      DISP_PrintCenter(C_DISP_LINE2,"MONO           32kHz"); break;
                case E_USB_RATE_44KHZ:      DISP_PrintCenter(C_DISP_LINE2,"MONO         44.1kHz"); break;
                case E_USB_RATE_48KHZ:      DISP_PrintCenter(C_DISP_LINE2,"MONO           48kHz"); break;
                case E_USB_RATE_88KHZ:      DISP_PrintCenter(C_DISP_LINE2,"MONO         88.2kHz"); break;
                case E_USB_RATE_96KHZ:      DISP_PrintCenter(C_DISP_LINE2,"MONO           96kHz"); break;
                case E_USB_RATE_176KHZ:     DISP_PrintCenter(C_DISP_LINE2,"MONO        176.4kHz"); break;
                case E_USB_RATE_192KHZ:     DISP_PrintCenter(C_DISP_LINE2,"MONO          192kHz"); break;
                case E_USB_RATE_352KHZ:     DISP_PrintCenter(C_DISP_LINE2,"MONO        352.8kHz"); break;
                case E_USB_RATE_384KHZ:     DISP_PrintCenter(C_DISP_LINE2,"MONO          384kHz"); break;
                case E_USB_RATE_DSD64:      DISP_PrintCenter(C_DISP_LINE2,"MONO           DSD64"); break;
                case E_USB_RATE_DSD128:     DISP_PrintCenter(C_DISP_LINE2,"MONO          DSD128"); break;
                case E_USB_RATE_DSD256:     DISP_PrintCenter(C_DISP_LINE2,"MONO          DSD256"); break;
                case E_USB_RATE_DSD512:     DISP_PrintCenter(C_DISP_LINE2,"MONO          DSD512"); break;
                default: break;
            }
        }
        else {
            switch(disp) {
                case E_USB_RATE_SILENT:     DISP_PrintCenter(C_DISP_LINE2,"------\0");       break;
                case E_USB_RATE_32KHZ:      DISP_PrintCenter(C_DISP_LINE2,"32kHz\0");        break;
                case E_USB_RATE_44KHZ:      DISP_PrintCenter(C_DISP_LINE2,"44.1kHz\0");      break;
                case E_USB_RATE_48KHZ:      DISP_PrintCenter(C_DISP_LINE2,"48kHz\0");        break;
                case E_USB_RATE_88KHZ:      DISP_PrintCenter(C_DISP_LINE2,"88.2kHz\0");      break;
                case E_USB_RATE_96KHZ:      DISP_PrintCenter(C_DISP_LINE2,"96kHz\0");        break;
                case E_USB_RATE_176KHZ:     DISP_PrintCenter(C_DISP_LINE2,"176.4kHz\0");     break;
                case E_USB_RATE_192KHZ:     DISP_PrintCenter(C_DISP_LINE2,"192kHz\0");       break;
                case E_USB_RATE_352KHZ:     DISP_PrintCenter(C_DISP_LINE2,"352.8kHz\0");     break;
                case E_USB_RATE_384KHZ:     DISP_PrintCenter(C_DISP_LINE2,"384kHz\0");       break;
                case E_USB_RATE_DSD64:      DISP_PrintCenter(C_DISP_LINE2,"DSD64\0");        break;
                case E_USB_RATE_DSD128:     DISP_PrintCenter(C_DISP_LINE2,"DSD128\0");       break;
                case E_USB_RATE_DSD256:     DISP_PrintCenter(C_DISP_LINE2,"DSD256\0");       break;
                case E_USB_RATE_DSD512:     DISP_PrintCenter(C_DISP_LINE2,"DSD512\0");       break;
                default: break;
            }
        }
        periodic = 0;

        if(!VOL_GetMute())
            VOL_ChangeVolume(TRUE,FALSE,pNVM_GetVol()); // refresh display
        else
            UI_SetMute(ON);
    }
    prev_disp = disp;
}

void DA_DisplayMctRate(Mct_Rate_Type disp)
{
    static uint8_t periodic;
    static Mct_Rate_Type prev_disp = (Mct_Rate_Type) 0xFF;

    if(!SRC_IsDigital(NVM_GetSrc()) || SRC_ChangeInProgress())
        return;

    if(prev_disp != disp || periodic++ > 5) {
        if(NVM_GetMono(NVM_GetSrc())) {
            switch(disp) {
                case E_MCT_RATE_SILENT: DISP_PrintCenter(C_DISP_LINE2,"MONO          ------"); break;
                case E_MCT_RATE_CD:     DISP_PrintCenter(C_DISP_LINE2,"MONO              CD"); break;
                case E_MCT_RATE_SACD:   DISP_PrintCenter(C_DISP_LINE2,"MONO            SACD"); break;
                default: break;
            }
        }
        else {
            switch(disp) {
                case E_MCT_RATE_SILENT: DISP_PrintCenter(C_DISP_LINE2, "------\0"); break;
                case E_MCT_RATE_CD:     DISP_PrintCenter(C_DISP_LINE2, "CD\0");     break;
                case E_MCT_RATE_SACD:   DISP_PrintCenter(C_DISP_LINE2, "SACD\0");   break;
                default: break;
            }
        }

        periodic = 0;

        if(!VOL_GetMute())
            VOL_ChangeVolume(TRUE,FALSE,pNVM_GetVol()); // refresh display
        else
            UI_SetMute(ON);
    }
    prev_disp = disp;
}

void DA_DisplaySpdifRate(Spdif_Rate_Type disp)
{
    static uint8_t periodic;
    static Spdif_Rate_Type prev_disp = (Spdif_Rate_Type) 0xFF;
    static Src_Type prev_input;

    if(!SRC_IsDigital(NVM_GetSrc()) || SRC_ChangeInProgress())
        return;

    if(prev_disp != disp || NVM_GetSrc() != prev_input|| periodic++ > 5) {
        if(NVM_GetMono(NVM_GetSrc())) {
            switch(disp) {
                case E_SPDIF_RATE_SILENT: DISP_PrintCenter(C_DISP_LINE2,"MONO          ------"); break;
                case E_SPDIF_RATE_32KHZ:  DISP_PrintCenter(C_DISP_LINE2,"MONO           32kHz"); break;
                case E_SPDIF_RATE_44KHZ:  DISP_PrintCenter(C_DISP_LINE2,"MONO         44.1kHz"); break;
                case E_SPDIF_RATE_48KHZ:  DISP_PrintCenter(C_DISP_LINE2,"MONO           48kHz"); break;
                case E_SPDIF_RATE_88KHZ:  DISP_PrintCenter(C_DISP_LINE2,"MONO         88.2kHz"); break;
                case E_SPDIF_RATE_96KHZ:  DISP_PrintCenter(C_DISP_LINE2,"MONO           96kHz"); break;
                case E_SPDIF_RATE_176KHZ: DISP_PrintCenter(C_DISP_LINE2,"MONO        176.4kHz"); break;
                case E_SPDIF_RATE_192KHZ: DISP_PrintCenter(C_DISP_LINE2,"MONO          192kHz"); break;
                case E_SPDIF_RATE_352KHZ: DISP_PrintCenter(C_DISP_LINE2,"MONO        352.8kHz"); break;
                case E_SPDIF_RATE_384KHZ: DISP_PrintCenter(C_DISP_LINE2,"MONO          384kHz"); break;
                case E_SPDIF_RATE_DOLBY:  DISP_PrintCenter(C_DISP_LINE2,"MONO           Dolby"); break;
                case E_SPDIF_RATE_DTS:    DISP_PrintCenter(C_DISP_LINE2,"MONO             DTS"); break;
                default: break;
            }
        }
        else {
            switch(disp) {
                case E_SPDIF_RATE_SILENT: DISP_PrintCenter(C_DISP_LINE2, "------\0");   break;
                case E_SPDIF_RATE_32KHZ:  DISP_PrintCenter(C_DISP_LINE2, "32kHz\0");    break;
                case E_SPDIF_RATE_44KHZ:  DISP_PrintCenter(C_DISP_LINE2, "44.1kHz\0");  break;
                case E_SPDIF_RATE_48KHZ:  DISP_PrintCenter(C_DISP_LINE2, "48kHz\0");    break;
                case E_SPDIF_RATE_88KHZ:  DISP_PrintCenter(C_DISP_LINE2, "88.2kHz\0");  break;
                case E_SPDIF_RATE_96KHZ:  DISP_PrintCenter(C_DISP_LINE2, "96kHz\0");    break;
                case E_SPDIF_RATE_176KHZ: DISP_PrintCenter(C_DISP_LINE2, "176.4kHz\0"); break;
                case E_SPDIF_RATE_192KHZ: DISP_PrintCenter(C_DISP_LINE2, "192kHz\0");   break;
                case E_SPDIF_RATE_352KHZ: DISP_PrintCenter(C_DISP_LINE2, "352.8kHz\0"); break;
                case E_SPDIF_RATE_384KHZ: DISP_PrintCenter(C_DISP_LINE2, "384kHz\0");   break;
                case E_SPDIF_RATE_DOLBY:  DISP_PrintCenter(C_DISP_LINE2, "Dolby\0");    break;
                case E_SPDIF_RATE_DTS:    DISP_PrintCenter(C_DISP_LINE2, "DTS\0");      break;
                default: break;
            }
        }

        periodic = 0;

        if(!VOL_GetMute())
            VOL_ChangeVolume(TRUE,FALSE,pNVM_GetVol()); // refresh display
        else
            UI_SetMute(ON);
    }
    prev_disp = disp;
    prev_input = NVM_GetSrc();
}

void DA_DisplayHdmiRate(Hdmi_Rate_Type disp)
{
    static uint8_t periodic;
    static Hdmi_Rate_Type prev_disp = (Hdmi_Rate_Type) 0xFF;
    static Src_Type prev_input;

    if(!SRC_IsDigital(NVM_GetSrc()) || SRC_ChangeInProgress())
        return;

    if(prev_disp != disp || NVM_GetSrc() != prev_input|| periodic++ > 5) {
        if(NVM_GetMono(NVM_GetSrc())) {
            switch(disp) {
                case E_HDMI_RATE_SILENT: DISP_PrintCenter(C_DISP_LINE2,"MONO          ------"); break;
                case E_HDMI_RATE_32KHZ:  DISP_PrintCenter(C_DISP_LINE2,"MONO           32kHz"); break;
                case E_HDMI_RATE_44KHZ:  DISP_PrintCenter(C_DISP_LINE2,"MONO         44.1kHz"); break;
                case E_HDMI_RATE_48KHZ:  DISP_PrintCenter(C_DISP_LINE2,"MONO           48kHz"); break;
                case E_HDMI_RATE_88KHZ:  DISP_PrintCenter(C_DISP_LINE2,"MONO         88.2kHz"); break;
                case E_HDMI_RATE_96KHZ:  DISP_PrintCenter(C_DISP_LINE2,"MONO           96kHz"); break;
                case E_HDMI_RATE_176KHZ: DISP_PrintCenter(C_DISP_LINE2,"MONO        176.4kHz"); break;
                case E_HDMI_RATE_192KHZ: DISP_PrintCenter(C_DISP_LINE2,"MONO          192kHz"); break;
                case E_HDMI_RATE_DOLBY:  DISP_PrintCenter(C_DISP_LINE2,"MONO           Dolby"); break;
                case E_HDMI_RATE_DTS:    DISP_PrintCenter(C_DISP_LINE2,"MONO             DTS"); break;
                default: break;
            }
        }
        else {
            switch(disp) {
                case E_HDMI_RATE_SILENT: DISP_PrintCenter(C_DISP_LINE2, "------\0");   break;
                case E_HDMI_RATE_32KHZ:  DISP_PrintCenter(C_DISP_LINE2, "32kHz\0");    break;
                case E_HDMI_RATE_44KHZ:  DISP_PrintCenter(C_DISP_LINE2, "44.1kHz\0");  break;
                case E_HDMI_RATE_48KHZ:  DISP_PrintCenter(C_DISP_LINE2, "48kHz\0");    break;
                case E_HDMI_RATE_88KHZ:  DISP_PrintCenter(C_DISP_LINE2, "88.2kHz\0");  break;
                case E_HDMI_RATE_96KHZ:  DISP_PrintCenter(C_DISP_LINE2, "96kHz\0");    break;
                case E_HDMI_RATE_176KHZ: DISP_PrintCenter(C_DISP_LINE2, "176.4kHz\0"); break;
                case E_HDMI_RATE_192KHZ: DISP_PrintCenter(C_DISP_LINE2, "192kHz\0");   break;
                case E_HDMI_RATE_DOLBY:  DISP_PrintCenter(C_DISP_LINE2, "Dolby\0");    break;
                case E_HDMI_RATE_DTS:    DISP_PrintCenter(C_DISP_LINE2, "DTS\0");      break;
                default: break;
            }
        }

        periodic = 0;

        if(!VOL_GetMute())
            VOL_ChangeVolume(TRUE,FALSE,pNVM_GetVol()); // refresh display
        else
            UI_SetMute(ON);
    }
    prev_disp = disp;
    prev_input = NVM_GetSrc();
}

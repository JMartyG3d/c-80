//-----------------------------------------------------------------------------|
//	DA_ProcessRx.h
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __DA_PROCESSRX_H__
#define __DA_PROCESSRX_H__

#include "DA_Config.h"

// Rx Msgs
#define MSG_ID_RX_READY         0xA1
#define MSG_ID_RX_FW_VER_MAJOR  0xA2
#define MSG_ID_RX_FW_VER_MINOR  0xA3
#define MSG_ID_RX_USB_DISPLAY   0xA4
#define MSG_ID_RX_MCT_DISPLAY   0xA5
#define MSG_ID_RX_SPDIF_DISPLAY 0xA6
#define MSG_ID_RX_HW_VER        0xA7

//===================================================================
// Public Function Prototypes
//===================================================================
void DA_ProcessRxMsg(Da_Msg_Type rx);

#endif /* __DA_PROCESSRX_H__ */

//-----------------------------------------------------------------------------|
//	DA_ProcessTx.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include <string.h>
#include "DA_ProcessTx.h"
#include "DA_Queue.h"
#include "DA_Config.h"
#include "APP_Process.h"

// Tx Message Definitions
static const Da_Msg_Type TxInputSelUsb   = {MSG_START, E_MSG_ID_TX_INPUT_SELECT, E_MSG_INPUT_SEL_USB};
static const Da_Msg_Type TxInputSelOpt1  = {MSG_START, E_MSG_ID_TX_INPUT_SELECT, E_MSG_INPUT_SEL_OPT1};
static const Da_Msg_Type TxInputSelOpt2  = {MSG_START, E_MSG_ID_TX_INPUT_SELECT, E_MSG_INPUT_SEL_OPT2};
static const Da_Msg_Type TxInputSelOpt3  = {MSG_START, E_MSG_ID_TX_INPUT_SELECT, E_MSG_INPUT_SEL_OPT3};
static const Da_Msg_Type TxInputSelCoax1 = {MSG_START, E_MSG_ID_TX_INPUT_SELECT, E_MSG_INPUT_SEL_COAX1};
static const Da_Msg_Type TxInputSelCoax2 = {MSG_START, E_MSG_ID_TX_INPUT_SELECT, E_MSG_INPUT_SEL_COAX2};
static const Da_Msg_Type TxInputSelMct   = {MSG_START, E_MSG_ID_TX_INPUT_SELECT, E_MSG_INPUT_SEL_MCT};
static const Da_Msg_Type TxInputSelHdmi  = {MSG_START, E_MSG_ID_TX_INPUT_SELECT, E_MSG_INPUT_SEL_HDMI};
static const Da_Msg_Type TxInputSelNone  = {MSG_START, E_MSG_ID_TX_INPUT_SELECT, E_MSG_INPUT_SEL_NONE};

static const Da_Msg_Type TxReqFwVerMajor        = {MSG_START, E_MSG_ID_TX_REQ_FW_VER_MAJOR, NONVALUE_PAYLOAD};
static const Da_Msg_Type TxReqFwVerMinor        = {MSG_START, E_MSG_ID_TX_REQ_FW_VER_MINOR, NONVALUE_PAYLOAD};
static const Da_Msg_Type TxReqHwVer             = {MSG_START, E_MSG_ID_TX_REQ_HW_VER, NONVALUE_PAYLOAD};

static const Da_Msg_Type TxReqSambaMode         = {MSG_START, E_MSG_ID_TX_REQ_SAMBA_MODE, NONVALUE_PAYLOAD};
static const Da_Msg_Type TxReqUsbAutomuteOn     = {MSG_START, E_MSG_ID_TX_REQ_USB_AUTOMUTE, ON};
static const Da_Msg_Type TxReqUsbAutomuteOff    = {MSG_START, E_MSG_ID_TX_REQ_USB_AUTOMUTE, OFF};

static const Da_Msg_Type TxReqHdmiCecOn         = {MSG_START, E_MSG_ID_TX_SET_HDMI_CEC_STATE, ON};
static const Da_Msg_Type TxReqHdmiCecOff        = {MSG_START, E_MSG_ID_TX_SET_HDMI_CEC_STATE, OFF};
static const Da_Msg_Type TxReqHdmiCecPowerOn    = {MSG_START, E_MSG_ID_TX_SET_HDMI_CEC_PWR_MODE, ON};
static const Da_Msg_Type TxReqHdmiCecPowerOff   = {MSG_START, E_MSG_ID_TX_SET_HDMI_CEC_PWR_MODE, OFF};
static const Da_Msg_Type TxReqLipSyncAuto       = {MSG_START, E_MSG_ID_TX_SET_LIP_SYNC_MODE, ON};
static const Da_Msg_Type TxReqLipSyncManual     = {MSG_START, E_MSG_ID_TX_SET_LIP_SYNC_MODE, OFF};

static const Da_Msg_Type TxMuteOn               = {MSG_START, E_MSG_ID_TX_RPT_HDMI_CEC_MUTE_STATUS, ON};
static const Da_Msg_Type TxMuteOff              = {MSG_START, E_MSG_ID_TX_RPT_HDMI_CEC_MUTE_STATUS, OFF};


//===================================================================
// Private Function Prototypes
//===================================================================
static void da_tx(uint8_t * buffer);

//===================================================================
// Public Functions
//===================================================================
void DA_TxReqFwVerMajor(void)
{
    uint8_t buffer[MSG_LENGTH];
    memcpy(&buffer, &TxReqFwVerMajor, MSG_LENGTH);
    da_tx(buffer);
}

void DA_TxReqFwVerMinor(void)
{
    uint8_t buffer[MSG_LENGTH];
    memcpy(&buffer, &TxReqFwVerMinor, MSG_LENGTH);
    da_tx(buffer);
}

void DA_TxReqHwVer(void)
{
    uint8_t buffer[MSG_LENGTH];
    memcpy(&buffer, &TxReqHwVer, MSG_LENGTH);
    da_tx(buffer);
}

void DA_TxInputSelect(Da_Input_Type input)
{
    uint8_t buffer[MSG_LENGTH];

    switch(input) {
        case E_MSG_INPUT_SEL_USB:   memcpy(&buffer, &TxInputSelUsb,   MSG_LENGTH); break;
        case E_MSG_INPUT_SEL_OPT1:  memcpy(&buffer, &TxInputSelOpt1,  MSG_LENGTH); break;
        case E_MSG_INPUT_SEL_OPT2:  memcpy(&buffer, &TxInputSelOpt2,  MSG_LENGTH); break;
        case E_MSG_INPUT_SEL_OPT3:  memcpy(&buffer, &TxInputSelOpt3,  MSG_LENGTH); break;
        case E_MSG_INPUT_SEL_COAX1: memcpy(&buffer, &TxInputSelCoax1, MSG_LENGTH); break;
        case E_MSG_INPUT_SEL_COAX2: memcpy(&buffer, &TxInputSelCoax2, MSG_LENGTH); break;
        case E_MSG_INPUT_SEL_MCT:   memcpy(&buffer, &TxInputSelMct,   MSG_LENGTH); break;
        case E_MSG_INPUT_SEL_HDMI:  memcpy(&buffer, &TxInputSelHdmi,  MSG_LENGTH); break;
        case E_MSG_INPUT_SEL_NONE:  memcpy(&buffer, &TxInputSelNone,  MSG_LENGTH); break;
        default: break;
    }

    da_tx(buffer);
}

void DA_TxSambaMode(void)
{
    uint8_t buffer[MSG_LENGTH];
    memcpy(&buffer, &TxReqSambaMode, MSG_LENGTH);
    da_tx(buffer);
}

void DA_TxUsbAutomute(bool state)
{
    uint8_t buffer[MSG_LENGTH];
    if(state == ON)
        memcpy(&buffer, &TxReqUsbAutomuteOn, MSG_LENGTH);
    else
        memcpy(&buffer, &TxReqUsbAutomuteOff, MSG_LENGTH);
    da_tx(buffer);
}

void DA_TxHdmiCecState(bool state)
{
    uint8_t buffer[MSG_LENGTH];
    if(state == ON)
        memcpy(&buffer, &TxReqHdmiCecOn, MSG_LENGTH);
    else
        memcpy(&buffer, &TxReqHdmiCecOff, MSG_LENGTH);
    da_tx(buffer);
}

void DA_TxHdmiCecPower(bool state)
{
    uint8_t buffer[MSG_LENGTH];
    if(state == ON)
        memcpy(&buffer, &TxReqHdmiCecPowerOn, MSG_LENGTH);
    else
        memcpy(&buffer, &TxReqHdmiCecPowerOff, MSG_LENGTH);
    da_tx(buffer);
}

void DA_TxLipSyncMode(Lip_Mode_Type mode)
{
    uint8_t buffer[MSG_LENGTH];
    if(mode == E_LIP_SYNC_AUTO)
        memcpy(&buffer, &TxReqLipSyncAuto, MSG_LENGTH);
    else
        memcpy(&buffer, &TxReqLipSyncManual, MSG_LENGTH);
    da_tx(buffer);
}

void DA_TxLipSyncDelay(Lip_Time_Type time)
{
    uint8_t buffer[MSG_LENGTH];
    Da_Msg_Type msg;
    msg.start = MSG_START;
    msg.id    = E_MSG_ID_TX_SET_LIP_SYNC_DELAY;
    msg.data  = time;

    memcpy(&buffer, &msg, MSG_LENGTH);
    da_tx(buffer);
}

void DA_TxMute(bool state)
{
    uint8_t buffer[MSG_LENGTH];
    if(state == ON)
        memcpy(&buffer, &TxMuteOn, MSG_LENGTH);
    else
        memcpy(&buffer, &TxMuteOff, MSG_LENGTH);
    da_tx(buffer);
}

void DA_TxVolume(uint8_t vol)
{
    uint8_t buffer[MSG_LENGTH];
    Da_Msg_Type vol_msg;
    vol_msg.start = MSG_START;
    vol_msg.id = E_MSG_ID_TX_RPT_HDMI_CEC_VOLUME_STATUS;
    vol_msg.data = vol;

    memcpy(&buffer, &vol_msg, MSG_LENGTH);
    da_tx(buffer);
}

void DA_TxDigGain(Dig_Gain_Type input, uint8_t gain)
{
    uint8_t buffer[MSG_LENGTH];
    Da_Msg_Type gain_msg;
    gain_msg.start = MSG_START;

    switch(input) {
        case E_DIG_GAIN_HDMI: gain_msg.id = E_MSG_ID_TX_SET_HDMI_GAIN; break;
        case E_DIG_GAIN_OPT1: gain_msg.id = E_MSG_ID_TX_SET_OPT1_GAIN; break;
        case E_DIG_GAIN_OPT2: gain_msg.id = E_MSG_ID_TX_SET_OPT2_GAIN; break;
    }

    gain_msg.data = gain;

    memcpy(&buffer, &gain_msg, MSG_LENGTH);
    da_tx(buffer);
}


void DA_TxOsdName(void)
{

// TODO
    uint8_t msg[15];
    uint8_t *msgp = &msg[0];
    msg[0] = MSG_START;
    msg[1] = E_MSG_ID_TX_RPT_HDMI_CEC_OSD;
    memcpy(&msg[2],"C80          ",13);
    pdc_packet_t tx_packet;
    tx_packet.ul_size = 15;
    tx_packet.ul_addr = (uint32_t)msgp;
    pdc_tx_init(PDC_USART1, &tx_packet, NULL);
	while(!(usart_is_tx_buf_end(USART1))){};
}

//===================================================================
// Private Functions
//===================================================================
static void da_tx(uint8_t *buffer)
{
    pdc_packet_t tx_packet;
    tx_packet.ul_size = MSG_LENGTH;
    tx_packet.ul_addr = (uint32_t)buffer;
    pdc_tx_init(PDC_USART1, &tx_packet, NULL);
	while(!(usart_is_tx_buf_end(USART1))){};
}

//-----------------------------------------------------------------------------|
//	DA_ProcessRx.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include "DA_ProcessRx.h"
#include "APP_Process.h"
#include "PWR_Control.h"
#include "DA_ProcessTx.h"
#include "DA_Queue.h"
#include "DA_Display.h"
#include "DA_Config.h"
#include "UI_Control.h"
#include "UI_Common.h"
#include "VOL_Control.h"
#include "NVM_access.h"
#include "IOX_Control.h"

static const Da_Msg_Type RxMsgInit = {0,0,0};

//===================================================================
// Public Functions
//===================================================================
void USART1_Handler(void) {
    static vuint8_t nextState = MSG_START_IDX;  // TODO: check Init next_state
    static Da_Msg_Type rx;
    static uint32_t rx_char;

    if(usart_read(USART1, &rx_char))
        return;

	switch(nextState){
		case MSG_START_IDX:
            if(rx_char == MSG_START) {
                rx.start = rx_char;
                nextState = MSG_ID_IDX;
            }
			break;
		case MSG_ID_IDX:
            rx.id = rx_char;
            nextState = MSG_DATA_IDX;
			break;
		case MSG_DATA_IDX:
            rx.data = rx_char;
            DA_RxQueuePushMsg(rx);
            rx = RxMsgInit;
            nextState = MSG_START_IDX;
			break;
		default:
            rx = RxMsgInit;
			nextState = MSG_START_IDX;
			break;
	}

	uint32_t status = usart_get_status(USART1);
}

void DA_ProcessRxMsg(Da_Msg_Type rx) {

    if(rx.start != MSG_START)
        return;

	switch(rx.id){
		case E_MSG_ID_RX_USB_RATE:
            if(UI_GetMenu() == E_MENU_MAIN)
                DA_DisplayUsbRate((Usb_Rate_Type)rx.data);
            DA.usb_rate = (Usb_Rate_Type)rx.data;
			break;
		case E_MSG_ID_RX_MCT_RATE:
            if(UI_GetMenu() == E_MENU_MAIN)
                DA_DisplayMctRate((Mct_Rate_Type)rx.data);
            DA.mct_rate = (Mct_Rate_Type)rx.data;
			break;
		case E_MSG_ID_RX_SPDIF_RATE:
            if(UI_GetMenu() == E_MENU_MAIN)
                DA_DisplaySpdifRate((Spdif_Rate_Type)rx.data);
            DA.spdif_rate = (Spdif_Rate_Type)rx.data;
			break;
		case E_MSG_ID_RX_HDMI_RATE:
            if(UI_GetMenu() == E_MENU_MAIN)
                DA_DisplayHdmiRate((Hdmi_Rate_Type)rx.data);
            DA.hdmi_rate = (Hdmi_Rate_Type)rx.data;
			break;
        case E_MSG_ID_RX_SET_HDMI_CEC_POWER:
            // Only Received when HDMI is current input
            if(NVM_GetSrc() == E_SRC_HDMI) {
                if (NVM_GetHdmiCecPwr() == ON) {
                    if(rx.data == 0) {  // Turn OFF
                        DA_Init_PwrTimer();
                        DA_Set_CEC_Pwr(YES);
                        if(PWR_GetSleepState() == NO) { // NOT sleeping
                            UI_Init();
                            IOX_SleepEnter();
                            PWR_SetSleepState(YES);
                        }
                    }
                    else {  // Turn ON
                        if((PWR_GetSleepState() == YES) && (DA_Get_CEC_Pwr() == NO)) {
                            IOX_SleepExit();
                            PWR_SetSleepState(NO);
                        }
                    }
                }
            }
            break;
        case E_MSG_ID_RX_SET_HDMI_CEC_VOL_MUTE:
            UI_Input(C_KEY_MUTE);
            break;
        case E_MSG_ID_RX_SET_HDMI_CEC_VOL_UP:
            VOL_Accum(1);
            break;
        case E_MSG_ID_RX_SET_HDMI_CEC_VOL_DN:
            VOL_Accum(-1);
            break;
        case E_MSG_ID_RX_RPT_INPUT:
            // response to Set input command
            break;
        case E_MSG_ID_RX_RPT_HDMI_CEC_STATE:
            // verify change to CEC state
            break;
        case E_MSG_ID_RX_RPT_DSP_DELAY:
            // TODO:


            break;
		case E_MSG_ID_RX_READY:
        case E_MSG_ID_RX_FW_VER_MAJOR:
        case E_MSG_ID_RX_FW_VER_MINOR:
		default:
			break;
	}
}

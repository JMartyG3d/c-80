//-----------------------------------------------------------------------------|
//	DA_Display.h
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __DA_DISPLAY_H__
#define __DA_DISPLAY_H__

// USB Display
typedef enum {
    E_USB_RATE_SILENT,
    E_USB_RATE_32KHZ,
    E_USB_RATE_44KHZ,
    E_USB_RATE_48KHZ,
    E_USB_RATE_88KHZ,
    E_USB_RATE_96KHZ,
    E_USB_RATE_176KHZ,
    E_USB_RATE_192KHZ,
    E_USB_RATE_352KHZ,
    E_USB_RATE_384KHZ,
    E_USB_RATE_DSD64,
    E_USB_RATE_DSD128,
    E_USB_RATE_DSD256,
    E_USB_RATE_DSD512,
} Usb_Rate_Type;

typedef enum {
    E_MCT_RATE_SILENT,
    E_MCT_RATE_CD,    // PCM
    E_MCT_RATE_SACD,  // DSD
} Mct_Rate_Type;

// MCT Display
typedef enum {
    E_SPDIF_RATE_SILENT,
    E_SPDIF_RATE_32KHZ,
    E_SPDIF_RATE_44KHZ,
    E_SPDIF_RATE_48KHZ,
    E_SPDIF_RATE_88KHZ,
    E_SPDIF_RATE_96KHZ,
    E_SPDIF_RATE_176KHZ,
    E_SPDIF_RATE_192KHZ,
    E_SPDIF_RATE_352KHZ,
    E_SPDIF_RATE_384KHZ,
    E_SPDIF_RATE_DOLBY,
    E_SPDIF_RATE_DTS,
} Spdif_Rate_Type;

typedef enum {
    E_HDMI_RATE_SILENT,
    E_HDMI_RATE_32KHZ,
    E_HDMI_RATE_44KHZ,
    E_HDMI_RATE_48KHZ,
    E_HDMI_RATE_88KHZ,
    E_HDMI_RATE_96KHZ,
    E_HDMI_RATE_176KHZ,
    E_HDMI_RATE_192KHZ,
    E_HDMI_RATE_DOLBY,
    E_HDMI_RATE_DTS,
} Hdmi_Rate_Type;

//===================================================================
// Public Function Prototypes
//===================================================================
void DA_DisplayUsbRate(Usb_Rate_Type disp);
void DA_DisplayMctRate(Mct_Rate_Type disp);
void DA_DisplaySpdifRate(Spdif_Rate_Type disp);
void DA_DisplayHdmiRate(Hdmi_Rate_Type disp);

#endif /* __DA_DISPLAY_H__ */
//-----------------------------------------------------------------------------|
//	DA_Config.h
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __DA_CONFIG_H__
#define __DA_CONFIG_H__

#include "DA_Display.h"

#define ALL_INTERRUPT_MASK    0xFFFFFFFF
#define NONVALUE_PAYLOAD      -1

// Message Fields
#define MSG_START_IDX          0
#define MSG_ID_IDX             1
#define MSG_DATA_IDX           2

#define MSG_LENGTH             3
#define MSG_START              0x55

#define DA_HDWR_DA1           0
#define DA_HDWR_DA2           1

typedef enum {
    E_MSG_ID_RX_READY                      = 0xA1,
    E_MSG_ID_RX_FW_VER_MAJOR               = 0xA2,
    E_MSG_ID_RX_FW_VER_MINOR               = 0xA3,
    E_MSG_ID_RX_USB_RATE                   = 0xA4,
    E_MSG_ID_RX_MCT_RATE                   = 0xA5,
    E_MSG_ID_RX_SPDIF_RATE                 = 0xA6,
    E_MSG_ID_RX_HW_VER                     = 0xA7,
    E_MSG_ID_RX_HDMI_RATE                  = 0xA8, // DA2
    E_MSG_ID_RX_SET_HDMI_CEC_POWER         = 0xA9, // DA2
    E_MSG_ID_RX_SET_HDMI_CEC_VOL_MUTE      = 0xAA, // DA2 commands HOST to MUTE/UNMUTE
    E_MSG_ID_RX_SET_HDMI_CEC_VOL_UP        = 0xAB, // DA2 commands HOST
    E_MSG_ID_RX_SET_HDMI_CEC_VOL_DN        = 0xAC, // DA2
    E_MSG_ID_RX_RPT_INPUT                  = 0xAD, // DA2
    E_MSG_ID_RX_REQ_HDMI_CEC_VOLUME_STATUS = 0xAE, // DA2
    E_MSG_ID_RX_REQ_HDMI_CEC_MUTE_STATUS   = 0xAF, // DA2
    E_MSG_ID_RX_RPT_HDMI_CEC_STATE         = 0xB0, // DA2
//    E_MSG_ID_RX_RPT_MQA_PLAYING            = 0xB1, // DA2 DEPRICATED
    E_MSG_ID_RX_RPT_DSP_DELAY              = 0xB2, // DA2
    E_MSG_ID_RX_RPT_LIP_SYNC_DELAY         = 0xB3, // DA2
} Da_Rx_Msg_Type;

typedef enum {
    E_MSG_ID_TX_INPUT_SELECT               = 0xC1,
    E_MSG_ID_TX_MUTE                       = 0xC2, // Unused: NOT vol, Relay to quiet DA?
    E_MSG_ID_TX_UNMUTE                     = 0xC3, // Unused: NOT Vol, Relay to quiet DA? transistors
    E_MSG_ID_TX_REQ_FW_VER_MAJOR           = 0xC4,
    E_MSG_ID_TX_REQ_FW_VER_MINOR           = 0xC5,
    E_MSG_ID_TX_REQ_SAMBA_MODE             = 0xC6, // DA2 "RX_REQ_FW_UPDATE_MODE"
    E_MSG_ID_TX_REQ_HW_VER                 = 0xC7,
    E_MSG_ID_TX_REQ_USB_AUTOMUTE           = 0xC8,
    E_MSG_ID_TX_REQ_POWERDOWN              = 0xC9, // DA2
    E_MSG_ID_TX_SET_HDMI_CEC_STATE         = 0xCA, // DA2
    E_MSG_ID_TX_SET_LIP_SYNC_DELAY         = 0xCB, // DA2
//    E_MSG_ID_TX_SET_LIP_SYNC_DELAY_DOWN    = 0xCC, // DA2
    E_MSG_ID_TX_SET_LIP_SYNC_MODE          = 0xCD, // DA2
    E_MSG_ID_TX_SET_HDMI_CEC_PWR_MODE      = 0xCE, // DA2
//    E_MSG_ID_TX_REQ_LOW_POWER_MODE         = 0xCF, // DA2
    E_MSG_ID_TX_RPT_HDMI_CEC_VOLUME_STATUS = 0xD0, // DA2
    E_MSG_ID_TX_RPT_HDMI_CEC_MUTE_STATUS   = 0xD1, // DA2
    E_MSG_ID_TX_RPT_HDMI_CEC_OSD           = 0xD2, // DA2
//    E_MSG_ID_TX_SET_MQA_STATE              = 0xD3, // DA2
    E_MSG_ID_TX_SET_HDMI_GAIN              = 0xD4, // DA2
    E_MSG_ID_TX_SET_OPT1_GAIN              = 0xD5, // DA2
    E_MSG_ID_TX_SET_OPT2_GAIN              = 0xD6, // DA2
} Da_Tx_Msg_Type;

typedef struct {
    uint8_t start;
    uint8_t id;
    uint8_t data;
} Da_Msg_Type;

// Input Select Message Data
typedef enum {
    E_MSG_INPUT_SEL_USB   = 9,  // match HOST SRC definitions
    E_MSG_INPUT_SEL_MCT   = 10,
    E_MSG_INPUT_SEL_OPT1  = 11,
    E_MSG_INPUT_SEL_OPT2  = 12,
    E_MSG_INPUT_SEL_OPT3  = 13,
    E_MSG_INPUT_SEL_COAX1 = 14,
    E_MSG_INPUT_SEL_COAX2 = 15,
    E_MSG_INPUT_SEL_HDMI  = 16,
    E_MSG_INPUT_SEL_NONE  = 0xFF,
} Da_Input_Type;

typedef struct {
    bool RxReadyMsg;
    bool RxFwVerMajorMsg;
    bool RxFwVerMinorMsg;
    bool RxHwVerMsg;
    uint8_t FwVerMajor;
    uint8_t FwVerMinor;
    uint8_t HwVer;
    Da_Input_Type Input_Selected;
    Usb_Rate_Type usb_rate;
    Mct_Rate_Type mct_rate;
    Spdif_Rate_Type spdif_rate;
    Hdmi_Rate_Type hdmi_rate;
} Da_Status_Type;

extern Da_Status_Type DA;

//===================================================================
// Public Function Prototypes
//===================================================================
void DA_Config(uint8_t irq_priority, uint8_t DA_mode);
void DA_Periodic(void);
bool DA_DspDelay(void);
bool DA_Installed(void);
bool DA1_Installed(void);
bool DA2_Installed(void);
void DA_Sleep(void);
void DA_SourceChange(void);
Usb_Rate_Type DA_GetUsbRate(void);
Spdif_Rate_Type DA_GetSpdifRate(void);
Mct_Rate_Type DA_GetMctRate(void);
Hdmi_Rate_Type DA_GetHdmiRate(void);
uint8_t DA_MinorVersion(void);
uint8_t DA_MajorVersion(void);
uint8_t DA_GetHwVersion(void);
void DA_Init_PwrTimer(void);
void DA_Set_CEC_Pwr(bool state);
bool DA_Get_CEC_Pwr(void);

#endif // __DA_CONFIG_H__



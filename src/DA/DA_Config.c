//-----------------------------------------------------------------------------|
//	DA_Config.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include "PWR_Control.h"
#include "SRC_Control.h"
#include "DA_Config.h"
#include "DA_ProcessTx.h"
#include "DA_Queue.h"
#include "DA_ProcessRx.h"
#include "DA_ProcessTx.h"
#include "DA_Display.h"
#include "IOX_Control.h"
#include "Common.h"
#include "NVM_Access.h"
#include "APP_Process.h"
#include "DISP_Control.h"

Da_Status_Type DA;
static const Da_Status_Type __da_init = {
    .RxReadyMsg       = FALSE,
    .RxFwVerMajorMsg  = FALSE,
    .RxFwVerMinorMsg  = FALSE,
    .RxHwVerMsg       = FALSE,
    .FwVerMajor       = 0xFF,
    .FwVerMinor       = 0xFF,
    .HwVer            = 0xFF,
    .Input_Selected   = E_MSG_INPUT_SEL_NONE,
    .usb_rate         = E_USB_RATE_SILENT,
    .mct_rate         = E_MCT_RATE_SILENT,
    .spdif_rate       = E_SPDIF_RATE_SILENT,
    .hdmi_rate        = E_HDMI_RATE_SILENT,
};

static bool __handshake_done, __da_installed, __dsp_delay_inprogress;
static uint16_t Timer_CEC_Power = 0;
static bool __CEC_Just_Turned_Off = YES;

//===================================================================
// Private Function Prototypes
//===================================================================
static void da_process_handshake(void);

//===================================================================
// Public Functions
//===================================================================
void DA_Config(uint8_t irq_priority, uint8_t DA_mode)
{
    APP_IdSet_DA(DA_mode);
    DA = __da_init;
    pio_configure_pin_group(PINS_USART1_PIO, PINS_USART1_RXD, PINS_USART1_RXD_FLAGS);
    pio_configure_pin_group(PINS_USART1_PIO, PINS_USART1_TXD, PINS_USART1_TXD_FLAGS);

    __handshake_done = __da_installed = FALSE;

	const sam_usart_opt_t da_config =
      { DA_mode ? BOARD_USART1_BAUDRATE2 : BOARD_USART1_BAUDRATE1,
        US_MR_CHRL_8_BIT,
        US_MR_PAR_NO,
		US_MR_NBSTOP_1_BIT,
        US_MR_CHMODE_NORMAL, 0 };

	usart_init_rs232(USART1, &da_config, sysclk_get_cpu_hz());
	usart_disable_interrupt(USART1, ALL_INTERRUPT_MASK);
	usart_enable_tx(USART1);
	usart_enable_rx(USART1);

	NVIC_EnableIRQ(USART1_IRQn);
    NVIC_SetPriority((IRQn_Type)USART1_IRQn,irq_priority);

	pdc_enable_transfer(PDC_USART1, PERIPH_PTCR_TXTEN);
    usart_enable_interrupt(USART1, US_IER_RXRDY);

    IOX_DaMcuReset(E_DA_RUN);
    DA_QueueInit();
}

void DA_Sleep(void)
{
    IOX_DaMcuReset(E_DA_RESET);

    /* clear pin outputs */
    PIOA->PIO_PER = (PIO_PA21A_RXD1 | PIO_PA22A_TXD1);
    PIOA->PIO_OER = (PIO_PA21A_RXD1 | PIO_PA22A_TXD1);
    PIOA->PIO_CODR = (PIO_PA21A_RXD1 | PIO_PA22A_TXD1);
}

void DA_Periodic(void)
{

    if(!__handshake_done) {
        da_process_handshake();
        return;
    }
    if(++Timer_CEC_Power > 10000){
        DA_Set_CEC_Pwr(NO);
    }
    if(DA_RxQueueDirty() && (APP_GetSchedule() == E_SCHED_ACTIVE))
        DA_ProcessRxMsg(DA_RxQueuePopMsg());
}

bool DA_DspDelay(void)
{
    return __dsp_delay_inprogress;
}

bool DA_Installed(void)
{
    return __da_installed;
}

bool DA1_Installed(void)
{
    return (DA.HwVer == 1);
}

bool DA2_Installed(void)
{
    return (DA.HwVer == 2);
}

void DA_SourceChange(void) {

    switch(NVM_GetSrc()) {
        case E_SRC_USB:
            if(DA.Input_Selected != E_MSG_INPUT_SEL_USB) {
                DA.Input_Selected = E_MSG_INPUT_SEL_USB;
                DA_TxInputSelect(E_MSG_INPUT_SEL_USB);
            }
            break;
        case E_SRC_MCT:
            if(DA.Input_Selected != E_MSG_INPUT_SEL_MCT) {
                DA.Input_Selected = E_MSG_INPUT_SEL_MCT;
                DA_TxInputSelect(E_MSG_INPUT_SEL_MCT);
            }
            break;
        case E_SRC_HDMI:
            if(DA.Input_Selected != E_MSG_INPUT_SEL_HDMI) {
                DA.Input_Selected = E_MSG_INPUT_SEL_HDMI;
                DA_TxInputSelect(E_MSG_INPUT_SEL_HDMI);
            }
            break;
        case E_SRC_OPTI1:
            if(DA.Input_Selected != E_MSG_INPUT_SEL_OPT1) {
                DA.Input_Selected = E_MSG_INPUT_SEL_OPT1;
                DA_TxInputSelect(E_MSG_INPUT_SEL_OPT1);
            }
            break;
        case E_SRC_OPTI2:
            if(DA.Input_Selected != E_MSG_INPUT_SEL_OPT2) {
                DA.Input_Selected = E_MSG_INPUT_SEL_OPT2;
                DA_TxInputSelect(E_MSG_INPUT_SEL_OPT2);
            }
            break;
        case E_SRC_COAX1:
            if(DA.Input_Selected != E_MSG_INPUT_SEL_COAX1) {
                DA.Input_Selected = E_MSG_INPUT_SEL_COAX1;
                DA_TxInputSelect(E_MSG_INPUT_SEL_COAX1);
            }
            break;
        case E_SRC_COAX2:
            if(DA.Input_Selected != E_MSG_INPUT_SEL_COAX2) {
                DA.Input_Selected = E_MSG_INPUT_SEL_COAX2;
                DA_TxInputSelect(E_MSG_INPUT_SEL_COAX2);
            }
            break;
        default:
            DA.Input_Selected = E_MSG_INPUT_SEL_NONE;
            DA_TxInputSelect(E_MSG_INPUT_SEL_NONE);
            break;
    }
}

Usb_Rate_Type DA_GetUsbRate(void)
{
    return DA.usb_rate;
}

Spdif_Rate_Type DA_GetSpdifRate(void)
{
    return DA.spdif_rate;
}

Mct_Rate_Type DA_GetMctRate(void)
{
    return DA.mct_rate;
}

Hdmi_Rate_Type DA_GetHdmiRate(void)
{
    return DA.hdmi_rate;
}

uint8_t DA_MinorVersion(void)
{
    return DA.FwVerMinor;
}

uint8_t DA_MajorVersion(void)
{
    return DA.FwVerMajor;
}

uint8_t DA_GetHwVersion(void)
{
    return DA.HwVer;
}

void DA_Init_PwrTimer(void)
{
    Timer_CEC_Power = 0;
}
void DA_Set_CEC_Pwr(bool state)
{
    __CEC_Just_Turned_Off = state;
}
bool DA_Get_CEC_Pwr(void)
{
    return __CEC_Just_Turned_Off;
}

//===================================================================
// Private Functions
//===================================================================
static void da_process_handshake(void) {

    if(!DA_RxQueueDirty())
        return;

    Da_Msg_Type rx = DA_RxQueuePopMsg();

    switch(rx.id){
        case E_MSG_ID_RX_RPT_DSP_DELAY:
            __dsp_delay_inprogress = TRUE;
            DISP_PrintCenter(C_DISP_LINE2,"Update In Progress\0");
            break;
        case E_MSG_ID_RX_READY:
            if(!DA.RxReadyMsg) {
                DISP_ClearLine(C_DISP_LINE2);
                __dsp_delay_inprogress = FALSE;
                DA.RxReadyMsg = TRUE;
                DA_QueueInit();
                DA_TxReqFwVerMajor();
            }
            break;
        case E_MSG_ID_RX_FW_VER_MAJOR:
            if(!DA.RxFwVerMajorMsg && DA.RxReadyMsg) {
                DA.RxFwVerMajorMsg = TRUE;
                DA.FwVerMajor = rx.data;
                DA_TxReqFwVerMinor();
            }
            break;
        case E_MSG_ID_RX_FW_VER_MINOR:
            if(!DA.RxFwVerMinorMsg && DA.RxReadyMsg && DA.RxFwVerMajorMsg) {
                DA.RxFwVerMinorMsg = TRUE;
                DA.FwVerMinor = rx.data;
                DA_TxReqHwVer();
            }
            break;
        case E_MSG_ID_RX_HW_VER:
            if(!DA.RxHwVerMsg && DA.RxFwVerMinorMsg && DA.RxReadyMsg && DA.RxFwVerMajorMsg) {
                DA_TxUsbAutomute(NVM_GetUsbAutomute());
                __handshake_done = __da_installed = TRUE;
                DA.RxHwVerMsg = TRUE;
                DA_TxOsdName();
                DA_TxVolume(NVM_GetVolume());
                DA_TxLipSyncMode(NVM_GetLipSyncMode());
                DA_TxLipSyncDelay(NVM_GetLipSyncTime());
                DA_TxHdmiCecPower(NVM_GetHdmiCecPwr());
                DA_TxHdmiCecState(NVM_GetHdmiCec());
                DA_TxDigGain(E_DIG_GAIN_HDMI, NVM_GetDigGain(E_DIG_GAIN_HDMI));
                DA_TxDigGain(E_DIG_GAIN_OPT1, NVM_GetDigGain(E_DIG_GAIN_OPT1));
                DA_TxDigGain(E_DIG_GAIN_OPT2, NVM_GetDigGain(E_DIG_GAIN_OPT2));
                DA.HwVer = rx.data;
            }
            DA.Input_Selected = E_MSG_INPUT_SEL_NONE;
            break;
        default:
            break;
    }
}

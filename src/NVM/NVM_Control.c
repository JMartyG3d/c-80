//-----------------------------------------------------------------------------|
//	NVM_Control.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include <string.h>
#include "NVM_Control.h"
#include "NVM_Access.h"
#include "Common.h"
#include "SRC_Control.h"
#include "PRODUCT_Config.h"

NVM_SerialNum_Type __nvm_serial;
NVM_Status_Type    __nvm_status;
NVM_Setup_Type     __nvm_setup;
NVM_Data_Type      __nvm_data;

static NVM_Bank_Type    __nvm_bank;
static NVM_Names_Type   __nvm_input_names[C_INPUT_BANKS];
static Nvm_Status_eType __nvm_save[E_NVM_LAST];
static int32_t          __nvm_write_delay; // delay for writing

char * NVM_SerialString = __nvm_serial.serial; // (GLOBAL) access to the serial string

/* (GLOBAL) linear array of names for easy random access */
char * NVM_SavedNames[E_SRC_NUM] = {
    __nvm_input_names[0].name[0],
    __nvm_input_names[0].name[1],
    __nvm_input_names[0].name[2],
    __nvm_input_names[0].name[3],
    __nvm_input_names[1].name[0],
    __nvm_input_names[1].name[1],
    __nvm_input_names[1].name[2],
    __nvm_input_names[1].name[3],
    __nvm_input_names[2].name[0],
    __nvm_input_names[2].name[1],
    __nvm_input_names[2].name[2],
    __nvm_input_names[2].name[3],
 //   __nvm_input_names[3].name[0],
 //   __nvm_input_names[3].name[1],
};

// Lookup table for each bank -- indexed by Nvm_Bank_eType
const static Nvm_Bankinfo_Type __nvm_bankinfo[E_NVM_LAST] = {
    {&__nvm_serial,         sizeof(NVM_SerialNum_Type), C_NVM_SERIALNUM_KEY},// E_NVM_SERIALNUM
    {&__nvm_bank,           sizeof(NVM_Bank_Type),      C_NVM_BANKDATA_KEY}, // E_NVM_BANK
    {&__nvm_status,         sizeof(NVM_Status_Type),    C_NVM_STATUS_KEY},   // E_NVM_STATUS
    {&__nvm_setup,          sizeof(NVM_Setup_Type),     C_NVM_SETUP_KEY},    // E_NVM_SETUP
    {&__nvm_data,           sizeof(NVM_Data_Type),      C_NVM_DATA_KEY},     // E_NVM_DATA
    {&__nvm_input_names[0], sizeof(NVM_Names_Type),     C_NVM_NAMES_KEY},    // E_NVM_NAMES_0
    {&__nvm_input_names[1], sizeof(NVM_Names_Type),     C_NVM_NAMES_KEY},    // E_NVM_NAMES_1
    {&__nvm_input_names[2], sizeof(NVM_Names_Type),     C_NVM_NAMES_KEY},    // E_NVM_NAMES_2
//    {&__nvm_input_names[3], sizeof(NVM_Names_Type),     C_NVM_NAMES_KEY},    // E_NVM_NAMES_3
};

//==============================================================================
// Private Function Prototypes
//==============================================================================
static void nvm_restore_local_all(void);            // restore buffers from nvm
static bool nvm_restore_bank(Nvm_Bank_eType bank);  // retreive the bank from nvm
static bool nvm_restore(Nvm_Bank_eType bank);
static void nvm_factory_restore_bank(void);
static void nvm_factory_restore_names(uint8_t bank);
static void nvm_rotate_bank(uint8_t bank);
static bool nvm_write(uint16_t iaddr, uint8_t bytecount, uint8_t * buffer, uint32_t page);
static bool nvm_read(uint16_t iaddr, uint8_t bytecount, uint8_t * dest, uint32_t page);
static uint32_t nvm_checksum(uint8_t bytes[], int32_t len);

//==============================================================================
// Public Functions
//==============================================================================
void NVM_Startup(void)
{
    __nvm_write_delay = C_NVM_NO_WRITE_REQ;

    // restore local copies, if a read error/invalid bank is found, auto factory reset
    nvm_restore_local_all();

//    NVM_ClearAll();
//    NVM_SetProduct(1);

}

void NVM_Shutdown(void)
{
    // immediately store all outstanding writes
    for(uint32_t i=0; i<E_NVM_LAST; i++) {
        if (__nvm_save[i] == E_NVM_WRITE_REQ)
           NVM_WriteBank(i); // write the bank
    }
}

void NVM_Periodic(void)
{
    if (__nvm_write_delay != C_NVM_NO_WRITE_REQ) {
        if (__nvm_write_delay == 0) {
            /* save outstanding writes */
            for (uint32_t i=0; i<E_NVM_LAST; i++) {
                if (__nvm_save[i] == E_NVM_WRITE_REQ)
                   NVM_WriteBank(i); // write the bank
            }
            __nvm_write_delay = C_NVM_NO_WRITE_REQ; // reset write delay
        }
        else
            __nvm_write_delay--;
    }
}

void NVM_FactoryResetAll(void)
{
    NVM_FactoryRestoreStatus();
    NVM_FactoryRestoreSetup();
    NVM_FactoryRestoreData();
    nvm_factory_restore_names(0);
    nvm_factory_restore_names(1);
    nvm_factory_restore_names(2);
//    nvm_factory_restore_names(3);   C80 Removed as only 3 banks are used now
}

void NVM_MarkBank(uint8_t bank)
{
    __nvm_save[bank] = E_NVM_WRITE_REQ;
    __nvm_write_delay = C_NVM_WRITE_DELAY;
}

void NVM_FactoryRestoreStatus(void)
{
    uint8_t saved_MCU_DL_Flag = __nvm_status.mcu_fw_dl;  // Preserve Update flag
    char saveString[TARGET_VERSION_SIZE];            
    for(uint8_t i=0; i<TARGET_VERSION_SIZE; i++)         // Save name of previous version
        saveString[i] = __nvm_status.fw_ver_prev[i];  
    
    /* clear/reset all values */
    memset(&__nvm_status, 0, sizeof(NVM_Status_Type));

    NVM_SetFwPrevVersion(saveString);           // Restore name of previous version
    __nvm_status.mcu_fw_dl = saved_MCU_DL_Flag; // Restore state of the Update flag

    __nvm_status.da_fw_dl = FALSE;
    __nvm_status.src = E_SRC_BAL1;
    __nvm_status.src_pt = E_SRC_PT_OFF;
    __nvm_status.volume = 15;
    __nvm_status.hp_vol = 15;
    __nvm_status.balance = 0;
    __nvm_status.out1 = ON;
    __nvm_status.out2 = ON;
    __nvm_status.out1_pt = ON;
    __nvm_status.out2_pt = ON;
    __nvm_status.hxd = 1;
    __nvm_status.trig_meterlight = 1;
    __nvm_status.brightness = E_DISP_DUTY1;
    __nvm_status.brightness_pt = E_DISP_DUTY1;
 
    if(NVM_WriteBank(E_NVM_STATUS) == FALSE)        // write data to nvm
        nvm_rotate_bank(E_NVM_STATUS);              // trigger bank rotation
}

void NVM_FactoryRestoreSetup(void)
{
    for(uint8_t i=0;i<E_TRIG_ID_NUM; i++)
        __nvm_setup.triggers[i] = E_TRIG_ON;

    for(uint8_t i=0;i<E_TRIG_ID_NUM; i++)
        __nvm_setup.trigger_input[i] = 0x00000000;

    for(uint8_t i=0;i<E_DPORT_ID_NUM; i++)
        __nvm_setup.dataports[i] = E_SRC_DP_ALL;

    __nvm_setup.rs232_baud    = E_BAUD_115200;
    __nvm_setup.pwr_autooff   = ON;
    __nvm_setup.ir_front      = ON;
    __nvm_setup.ir_coding     = ON;   // NORMAL
    __nvm_setup.output_1sw    = E_OUTPUT_SWITCHED;
    __nvm_setup.output_2sw    = E_OUTPUT_SWITCHED;
    __nvm_setup.hp_out        = E_HP_OUT_MUTE_ALL;
    __nvm_setup.usb_automute  = 1; // Default to ON
    __nvm_setup.hdmi_cec      = 1;
    __nvm_setup.hdmi_cec_pwr  = 1;
    __nvm_setup.lip_sync_mode = E_LIP_SYNC_AUTO;
    __nvm_setup.lip_sync_time = E_LIP_TIME_0MS;
    __nvm_setup.mono         = 0x0000;   // Stereo Default
    __nvm_setup.tone         = 0x0000;   // Default to OFF
    __nvm_setup.mc_phono_res = E_PRES_200_OHMS;
    __nvm_setup.mm_phono_cap = E_PCAP_50_PF;

    // write data to nvm
    if(NVM_WriteBank(E_NVM_SETUP) == FALSE)
        nvm_rotate_bank(E_NVM_SETUP);   // trigger bank rotation
}

void NVM_FactoryRestoreData(void)
{
    /* clear/reset all values */
    memset(&__nvm_data, 0, sizeof(NVM_Data_Type));

    for(uint8_t i=0;i<E_SRC_NUM; i++)
        __nvm_data.src_state[i] = ON;

    __nvm_data.hdmi_gain = 15;
    __nvm_data.opt1_gain = 0;
    __nvm_data.opt2_gain = 0;

    // write data to nvm
    if(NVM_WriteBank(E_NVM_DATA) == FALSE)
        nvm_rotate_bank(E_NVM_DATA);   // trigger bank rotation
}

// bool the indicated bank to nvm
bool NVM_WriteBank(uint8_t bank)
{
    bool rc = TRUE; // default to good write

    // locate the bank's key address
    uint32_t * addr = (uint32_t*)((uint32_t*)__nvm_bankinfo[bank].buffer + (__nvm_bankinfo[bank].buffer_size/4) - 2);

    *addr = __nvm_bankinfo[bank].bank_key;    // set the key
    addr++;    // advance to the checksum addr

    // calculate and set the checksum
    *addr = nvm_checksum((uint8_t*)__nvm_bankinfo[(uint8_t)bank].buffer, (__nvm_bankinfo[bank].buffer_size - 4));

    // write the bank
    while(!nvm_write(C_NVM_IADDR, __nvm_bankinfo[bank].buffer_size, (uint8_t*)__nvm_bankinfo[bank].buffer, __nvm_bank.banks[bank]));

    //! TODO read bank bank to validate write/checksum

    // set flag
    __nvm_save[bank] = E_NVM_OK;

    return rc;
}

//==============================================================================
// Private Functions
//==============================================================================
static void nvm_restore_local_all(void)
{
    bool success = TRUE;

    // prime some default bank data
    __nvm_bank.banks[0] = 0;
    __nvm_bank.banks[1] = 1;
    nvm_restore(E_NVM_SERIALNUM);
    success = nvm_restore(E_NVM_BANK);

    // if the bankdata is good, restore the remaining banks
    if(success) {
        nvm_restore(E_NVM_STATUS);
        nvm_restore(E_NVM_SETUP);
        nvm_restore(E_NVM_DATA);
        nvm_restore(E_NVM_NAMES_0);
        nvm_restore(E_NVM_NAMES_1);
        nvm_restore(E_NVM_NAMES_2);
//        nvm_restore(E_NVM_NAMES_3);
    }
    else {
        nvm_factory_restore_bank();
        NVM_FactoryRestoreStatus();
        NVM_FactoryRestoreSetup();
        NVM_FactoryRestoreData();
        nvm_factory_restore_names(0);
        nvm_factory_restore_names(1);
        nvm_factory_restore_names(2);
//        nvm_factory_restore_names(3);
    }
}

static bool nvm_restore_bank(Nvm_Bank_eType bank)
{
    uint32_t chksum; // stored checksum
    bool rc = TRUE;  // default to good bank

    // locate the bank's checksum address
    uint32_t * chksum_addr = (uint32_t*)((uint32_t*)__nvm_bankinfo[bank].buffer + (__nvm_bankinfo[bank].buffer_size/4) - 1);

    *chksum_addr = 0xD4D5D4D5;

    // read the bank
    while (!nvm_read(C_NVM_IADDR, __nvm_bankinfo[bank].buffer_size, (uint8_t*)__nvm_bankinfo[bank].buffer, __nvm_bank.banks[bank]));

    // calc checksum
    chksum = nvm_checksum((uint8_t*)__nvm_bankinfo[(uint8_t)bank].buffer, (__nvm_bankinfo[bank].buffer_size - 4));

    if (chksum != (*chksum_addr))
        rc = FALSE; // bad bank

    return rc;
}

/** restore banks -------------------------------------------------*/
static bool nvm_restore(Nvm_Bank_eType bank)
{
    bool rc = TRUE; // assume good data

    // Make 2 attempts to restore bank
    if(!nvm_restore_bank(bank)) {
        if(!nvm_restore_bank(bank)) {

            // Failure - Restore Defaults
            switch(bank) {
                case E_NVM_SERIALNUM:
                    // NOTE: Serial # always resides in bank 0 - do not trigger bank rotation on a failure
                    // serial # is written in production, and in the event of an nvm failure, it should never fail
                    strcpy(__nvm_serial.serial, "#######\0");
                    NVM_WriteBank(E_NVM_SERIALNUM);
                    break;
                case E_NVM_BANK:
                    // NOTE bankdata # always resides in bank 1 - do not trigger bank rotation on failure
                    // since bankdata is only written in the occurence of other bank failures, it should
                    // not be written to enough to ever fail
                    // Trigger everything set to factory defaults
                    break;
                case E_NVM_STATUS:  NVM_FactoryRestoreStatus();   break;
                case E_NVM_SETUP:   NVM_FactoryRestoreSetup();    break;
                case E_NVM_DATA:    NVM_FactoryRestoreData();     break;
                case E_NVM_NAMES_0: nvm_factory_restore_names(0); break;
                case E_NVM_NAMES_1: nvm_factory_restore_names(1); break;
                case E_NVM_NAMES_2: nvm_factory_restore_names(2); break;
                case E_NVM_NAMES_3: nvm_factory_restore_names(3); break;
                case E_NVM_FAULTS:
                default: break;
            }

            if(bank != E_NVM_SERIALNUM)
                rc = FALSE;
        }
    }

    return rc;
}

/** factory resets -----------------------------------------------------------*/
static void nvm_factory_restore_bank(void)
{
    for(uint8_t i=0;i<E_NVM_LAST;i++)
        __nvm_bank.banks[i] = i;

    NVM_WriteBank(E_NVM_BANK);
}

static void nvm_factory_restore_names(uint8_t bank)
{
    uint8_t write_bank;

    if (bank == 0) {
        SRC_GetDefaultName(E_SRC_BAL1,     &NVM_SavedNames[0][0]);
//        SRC_GetDefaultName(E_SRC_BAL2,     &NVM_SavedNames[1][0]);  // C80 removed
        SRC_GetDefaultName(E_SRC_UNBAL1,   &NVM_SavedNames[1][0]);      // C80 was [2][0]
        SRC_GetDefaultName(E_SRC_UNBAL2,   &NVM_SavedNames[2][0]);      // C80 was [3][0]
        SRC_GetDefaultName(E_SRC_MM_PHONO, &NVM_SavedNames[3][0]);      // C80 was [5][0]
        write_bank = E_NVM_NAMES_0;
    }

    if (bank == 1) {
//        SRC_GetDefaultName(E_SRC_UNBAL3,   &NVM_SavedNames[4][0]);  // C80 removed 
        SRC_GetDefaultName(E_SRC_MC_PHONO, &NVM_SavedNames[4][0]);      // C80 was [6][0]
        SRC_GetDefaultName(E_SRC_COAX1,    &NVM_SavedNames[5][0]);      // C80 was [7][0]
        SRC_GetDefaultName(E_SRC_COAX2,    &NVM_SavedNames[6][0]);      // C80 was [8][0]
        SRC_GetDefaultName(E_SRC_OPTI1,    &NVM_SavedNames[7][0]);      // C80 was [9][0]
        write_bank = E_NVM_NAMES_1;
    }

    if (bank == 2) {
        SRC_GetDefaultName(E_SRC_OPTI2,    &NVM_SavedNames[8][0]);      // C80 was [10][0]
        SRC_GetDefaultName(E_SRC_USB,      &NVM_SavedNames[9][0]);      // C80 was [11][0]
        SRC_GetDefaultName(E_SRC_MCT,      &NVM_SavedNames[10][0]);      // C80 was [12][0]
        SRC_GetDefaultName(E_SRC_HDMI,     &NVM_SavedNames[11][0]);      // C80 was [13][0]
        write_bank = E_NVM_NAMES_2;
    }
/*
    if (bank == 3) {
        SRC_GetDefaultName(E_SRC_MCT,      &NVM_SavedNames[10][0]);      // C80 was [12][0]
        SRC_GetDefaultName(E_SRC_HDMI,     &NVM_SavedNames[11][0]);      // C80 was [13][0]
        write_bank = E_NVM_NAMES_3;
    }
*/
    // write data to nvm
    if(!NVM_WriteBank(write_bank))
        nvm_rotate_bank((E_NVM_NAMES_0+bank)); // trigger bank rotation
}

static void nvm_rotate_bank(uint8_t bank)
{
    bool rc;
    uint16_t next = __nvm_bank.last+1;

    __nvm_bank.banks[bank] = next;

    rc = NVM_WriteBank(bank);

    // while rc is false and next less than max
    while (!rc && (next < C_NVM_NUM_BANKS)) {
        next++;
        if (next < C_NVM_NUM_BANKS) {
            __nvm_bank.banks[bank] = next;  // set bank
            rc = NVM_WriteBank(bank);       // write bank
        }
    }

    if(rc) {
        __nvm_bank.last = next;
        NVM_WriteBank(1);
    }
}

static bool nvm_write(uint16_t iaddr, uint8_t bytecount, uint8_t * buffer, uint32_t page)
{
    return (at24c256_write_continuous((uint32_t)(page * C_NVM_PAGE_SIZE + iaddr) ,bytecount ,buffer) == AT24C_WRITE_SUCCESS);
}

static bool nvm_read(uint16_t iaddr, uint8_t bytecount, uint8_t * dest, uint32_t page)
{
    return(at24c256_read_continuous((uint32_t)(page * C_NVM_PAGE_SIZE + iaddr), bytecount, dest) == AT24C_READ_SUCCESS);
}

static uint32_t nvm_checksum(uint8_t bytes[], int32_t len)
{
    uint32_t checksum = 0;

    while(len > 0) {
        checksum += *(bytes++);
        len--;
    }
    return (~checksum);
}

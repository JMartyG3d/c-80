//-----------------------------------------------------------------------------|
//	NVM_Access.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include "NVM_Access.h"
#include "NVM_Control.h"
#include "PRODUCT_Config.h"
#include "RS232_Tx.h"
#include "OUT_Control.h"

uint8_t (*pNVM_GetVol)(void);
void    (*pNVM_SetVol)(uint8_t volume, bool rs232_tx);

//==============================================================================
// Public Functions
//==============================================================================
bool NVM_GetForceFactoryDefault(void)
{
    return TARGET_DEVICE_FORCE_FACTORY_RESET;
}

bool NVM_GetMcuFwDownload(void)
{
    return __nvm_status.mcu_fw_dl;
}

void NVM_SetMcuFwDownload(bool state)
{
    __nvm_status.mcu_fw_dl = state;
    NVM_MarkBank(E_NVM_STATUS);
}

void NVM_SetFwPrevVersion(char * ver)
{
    for(uint8_t i=0;i<TARGET_VERSION_SIZE;i++)
        __nvm_status.fw_ver_prev[i] = ver[i];
    __nvm_status.fw_ver_prev[TARGET_VERSION_SIZE-1] = 0;      // make sure to tie a knot in the bitter end

    NVM_MarkBank(E_NVM_STATUS);
}

char * NVM_GetFwPrevVersion(void)
{
    return __nvm_status.fw_ver_prev;
}

bool NVM_GetDaFwDownload(void)
{
    return __nvm_status.da_fw_dl;
}

void NVM_SetDaFwDownload(bool state)
{
    __nvm_status.da_fw_dl = state;
    NVM_MarkBank(E_NVM_STATUS);
}

void NVM_SaveSerial(void)
{
    NVM_MarkBank(E_NVM_SERIALNUM);
}

Src_Type NVM_GetSrc(void)
{
    return __nvm_status.src;
}

void NVM_SetSrc(Src_Type src)
{
    __nvm_status.src = src;
    NVM_MarkBank(E_NVM_STATUS);
}

Src_Type NVM_GetPtSrc(void)
{
    return __nvm_status.src_pt;
}

void NVM_SetPtSrc(Src_Type src)
{
    __nvm_status.src_pt = src;
    NVM_MarkBank(E_NVM_STATUS);
}

bool NVM_SrcIsOn(uint8_t source)
{
    return __nvm_data.src_state[source];
}

void NVM_ToggleSrc(uint8_t source)
{
    __nvm_data.src_state[source] = !__nvm_data.src_state[source];
    NVM_MarkBank(E_NVM_DATA);
}

uint8_t NVM_GetVolume(void)
{
    return __nvm_status.volume;
}

void NVM_SetVolume(uint8_t volume, bool rs232_tx)
{
    if(rs232_tx)
        RS232_TxVol(volume);

    __nvm_status.volume = volume;
    NVM_MarkBank(E_NVM_STATUS);
}

uint8_t NVM_GetHpVolume(void)
{
    return __nvm_status.hp_vol;
}

void NVM_SetHpVolume(uint8_t volume, bool rs232_tx)
{
    if(rs232_tx)
        RS232_TxVol(volume);

    __nvm_status.hp_vol = volume;
    NVM_MarkBank(E_NVM_STATUS);
}

int8_t NVM_GetBalance(void)
{
    return __nvm_status.balance;
}

void NVM_SetBalance(uint8_t balance)
{
    __nvm_status.balance = balance;
    NVM_MarkBank(E_NVM_STATUS);
}

bool NVM_GetTone(Src_Type src)
{
    return(__nvm_setup.tone & 0x0001 << src);
}

void NVM_SetTone(Src_Type src, bool state)
{
    if(state == ON)
        __nvm_setup.tone |= 0x0001 << src;
    else
        __nvm_setup.tone &= ~(0x0001 << src);

    NVM_MarkBank(E_NVM_SETUP);
}

int8_t NVM_GetBass(Src_Type src)
{
    return(__nvm_data.bass[src]);
}

void NVM_SetBass(Src_Type src, int8_t bass)
{
    __nvm_data.bass[src] = bass;
    NVM_MarkBank(E_NVM_DATA);
}

int8_t NVM_GetTreble(Src_Type src)
{
    return(__nvm_data.treble[src]);
}

void NVM_SetTreble(Src_Type src, int8_t treble)
{
    __nvm_data.treble[src] = treble;
    NVM_MarkBank(E_NVM_DATA);
}

int8_t NVM_GetLevel(void)
{
    return(__nvm_status.levels[__nvm_status.src]);
}

void NVM_SetLevel(uint8_t level)
{
    __nvm_status.levels[__nvm_status.src] = level;
    NVM_MarkBank(E_NVM_STATUS);
}

bool NVM_GetMono(Src_Type src)
{
    return(__nvm_setup.mono & 0x0001 << src);
}

void NVM_SetMono(Src_Type src, bool state)
{
    if(state)
        __nvm_setup.mono |= 0x0001 << src;
    else
        __nvm_setup.mono &= ~(0x0001 << src);

    NVM_MarkBank(E_NVM_SETUP);
}

Phono_Res_Type NVM_GetMcPhonoRes(void)
{
    return(__nvm_setup.mc_phono_res);
}

void NVM_SetMcPhonoRes(Phono_Res_Type res)
{
    __nvm_setup.mc_phono_res = res;
    NVM_MarkBank(E_NVM_SETUP);
}

Phono_Cap_Type NVM_GetMmPhonoCap(void)
{
    return(__nvm_setup.mm_phono_cap);
}

void NVM_SetMmPhonoCap(Phono_Cap_Type cap)
{
    __nvm_setup.mm_phono_cap = cap;
    NVM_MarkBank(E_NVM_SETUP);
}

uint8_t NVM_GetHXD(void)
{
    return __nvm_status.hxd;
}

void NVM_SetHXD(uint8_t state)
{
    __nvm_status.hxd = state;
    NVM_MarkBank(E_NVM_STATUS);
}

Meter_PT_Type NVM_GetMeterLightPT(void)
{
    return __nvm_status.meter_light_pt;
}

void NVM_SetMeterLightPT(Meter_PT_Type state)
{
    __nvm_status.meter_light_pt = state;
    NVM_MarkBank(E_NVM_STATUS);
}

uint8_t NVM_GetTrigMeterlight(void)
{
    return __nvm_status.trig_meterlight;
}

void NVM_SetTrigMeterlight(uint8_t state)
{
    __nvm_status.trig_meterlight = state;
    NVM_MarkBank(E_NVM_STATUS);
}

Disp_Duty_Type NVM_GetBrightness(void)
{
    return __nvm_status.brightness;
}

void NVM_SetBrightness(Disp_Duty_Type duty)
{
    if(duty <= E_DISP_DUTY_MAX) {
        __nvm_status.brightness = duty;
        NVM_MarkBank(E_NVM_STATUS);
    }
}

Disp_Duty_Type NVM_GetBrightnessPT(void)
{
    return __nvm_status.brightness_pt;
}

void NVM_SetBrightnessPT(Disp_Duty_Type duty)
{
    if(duty <= E_DISP_DUTY_MAX) {
        __nvm_status.brightness_pt = duty;
        NVM_MarkBank(E_NVM_STATUS);
    }
}


Trig_Type NVM_GetTrigger(Trig_Id_Type id) {
    return __nvm_setup.triggers[id];
}

void NVM_SetTrigger(Trig_Id_Type id, Trig_Type src) {
    __nvm_setup.triggers[id] = src;
    NVM_MarkBank(E_NVM_SETUP);
}

bool NVM_GetTriggerInput(Trig_Id_Type id, Src_Type src) {
    return __nvm_setup.trigger_input[id] & (0x00000001 << src);
}

void NVM_SetTriggerInput(Trig_Id_Type id, Src_Type src, bool set) {
    if(set)
        __nvm_setup.trigger_input[id] |= (0x00000001 << src);
    else
        __nvm_setup.trigger_input[id] &= ~(0x00000001 << src);

    NVM_MarkBank(E_NVM_SETUP);
}

Src_Type NVM_GetDataport(Dport_Id_Type id) {
    return __nvm_setup.dataports[id];
}

void NVM_SetDataport(Dport_Id_Type id, Src_Type src) {
    __nvm_setup.dataports[id] = src;
    NVM_MarkBank(E_NVM_SETUP);
}

void NVM_GetName(void) // placeholders
{

}

void NVM_SetName(void) // placeholders
{
    NVM_MarkBank(E_NVM_SETUP);
}

void NVM_SaveName(uint8_t index)
{
    if(index < 4)          // 0 - 3                // C80 was 0-3
        NVM_MarkBank(E_NVM_NAMES_0);
    else if(index < 8)        // 4 - 7                // C80 was 4-7
        NVM_MarkBank(E_NVM_NAMES_1);
    else if(index < 12)          // 8 - 11               // C80 was 8-11
        NVM_MarkBank(E_NVM_NAMES_2);
    else                                // 12 - 15              // C80 was 12-15
        NVM_MarkBank(E_NVM_NAMES_3);
}

uint8_t NVM_GetPwrAutoOff(void)
{
    return __nvm_setup.pwr_autooff;
}

void NVM_SetPwrAutoOff(uint8_t state)
{
    __nvm_setup.pwr_autooff = state;
    NVM_MarkBank(E_NVM_SETUP);
}

uint8_t NVM_GetOutputSw(Output_Type output)
{
    return(output == E_OUTPUT_2) ? __nvm_setup.output_2sw : __nvm_setup.output_1sw;
}

void NVM_SetOutputSw(Output_Type output, uint8_t mode)
{
    if (output == E_OUTPUT_2)
        __nvm_setup.output_2sw = mode;
    else
        __nvm_setup.output_1sw = mode;

    NVM_MarkBank(E_NVM_SETUP);
}

bool NVM_GetOutput(Output_Type output)
{
    if(output == E_OUTPUT_1)
        return __nvm_status.out1;

    return __nvm_status.out2;
}

void NVM_SetOutput(Output_Type output, bool state)
{
    switch(output) {
        case E_OUTPUT_1: __nvm_status.out1 = state; break;
        case E_OUTPUT_2: __nvm_status.out2 = state; break;
    }
    NVM_MarkBank(E_NVM_STATUS);
}

bool NVM_GetPtOutput(Output_Type output)
{
    if(output == E_OUTPUT_1)
        return __nvm_status.out1_pt;

    return __nvm_status.out2_pt;
}

void NVM_SetPtOutput(Output_Type output, bool state)
{
    switch(output) {
        case E_OUTPUT_1: __nvm_status.out1_pt = state; break;
        case E_OUTPUT_2: __nvm_status.out2_pt = state; break;
    }
    NVM_MarkBank(E_NVM_STATUS);
}

Hp_Out_Type NVM_GetHpOut(void)
{
    return __nvm_setup.hp_out;
}

void NVM_SetHpOut(Hp_Out_Type state)
{
    __nvm_setup.hp_out = state;
    NVM_MarkBank(E_NVM_SETUP);
}

bool NVM_GetUsbAutomute(void)
{
    return __nvm_setup.usb_automute;
}

void NVM_SetUsbAutomute(bool state)
{
    __nvm_setup.usb_automute = state;
    NVM_MarkBank(E_NVM_SETUP);
}

bool NVM_GetHdmiCec(void)
{
    return __nvm_setup.hdmi_cec;
}

void NVM_SetHdmiCec(bool state)
{
    __nvm_setup.hdmi_cec = state;
    NVM_MarkBank(E_NVM_SETUP);
}

bool NVM_GetHdmiCecPwr(void)
{
    return __nvm_setup.hdmi_cec_pwr;
}

void NVM_SetHdmiCecPwr(bool state)
{
    __nvm_setup.hdmi_cec_pwr = state;
    NVM_MarkBank(E_NVM_SETUP);
}

Lip_Mode_Type NVM_GetLipSyncMode(void)
{
    return __nvm_setup.lip_sync_mode;
}

void NVM_SetLipSyncMode(Lip_Mode_Type mode)
{
    __nvm_setup.lip_sync_mode = mode;
    NVM_MarkBank(E_NVM_SETUP);
}

Lip_Time_Type NVM_GetLipSyncTime(void)
{
    return __nvm_setup.lip_sync_time;
}

void NVM_SetLipSyncTime(Lip_Time_Type time)
{
    __nvm_setup.lip_sync_time = time;
    NVM_MarkBank(E_NVM_SETUP);
}

bool NVM_GetIrFront(void)
{
    return __nvm_setup.ir_front;
}

void NVM_SetIrFront(bool state)
{
    __nvm_setup.ir_front = state;
    NVM_MarkBank(E_NVM_SETUP);
}

uint8_t NVM_GetIrCoding(void)
{
    return __nvm_setup.ir_coding;
}

void NVM_SetIrCoding(uint8_t state)
{
    __nvm_setup.ir_coding = state;
    NVM_MarkBank(E_NVM_SETUP);
}

Baud_Type NVM_GetRS232Baud(void)
{
    return __nvm_setup.rs232_baud;
}

void NVM_SetRS232Baud(Baud_Type baud)
{
    __nvm_setup.rs232_baud = baud;
    NVM_MarkBank(E_NVM_SETUP);
}
uint8_t NVM_GetDigGain(Dig_Gain_Type input)
{
    uint8_t gain;

    switch(input) {
        case E_DIG_GAIN_HDMI: gain = __nvm_data.hdmi_gain; break;
        case E_DIG_GAIN_OPT1: gain = __nvm_data.opt1_gain; break;
        case E_DIG_GAIN_OPT2: gain = __nvm_data.opt2_gain; break;
    }
    return gain;
}

void NVM_SetDigGain(Dig_Gain_Type input, uint8_t gain)
{
    switch(input) {
        case E_DIG_GAIN_HDMI: __nvm_data.hdmi_gain = gain; break;
        case E_DIG_GAIN_OPT1: __nvm_data.opt1_gain = gain; break;
        case E_DIG_GAIN_OPT2: __nvm_data.opt2_gain = gain; break;
    }
}

//-----------------------------------------------------------------------------|
//	NVM_MemoryMap.h
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __NVM_MEMORY_MAP_H__
#define __NVM_MEMORY_MAP_H__

#include <asf.h>
#include "Common.h"
#include "FLT_Process.h"

#define C_NVM_PAGE_SIZE     64      // NVM device page size (in bytes)
#define C_NVM_NUM_BANKS     512     // Number of pages

#define C_NAMES_PER_BANK    4       // inputs per bank
#define C_INPUT_BANKS       3
// was 5       // number of banks for inputs

typedef enum {
    E_NVM_SERIALNUM = 0,
    E_NVM_BANK,
    E_NVM_STATUS,
    E_NVM_SETUP,
    E_NVM_DATA,
    E_NVM_NAMES_0,
    E_NVM_NAMES_1,
    E_NVM_NAMES_2,
    E_NVM_NAMES_3,
    E_NVM_FAULTS,
    E_NVM_LAST
} Nvm_Bank_eType;

// Serial #, permanant number set in production
typedef struct {
    uint8_t type;      // UNUSED (Purpose: Model Type designator)
    uint16_t bank_loc; // location of the bank data information
    char serial[8];
    // Validation Data - 8 Bytes ----------------------------------------------|
    uint32_t key;
    uint32_t checksum;
} NVM_SerialNum_Type; /* 19 bytes */

/** Bank location pointers (hold location for each nvm struct */
typedef struct {
    // Bank data - 32 Bytes --------------------------------------------------|
    uint16_t banks[E_NVM_LAST]; // location of each bank as indexed by Nvm_Bank_eType
    uint16_t last;              // last bank used (last+1 will be location of a bank that needs to rotate)

    // Validation Data - 8 Bytes ----------------------------------------------|
    uint32_t  key;      // key + checksum validates bank
    uint32_t  checksum;
} NVM_Bank_Type; /* 24 bytes */

typedef struct {
    Src_Type          src;                 // Currently selected source
    Src_Type          src_pt;              // Src selected in Passthru
    uint8_t           volume;              // Current volume
    uint8_t           hp_vol;              // Current Headphone volume
    int8_t            balance;             // Current Balance setting
    int8_t            levels[E_SRC_NUM];   // Current Input Trim
    char              fw_ver_prev[6];      // Previous Mcu Version
    Disp_Duty_Type    brightness;          // display brightness (0=100%,1=75%,2=50%,3=25%)
    Disp_Duty_Type    brightness_pt;       // PT display brightness (0=100%,1=75%,2=50%,3=25%)
    Meter_PT_Type     meter_light_pt;      // PT meter light status 0=OFF, 1=ON, 2 = Trigger
    uint8_t           out1            : 1; // Save the state of OUTPUT1
    uint8_t           out2            : 1; // Save the state of OUTPUT2
    uint8_t           out1_pt         : 1; // Save the state of OUTPUT1 in Passthru
    uint8_t           out2_pt         : 1; // Save the state of OUTPUT2 in Passthru
    uint8_t           hxd             : 1; // OFF=0, ON=1
    uint8_t           trig_meterlight : 1; // Trigger Meterlight On/Off
    uint8_t           mcu_fw_dl       : 1; // MCU Firmware Download operation
    uint8_t           da_fw_dl        : 1; // DA Firmware Download operation
    uint8_t           spare           : 1;
    // Validation Data - 8 Bytes ----------------------------------------------|
    uint32_t    key;
    uint32_t    checksum;
} NVM_Status_Type;

typedef struct {
    Trig_Type       triggers[2];         // Triggers 1-2 setting (ON=0, Output1=1, Output2=2, Input=3)
    uint32_t        trigger_input[2];    // Trigger Input settings
    Src_Type        dataports[4];        // Dataport setting (0..#input, 0xAA=All data)
    Baud_Type       rs232_baud;          // 0=9600, 1=19200, 2=38400, 3=57600, 4=115200
	Phono_Res_Type  mc_phono_res;
    Phono_Cap_Type  mm_phono_cap;
    Hp_Out_Type     hp_out;              // 0->Mute All Outputs, 1->Mute None
    Lip_Mode_Type   lip_sync_mode;       // 0->Manual mode, 1->Auto Mode
    Lip_Time_Type   lip_sync_time;       // 0-14
    uint32_t        mono;                // mono state for each input (0:STEREO[default],1:Mono)
    uint32_t        tone;                // Tone control state for each input (1 = ENABLED)
    uint8_t         pwr_autooff     : 1; // OFF, ON
    uint8_t         ir_front        : 1; // OFF, ON : Front IR remote control
    uint8_t         ir_coding       : 1; // 0=Normal, 1=Alternate
    uint8_t         output_1sw      : 1; // 0->Switched, 1->Unswitched
    uint8_t         output_2sw      : 1; // 0->Switched, 1->Unswitched
    uint8_t         usb_automute    : 1; // 0->Off, 1->On
    uint8_t         hdmi_cec        : 1; // 0->Off, 1->On
    uint8_t         hdmi_cec_pwr    : 1; // 0->Off, 1->On

    // Validation Data - 8 Bytes ----------------------------------------------|
    uint32_t    key;
    uint32_t    checksum;
} NVM_Setup_Type;

typedef struct {
    uint8_t src_state[E_SRC_NUM]; // Src State: OFF, ON
    int8_t  bass[E_SRC_NUM];   // Tone bass setting for each input  [-12 to 12]
    int8_t  treble[E_SRC_NUM]; // Tone Treble setting for each input[-12 to 12]
    uint8_t hdmi_gain;            // HDMI Gain from 0 to +15db
    uint8_t opt1_gain;            // Optical 1 gain
    uint8_t opt2_gain;            // Optical 2 gain
    // Validation Data - 8 Bytes ----------------------------------------------|
    uint32_t    key;      // key + checksum validates bank
    uint32_t    checksum;
} NVM_Data_Type;

typedef struct {
    // Name Data - 44 Bytes ---------------------------------------------------|
    char    name[C_NAMES_PER_BANK][C_MAX_INPUT_LEN]; // 10 chars + NULL char
    // Validation Data - 8 Bytes ----------------------------------------------|
    uint32_t     key;      // key + checksum validates bank
    uint32_t checksum;
} NVM_Names_Type; /* 52 bytes */

typedef struct {
    // Fault Data - 55 Bytes --------------------------------------------------|
    uint32_t number;              // total number of faults recorded since last factory reset
    uint8_t last;                 // index of last fault
    uint8_t faults[C_MAX_FAULTS]; // 50 faults
    // Validation Data - 8 Bytes ----------------------------------------------|
    uint32_t     key;      // key + checksum validates bank
    uint32_t checksum;
} NVM_Fault_Type; /* 63 bytes */

#endif // __NVM_MEMORY_MAP_H__

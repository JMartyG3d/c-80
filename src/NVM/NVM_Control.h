//-----------------------------------------------------------------------------|
//	NVM_Control.h
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __NVM_CONTROL_H__
#define __NVM_CONTROL_H__

#include "NVM_MemoryMap.h"
#include "Common.h"

#define C_NVM_IADDR             0x00

#define C_NVM_NO_WRITE_REQ      -1
#define C_NVM_WRITE_DELAY       250 // 20ms*250=5000ms

#define C_NVM_SERIALNUM_KEY     0xAB5AB5AB
#define C_NVM_BANKDATA_KEY      0xB5B5B5B5
#define C_NVM_STATUS_KEY        0xC5C5C5C5
#define C_NVM_SETUP_KEY         0xD5D5D5D5
#define C_NVM_DATA_KEY          0x3D3D3D3D
#define C_NVM_NAMES_KEY         0xE5E5E5E5
#define C_NVM_PROFILE_KEY       0xF5F5F5F5

typedef enum {
    E_NVM_OK = 0,
    E_NVM_WRITE_REQ,
    E_NVM_READ_FAIL
} Nvm_Status_eType;

typedef struct {
    void     * buffer;
    uint8_t    buffer_size; // size in bytes
    uint32_t   bank_key;    // unique key identifying this bank
} Nvm_Bankinfo_Type;

extern char * NVM_SerialString;
extern char * NVM_SavedNames[E_SRC_NUM];
extern NVM_SerialNum_Type __nvm_serial;
extern NVM_Status_Type    __nvm_status;
extern NVM_Setup_Type     __nvm_setup;
extern NVM_Data_Type      __nvm_data;

//==============================================================================
// Public Function Prototypes
//==============================================================================
void NVM_Startup(void);
void NVM_Shutdown(void);
void NVM_Periodic(void);
void NVM_FactoryResetAll(void);
void NVM_MarkBank(uint8_t bank);
void NVM_FactoryRestoreStatus(void);
void NVM_FactoryRestoreSetup(void);
void NVM_FactoryRestoreData(void);
bool NVM_WriteBank(uint8_t bank);

#endif // __NVM_CONTROL_H__
//-----------------------------------------------------------------------------|
//	NVM_Access.h
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __NVM_ACCESS_H__
#define __NVM_ACCESS_H__

#include "NVM_MemoryMap.h"
#include "Common.h"

extern uint8_t (*pNVM_GetVol)(void);
extern void    (*pNVM_SetVol)(uint8_t volume, bool rs232_tx);

//==============================================================================
// Public Function Prototypes
//==============================================================================
bool       NVM_GetForceFactoryDefault(void);
bool       NVM_GetMcuFwDownload(void);
void       NVM_SetMcuFwDownload(bool state);
void       NVM_SetFwPrevVersion(char * ver);
char *     NVM_GetFwPrevVersion(void);

bool       NVM_GetDaFwDownload(void);
void       NVM_SetDaFwDownload(bool state);

Src_Type   NVM_GetSrc(void);
void       NVM_SetSrc(Src_Type src);

bool       NVM_SrcIsOn(uint8_t source); // return true if given source is "ON"
void       NVM_ToggleSrc(uint8_t source); // toggles the on/off status of given source

Src_Type   NVM_GetPtSrc(void);
void       NVM_SetPtSrc(Src_Type src);

uint8_t    NVM_GetVolume(void);
void       NVM_SetVolume(uint8_t volume, bool rs232_tx);
uint8_t    NVM_GetHpVolume(void);
void       NVM_SetHpVolume(uint8_t volume, bool rs232_tx);

int8_t     NVM_GetBalance(void);
void       NVM_SetBalance(uint8_t balance);

bool       NVM_GetTone(Src_Type src);
void       NVM_SetTone(Src_Type src, bool state);
int8_t     NVM_GetBass(Src_Type src);
void       NVM_SetBass(Src_Type src, int8_t bass);
int8_t     NVM_GetTreble(Src_Type src);
void       NVM_SetTreble(Src_Type src, int8_t treble);

int8_t     NVM_GetLevel(void);
void       NVM_SetLevel(uint8_t level);

bool       NVM_GetMono(Src_Type src);
void       NVM_SetMono(Src_Type src, bool state);

Phono_Res_Type NVM_GetMcPhonoRes(void);
void           NVM_SetMcPhonoRes(Phono_Res_Type res);
Phono_Cap_Type NVM_GetMmPhonoCap(void);
void           NVM_SetMmPhonoCap(Phono_Cap_Type cap);

uint8_t    NVM_GetHXD(void);
void       NVM_SetHXD(uint8_t state);

bool       NVM_GetUsbAutomute(void);
void       NVM_SetUsbAutomute(bool state);
bool       NVM_GetHdmiCec(void);
void       NVM_SetHdmiCec(bool state);
bool       NVM_GetHdmiCecPwr(void);
void       NVM_SetHdmiCecPwr(bool state);
Lip_Mode_Type NVM_GetLipSyncMode(void);
void          NVM_SetLipSyncMode(Lip_Mode_Type mode);
Lip_Time_Type NVM_GetLipSyncTime(void);
void          NVM_SetLipSyncTime(Lip_Time_Type time);

uint8_t    NVM_GetTrigMeterlight(void);
void       NVM_SetTrigMeterlight(uint8_t state);

Meter_PT_Type    NVM_GetMeterLightPT(void);
void             NVM_SetMeterLightPT(Meter_PT_Type state);

Disp_Duty_Type  NVM_GetBrightness(void);
void            NVM_SetBrightness(Disp_Duty_Type duty);

Disp_Duty_Type  NVM_GetBrightnessPT(void);
void            NVM_SetBrightnessPT(Disp_Duty_Type duty);

Trig_Type  NVM_GetTrigger(Trig_Id_Type id);
void       NVM_SetTrigger(Trig_Id_Type id, Trig_Type src);

bool       NVM_GetTriggerInput(Trig_Id_Type id, Src_Type src);
void       NVM_SetTriggerInput(Trig_Id_Type id, Src_Type src, bool set);

Src_Type   NVM_GetDataport(Dport_Id_Type id);
void       NVM_SetDataport(Dport_Id_Type id, Src_Type src);

void       NVM_SaveSerial(void);

void       NVM_GetName(void); // placeholders
void       NVM_SetName(void); // placeholders
void       NVM_SaveName(uint8_t index);

uint8_t    NVM_GetOutputSw(Output_Type output); // return 0 for switched, 1 for unswitched
void       NVM_SetOutputSw(Output_Type output, uint8_t mode);

bool       NVM_GetOutput(Output_Type output);
void       NVM_SetOutput(Output_Type output, bool state);

bool       NVM_GetPtOutput(Output_Type output);
void       NVM_SetPtOutput(Output_Type output, bool state);

Hp_Out_Type NVM_GetHpOut(void);
void        NVM_SetHpOut(Hp_Out_Type state);

bool       NVM_GetIrFront(void);
void       NVM_SetIrFront(bool state);

uint8_t    NVM_GetIrCoding(void);
void       NVM_SetIrCoding(uint8_t state);

Baud_Type  NVM_GetRS232Baud(void);
void       NVM_SetRS232Baud(Baud_Type baud);

uint8_t    NVM_GetPwrAutoOff(void); // return ON if enabled, OFF if disabled
void       NVM_SetPwrAutoOff(uint8_t state);

uint8_t    NVM_GetDigGain(Dig_Gain_Type input);
void       NVM_SetDigGain(Dig_Gain_Type input, uint8_t gain);
#endif // __NVM_ACCESS_H__
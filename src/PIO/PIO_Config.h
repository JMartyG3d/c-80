//-----------------------------------------------------------------------------|
//	PIO_Config.h
//	Abstract: Hold configuration for various PIO
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __PIO_CONFIG_H__
#define __PIO_CONFIG_H__

//==============================================================================
// Public Function Prototypes
//==============================================================================
void PIO_Config(void);
void PIO_Shutdown(void);
void PIO_Restore(void);
void PIO_EnablePeriphClocks(void);
void PIO_DisablePeriphClocks(void);

#endif // __PIO_CONFIG_H__
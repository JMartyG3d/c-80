//-----------------------------------------------------------------------------|
//	PIO_Config.c
//	Abstract: Hold configuration for various PIO
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include "PIO_Config.h"
#include "KEY_Process.h"
#include "RTY_Process.h"
#include "VOL_Control.h"
#include "IR_Process.h"

static bool __pio_shutdown;

//==============================================================================
// Private Function Prototypes
//==============================================================================
static void pio_pwr_config(void);
static void pio_key_config(void);
static void pio_ir_config(void);
static void pio_rty_config(void);
static void pio_vol_config(void);
static void pio_pt_config(void);

//==============================================================================
// Public Functions
//==============================================================================
void PIO_Config(void)
{
    // set the slow clock divider
    PIOA->PIO_SCDR = 0x10;

    pio_pwr_config();
    pio_key_config();
    pio_ir_config();
    pio_rty_config();
    pio_vol_config();
    pio_pt_config();

    pio_handler_set_priority(PIOA, PIOA_IRQn, 5);   // Renables IRQ and NVIC
}

void PIO_Restore(void)
{
    if(__pio_shutdown) {
        pio_pwr_config();
        pio_key_config();
        pio_ir_config();
        pio_rty_config();
        pio_vol_config();
        pio_pt_config();
        __pio_shutdown = FALSE;
    }
}

void PIO_Shutdown(void)
{
    // clear all but power related pins
    PIOA->PIO_IDR    = ~C_PIO_POWER_PINS; // disable interrupts
    PIOA->PIO_CODR   = ~C_PIO_POWER_PINS; // clear outputs
    PIOA->PIO_MDDR   = ~C_PIO_POWER_PINS; // clear multi driver

    __pio_shutdown = TRUE; // mark shutdown
}

void PIO_EnablePeriphClocks(void)
{
    pmc_enable_periph_clk(ID_PIOA);
    pmc_enable_periph_clk(ID_PIOB);
    pmc_enable_periph_clk(ID_USART1);
    pmc_enable_periph_clk(ID_SPI);
}

void PIO_DisablePeriphClocks(void)
{   // Disable Non-critical peripheral clocks to reduce standby power consumption
    pmc_disable_periph_clk(ID_PIOB);
    pmc_disable_periph_clk(ID_SPI);
    pmc_disable_periph_clk(ID_ADC);
}

//==============================================================================
// Private Functions
//==============================================================================
static void pio_pwr_config(void)
{
    pio_set_input(PIOA, PIN19_AC_DETECT, PIO_PULLUP);

	PIOA->PIO_PER  = PIN38_DA_RESET;
	PIOA->PIO_PUER = PIN38_DA_RESET;
	PIOA->PIO_OER  = PIN38_DA_RESET;
	PIOA->PIO_IFER = PIN38_DA_RESET;
}

static void pio_key_config(void)
{
    PIOA->PIO_PER    = PIN20_POWER_KEY; // Pin Enable
    PIOA->PIO_ODR    = PIN20_POWER_KEY; // Output Disable
    PIOA->PIO_PUER   = PIN20_POWER_KEY; // Pull Up Enable
    PIOA->PIO_MDER   = PIN20_POWER_KEY; // Multi-driver enable (? necessary?)
    PIOA->PIO_IFER   = PIN20_POWER_KEY; // Glitch Input Filter Enable
    PIOA->PIO_IFSCER = PIN20_POWER_KEY; // Debounce Filter
    PIOA->PIO_SODR   = PIN20_POWER_KEY;
    PIOA->PIO_IER    = PIN20_POWER_KEY; // Interrupt Enable

    pio_handler_set(PIOA, ID_PIOA, PIN20_POWER_KEY, PIO_DEFAULT, PIO_PwrKeyHandler);
}

static void pio_ir_config(void)
{
	PIOA->PIO_PER  = PIN16_IR_FRONT | PIN15_IR_REAR;
	PIOA->PIO_IER  = PIN16_IR_FRONT | PIN15_IR_REAR;

	PIOA->PIO_PER  = PIN09_IR_CONTROL | PIN10_IR_DATA_OUT;
	PIOA->PIO_PUER = PIN09_IR_CONTROL | PIN10_IR_DATA_OUT;
	PIOA->PIO_OER  = PIN09_IR_CONTROL | PIN10_IR_DATA_OUT;
	PIOA->PIO_SODR = PIN09_IR_CONTROL;
	PIOA->PIO_CODR = PIN10_IR_DATA_OUT;

    pio_handler_set(PIOA, ID_PIOA, PIN16_IR_FRONT, PIO_DEFAULT, IR_PioHandlerFront);
    pio_handler_set(PIOA, ID_PIOA, PIN15_IR_REAR, PIO_DEFAULT, IR_PioHandlerRear);
}

static void pio_rty_config(void)
{
    PIOA->PIO_PER    = PIN25_RTY_INPUT_A | PIN26_RTY_INPUT_B; // Pin Enable
    PIOA->PIO_ODR    = PIN25_RTY_INPUT_A | PIN26_RTY_INPUT_B; // Output Disable
    PIOA->PIO_PUER   = PIN25_RTY_INPUT_A | PIN26_RTY_INPUT_B; // Pull Up Enable
    PIOA->PIO_MDER   = PIN25_RTY_INPUT_A | PIN26_RTY_INPUT_B; // Multi-driver enable (? necessary?)
    PIOA->PIO_IFER   = PIN25_RTY_INPUT_A | PIN26_RTY_INPUT_B; // Glitch Input Filter Enable
    PIOA->PIO_IFSCER = PIN25_RTY_INPUT_A | PIN26_RTY_INPUT_B; // Debounce Filter
    PIOA->PIO_SODR   = PIN25_RTY_INPUT_A | PIN26_RTY_INPUT_B;
    PIOA->PIO_IER    = PIN25_RTY_INPUT_A | PIN26_RTY_INPUT_B; // Interrupt Enable

    pio_handler_set(PIOA, ID_PIOA, PIN25_RTY_INPUT_A | PIN26_RTY_INPUT_B, PIO_IT_RISE_EDGE, RTY_InputPioHandler);
}

static void pio_vol_config(void)
{
    // Volume and Tone Control
	PIOA->PIO_PER  = PIN52_VOL_CS | PIN42_TONE_CS | PIN44_VOL_TONE_DATA | PIN41_VOL_TONE_CLK;
	PIOA->PIO_OER  = PIN52_VOL_CS | PIN42_TONE_CS | PIN44_VOL_TONE_DATA | PIN41_VOL_TONE_CLK;
	PIOA->PIO_PUER = PIN52_VOL_CS | PIN42_TONE_CS | PIN44_VOL_TONE_DATA | PIN41_VOL_TONE_CLK;
	PIOA->PIO_IFER = PIN52_VOL_CS | PIN42_TONE_CS | PIN44_VOL_TONE_DATA | PIN41_VOL_TONE_CLK;
	PIOA->PIO_CODR = PIN52_VOL_CS | PIN42_TONE_CS | PIN44_VOL_TONE_DATA | PIN41_VOL_TONE_CLK;

    // set pins for the volume rotary
    PIOA->PIO_PER    = PIN31_RTY_VOL_A | PIN32_RTY_VOL_B; // Pin Enable
    PIOA->PIO_ODR    = PIN31_RTY_VOL_A | PIN32_RTY_VOL_B; // Output Disable
    PIOA->PIO_PUER   = PIN31_RTY_VOL_A | PIN32_RTY_VOL_B; // Pull Up Enable
    PIOA->PIO_MDER   = PIN31_RTY_VOL_A | PIN32_RTY_VOL_B; // Multi-driver enable (? necessary?)
    PIOA->PIO_IFER   = PIN31_RTY_VOL_A | PIN32_RTY_VOL_B; // Glitch Input Filter Enable

    PIOA->PIO_IFSCER = PIN31_RTY_VOL_A | PIN32_RTY_VOL_B; // Debounce Filter
    PIOA->PIO_SODR   = PIN31_RTY_VOL_A | PIN32_RTY_VOL_B;

    pio_handler_set(PIOA, ID_PIOA, (PIN31_RTY_VOL_A | PIN32_RTY_VOL_B), PIO_DEFAULT, VOL_PioHandler);
}

static void pio_pt_config(void)
{
	PIOA->PIO_PER  = PIN23_PASS_IN | PIN13_PASS_LGT_CTL;
	PIOA->PIO_ODR  = PIN23_PASS_IN | PIN13_PASS_LGT_CTL;
	PIOA->PIO_PUER = PIN23_PASS_IN | PIN13_PASS_LGT_CTL;
	PIOA->PIO_MDER = PIN23_PASS_IN | PIN13_PASS_LGT_CTL;
	PIOA->PIO_IFER = PIN23_PASS_IN | PIN13_PASS_LGT_CTL;
	PIOA->PIO_SODR = PIN23_PASS_IN | PIN13_PASS_LGT_CTL;
}
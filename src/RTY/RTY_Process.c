//-----------------------------------------------------------------------------|
//	RTY_Process.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include "RTY_Process.h"
#include "UI_Common.h"
#include "UI_Control.h"
#include "SRC_Control.h"
#include "APP_Process.h"
#include "UI_Setup.h"
#include "NVM_Access.h"
#include "PWR_Control.h"

static rty_status_t __rty_input;

//===================================================================
// Private Function Prototypes
//===================================================================
static void rty_check_input(void);
static void rty_set_input(uint8_t out);
static void rty_clear_input(uint8_t out);

//===================================================================
// Public Functions
//===================================================================
void RTY_Init(void)
{
    __rty_input.byte = 0;
}

void RTY_InputPioHandler(uint32_t id, uint32_t mask)
{
    // set/clear values
    uint32_t status = PIOA->PIO_PDSR;

    if(PWR_GetSleepState() == YES)
        return;

    // input rotary
    if ((status & PIN25_RTY_INPUT_A) == 0)
        rty_set_input(C_RTY_QUAD_A);
    else
        rty_clear_input(C_RTY_QUAD_A);

    if ((status & PIN26_RTY_INPUT_B) == 0)
        rty_set_input(C_RTY_QUAD_B);
    else
        rty_clear_input(C_RTY_QUAD_B);

    rty_check_input();
}

//===================================================================
// Private Functions
//===================================================================
static void rty_check_input(void)
{
    if (__rty_input.nib.current.val != __rty_input.nib.previous.val)
    {
        switch (__rty_input.byte) {
            case 0x10: case 0x31: case 0x02: // CW - input up
                if(APP_GetSchedule() == E_SCHED_ACTIVE)
                    UI_Input(C_RTY_INPUT_UP);
                break;
            case 0x20: case 0x32: case 0x01: // CCW - input down
                if(APP_GetSchedule() == E_SCHED_ACTIVE)
                    UI_Input(C_RTY_INPUT_DOWN);
                break;
        }

        __rty_input.nib.previous.val = __rty_input.nib.current.val;
    }
}

static void rty_set_input(uint8_t out)
{
    if (out == C_RTY_QUAD_A)
        __rty_input.nib.current.bit.a = 1;
    else
        __rty_input.nib.current.bit.b = 1;
}

static void rty_clear_input(uint8_t out)
{
    if (out == C_RTY_QUAD_A)
        __rty_input.nib.current.bit.a = 0;
    else
        __rty_input.nib.current.bit.b = 0;
}

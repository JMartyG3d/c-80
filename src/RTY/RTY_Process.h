//-----------------------------------------------------------------------------|
//	RTY_Process.h
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __RTY_PROCESS_H__
#define __RTY_PROCESS_H__

#include "common.h"

#define C_RTY_QUAD_A 0
#define C_RTY_QUAD_B 1

typedef union {
    struct {
        union {
            struct {
                uint8_t a:1;
                uint8_t b:1;
                uint8_t spare:2;
            } bit;
            uint8_t val;
        } current;
        union {
            struct {
                uint8_t a:1;
                uint8_t b:1;
                uint8_t spare:2;
            } bit;
            uint8_t val;
        } previous;
    } nib;
    uint8_t byte;
} rty_status_t;

//===================================================================
// Public Function Prototypes
//===================================================================
void RTY_Init(void);
void RTY_InputPioHandler(uint32_t id, uint32_t mask);

#endif // __RTY_PROCESS_H__
//-----------------------------------------------------------------------------|
//	IOX_Control.h
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __IOX_CONTROL_H__
#define __IOX_CONTROL_H__

#include "Common.h"

typedef enum {
    E_Q0,
    E_Q1,
    E_Q2,
    E_Q3,
    E_Q4,
    E_Q5,
    E_Q6,
    E_Q7,
}Iox_Pin_Id_Type;

typedef enum {
    E_IOX_FIRST     = 0,
    E_IOX_DPORT     = 0,
    E_IOX_TRIG      = 1,
    E_IOX_SELECT    = 2,
    E_IOX_LED       = 3,    // ToDo:  Not used in C80
    E_IOX_OUTPUT    = 4,
    E_IOX_SRC1      = 5,    // ToDo:  Not used in C80
    E_IOX_SRC2      = 6,
    E_IOX_PHONO     = 7,
    E_IOX_LAST      = 7,
    E_IOX_NUM       = 8,
} Iox_Id_Type;

typedef struct {
    Iox_Id_Type     XID;
    Iox_Pin_Id_Type PID;
} Iox_Map_Type;

typedef struct {
    uint8_t BADDR;
    uint8_t SADDR;
    uint8_t DREG;
} Iox_Config_Reg_Type;

typedef struct {
    Iox_Config_Reg_Type C;
    union {
        uint8_t REG;
        struct {
            uint8_t Q0 : 1;
            uint8_t Q1 : 1;
            uint8_t Q2 : 1;
            uint8_t Q3 : 1;
            uint8_t Q4 : 1;
            uint8_t Q5 : 1;
            uint8_t Q6 : 1;
            uint8_t Q7 : 1;
        } BIT;
    } U;
} IOX_Type;

//===================================================================
// Public Function Prototypes
//===================================================================
void IOX_PowerUp(void);
void IOX_Standby(void);
void IOX_SleepEnter(void);
void IOX_SleepExit(void);

void IOX_PowerRelays(bool state);

void IOX_Dataport(Dport_Id_Type id, bool state);
void IOX_Trigger(Trig_Id_Type id, bool state);
void IOX_TriggerMain(bool state);

void IOX_Rs232Enable(bool state);
void IOX_Rs232Shutdown(bool state);

void IOX_UsbSwitchSel(Usb_Select_Type sel);
void IOX_UsbSwitchPwr(bool state);
void IOX_DaMcuReset(Da_Reset_Type state);

void IOX_BackgroundLed(bool state);
void IOX_Output(uint8_t out, bool state);
void IOX_OutputLedSet(Output_Type out, bool state);
bool IOX_OutputLedGet(Output_Type out);
void IOX_OutputsOff(void);

void IOX_TrigMeterlight(bool state);

void IOX_Hxd(bool state);
void IOX_Headphones(bool state);

void IOX_FixedOut(bool state);

void IOX_Tone(bool state);
void IOX_ToneLed(bool state);
void IOX_Mono(bool state);
void IOX_PhonoResistance(Phono_Res_Type res, bool mute);
void IOX_PhonoCapacitance(Phono_Cap_Type cap, bool mute);
bool IOX_PhonoDelay(void);
void IOX_InputSet(Src_Type src);
void IOX_InputClear(void);

void IOX_SetReg(Iox_Id_Type iox_id, uint8_t value);
void IOX_SetPin(Iox_Id_Type iox_id, Iox_Pin_Id_Type pin, bool state);
uint8_t IOX_GetReg(Iox_Id_Type iox_id);

#endif /* __IOX_CONTROL_H__ */

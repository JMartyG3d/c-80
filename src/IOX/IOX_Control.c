//-----------------------------------------------------------------------------|
//	I2C_Control.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include <string.h>
#include "IOX_Control.h"
#include "NVM_Access.h"
#include "PWR_Control.h"
#include "I2C_Control.h"
#include "DISP_Control.h"
#include "APP_Process.h"
#include "UI_Trim.h"
#include "UI_Setup.h"
#include "UI_Main.h"
#include "DA_Config.h"
#include "PT_Control.h"
#include "VOL_Control.h"
#include "TRIG_Control.h"
#include "OUT_Control.h"

/*   DPORT3 and DPORT4 have been removed from Address 000, in the C80 (relative to C49
static const Iox_Map_Type DPORT4	       = {E_IOX_DPORT, E_Q2};     // [0-ON,1-OFF]
static const Iox_Map_Type DPORT3           = {E_IOX_DPORT, E_Q3};     // [0-ON,1-OFF]  */
static const Iox_Map_Type DPORT2           = {E_IOX_DPORT, E_Q4};     // [0-ON,1-OFF]
static const Iox_Map_Type DPORT1  	       = {E_IOX_DPORT, E_Q5};     // [0-ON,1-OFF]

static const Iox_Map_Type TRIG2_PC         = {E_IOX_TRIG, E_Q2};      // [0-OFF,1-ON]
static const Iox_Map_Type TRIG1_PC         = {E_IOX_TRIG, E_Q3};      // [0-OFF,1-ON]
static const Iox_Map_Type TRIG_METERLIGHT  = {E_IOX_TRIG, E_Q4};      // [0-OFF,1-ON]
static const Iox_Map_Type TRIG_MAIN_PC     = {E_IOX_TRIG, E_Q7};      // [0-OFF,1-ON]


// Tube Lights have been added to C80, from C49
static const Iox_Map_Type BACKGROUND_LED   = {E_IOX_SELECT, E_Q0};     // [0-OFF,1-ON]
static const Iox_Map_Type RED_TUBE_LEDS    = {E_IOX_SELECT, E_Q1};     // [0-OFF,1-ON]
static const Iox_Map_Type GREEN_TUBE_LEDS  = {E_IOX_SELECT, E_Q2};     // [0-OFF,1-ON]
static const Iox_Map_Type PWR_RLY_MAIN     = {E_IOX_SELECT, E_Q7};     // [0-OFF,1-ON]
// *   PWR_RLY_VFD and PWR_RLY_VFD have been removed from Address 000, in the C80 (relative to C49
static const Iox_Map_Type PWR_RLY_VFD      = {E_IOX_SELECT, E_Q5};     // [0-ON,1-OFF]
static const Iox_Map_Type PWR_RLY_LV       = {E_IOX_SELECT, E_Q6};     // [0-ON,1-OFF] */


/*   The following FRONT PANEL LIGHTING at address 011, have been removed in the C80 (relative to C49
static const Iox_Map_Type TONE_LED         = {E_IOX_LED, E_Q0};     // [0-OFF,1-ON]
static const Iox_Map_Type BACKGROUND_LED   = {E_IOX_LED, E_Q4};     // [0-ON,1-OFF]
static const Iox_Map_Type OUTPUT2_LED      = {E_IOX_LED, E_Q6};     // [0-ON,1-OFF]
static const Iox_Map_Type OUTPUT1_LED      = {E_IOX_LED, E_Q7};     // [0-OFF,1-ON]   */

static const Iox_Map_Type FIXED_OUT        = {E_IOX_OUTPUT, E_Q0};     // [0-OFF,1-ON]  // MAIN OUT
static const Iox_Map_Type OUTPUT1          = {E_IOX_OUTPUT, E_Q1};     // [0-OFF,1-ON]
static const Iox_Map_Type OUTPUT2          = {E_IOX_OUTPUT, E_Q2};     // [0-OFF,1-ON]
static const Iox_Map_Type HEADPHONE_OUT    = {E_IOX_OUTPUT, E_Q3};     // [0-OFF,1-ON]
static const Iox_Map_Type SUB_OUT          = {E_IOX_OUTPUT, E_Q4};     // [0-OFF,1-ON]     // New in C80
static const Iox_Map_Type TONE             = {E_IOX_OUTPUT, E_Q5};     // [0-ON,1-OFF]
static const Iox_Map_Type HXD              = {E_IOX_OUTPUT, E_Q6};     // [0-ON,1-OFF]
static const Iox_Map_Type MONO             = {E_IOX_OUTPUT, E_Q7};     // [0-OFF,1-ON]


/*=-=-=-=-  BAL1 moved to SRC2, BAL2 NOT USED IN C80 =-=-=-=-=-
static const Iox_Map_Type BAL1             = {E_IOX_SRC1, E_Q1};      // [0-ON,1-OFF]
static const Iox_Map_Type BAL2             = {E_IOX_SRC1, E_Q2};      // [0-ON,1-OFF]
=-=-=-=-  NOT USED IN C80 =-=-=-=-=-*/

static const Iox_Map_Type BAL1             = {E_IOX_SRC2, E_Q0};      // [0-OFF,1-ON]   // New in C80
static const Iox_Map_Type UNBAL1           = {E_IOX_SRC2, E_Q1};      // [0-OFF,1-ON]
static const Iox_Map_Type UNBAL2           = {E_IOX_SRC2, E_Q2};      // [0-OFF,1-ON]
                                          // E_Q3 UNBAL3 removed in C80
static const Iox_Map_Type MM_SEL           = {E_IOX_SRC2, E_Q5};      // [0-OFF,1-ON]
static const Iox_Map_Type MC_SEL           = {E_IOX_SRC2, E_Q6};      // [0-OFF,1-ON]
static const Iox_Map_Type DAC              = {E_IOX_SRC2, E_Q7};      // [0-OFF,1-ON]

static const Iox_Map_Type MC_OHM1          = {E_IOX_PHONO, E_Q0};     // [0-OFF,1-ON]
static const Iox_Map_Type MC_OHM2          = {E_IOX_PHONO, E_Q1};     // [0-OFF,1-ON]
static const Iox_Map_Type MC_OHM3          = {E_IOX_PHONO, E_Q2};     // [0-OFF,1-ON]
//static const Iox_Map_Type MC_OHM4          = {E_IOX_PHONO, E_Q3};     // [0-OFF,1-ON]  // Removed in C80
static const Iox_Map_Type MM_PF1           = {E_IOX_PHONO, E_Q4};     // [0-OFF,1-ON]
static const Iox_Map_Type MM_PF2           = {E_IOX_PHONO, E_Q5};     // [0-ON,1-OFF]
static const Iox_Map_Type MM_PF3           = {E_IOX_PHONO, E_Q6};     // [0-ON,1-OFF]
//static const Iox_Map_Type MM_PF4           = {E_IOX_PHONO, E_Q7};     // [0-OFF,1-ON]  // Removed in C80

IOX_Type IOX[E_IOX_NUM] = {{0x38, 0x00, 0x00},
                           {0x38, 0x01, 0x00},
                           {0x38, 0x02, 0x00},
                           {0x38, 0x03, 0x00},   // ToDo:  Not used in C80, so perhaps we should remove these items?
                           {0x38, 0x04, 0x00},
                           {0x38, 0x05, 0x00},   // ToDo:  Not used in C80, Determine whether unused rows should be removed.
                           {0x38, 0x06, 0x00},
                           {0x38, 0x07, 0x00}};

static bool __phono_delay;
static bool __out1_led, __out2_led;

//==============================================================================
// Private Function Prototypes
//==============================================================================
static void iox_send(Iox_Config_Reg_Type *set, uint8_t data);
static void iox_set_pin(Iox_Map_Type pin, bool state);

//==============================================================================
// Public Functions
//==============================================================================
void IOX_PowerUp(void)
{
    IOX_PowerRelays(ON);
    delay_ms(20);
    IOX_OutputsOff();
    IOX_BackgroundLed(ON);
    IOX_FixedOut(ON);
    __phono_delay = FALSE;

    IOX_PhonoResistance(NVM_GetMcPhonoRes(), FALSE);
    IOX_PhonoCapacitance(NVM_GetMmPhonoCap(), FALSE);
}

void IOX_Standby(void)
{
//    IOX_OutputLedSet(E_OUTPUT_1, OFF);  Removed for C80
//    IOX_OutputLedSet(E_OUTPUT_2, OFF);  Removed for C80
    IOX_OutputsOff();
//    IOX_ToneLed(OFF);     Removed for C80
    IOX_BackgroundLed(OFF);
    delay_ms(20);
    IOX_PowerRelays(OFF);  // Turns Off Triggers
}

void IOX_SleepEnter(void)
{
    DPORT_Off();
    TRIG_Off();

//    IOX_OutputLedSet(E_OUTPUT_1, OFF);      Removed for C80
//    IOX_OutputLedSet(E_OUTPUT_2, OFF);      Removed for C80
    IOX_OutputsOff();
 //   IOX_UsbSwitchPwr(OFF);
 //   IOX_ToneLed(OFF);     Removed for C80
//    IOX_Rs232Enable(ON);              // RS232 stays on even in Sleep mode, ToDo: Look into posting banner then turning off
//    IOX_Rs232Shutdown(FALSE);
    IOX_BackgroundLed(OFF);
    iox_set_pin(PWR_RLY_VFD, OFF);  // ToDo: remove in C80
    iox_send(&IOX[PWR_RLY_MAIN.XID].C, IOX[PWR_RLY_MAIN.XID].U.REG);
}


void IOX_SleepExit(void)
{
    iox_set_pin(PWR_RLY_VFD, ON);  // ToDo: remove in C80
    iox_send(&IOX[PWR_RLY_MAIN.XID].C, IOX[PWR_RLY_MAIN.XID].U.REG);
    delay_ms(20);

    DISP_SpiInit(); // Initialize SPI for the display
    delay_ms(300);
    DISP_Initialize();

    TRIG_Config();
    IOX_TriggerMain(ON);

    DPORT_Config();

    OUT_Init();
    OUT_Warmup();
    IOX_BackgroundLed(ON);
//    IOX_ToneLed(NVM_GetTone(NVM_GetSrc()));       Removed for C80

    if(PT_Active()){
        if(NVM_GetMeterLightPT() == E_METER_PT_TRIG){
            IOX_TrigMeterlight(!(PIOA->PIO_PDSR & PIN13_PASS_LGT_CTL));
        }
        else if (NVM_GetMeterLightPT() == E_METER_PT_ON){
            IOX_TrigMeterlight(ON);
        }
        else{
            IOX_TrigMeterlight(OFF);
        }
    }


//    IOX_Rs232Enable(ON);          // RS232 stays on even in Sleep mode
//    IOX_Rs232Shutdown(FALSE);
    IOX_FixedOut(ON);

    UI_Main_Display();
}


void IOX_PowerRelays(bool state)
{
    iox_set_pin(PWR_RLY_LV, state);   // ToDo:  remove in C80
    iox_set_pin(PWR_RLY_VFD, state);  // ToDo:  remove in C80
    iox_set_pin(PWR_RLY_MAIN, state);
    iox_send(&IOX[PWR_RLY_MAIN.XID].C, IOX[PWR_RLY_MAIN.XID].U.REG);
}

// ----------------------------------------------------------------------------|
// DA -------------------------------------------------------------------------|
// ----------------------------------------------------------------------------|
void IOX_DaMcuReset(Da_Reset_Type state) {
    if(state)
        PIOA->PIO_SODR = PIN38_DA_RESET;
    else
        PIOA->PIO_CODR = PIN38_DA_RESET;
}

// ----------------------------------------------------------------------------|
// LED ------------------------------------------------------------------------|
// ----------------------------------------------------------------------------|
void IOX_BackgroundLed(bool state)
{
    iox_set_pin(BACKGROUND_LED, !state);
    iox_send(&IOX[BACKGROUND_LED.XID].C, IOX[BACKGROUND_LED.XID].U.REG);
}

//void IOX_ToneLed(bool state)  removed in C80
//{
//    iox_set_pin(TONE_LED, !state);  removed in C80
//    iox_send(&IOX[TONE_LED.XID].C, IOX[TONE_LED.XID].U.REG);
//}

void IOX_TrigMeterlight(bool state)
{
    iox_set_pin(TRIG_METERLIGHT, state);
    iox_send(&IOX[TRIG_METERLIGHT.XID].C, IOX[TRIG_METERLIGHT.XID].U.REG);
}

/*void IOX_OutputLedSet(Output_Type out, bool state)  removed in C80
{
    switch(out) {
        case E_OUTPUT_1: iox_set_pin(OUTPUT1_LED, !state);
                         __out1_led = state;
                         break;
        case E_OUTPUT_2: iox_set_pin(OUTPUT2_LED, !state);
                         __out2_led = state;
                         break;
        default: break;
    }
    iox_send(&IOX[E_IOX_LED].C, IOX[E_IOX_LED].U.REG);
} 

bool IOX_OutputLedGet(Output_Type out)
{
    if(out == E_OUTPUT_1)
        return __out1_led;
    else
        return __out2_led;
}
*/ 

// ----------------------------------------------------------------------------|
// HEADPHONES -----------------------------------------------------------------|
// ----------------------------------------------------------------------------|
void IOX_Hxd(bool state)
{
    iox_set_pin(HXD, state);
    iox_send(&IOX[HXD.XID].C, IOX[HXD.XID].U.REG);
}

void IOX_Headphones(bool state)
{
    iox_set_pin(HEADPHONE_OUT, state);
    iox_send(&IOX[HEADPHONE_OUT.XID].C, IOX[HEADPHONE_OUT.XID].U.REG);
}

// ----------------------------------------------------------------------------|
// PWR OUTPUT USB RELAYS IOX --------------------------------------------------|
// ----------------------------------------------------------------------------|
void IOX_Dataport(Dport_Id_Type id, bool state)
{
    switch(id) {
        case E_DPORT_ID1:    iox_set_pin(DPORT1, !state); break;
        case E_DPORT_ID2:    iox_set_pin(DPORT2, !state); break;
//        case E_DPORT_ID3:    iox_set_pin(DPORT3, !state); break;  removed in C80
//       case E_DPORT_ID4:    iox_set_pin(DPORT4, !state); break;  removed in C80
        case E_DPORT_ID_ALL: iox_set_pin(DPORT1, !state);
                             iox_set_pin(DPORT2, !state);
//                             iox_set_pin(DPORT3, !state);  removed in C80
//                             iox_set_pin(DPORT4, !state);  removed in C80
                             break;
        default: break;
    }
    iox_send(&IOX[E_IOX_DPORT].C, IOX[E_IOX_DPORT].U.REG);
}

void IOX_Trigger(Trig_Id_Type id, bool state)
{
    switch(id) {
        case E_TRIG_ID1:    iox_set_pin(TRIG1_PC, state); break;
        case E_TRIG_ID2:    iox_set_pin(TRIG2_PC, state); break;
        case E_TRIG_ID_NUM: iox_set_pin(TRIG1_PC, state);
                            iox_set_pin(TRIG2_PC, state); break;
        default: break;
    }
    iox_send(&IOX[E_IOX_TRIG].C, IOX[E_IOX_TRIG].U.REG);
}

// No User Control always ON/OFF
void IOX_TriggerMain(bool state)
{
    iox_set_pin(TRIG_MAIN_PC, state);
    iox_send(&IOX[E_IOX_TRIG].C, IOX[E_IOX_TRIG].U.REG);
}

void IOX_Output(uint8_t out, bool state)
{
    switch(out) {
        case E_OUTPUT_1: iox_set_pin(OUTPUT1, state); break;
        case E_OUTPUT_2: iox_set_pin(OUTPUT2, state); break;
        default: break;
    }
    iox_send(&IOX[E_IOX_OUTPUT].C, IOX[E_IOX_OUTPUT].U.REG);
}

void IOX_OutputsOff(void)
{
    IOX_Headphones(OFF);
//    iox_set_pin(FIXED_OUT, OFF_HIGH);
    iox_set_pin(OUTPUT1, OFF);
    iox_set_pin(OUTPUT2, OFF);
    iox_send(&IOX[E_IOX_OUTPUT].C, IOX[E_IOX_OUTPUT].U.REG);
}

void IOX_FixedOut(bool state)
{   // Always On?
    iox_set_pin(FIXED_OUT, state);
    iox_send(&IOX[FIXED_OUT.XID].C, IOX[FIXED_OUT.XID].U.REG);
}


void IOX_Tone(bool state)    // ToDo: should this be removed in C80?
{
    iox_set_pin(TONE, state);
    iox_send(&IOX[TONE.XID].C, IOX[TONE.XID].U.REG);
}

void IOX_Mono(bool state)
{
    iox_set_pin(MONO, state);
    iox_send(&IOX[MONO.XID].C, IOX[MONO.XID].U.REG);
}

void IOX_PhonoResistance(Phono_Res_Type res, bool mute)
{
    iox_set_pin(MC_OHM1, OFF);
    iox_set_pin(MC_OHM2, OFF);
    iox_set_pin(MC_OHM3, OFF);
//    iox_set_pin(MC_OHM4, OFF);  // removed in C80

    switch(res) {
        case E_PRES_25_OHMS:
            iox_set_pin(MC_OHM1, ON);
            iox_set_pin(MC_OHM2, ON);
            iox_set_pin(MC_OHM3, ON);
// removed in C80            iox_set_pin(MC_OHM4, ON);
            break;
//        case E_PRES_50_OHMS:  iox_set_pin(MC_OHM4, ON); break;
        case E_PRES_100_OHMS: iox_set_pin(MC_OHM3, ON); break;
        case E_PRES_200_OHMS: iox_set_pin(MC_OHM2, ON); break;
        case E_PRES_400_OHMS: iox_set_pin(MC_OHM1, ON); break;
        case E_PRES_1K_OHMS:
        default:
            break;
    }

    if(mute) {
        __phono_delay = TRUE;

        if(!VOL_GetMute())
            VOL_Mute();

        delay_ms(200);
    }

    iox_send(&IOX[E_IOX_PHONO].C, IOX[E_IOX_PHONO].U.REG);

    if(mute) {
        delay_ms(800);

        if(VOL_GetMute())
            VOL_UnMute(0);

        __phono_delay = FALSE;
    }
}

void IOX_PhonoCapacitance(Phono_Cap_Type cap, bool mute)
{
    iox_set_pin(MM_PF1, OFF_HIGH);
    iox_set_pin(MM_PF2, OFF_HIGH);
    iox_set_pin(MM_PF3, OFF_HIGH);
//    iox_set_pin(MM_PF4, OFF_HIGH);

    switch(cap) {
        case E_PCAP_50_PF:
            iox_set_pin(MM_PF1,ON_LOW);
            iox_set_pin(MM_PF2,ON_LOW);
            iox_set_pin(MM_PF3,ON_LOW);
//            iox_set_pin(MM_PF4,ON_LOW);
            break;
        case E_PCAP_100_PF:
            iox_set_pin(MM_PF2,ON_LOW);
            iox_set_pin(MM_PF3,ON_LOW);
//            iox_set_pin(MM_PF4,ON_LOW);
            break;
        case E_PCAP_150_PF:
            iox_set_pin(MM_PF1,ON_LOW);
            iox_set_pin(MM_PF3,ON_LOW);
//            iox_set_pin(MM_PF4,ON_LOW);
            break;
        case E_PCAP_200_PF: iox_set_pin(MM_PF3,ON_LOW); // iox_set_pin(MM_PF4,ON_LOW);  removed in C80
            break;
        case E_PCAP_250_PF:
            iox_set_pin(MM_PF1,ON_LOW);
            iox_set_pin(MM_PF2,ON_LOW);
//            iox_set_pin(MM_PF4,ON_LOW);
            break;
        case E_PCAP_300_PF: iox_set_pin(MM_PF2,ON_LOW); break;  // iox_set_pin(MM_PF4,ON_LOW); removed in C80
        case E_PCAP_350_PF: iox_set_pin(MM_PF1,ON_LOW); break;  // iox_set_pin(MM_PF4,ON_LOW); removed in C80
//        case E_PCAP_400_PF: iox_set_pin(MM_PF4,ON_LOW); break;
        case E_PCAP_450_PF:
            iox_set_pin(MM_PF1,ON_LOW);
            iox_set_pin(MM_PF2,ON_LOW);
            iox_set_pin(MM_PF3,ON_LOW);
            break;
        case E_PCAP_500_PF:
            iox_set_pin(MM_PF2,ON_LOW);
            iox_set_pin(MM_PF3,ON_LOW);
            break;
        case E_PCAP_550_PF: iox_set_pin(MM_PF1,ON_LOW); iox_set_pin(MM_PF3,ON_LOW); break;
        case E_PCAP_600_PF:                             iox_set_pin(MM_PF3,ON_LOW); break;
        case E_PCAP_650_PF: iox_set_pin(MM_PF1,ON_LOW); iox_set_pin(MM_PF2,ON_LOW); break;
        case E_PCAP_700_PF:                             iox_set_pin(MM_PF2,ON_LOW); break;
        case E_PCAP_750_PF: iox_set_pin(MM_PF1,ON_LOW); break;
        case E_PCAP_800_PF:
        default:
            break;
    }

    if(mute) {
        __phono_delay = TRUE;

        if(!VOL_GetMute())
            VOL_Mute();

        delay_ms(200);
    }

    iox_send(&IOX[E_IOX_PHONO].C, IOX[E_IOX_PHONO].U.REG);

    if(mute) {
        delay_ms(800);

        if(VOL_GetMute())
            VOL_UnMute(0);

        __phono_delay = FALSE;
    }
}

bool IOX_PhonoDelay(void)
{
    return __phono_delay;
}

void IOX_InputSet(Src_Type src)
{
    IOX_InputClear();

    switch(src) {
/*        case E_SRC_BAL1:     iox_set_pin(BAL1, ON);
                             iox_send(&IOX[E_IOX_SRC1].C, IOX[E_IOX_SRC1].U.REG); break;
        case E_SRC_BAL2:     iox_set_pin(BAL2, ON);
                             iox_send(&IOX[E_IOX_SRC1].C, IOX[E_IOX_SRC1].U.REG); break;
=-=-=-=-  NOT USED IN C80 =-=-=-=-=-*/
        case E_SRC_BAL1:     iox_set_pin(BAL1, ON);
                             iox_send(&IOX[E_IOX_SRC2].C, IOX[E_IOX_SRC2].U.REG); break;  // New in C80 from C49
        case E_SRC_UNBAL1:   iox_set_pin(UNBAL1, ON);
                             iox_send(&IOX[E_IOX_SRC2].C, IOX[E_IOX_SRC2].U.REG);   break;
        case E_SRC_UNBAL2:   iox_set_pin(UNBAL2, ON);
                             iox_send(&IOX[E_IOX_SRC2].C, IOX[E_IOX_SRC2].U.REG);   break;
//        case E_SRC_UNBAL3:   iox_set_pin(UNBAL3, ON);
//                             iox_send(&IOX[E_IOX_SRC2].C, IOX[E_IOX_SRC2].U.REG);   break;  // Removed in C80
        case E_SRC_MM_PHONO: iox_set_pin(MM_SEL, ON);
                             iox_send(&IOX[E_IOX_SRC2].C, IOX[E_IOX_SRC2].U.REG);   break;
        case E_SRC_MC_PHONO: iox_set_pin(MC_SEL, ON);
                             iox_send(&IOX[E_IOX_SRC2].C, IOX[E_IOX_SRC2].U.REG);   break;
        
        case E_SRC_COAX1:           // DA-2 Input Section
        case E_SRC_COAX2:
        case E_SRC_OPTI1:
        case E_SRC_OPTI2:
        case E_SRC_USB:
		case E_SRC_HDMI:
        case E_SRC_MCT:      iox_set_pin(DAC, ON);
                             iox_send(&IOX[E_IOX_SRC2].C, IOX[E_IOX_SRC2].U.REG);
                             break;
        default: break;
    }

    DA_SourceChange();
}

void IOX_InputClear(void)
{
/*=-=-=-=-  NOT USED IN C80 =-=-=-=-=-
    iox_set_pin(BAL1,   OFF);
    iox_set_pin(BAL2,   OFF);
=-=-=-=-  NOT USED IN C80 =-=-=-=-=-*/
    iox_set_pin(BAL1,   OFF);
    iox_set_pin(UNBAL1, OFF);
    iox_set_pin(UNBAL2, OFF);
//    iox_set_pin(UNBAL3, OFF);     // Removed in C80
    iox_set_pin(MC_SEL, OFF);
    iox_set_pin(MM_SEL, OFF);
    iox_set_pin(DAC,    OFF);
//    iox_send(&IOX[E_IOX_SRC1].C, IOX[E_IOX_SRC1].U.REG);      // Not used in C80, BAL1 moved to SRC2
    iox_send(&IOX[E_IOX_SRC2].C, IOX[E_IOX_SRC2].U.REG);
}

void IOX_SetReg(Iox_Id_Type iox_id, uint8_t value)
{
    IOX[iox_id].U.REG = value;
    iox_send(&IOX[iox_id].C, IOX[iox_id].U.REG);
}

void IOX_SetPin(Iox_Id_Type iox_id, Iox_Pin_Id_Type pin, bool state)
{
    Iox_Map_Type map = {iox_id, pin};

    iox_set_pin(map, state);
    iox_send(&IOX[iox_id].C, IOX[iox_id].U.REG);
}

uint8_t IOX_GetReg(Iox_Id_Type iox_id)
{
    return IOX[iox_id].U.REG;
}

//===================================================================
// Private Functions
//===================================================================
static void iox_send(Iox_Config_Reg_Type *set, uint8_t data)
{
    I2C_Tx(TWI0,set->BADDR,set->SADDR,0,0,1,&data);
}

static void iox_set_pin(Iox_Map_Type pin, bool state) {

    switch(pin.PID) {
        case E_Q0: IOX[pin.XID].U.BIT.Q0 = state; break;
        case E_Q1: IOX[pin.XID].U.BIT.Q1 = state; break;
        case E_Q2: IOX[pin.XID].U.BIT.Q2 = state; break;
        case E_Q3: IOX[pin.XID].U.BIT.Q3 = state; break;
        case E_Q4: IOX[pin.XID].U.BIT.Q4 = state; break;
        case E_Q5: IOX[pin.XID].U.BIT.Q5 = state; break;
        case E_Q6: IOX[pin.XID].U.BIT.Q6 = state; break;
        case E_Q7: IOX[pin.XID].U.BIT.Q7 = state; break;
        default: break;
    }
}

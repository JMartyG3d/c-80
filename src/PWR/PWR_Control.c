//-----------------------------------------------------------------------------|
//	PWR_Control.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include <string.h>
#include "PWR_Control.h"
#include "IOX_Control.h"
#include "VOL_Control.h"
#include "OUT_Control.h"
#include "TMR_Control.h"
#include "DISP_Control.h"
#include "APP_Process.h"
#include "RTY_Process.h"
#include "ADC_Process.h"
#include "TRIG_Control.h"
#include "NVM_Access.h"
#include "NVM_Control.h"
#include "PIO_Config.h"
#include "UI_Control.h"
#include "DA_Config.h"
#include "PT_Control.h"
#include "I2C_Control.h"
#include "KEY_Process.h"
#include "IR_Control.h"
#include "PRODUCT_Config.h"
#include "RS232_Control.h"
#include "RS232_Commands.h"
#include "RS232_Tx.h"
#include "UI_Function.h"

static uint32_t __pwr_ac_loss_tmr;      // time since A/C was last detected
static uint32_t __pwr_audio_detected;   // counts up when audio signal not detected
static bool     __pwr_command;          // true if a power on/off signal recv'd
static bool     __pwr_sleeping;

//==============================================================================
// Private Function Prototypes
//==============================================================================
static void pwr_ac_loss_detect(void);
static void pwr_audio_detect(void);

//==============================================================================
// Public Function Definitions
//==============================================================================
// Power command was recieved (from button, ir or rs232)
void PWR_Command(void)
{
    if(!PT_Active())
        __pwr_command = TRUE;
}

// Check power command, auto-off, ac detect and audio detect
void PWR_Periodic(void)
{
    pwr_ac_loss_detect();    // handle ac detect

    // handle audio detect
    if(APP_GetSchedule() == E_SCHED_ACTIVE)
        pwr_audio_detect();

    if(__pwr_command) {
        switch (APP_GetSchedule()) {
            case E_SCHED_INIT:     // initiate power down
            case E_SCHED_ACTIVE:
            case E_SCHED_WARMUP:
                APP_SetSchedule(E_SCHED_POWERDOWN);
                break;
            case E_SCHED_INACTIVE: // initiate power up
                APP_SetSchedule(E_SCHED_POWERUP);
                break;
            case E_SCHED_POWERDOWN:
            case E_SCHED_POWERUP:
            default:
                break;
        }
        __pwr_command = FALSE;
    }
}

void PWR_Initialize(void)
{
    sysclk_init();  // Configures system clocks according to settings in conf_clock.h
    board_init();   // init.c
    PIO_EnablePeriphClocks(); // ensure peripheral clocks are enabled
    rstc_set_external_reset(RSTC, 0x7); // set external reset length to maximum
    I2C_Config();   // configure the I2C bus
    NVM_Startup();  // Configure and restore values from NVM
    RS232_Init();   // Start the RS232 drivers
    DISP_SpiInit(); // Initialize SPI for the display
    TMR_Init();     // Initialize Timer control
    IR_Initialize();
    UI_Init();      // Initialize the UI modules
    PT_Init();
    DPORT_Off();
    IOX_Standby();
    PRODUCT_Init();
    RS232_AcPwrStatus();
    __pwr_sleeping = FALSE;
    KEY_FirstPwrOn();

    APP_SetSchedule(E_SCHED_INACTIVE);
}

void PWR_Powerup(void)
{
    __pwr_command = FALSE;
    __pwr_audio_detected = 0;

    IR_Initialize();
    PT_ResetPtPinRead();
    IOX_TriggerMain(ON);
    PIO_EnablePeriphClocks();   // ensure peripheral clocks are enabled
    PIO_Restore();              // restore pins if necessary
    IOX_PowerUp();
    DA_Config(3,DA_HDWR_DA2);   // First attempt to config as DA1
    TRIG_Config();
    DISP_SpiInit(); // Initialize SPI for the display
    delay_ms(50);

    DPORT_Config();
    IOX_TrigMeterlight(NVM_GetTrigMeterlight());
    IOX_Mono(NVM_GetMono(NVM_GetSrc()));
    IOX_Tone(NVM_GetTone(NVM_GetSrc()));
//    IOX_ToneLed(NVM_GetTone(NVM_GetSrc()));     Removed for C80

    VOL_Initialize();           // Initialize volume module
    OUT_Init();
    VOL_GuardPowerup();
    ADC_Init();                 // initialize the ADC
    RTY_Init();                 // Initialize the rotary controls

    delay_ms(150);
    RS232_Query();
    DISP_Initialize();          // Initialize the display module
    UI_Init();                  // Initialize the UI modules

    DISP_PrintCenter(C_DISP_LINE1,"C80\0");

    TMR_Reset(E_TMR_AUTOOFF); // Reset Auto-off timer

    if(PT_Active())
        PT_SaveState();
}

void PWR_Powerdown(void)
{
    RS232_TxPwr(OFF);
    NVM_Shutdown();     // save NVM
    DPORT_Off();
    TRIG_Off();
    PT_SetActive(OFF);
    DA_Sleep();
    VOL_Shutdown(); // shutdown the volume (and set to 0)
    DISP_ClearLine(C_DISP_BOTHLINES);
    spi_disable(SPI);

    // Disable Non-critical peripheral clocks to reduce standby power consumption
    pmc_disable_periph_clk(ID_PIOB);
    pmc_disable_periph_clk(ID_SPI);
    pmc_disable_periph_clk(ID_ADC);

    PIO_Shutdown();
    IOX_Standby();

    APP_SetSchedule(E_SCHED_INACTIVE);
}

void PWR_ResetAutoOff(void)
{
    __pwr_audio_detected = 0;
}

void PWR_CheckFwDownload(void)
{
    char displayString[21];   // was 17 and would overflow for "Old Version..."
    displayString[20] = 0;    // Tie a stop knot in the bitter end
	char* pDefaultOldVersion = "1.0X";

    char* pOldVersionString = NVM_GetFwPrevVersion();
    char  majVNumChar = pOldVersionString[0];
    bool bIsUnknownOldVersion = ( '0' >= majVNumChar || '9' < majVNumChar);
	
    if(TRUE == bIsUnknownOldVersion) 
        pOldVersionString = pDefaultOldVersion;

    if(NVM_GetMcuFwDownload() || bIsUnknownOldVersion) {
        DISP_PrintCenter(C_DISP_LINE1,"C80 FIRMWARE\0");
        DISP_PrintCenter(C_DISP_LINE2,"Download Completed\0");
        delay_s(2);

        snprintf(displayString, 21, "Old Version: V%s ", pOldVersionString);
        DISP_PrintLine(C_DISP_LINE1, displayString);
        snprintf(displayString, 21, "New Version: V%s ", PRODUCT_Version());
        DISP_PrintLine(C_DISP_LINE2, displayString);
        delay_s(4);
            
        if(NVM_GetForceFactoryDefault() || bIsUnknownOldVersion){
            DISP_PrintCenter(C_DISP_LINE1,"FACTORY RESET\0");
        }
        else{
            DISP_PrintCenter(C_DISP_LINE1,"FACTORY RESET?\0");
            DISP_PrintCenter(C_DISP_LINE2,"(PRESS INPUT)\0");
        }

        if(NVM_GetForceFactoryDefault() || bIsUnknownOldVersion || UI_WaitforKeyPress()) {
            DISP_PrintCenter(C_DISP_LINE2,"In Progress\0");
            delay_s(1);
            NVM_FactoryResetAll();
            delay_s(1);
            NVM_Startup();
            DISP_PrintCenter(C_DISP_LINE2,"Completed\0");
            delay_s(2);
        }
        DISP_ClearLine(C_DISP_BOTHLINES);

        NVM_SetMcuFwDownload(FALSE);
        NVM_SetFwPrevVersion(pOldVersionString);
    }

    if(NVM_GetDaFwDownload()) {
        DISP_PrintCenter(C_DISP_LINE1,"DA FIRMWARE\0");
        DISP_PrintCenter(C_DISP_LINE2,"Download Completed\0");
        delay_s(2);
        snprintf(displayString, 21, "Old Version: V%s ", pOldVersionString);
        DISP_PrintLine(C_DISP_LINE1, displayString);
        snprintf(displayString, 21, "New Version: V%u.0%u\0", DA_MajorVersion(),DA_MinorVersion());
        DISP_PrintLine(C_DISP_LINE2, displayString);
        delay_s(4);

        DISP_ClearLine(C_DISP_BOTHLINES);
        NVM_SetDaFwDownload(FALSE);
    }

    if(!PT_Active())
        DISP_PrintCenter(C_DISP_LINE1,"C80\0");
    else {
        DISP_ClearLine(C_DISP_BOTHLINES);
        DISP_PrintCenter(C_DISP_LINE1, "PASSTHRU\0");
    }
}

void PWR_SetSleepState(bool state)
{
    __pwr_sleeping = state;
}

bool PWR_GetSleepState(void)
{
    return __pwr_sleeping;
}

//==============================================================================
// Private Fuction Definitions
//==============================================================================
static void pwr_ac_loss_detect(void)
{
    // Check for AC
    if((PIOA->PIO_PDSR & PIN19_AC_DETECT)) // AC detected
        __pwr_ac_loss_tmr = 0;
    else
        __pwr_ac_loss_tmr++; // No AC detected

    // check to see if AC loss exceeds threshold
    if (__pwr_ac_loss_tmr >= C_PWR_AC_LOSS_TMR_REST) {

        // Take action based on current schedule
        if (APP_GetSchedule() != E_SCHED_INACTIVE)
            APP_SetSchedule(E_SCHED_POWERDOWN); // immediately transition to shutdown
    }
}

static void pwr_audio_detect(void)
{
    if((PIOA->PIO_PDSR & PIN37_AUDIO_DET) || PT_Active())
        __pwr_audio_detected = 0;
    else
        __pwr_audio_detected++;

    // if no audio detected in 30 mins and auto-off enabled, then transition to powerdown
    if ((__pwr_audio_detected >= C_PWR_AUTOOFF_TMR_RESET) && (NVM_GetPwrAutoOff() == ON)) {

        // Sleep if current input is HDMI
        if(NVM_GetSrc() == E_SRC_HDMI) {
            UI_Init();
            IOX_SleepEnter();
            PWR_SetSleepState(TRUE);
        }
        else
            APP_SetSchedule(E_SCHED_POWERDOWN);
    }
}

//-----------------------------------------------------------------------------|
//	PWR_Control.h
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __PWR_CONTROL_H__
#define __PWR_CONTROL_H__

#define C_PWR_AC_LOSS_TMR_REST  25      // 50ms (2ms period + 25)
#define C_PWR_AUTOOFF_TMR_RESET 5400000 // 30 minutes

//===================================================================
// Public Function Prototypes
//===================================================================
void PWR_Command(void);      // Power command was recieved (from button, ir or rs232)
void PWR_Periodic(void);     // Periodically check power command, ac detect and audio detect
void PWR_Initialize(void);   // AC power on
void PWR_Powerup(void);      // initial power on from AC on
void PWR_Powerdown(void);    // transition to low power mode
void PWR_ResetAutoOff(void); // reset auto-off timer
void PWR_CheckFwDownload(void);
void PWR_SetSleepState(bool state);
bool PWR_GetSleepState(void);

#endif // __PWR_CONTROL_H__
//-----------------------------------------------------------------------------|
//	PT_Control.h
//	Abstract: PassThru Port
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __PT_CONTROL_H__
#define __PT_CONTROL_H__

//===================================================================
// Public Function Prototypes
//===================================================================
void PT_Init(void);
void PT_Periodic(void);
bool PT_Active(void);
bool PT_PoweringOff(void);
void PT_PoweringOffClr(void);
void PT_SetActive(uint8_t state);
void PT_SaveState(void);
void PT_RestoreState(void);
void PT_ResetPtPinRead(void);

#endif // __PT_CONTROL_H__

//-----------------------------------------------------------------------------|
//	PT_Control.c
//	Abstract: PassThru Port
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include "NVM_Access.h"
#include "PT_Control.h"
#include "VOL_Control.h"
#include "SRC_Control.h"
#include "PWR_Control.h"
#include "I2C_Control.h"
#include "TMR_Control.h"
#include "UI_Control.h"
#include "UI_Function.h"
#include "UI_Passthru.h"
#include "UI_Setup.h"
#include "UI_Trim.h"
#include "TRIG_Control.h"
#include "IOX_Control.h"
#include "APP_Process.h"
#include "OUT_Control.h"
#include "DISP_Control.h"

static bool     __pt_initdone, __pt_pinread, __pt_active;
static bool     __pt_poweroff; // records state of power prior to passthru being activated
static vuint8_t __pt_volsave;  // Vol restore value when exiting Passthru
static vint8_t  __pt_balsave;  // Bal restore value when exiting Passthru
static Src_Type __pt_srcsave;  // Src restore value when exiting Passthru
static bool     __pt_hp_out1, __pt_hp_out2;

//===================================================================
// Public Functions
//===================================================================
void PT_Init(void)
{
    __pt_volsave = __pt_balsave = 0;
    __pt_srcsave = E_SRC_PT_OFF;
    __pt_active = __pt_pinread = FALSE;
    __pt_initdone = TRUE;
    __pt_hp_out1 = __pt_hp_out2 = FALSE;
}

void PT_Periodic(void)
{
    if(UI_GetMenu() == E_MENU_SETUP)
        return;

    if(NVM_GetPtSrc() == E_SRC_PT_OFF) {
        if(__pt_active)
            UI_Passthru_Exit();
        return;
    }

    if((APP_GetSchedule() == E_SCHED_POWERUP) || !__pt_initdone)
        return;

    if((REG_PIOA_PDSR & PIN23_PASS_IN)) {
        if(__pt_active) {
            if(__pt_pinread) {
                UI_Passthru_Exit();

                // if indicated, go to standby
                if (__pt_poweroff) {
                    __pt_poweroff = FALSE;
                    if((NVM_GetSrc() == E_SRC_HDMI) && (NVM_GetHdmiCecPwr() == ON)){
                        UI_Init();
                        IOX_SleepEnter();
                        PWR_SetSleepState(TRUE);
                    }
                    else{
                        APP_SetSchedule(E_SCHED_POWERDOWN);
                    }
                }
            }
            else
                __pt_pinread = TRUE;
        }
    }
    else {
        if(APP_GetSchedule() == E_SCHED_ACTIVE) {
            if(!__pt_active) {
                if(!__pt_pinread) {
                    __pt_poweroff = FALSE;
                    if(PWR_GetSleepState()){
                        IOX_SleepExit();
                        PWR_SetSleepState(FALSE);
                        __pt_poweroff = TRUE;
                    }

                    PT_SaveState();
                }
            }
        }
        else if(!__pt_pinread) {
            __pt_active = __pt_poweroff = TRUE;
            APP_SetSchedule(E_SCHED_POWERUP);
        }
    }

    __pt_pinread = PIOA->PIO_PDSR & PIN23_PASS_IN ? 0 : 1;

    if(NVM_GetMeterLightPT() == E_METER_PT_TRIG){
        if((APP_GetSchedule() == E_SCHED_ACTIVE) && __pt_active) {
            IOX_TrigMeterlight(!(PIOA->PIO_PDSR & PIN13_PASS_LGT_CTL));
        }
    }
}

bool PT_Active(void)
{
    return __pt_active;
}

bool PT_PoweringOff(void)
{
    return __pt_poweroff;
}

void PT_PoweringOffClr(void)
{
    __pt_poweroff = FALSE;
}

void PT_SetActive(uint8_t state)
{
    __pt_active = state;

    if ((state == OFF) && (UI_GetMenu() == E_MENU_PASSTHRU))
        UI_Passthru_Exit();
}

void PT_ResetPtPinRead(void)
{
    __pt_pinread = FALSE;
}

void PT_SaveState(void)
{
    // Exit gracefully to Main Menu
	if(UI_GetMenu() == E_MENU_SETUP)
        UI_SetupExit();

	if(UI_GetMenu() == E_MENU_TRIM)
        UI_Trim_Exit();

	__pt_srcsave = NVM_GetSrc();
	__pt_volsave = pNVM_GetVol();
	__pt_balsave = NVM_GetBalance();
	pNVM_SetVol(C_VOL_UNITY, YES);
	NVM_SetBalance(0);
    NVM_SetSrc(NVM_GetPtSrc());
    DISP_SetDutyCycle((Disp_Duty_Type)(NVM_GetBrightnessPT()));

    __pt_active = TRUE;

    UI_SetMenu(E_MENU_PASSTHRU);    // Abort any current vol Ramp

	SRC_Set(NVM_GetPtSrc());

    if(OUT_Headphone() == E_HEADPHONE_IN) {
        __pt_hp_out1 = NVM_GetPtOutput(E_OUTPUT_1);//IOX_OutputLedGet(E_OUTPUT_1);  ToDo: Save state of outputs selection for Headphones since no more OUTPUT LEDs in C80
        __pt_hp_out2 = NVM_GetPtOutput(E_OUTPUT_2);//IOX_OutputLedGet(E_OUTPUT_2);  ToDo: Save state of outputs selection for Headphones since no more OUTPUT LEDs in C80
    }

    if(APP_GetSchedule() == E_SCHED_POWERUP)
        delay_ms(2000); // pop noise at power up
    IOX_Output(E_OUTPUT_1, NVM_GetPtOutput(E_OUTPUT_1));
//    IOX_OutputLedSet(E_OUTPUT_1, NVM_GetPtOutput(E_OUTPUT_1));  ToDo:  Removed in C80

    IOX_Output(E_OUTPUT_2, NVM_GetPtOutput(E_OUTPUT_2));
//    IOX_OutputLedSet(E_OUTPUT_2, NVM_GetPtOutput(E_OUTPUT_2));  ToDo:  Removed in C80

    IOX_Tone(OFF);
//    IOX_ToneLed(OFF);       Removed for C80
}

void PT_RestoreState(void)
{
	__pt_active = FALSE;
    NVM_SetSrc(__pt_srcsave);
	pNVM_SetVol(__pt_volsave, YES);
	NVM_SetBalance(__pt_balsave);
    DISP_SetDutyCycle((Disp_Duty_Type)(NVM_GetBrightness()));

    if(!__pt_poweroff) {
        if(OUT_Headphone() == E_HEADPHONE_IN) {
            IOX_Output(E_OUTPUT_1, __pt_hp_out1);
//            IOX_OutputLedSet(E_OUTPUT_1, __pt_hp_out1);  Removed for C80

            IOX_Output(E_OUTPUT_2, __pt_hp_out2);
//            IOX_OutputLedSet(E_OUTPUT_2, __pt_hp_out2);  Removed for C80
        }
        else {
            IOX_Output(E_OUTPUT_1, NVM_GetOutput(E_OUTPUT_1));
//            IOX_OutputLedSet(E_OUTPUT_1, NVM_GetOutput(E_OUTPUT_1));    Removed for C80

            IOX_Output(E_OUTPUT_2, NVM_GetOutput(E_OUTPUT_2));
//            IOX_OutputLedSet(E_OUTPUT_2, NVM_GetOutput(E_OUTPUT_2));    Removed for C80
        }

        IOX_Tone(NVM_GetTone(NVM_GetSrc()));
//        IOX_ToneLed(NVM_GetTone(NVM_GetSrc()));           Removed for C80
        IOX_TrigMeterlight(NVM_GetTrigMeterlight());
    }

	SRC_Set(NVM_GetSrc());

    __pt_hp_out1 = __pt_hp_out2 = FALSE;
}

//-----------------------------------------------------------------------------|
//	Main.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include "APP_Process.h"

int main(void);

int main(void)
{
    // BOOT
        //! todo - integrate a "boot" section
        //! some of the powerup items will be moved from app process to boot

    // Branch to main, function does not return
    APP_main();

    for(;;);
}

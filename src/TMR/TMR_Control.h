//-----------------------------------------------------------------------------|
//	TMR_Control.h
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __TMR_CONTROL_H__
#define __TMR_CONTROL_H__

#define C_RC_500US		0xBB

// TC0 increments timer every 500us (0.5millseconds)
#define C_TMR_1MS       2       //    2 * 500us = 1ms
#define C_TMR_2MS		4       //    4 * 500us = 2ms
#define C_TMR_20MS		40      //   40 * 500us = 20ms
#define C_TMR_100MS     200     //  200 * 500us = 100ms
#define C_TMR_200MS     400     //  400 * 500us = 200ms
#define C_TMR_300MS     600     //  400 * 500us = 200ms
#define C_TMR_1S        2000    // 2000 * 500us = 1s
#define C_TMR_30MIN     (uint32_t)3600000000 // fits in uint32_t

typedef enum {
    E_TMR_WARMUP = 0,           // 7 seconds
    E_TMR_SRCMAKE,
    E_TMR_SRCDELAY,
    E_TMR_UNMUTE,
    E_TMR_2MS,
    E_TMR_20MS,
    E_TMR_100MS,
    E_TMR_300MS,
    // insert above
    E_TMR_AUTOOFF,              // 30 minutes
    E_TMR_LAST
} TMR_T;

//===================================================================
// Public Function Prototypes
//===================================================================
void TMR_Init(void);
void TMR_Reset(TMR_T timer);
bool TMR_Timeout(TMR_T timer);

#endif // __TMR_CONTROL_H__

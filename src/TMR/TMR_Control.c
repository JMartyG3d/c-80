//-----------------------------------------------------------------------------|
//	TMR_Control.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include <limits.h>
#include "TMR_Control.h"

/* __timers and __TIMEOUTS are indexed by TMR_T */
static const uint32_t __TIMEOUTS[E_TMR_LAST] = {
    C_TMR_1S,           // E_TMR_WARMUP
    (128*C_TMR_1MS),    // E_TMR_SRCMAKE
    C_TMR_100MS,        // E_TMR_SRCDELAY
    C_TMR_2MS,          // E_TMR_UNMUTE
    C_TMR_2MS,          // E_TMR_2MS
    C_TMR_20MS,         // E_TMR_20MS
    C_TMR_100MS,        // E_TMR_100MS
    C_TMR_300MS,
    C_TMR_30MIN,        // E_TMR_AUTOOFF
};

static uint32_t __timers[E_TMR_LAST];
static vuint32_t __timer;
//==============================================================================
// Private Function Prototypes
//==============================================================================
static uint32_t tmr_calc_dif(uint32_t timeout);

//==============================================================================
// Public Functions
//==============================================================================
void TC0_Handler(void) // fires off every 500us (0.5ms)
{
    __timer++;
    if (__timer == 0) // 0 is reserved for inactive timers
        __timer = 1;

    tc_get_status(TC0, 0);
}

void TMR_Init(void)
{
    pmc_enable_periph_clk(ID_TC0);
	TC0->TC_CHANNEL[0].TC_IER = TC_IER_CPCS;
	TC0->TC_CHANNEL[0].TC_RC  = C_RC_500US;		// Set RC compare to 500US
	TC0->TC_CHANNEL[0].TC_CMR = TC_CMR_TCCLKS_TIMER_CLOCK4 | TC_CMR_CPCTRG;
	TC0->TC_CHANNEL[0].TC_CCR = TC_CCR_CLKEN| TC_CCR_SWTRG;

	/* Setup System Interrupt Mode and Vector with Priority 2 and Enable it */
	NVIC_DisableIRQ((IRQn_Type)TC0_IRQn);
	NVIC_ClearPendingIRQ((IRQn_Type)TC0_IRQn);
	NVIC_SetPriority((IRQn_Type)TC0_IRQn, 1);
	NVIC_EnableIRQ((IRQn_Type)TC0_IRQn);

    // reset timer value to 0
    __timer = 0;
}

void TMR_Reset(TMR_T timer)
{
    // set the selected timer to the current __timer value
    __timers[timer] = __timer;
}

bool TMR_Timeout(TMR_T timer)
{
    // determine queried timer has timed out
    uint32_t dif = tmr_calc_dif(__timers[timer]);
    bool rc = (dif >= __TIMEOUTS[timer]) ? TRUE : FALSE;

    // reset timer for next use
    if (rc == TRUE)
        __timers[timer] = __timer;

    return rc;
}

//==============================================================================
// Private Function Prototypes
//==============================================================================
static uint32_t tmr_calc_dif(uint32_t timeout)
{
    uint32_t rc;
    if (__timer > timeout)
        rc = __timer - timeout;
    else // timer wrap
        rc = (UINT_MAX - timeout) + __timer;

    return rc;
}
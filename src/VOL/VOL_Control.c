//-----------------------------------------------------------------------------|
//	VOL_Control.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include <math.h>
#include "VOL_Control.h"
#include "NVM_Access.h"
#include "TMR_Control.h"
#include "DISP_Control.h"
#include "UI_Common.h"
#include "UI_Control.h"
#include "SRC_Control.h"
#include "APP_Process.h"
#include "PT_Control.h"
#include "IOX_Control.h"
#include "DA_ProcessTx.h"
#include "PWR_Control.h"

// converts vol% to step
static const uint8_t VolPcntToStepTable[101] =
    // X0  X1   X2   X3   X4   X5   X6   X7   X8   X9
    {  0,  15,  23,  31,  39,  47,  55,  59,  63,  67, //  X%
      71,  75,  79,  83,  87,  91,  95,  97,  99, 101, // 1X%
     103, 105, 107, 109, 111, 113, 115, 117, 119, 121, // 2X%
     123, 125, 127, 129, 131, 133, 135, 137, 139, 141, // 3X%
     143, 145, 147, 149, 151, 153, 155, 157, 159, 161, // 4X%
     163, 165, 166, 167, 168, 169, 170, 171, 172, 173, // 5X%
     174, 175, 176, 177, 178, 179, 180, 181, 182, 183, // 6X%
     184, 185, 186, 187, 188, 189, 190, 191, 192, 193, // 7X%
     194, 195, 196, 197, 198, 199, 200, 201, 202, 203, // 8X%
     204, 205, 206, 207, 208, 209, 210, 211, 212, 213, // 9X%
     214}; // 100%

static vol_rty_status_t __vol_rty;
static vol_reg_t __vol_reg;
static int32_t __vol_accum;       // Current volume accumulation
static bool __vol_wakeup, __vol_wakeup_disp;         // wake-up flag
static bool __vol_abort;	      // Halt an in-progress volume ramp up
static bool __vol_unmute_req;     // volume unmute requested
static bool __vol_mute;           // volume muted
static uint16_t __vol_unmute_spd; // Un-mute speed, lower is faster
static uint16_t __vol_pass_cnt;   //  Unmute pass counter
static uint8_t  __volume;          // actual volume, may differ from displayed volume

//===================================================================
// Private Function Prototypes
//===================================================================
static void vol_set_irq(uint8_t state);
static void vol_set_rty_phase(void);
static uint8_t vol_level(uint8_t volpos);

//===================================================================
// Public Functions
//===================================================================
void VOL_Initialize(void)
{
    VOL_Tx(C_VOL_LR_ATTN_MIN, C_VOL_LEFT_ATTN, E_VOL_TX);
    VOL_Tx(C_VOL_LR_GAIN_MIN, C_VOL_LEFT_GAIN, E_VOL_TX);
    VOL_Tx(C_VOL_LR_ATTN_MIN, C_VOL_RGHT_ATTN, E_VOL_TX);
    VOL_Tx(C_VOL_LR_GAIN_MIN, C_VOL_RGHT_GAIN, E_VOL_TX);
    VOL_Tx(C_VOL_ZERO_CROSS, C_VOL_CTRL, E_VOL_TX);

    __vol_rty.byte = 0;
    __vol_abort = __vol_mute = FALSE;
}

void VOL_GuardPowerup(void)
{
    if(pNVM_GetVol() > C_VOL_GUARD)
        pNVM_SetVol(C_VOL_GUARD, NO);
}

void VOL_Shutdown(void)
{
    vol_set_irq(OFF); // disable the irq
    VOL_ChangeVolume(FALSE,TRUE,0); // set internal volume to 0
}

void VOL_PioHandler(uint32_t id, uint32_t mask)
{
    static bool prev_up;
	
    if(PWR_GetSleepState() == YES)
        return;

    vol_set_rty_phase();

    // if phases differ, a rotary has changed
    if (__vol_rty.nibs.high != __vol_rty.nibs.low) {
        switch (__vol_rty.byte) {
            case 0x01: // UP
                // Fix quick reverse direction at end of turn
                if(!prev_up) {
                    prev_up = TRUE;
                    break;
                }

                if(UI_GetMenu() == E_MENU_MAIN) {
                    VOL_Accum(1);
                    UI_Input(C_RTY_VOLUME_UP);
                } else if((UI_GetMenu() != E_MENU_MAIN) && !IOX_PhonoDelay()){
                    UI_Input(C_RTY_VOLUME_UP);
                }
                break;
            case 0x02: // DOWN
                // Fix quick reverse direction at end of turn
                if(prev_up) {
                    prev_up = FALSE;
                    break;
                }

                if(UI_GetMenu() == E_MENU_MAIN) {
                    VOL_Accum(-1);
                    UI_Input(C_RTY_VOLUME_DOWN);
                } else if((UI_GetMenu() != E_MENU_MAIN) && !IOX_PhonoDelay()){
                    UI_Input(C_RTY_VOLUME_DOWN);
                }
                break;
            default:
                break;
        }
        __vol_rty.nibs.high = __vol_rty.nibs.low; // store current -> previous
    }
}

void VOL_Accum(int32_t val)
{
    uint32_t volume = pNVM_GetVol();

    if(__vol_mute) { // unmute on volume change
        __vol_mute = FALSE;
        VOL_UnMute(0);
    }

    // if wakeup, set abort
    if (__vol_wakeup && !__vol_abort)
        __vol_abort = TRUE;
    else {
        if (val > 0) {
            if ((volume + val) <= 100) // volume max in this context is 100%
                __vol_accum += val;
            else
                __vol_accum = (100-volume); // max
        }
        else {
            if (((int32_t)volume + val) >= 0) // volume min in this context is 0%
                __vol_accum += val;
            else
                __vol_accum = -((int32_t)volume); // min
        }
    }
}

// 2ms task - ACTIVE and WARMUP
void VOL_PeriodicRamp(void)
{
    // check if an unmute has been requested
    if(__vol_unmute_req) {
        // continue ramping volume if not aborted and current volume hasn't reached set volume
        if (!__vol_abort && (__volume < pNVM_GetVol())) {
            // check unmute speed
            if (__vol_unmute_spd == __vol_pass_cnt) {
                __vol_mute = FALSE;
                __vol_pass_cnt = 0;
                __volume++;

                // if wakeup, visually ramp volume
                if (__vol_wakeup) {
                    if(__vol_wakeup_disp) {
                        __vol_wakeup_disp = FALSE;
                        DISP_Src();
                    }
                    VOL_ChangeVolume(TRUE, TRUE, __volume);
                }
                else // just ramp volume
                    VOL_ChangeVolume(FALSE, TRUE, __volume);
            }
            else
                __vol_pass_cnt++;
        }
        else {
            __vol_unmute_req = FALSE;   // cancel unmute request

            // if wakeup, set to current volume
            if (__vol_wakeup)
                pNVM_SetVol(__volume, NO);

            // Special Case: Wakeup with Volume = 0%
            if((__volume == 0) && __vol_wakeup_disp) {
                __vol_wakeup_disp = __vol_mute = FALSE;
                DISP_Src();
            }

            __vol_wakeup = __vol_abort = __vol_mute = FALSE;
            VOL_ChangeVolume(FALSE, TRUE, pNVM_GetVol());
        }
    }
}

// 20ms task - ACTIVE
void VOL_PeriodicAccum(void)
{
    uint32_t volume = pNVM_GetVol();

    if(__vol_accum > 0 ) {
        if ((volume + __vol_accum) > 100) // 100% is max in this context
            VOL_Set(100);
        else
            VOL_Set(volume + (uint8_t)__vol_accum);
    }
    else if(__vol_accum < 0) {
        if((uint8_t)abs(__vol_accum) > volume)
            VOL_Set(0);
        else
            VOL_Set(volume - (uint8_t)abs(__vol_accum));
    }

    __vol_accum = 0;
}

void VOL_ChangeVolume(bool disp, bool send, uint8_t volpercent)
{
	uint8_t vollAttn,volrAttn,vollGain,volrGain;
	uint8_t volpos, posl, posr;
    uint8_t current_volume  = pNVM_GetVol();
    int8_t  current_balance = NVM_GetBalance();

    volpos = VolPcntToStepTable[volpercent]; // set the vol position based on the vol percent
    posl = posr = vol_level(volpos);         // set left and right channel based on trim level

    // Adjust volume for Balance if necessary
    if (current_balance != 0) {
        if(current_balance > 0)
            posl = ((2*abs(current_balance)) > posl) ? 0 : (posl - (2*abs(current_balance)));
        else
            posr = ((2*abs(current_balance)) > posr) ? 0 : (posr - (2*abs(current_balance)));
    }

    // Check for max volume
	if((posr > C_VOL_MAX) || (posl > C_VOL_MAX))
		posr = posl = C_VOL_MAX;
    else { // if vol is clamped, don't change vol ic, allow it to update display if necessary
        // Calculate Vol Control Reg Data
        if(volpos == 0) {
            volrAttn = vollAttn = 0xFF;
            volrGain = vollGain = 0x00;
        }
        else {
            if(posr < 197) { // up to zero dB
                volrAttn = (196 - posr) + 16;
                volrGain = 0;
            }
            else {
                volrAttn = 16;
                volrGain = posr - 196;
            }
            if(posl < 197) { // up to zero dB
                vollAttn = (196 - posl) + 16;
                vollGain = 0;
            }
            else {
                vollAttn = 16;
                vollGain = posl - 196;
            }
        }

        if(send) {
            VOL_Tx(vollAttn, C_VOL_LEFT_ATTN, E_VOL_TX);
            VOL_Tx(vollGain, C_VOL_LEFT_GAIN, E_VOL_TX);
            VOL_Tx(volrAttn, C_VOL_RGHT_ATTN, E_VOL_TX);
            VOL_Tx(volrGain, C_VOL_RGHT_GAIN, E_VOL_TX);
        }
    }

	if(disp && !PT_Active())
        DISP_Vol();
}

void VOL_Set(uint8_t newvolume)
{
    uint32_t volume =  pNVM_GetVol();

    // unmute on volume change
    if(__vol_mute) {
        __vol_mute = FALSE;
        VOL_UnMute(0);
    }

    // deceasing volume
	if(newvolume < volume) {
		__volume = volume;
		do {
			__volume--;
            pNVM_SetVol(__volume, YES);
		    VOL_ChangeVolume(TRUE,TRUE,__volume);
		}while(__volume != newvolume);
        DA_TxVolume(volume-1);
	}    // increasing volume
	else if((newvolume > volume) || __vol_mute) {
		__volume = __vol_mute ? 0 : volume;
		do {
			__volume++;
            pNVM_SetVol(__volume, YES);
		    VOL_ChangeVolume(TRUE,TRUE,__volume);
		}while(__volume != newvolume);
        DA_TxVolume(volume+1);
	}
}

void VOL_UnMute(uint16_t time)
{
	__vol_abort = FALSE;
    __vol_unmute_req = TRUE;
    __vol_unmute_spd = time;
    __vol_pass_cnt = __volume = 0;
    TMR_Reset(E_TMR_AUTOOFF);
    TMR_Reset(E_TMR_UNMUTE); //! disable?
}

void VOL_Mute(void)
{
	__vol_mute = TRUE;
    TMR_Reset(E_TMR_AUTOOFF);
	VOL_ChangeVolume(FALSE,TRUE,0);
}

void VOL_WakeupRamp(void)
{
    __vol_wakeup = __vol_mute = __vol_wakeup_disp = TRUE;
	__volume = 0;

    vol_set_irq(ON); // enable the IRQ
    VOL_UnMute(10);  // start volume ramp
}

void VOL_SetAbort(void)
{
    __vol_abort = TRUE;
}

bool VOL_GetMute(void)
{
    return __vol_mute;
}

uint8_t VOL_Get(void)
{
    return __volume;
}

bool VOL_GetUnmuteReq(void)
{
    return __vol_unmute_req;
}

bool VOL_GetWakeup(void)
{
    return __vol_wakeup;
}

void VOL_Tx(uint8_t data, uint8_t addr, Vol_Tx_Type tx)
{
    switch(tx) {
        case E_VOL_TX:
            PIOA->PIO_CODR = PIN52_VOL_CS;
            break;
        case E_TONE_TX:
            PIOA->PIO_CODR = PIN42_TONE_CS;
            break;
        default:
            return;
    }

   	delay_us(2);

    __vol_reg.Field.Data = data;
    __vol_reg.Field.Addr = addr;

	for(uint8_t i = 16; i > 0; i--){
		PIOA->PIO_SODR = PIN41_VOL_TONE_CLK;

		if(__vol_reg.Reg & (0x0001 << i-1))
			PIOA->PIO_SODR = PIN44_VOL_TONE_DATA;
		else
			PIOA->PIO_CODR = PIN44_VOL_TONE_DATA;

      	delay_us(2);
		PIOA->PIO_CODR = PIN41_VOL_TONE_CLK;
      	delay_us(4);
	}

	PIOA->PIO_SODR = PIN52_VOL_CS | PIN42_TONE_CS;
  	delay_us(2);
}

//===================================================================
// Private Functions
//===================================================================
static void vol_set_irq(uint8_t state)
{
    if(state == ON) {
        vol_set_rty_phase(); // prime the phase, prevents false trigger on interrupt enable
        PIOA->PIO_IER = PIN31_RTY_VOL_A | PIN32_RTY_VOL_B; // Interrupt Enable
    }
    else
        PIOA->PIO_IDR = PIN31_RTY_VOL_A | PIN32_RTY_VOL_B; // Interrupt Enable
}

static void vol_set_rty_phase(void)
{
    uint32_t status = PIOA->PIO_PDSR;

    // set current phase values
    if ((status & PIN31_RTY_VOL_A) == 0)
        __vol_rty.bits.a0 = 1;
    else
        __vol_rty.bits.a0 = 0;

    if ((status & PIN32_RTY_VOL_B) == 0)
        __vol_rty.bits.b0 = 1;
    else
        __vol_rty.bits.b0 = 0;
}

static uint8_t vol_level(uint8_t volpos)
{
    uint8_t adjpos;
    int8_t  level = NVM_GetLevel();

    if (level >= 0)
        adjpos = volpos + level;
    else {
        if (abs(level) > volpos)
            adjpos = 0;
        else
            adjpos = volpos + level;
    }
    return adjpos;
}

//-----------------------------------------------------------------------------|
//	VOL_Control.h
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __VOL_CONTROL_H__
#define __VOL_CONTROL_H__

#include "Common.h"

#define C_VOL_GUARD     85  // 85%
#define C_VOL_UNITY     69  // 69% (184)
#define C_VOL_MAX       214 // 208

#define C_VOL_LEFT_ATTN   0x00
#define C_VOL_LEFT_GAIN   0x10
#define C_VOL_RGHT_ATTN   0x20
#define C_VOL_RGHT_GAIN   0x30
#define C_VOL_CTRL        0x40
#define C_VOL_LR_GAIN_MIN 0x00
#define C_VOL_LR_ATTN_MIN 0x00
#define C_VOL_ZERO_CROSS  0x20

//#define C_VOL_LRATT_SYNC  0x40
//#define C_VOL_LRGAIN_SYNC 0x80
//#define C_VOL_ATTN VOL_LEFT_ATTN
//#define C_VOL_GAIN VOL_LEFT_GAIN
//#define C_VOL_SYNC_ENABLED (VOL_ZERO_CROSS | VOL_LRATT_SYNC | VOL_LRGAIN_SYNC)

// spi packet
typedef union {
    struct {
        uint8_t Addr;
        uint8_t Data;
    } Field;
    uint16_t Reg;
} vol_reg_t;

typedef struct {
    uint8_t a0:1;
    uint8_t b0:1;
    uint8_t s0:2;
    uint8_t a1:1;
    uint8_t b1:1;
    uint8_t s1:2;
} vol_status_bits_t;

typedef struct {
    uint8_t low:4;
    uint8_t high:4;
} vol_status_nibs_t;

typedef union {
    vol_status_bits_t bits;
    vol_status_nibs_t nibs;
    uint8_t           byte;
} vol_rty_status_t;

//===================================================================
// Public Function Prototypes
//===================================================================
void VOL_Initialize(void);                                      // initialize volume module
void VOL_GuardPowerup(void);
void VOL_Shutdown(void);                                        // close the volume module
void VOL_PioHandler(uint32_t id, uint32_t mask);                // rotary control interrupt handler
void VOL_Accum(int32_t val);                                    // stores volume accumulation from irq
void VOL_PeriodicRamp(void);                                    // handles volume ramping after unmute
void VOL_PeriodicAccum(void);                                   // periodically handles volume accumulation
void VOL_Set(uint8_t newvolume);                                // sets new volume based on accumulation
void VOL_Mute(void);                                            // mutes the volume control
void VOL_UnMute(uint16_t time);                                 // unmutes the volume control
void VOL_ChangeVolume(bool disp, bool send, uint8_t volpercent);// change the volume to the desired percent
void VOL_WakeupRamp(void);                                      // initiate the volume ramp at the end of the warm up period
void VOL_SetAbort(void);
uint8_t VOL_Get(void);
bool VOL_GetMute(void);
bool VOL_GetUnmuteReq(void);
bool VOL_GetWakeup(void);
void VOL_Tx(uint8_t data, uint8_t addr, Vol_Tx_Type tx);


#endif // __VOL_CONTROL_H__

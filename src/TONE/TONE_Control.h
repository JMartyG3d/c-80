//-----------------------------------------------------------------------------|
//	TONE_Control.h
//	Abstract: 3-band Tone Control
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __TONE_CONTROL_H__
#define __TONE_CONTROL_H__

#define TONE_CHIP_ADDR          0x0C

#define TONE_TCBR_SEL_ADDR	    0x00 // Treble Cut/Boost Right Channel
#define	TONE_TCBL_SEL_ADDR	    0x30 // Treble Cut/Boost Left  Channel
#define TONE_MCBR_SEL_ADDR	    0x10 // Middle Cut/Boost Right Channel
#define TONE_MCBL_SEL_ADDR	    0x40 // Middle Cut/Boost Left  Channel

#define TONE_BOOST_MASK         0x80 // Boost Bit Mask
#define TONE_BYPASS_OFF_MASK    0x04 // Boost Bit Mask

#define TONE_MIN_GAIN           -12
#define TONE_MAX_GAIN            12

// D15  D14  D13  D12  D11  D10  D9  D8  D7  D6  D5  D4  D3  D2  D1  D0
// |--------------- DATA --------------|--Select Addr--|-- Chip Addr --|
// |                                   |               | 1   1   0   0 |

typedef enum {
    E_TONE_BOOST,
    E_TONE_CUT,
} Tone_Cut_Boost_Type;

//===================================================================
// Public Function Prototypes
//===================================================================
void TONE_Set(void);
bool TONE_TrebleAdjust(bool dir);
bool TONE_BassAdjust(bool dir);
bool TONE_TrebleAtMin(void);
bool TONE_TrebleAtMax(void);
bool TONE_BassAtMin(void);
bool TONE_BassAtMax(void);

#endif // __TONE_CONTROL_H__

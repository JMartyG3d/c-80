//-----------------------------------------------------------------------------|
//	TONE_Control.c
//	Abstract: 3-band Tone Control
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include "TONE_Control.h"
#include "VOL_Control.h"
#include "NVM_Access.h"

static const uint16_t C_TONE_TREB_R_ADDR = 0x0C; // Treble Right Channel Address
static const uint16_t C_TONE_TREB_L_ADDR = 0x3C; // Treble Left  Channel Address
static const uint16_t C_TONE_MIDD_R_ADDR = 0x1C; // Middle Right Channel Address
static const uint16_t C_TONE_MIDD_L_ADDR = 0x4C; // Middle Left  Channel Address

//===================================================================
// Private Function Prototypes
//===================================================================
static uint8_t tone_translate(int8_t data, bool dir);

//===================================================================
// Public Functions
//===================================================================
void TONE_Set(void) {
    uint8_t data;

    if(NVM_GetBass(NVM_GetSrc()) >= 0)
        data = (NVM_GetBass(NVM_GetSrc()) << 3) | TONE_BOOST_MASK;
    else
        data = ((NVM_GetBass(NVM_GetSrc()) ^ 0xFF) + 0x01) << 3;

    // TX Left and Right Channels
    VOL_Tx(data, C_TONE_MIDD_R_ADDR, E_TONE_TX);
    VOL_Tx(data, C_TONE_MIDD_L_ADDR, E_TONE_TX);

    if(NVM_GetTreble(NVM_GetSrc()) >= 0)
        data = (NVM_GetTreble(NVM_GetSrc()) << 3) | TONE_BOOST_MASK;
    else
        data = ((NVM_GetTreble(NVM_GetSrc()) ^ 0xFF) + 0x01) << 3;

    data = TONE_BYPASS_OFF_MASK | data;

    // TX Left and Right Channels
    VOL_Tx(data, C_TONE_TREB_R_ADDR, E_TONE_TX);
    VOL_Tx(data, C_TONE_TREB_L_ADDR, E_TONE_TX);
}

bool TONE_TrebleAdjust(bool dir) {

    if(dir && TONE_TrebleAtMax() || !dir && TONE_TrebleAtMin())
        return(FALSE);

    // Repackage data for tx to IC
    uint8_t data = tone_translate(NVM_GetTreble(NVM_GetSrc()), dir);

    if(dir) // UP
        NVM_SetTreble(NVM_GetSrc(), NVM_GetTreble(NVM_GetSrc())+1);
    else    // DOWN
        NVM_SetTreble(NVM_GetSrc(), NVM_GetTreble(NVM_GetSrc())-1);

    data = TONE_BYPASS_OFF_MASK | data;

    // TX Left and Right Channels
    VOL_Tx(data, C_TONE_TREB_R_ADDR, E_TONE_TX);
    VOL_Tx(data, C_TONE_TREB_L_ADDR, E_TONE_TX);

    return(TRUE);
}

bool TONE_BassAdjust(bool dir) {

    if(dir && TONE_BassAtMax() || !dir && TONE_BassAtMin())
        return(FALSE);

    // Repackage data for tx to IC
    uint8_t data = tone_translate(NVM_GetBass(NVM_GetSrc()), dir);

    if(dir) // UP
        NVM_SetBass(NVM_GetSrc(), NVM_GetBass(NVM_GetSrc())+1);
    else    // DOWN
        NVM_SetBass(NVM_GetSrc(), NVM_GetBass(NVM_GetSrc())-1);

    // TX Left and Right Channels
    VOL_Tx(data, C_TONE_MIDD_R_ADDR, E_TONE_TX);
    VOL_Tx(data, C_TONE_MIDD_L_ADDR, E_TONE_TX);

    return(TRUE);
}

bool TONE_TrebleAtMin(void)
{
    return(NVM_GetTreble(NVM_GetSrc()) == TONE_MIN_GAIN);
}

bool TONE_TrebleAtMax(void)
{
    return(NVM_GetTreble(NVM_GetSrc()) == TONE_MAX_GAIN);
}

bool TONE_BassAtMin(void)
{
    return(NVM_GetBass(NVM_GetSrc()) == TONE_MIN_GAIN);
}

bool TONE_BassAtMax(void)
{
    return(NVM_GetBass(NVM_GetSrc()) == TONE_MAX_GAIN);
}

//===================================================================
// Private Functions
//===================================================================
static uint8_t tone_translate(int8_t data, bool dir) {
    uint8_t value;

    switch(data) {
        case  12: // 0110 0000
            value = dir ? 0xE0 : 0xD8; //  12 :  11
            break;
        case  11: // 0101 1000
            value = dir ? 0xE0 : 0xD0; //  12 :  10
            break;
        case  10: // 0101 0000
            value = dir ? 0xD8 : 0xC8; //  11 :   9
            break;
        case   9: // 0100 1000
            value = dir ? 0xD0 : 0xC0; //  10 :   8
            break;
        case   8: // 0100 0000
            value = dir ? 0xC8 : 0xB8; //   9 :   7
            break;
        case   7: // 0011 1000
            value = dir ? 0xC0 : 0xB0; //   8 :   6
            break;
        case   6: // 0011 0000
            value = dir ? 0xB8 : 0xA8; //   7 :   5
            break;
        case   5: // 0010 1000
            value = dir ? 0xB0 : 0xA0; //   6 :   4
            break;
        case   4: // 0010 0000
            value = dir ? 0xA8 : 0x98; //   5 :   3
            break;
        case   3: // 0001 1000
            value = dir ? 0xA0 : 0x90; //   4 :   2
            break;
        case   2: // 0001 0000
            value = dir ? 0x98 : 0x88; //   3 :   1
            break;
        case   1: // 0000 1000
            value = dir ? 0x90 : 0x80; //   2 :   0
            break;
        case   0: // 0000 0000
            value = dir ? 0x88 : 0x08; //   1 :  -1
            break;
        case  -1: // 0000 1000
            value = dir ? 0x00 : 0x10; //   0 :  -2
            break;
        case  -2: // 0001 0000
            value = dir ? 0x08 : 0x18; //  -1 :  -3
            break;
        case  -3: // 0001 1000
            value = dir ? 0x10 : 0x20; //  -2 :  -4
            break;
        case  -4: // 0010 0000
            value = dir ? 0x18 : 0x28; //  -3 :  -5
            break;
        case  -5: // 0010 1000
            value = dir ? 0x20 : 0x30; //  -4 :  -6
            break;
        case  -6: // 0011 0000
            value = dir ? 0x28 : 0x38; //  -5 :  -7
            break;
        case  -7: // 0011 1000
            value = dir ? 0x30 : 0x40; //  -6 :  -8
            break;
        case  -8: // 0100 0000
            value = dir ? 0x38 : 0x48; //  -7 :  -9
            break;
        case  -9: // 0100 1000
            value = dir ? 0x40 : 0x50; //  -8 : -10
            break;
        case -10: // 0101 0000
            value = dir ? 0x48 : 0x58; //  -9 : -11
            break;
        case -11: // 0101 1000
            value = dir ? 0x50 : 0x60; // -10 : -12
            break;
        case -12: // 0110 0000
            value = dir ? 0x58 : 0x60; // -11 : -12
            break;
        default:
            value = 0x00;
            break;
    }
    return(value);
}

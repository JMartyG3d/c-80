//-----------------------------------------------------------------------------|
//	IR_Queue.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include <string.h>
#include "IR_Process.h"
#include "IR_Queue.h"

Ir_Msg_Type __queue[C_IR_QUEUE_SIZE+1];
static uint8_t __head, __tail;

//===================================================================
// Public Functions
//===================================================================
void IR_QueueInit(void)
{
	__head = __tail = 0;
}

bool IR_QueueIsDirty(void)
{
	return(!(__head == __tail));
}

bool IR_QueuePush(uint8_t code, Ir_Key_Type key)
{
	if((__tail == __head - 1)||((__tail == C_IR_QUEUE_SIZE) && (__head == 0)))
		return(FALSE);
	else{
		__queue[__tail].Code = code;
		__queue[__tail].Key = key;
		__tail = (__tail == C_IR_QUEUE_SIZE ? 0 : __tail + 1);
		return(TRUE);
	}
}

uint8_t IR_QueuePop(Ir_Msg_Type *pMsg)
{
    if(__head == __tail){
		pMsg->Code = 0x00;
		pMsg->Key = E_KEY_NONE;
    }
	else{
		pMsg->Code = __queue[__head].Code;
		pMsg->Key = __queue[__head].Key;
		__head = (__head == C_IR_QUEUE_SIZE ? 0 : __head + 1);
	}
    return(pMsg->Code);
}

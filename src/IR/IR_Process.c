//-----------------------------------------------------------------------------|
//	IR_Process.c
//	Abstract: IR Processing / IRQ
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include "IR_Process.h"
#include "IR_Control.h"
#include "IR_Codes.h"
#include "IR_Queue.h"
#include "NVM_Access.h"
#include "UI_Common.h"
#include "UI_Trim.h"
#include "UI_Control.h"
#include "APP_Process.h"

Ir_Rx_Type __rx;
static const Ir_Rx_Type __rx_init = {
    .State        = IR_START,
    .HoldTimer    = 0,
    .EdgeTimer    = 0,
    .CurDataBit   = 0,
    .Code         = 0,
    .KeyHoldFirst = FALSE,
    .KeyHold      = FALSE
};

//===================================================================
// Public Functions
//===================================================================
// 100us sync Interrupt
void TC2_Handler( void )
{
    if(__rx.State == IR_DUPLICATE) {
        if(++__rx.HoldTimer > C_IR_MAX_HOLD_TIME) {
            if(!__rx.KeyHold)
                IR_QueuePush(__rx.Code, E_KEY_PRESS);

            IR_Reset();
        }
    }

    if(++__rx.EdgeTimer > (uint32_t)(C_IR_TIMEOUT/C_IR_SAMPLING_TIME)) // 450ms
        IR_Reset();

    tc_get_status(TC0, 2);
}

void IR_Reset(void)
{
    __rx = __rx_init;
}

void IR_PioHandlerFront(uint32_t id, uint32_t mask)
{
    static uint8_t edge_cnt;
    uint32_t cur_time = __rx.EdgeTimer;
    uint32_t rising_edge = (PIOA->PIO_PDSR & PIN16_IR_FRONT);  // 0: falling edge, 1: rising edge

    if(NVM_GetIrFront() == OFF)
        return;

    if((APP_GetSchedule() != E_SCHED_ACTIVE) && (APP_GetSchedule() != E_SCHED_INACTIVE)) {
        IR_Reset();
        return;
    }

	switch(__rx.State){
        case IR_START:
			if(!rising_edge) { // First Falling edge of Leader Code Detected
				__rx.EdgeTimer = __rx.CurDataBit = 0;
	            __rx.State = IR_LEADER9MS;
                edge_cnt = 0;
			}
			break;
		case IR_LEADER9MS:
			if(rising_edge) { // First Rising Edge of Leader Code Detected (Verify 9ms)
           		if((cur_time > C_IR_LEADER9_MIN) && (cur_time <= C_IR_LEADER9_MAX)){
					__rx.State = IR_LEADER45MS;
					__rx.EdgeTimer = 0;
				}
				else
					__rx.State = IR_START;
			}
			else
        		__rx.State = IR_START;
   			break;
		case IR_LEADER45MS:
			if(!rising_edge) {  // Last Edge of Leader Code Detected (Verify 4.5ms)
           		if((cur_time > C_IR_LEADER45_MIN) && (cur_time <= C_IR_LEADER45_MAX)){
					__rx.Code = __rx.EdgeTimer = 0;
           			__rx.State = IR_CUSTOMCODE;
           		}
           		else
              		__rx.State = IR_START;
     		}
        	else
        		__rx.State = IR_START;
    		break;
        case IR_CUSTOMCODE:
     		if(!rising_edge){ // Determine 0 or 1 identity
           		if ((cur_time > C_IR_DATA_1_MIN) && (cur_time <= C_IR_DATA_1_MAX))
           			__rx.Code |= (0x1 << __rx.CurDataBit);
           		else if (!((cur_time > C_IR_DATA_0_MIN) && (cur_time <= C_IR_DATA_0_MAX))) {
                    __rx.State = IR_START;
                    break;
                }

                __rx.EdgeTimer = 0;
				if(++__rx.CurDataBit > 15) {
					if(__rx.Code == 0x55CA) {
						__rx.State = IR_DATA;
						__rx.CurDataBit = __rx.Code = 0;
					}
					else
						__rx.State = IR_START;
				}
			}
			break;
        case IR_DATA:
     		if(!rising_edge){ // Determine 0 or 1 identity
           		if ((cur_time > C_IR_DATA_1_MIN))
           			__rx.Code |= 1 << __rx.CurDataBit;
           		else if (!((cur_time > C_IR_DATA_0_MIN) && (cur_time <= C_IR_DATA_0_MAX))) {
                    __rx.State = IR_START;
                    break;
                }

                __rx.EdgeTimer = 0;
				if(++__rx.CurDataBit > 15)
					__rx.State = IR_LAST;
           	}
			break;
        case IR_LAST:
			if(rising_edge) { // LAST edge of Initial Transmission Code
                __rx.EdgeTimer = 0;
                __rx.State = IR_DUPLICATE;
                edge_cnt = 0;
            }
            break;
        case IR_DUPLICATE:
            if(edge_cnt == 0) // Start Timer
                __rx.HoldTimer = 0;

            // Transmission Code contains 68 edges
            if(++edge_cnt > 68) {
                if(!rising_edge) { // First Falling edge of Next Leader Code
                    edge_cnt = 0;

                    if((__rx.HoldTimer > C_IR_MIN_HOLD_TIME) && (__rx.HoldTimer <= C_IR_MAX_HOLD_TIME)) { // 108ms
                        if(!__rx.KeyHoldFirst) {
                            IR_QueuePush(__rx.Code, E_KEY_HOLD_FIRST);
                            __rx.KeyHoldFirst = TRUE;
                        }
                        else
                            IR_QueuePush(__rx.Code, E_KEY_HOLD);

                        __rx.KeyHold = TRUE;
                        __rx.HoldTimer = 0;
                    }
                }
            }

            __rx.EdgeTimer = 0;
			break;
        default:
            break;
	}
}

void IR_PioHandlerRear(uint32_t id, uint32_t mask)
{
    static uint8_t edge_cnt;
    uint32_t cur_time = __rx.EdgeTimer;
    uint32_t rising_edge = (PIOA->PIO_PDSR & PIN15_IR_REAR);  // 0: falling edge, 1: rising edge

    if((APP_GetSchedule() != E_SCHED_ACTIVE) && (APP_GetSchedule() != E_SCHED_INACTIVE)) {
        IR_Reset();
        return;
    }

	switch(__rx.State){
        case IR_START:
			if(!rising_edge) { // First Falling edge of Leader Code Detected
				__rx.EdgeTimer = __rx.CurDataBit = 0;
	            __rx.State = IR_LEADER9MS;
                edge_cnt = 0;
			}
			break;
		case IR_LEADER9MS:
			if(rising_edge) { // First Rising Edge of Leader Code Detected (Verify 9ms)
           		if((cur_time > C_IR_LEADER9_MIN) && (cur_time <= C_IR_LEADER9_MAX)){
					__rx.State = IR_LEADER45MS;
					__rx.EdgeTimer = 0;
				}
				else
					__rx.State = IR_START;
			}
			else
        		__rx.State = IR_START;
   			break;
		case IR_LEADER45MS:
			if(!rising_edge) {  // Last Edge of Leader Code Detected (Verify 4.5ms)
           		if((cur_time > C_IR_LEADER45_MIN) && (cur_time <= C_IR_LEADER45_MAX)){
					__rx.Code = __rx.EdgeTimer = 0;
           			__rx.State = IR_CUSTOMCODE;
           		}
           		else
              		__rx.State = IR_START;
     		}
        	else
        		__rx.State = IR_START;
    		break;
        case IR_CUSTOMCODE:
     		if(!rising_edge){ // Determine 0 or 1 identity
           		if ((cur_time > C_IR_DATA_1_MIN) && (cur_time <= C_IR_DATA_1_MAX))
           			__rx.Code |= (0x1 << __rx.CurDataBit);
           		else if (!((cur_time > C_IR_DATA_0_MIN) && (cur_time <= C_IR_DATA_0_MAX))) {
                    __rx.State = IR_START;
                    break;
                }

                __rx.EdgeTimer = 0;
				if(++__rx.CurDataBit > 15) {
					if(__rx.Code == 0x55CA) {
						__rx.State = IR_DATA;
						__rx.CurDataBit = __rx.Code = 0;
					}
					else
						__rx.State = IR_START;
				}
			}
			break;
        case IR_DATA:
     		if(!rising_edge){ // Determine 0 or 1 identity
           		if ((cur_time > C_IR_DATA_1_MIN))
           			__rx.Code |= 1 << __rx.CurDataBit;
           		else if (!((cur_time > C_IR_DATA_0_MIN) && (cur_time <= C_IR_DATA_0_MAX))) {
                    __rx.State = IR_START;
                    break;
                }

                __rx.EdgeTimer = 0;
				if(++__rx.CurDataBit > 15)
					__rx.State = IR_LAST;
           	}
			break;
        case IR_LAST:
			if(rising_edge) { // LAST edge of Initial Transmission Code
                __rx.EdgeTimer = 0;
                __rx.State = IR_DUPLICATE;
                edge_cnt = 0;
            }
            break;
        case IR_DUPLICATE:
            if(edge_cnt == 0) // Start Timer
                __rx.HoldTimer = 0;

            // Transmission Code contains 68 edges
            if(++edge_cnt > 68) {
                if(!rising_edge) { // First Falling edge of Next Leader Code
                    edge_cnt = 0;

                    if((__rx.HoldTimer > C_IR_MIN_HOLD_TIME) && (__rx.HoldTimer <= C_IR_MAX_HOLD_TIME)) { // 108ms
                        if(!__rx.KeyHoldFirst) {
                            IR_QueuePush(__rx.Code, E_KEY_HOLD_FIRST);
                            __rx.KeyHoldFirst = TRUE;
                        }
                        else
                            IR_QueuePush(__rx.Code, E_KEY_HOLD);

                        __rx.KeyHold = TRUE;
                        __rx.HoldTimer = 0;
                    }
                }
            }

            __rx.EdgeTimer = 0;
			break;
        default:
            break;
	}
}

//-----------------------------------------------------------------------------|
//	IR_Control.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include "common.h"
#include "IR_Codes.h"
#include "IR_Control.h"
#include "IR_Process.h"
#include "IR_Queue.h"
#include "NVM_Access.h"
#include "NVM_Control.h"
#include "APP_Process.h"
#include "UI_Common.h"
#include "UI_Control.h"
#include "VOL_Control.h"
#include "SRC_Control.h"
#include "DA_Config.h"
#include "TRIG_Control.h"
#include "IOX_Control.h"
#include "UI_Trim.h"
#include "TMR_Control.h"
#include "DISP_Control.h"
#include "PWR_Control.h"

static uint8_t  __ir_hold_cnt, __ir_second_key;

//===================================================================
// Private Function Prototypes
//===================================================================
static bool ir_hold_func(uint8_t code);
static bool ir_hold_cnt_check(uint8_t limit);

//===================================================================
// Public Functions
//===================================================================
void IR_Initialize(void)
{
    PMC->PMC_PCER0 = 1 << ID_TC2;
    TC0->TC_CHANNEL[2].TC_CCR = TC_CCR_CLKEN | TC_CCR_SWTRG ;
    TC0->TC_CHANNEL[2].TC_CMR = TC_CMR_TCCLKS_TIMER_CLOCK4 | TC_CMR_CPCTRG;
	TC0->TC_CHANNEL[2].TC_RC  = IR_100US;	  // Set RC compare to 100US
	TC0->TC_CHANNEL[2].TC_IER = TC_IER_CPCS;  // IRQ enable RC compare

    NVIC_EnableIRQ((IRQn_Type)TC2_IRQn);
    NVIC_SetPriority((IRQn_Type)TC2_IRQn,1);

    __ir_hold_cnt = __ir_second_key = 0;

    IR_QueueInit();
}

void IR_Periodic(void)
{
    Ir_Msg_Type msg;

    if(!IR_QueueIsDirty())
        return;

    IR_QueuePop(&msg);

    if(PWR_GetSleepState() == YES) {
        if((msg.Code == IR_PWR_ON) || (msg.Code == IR_PWR_TOGGLE)) {
            IOX_SleepExit();
            PWR_SetSleepState(FALSE);
        }
        return;
    }

    // check second key --> clear second key flag if not a valid second key target
    if((__ir_second_key == ON) && ((msg.Code != IR_AM_OUT1) && (msg.Code != IR_FM_OUT2) && (msg.Code != IR_SHIFT)))
        __ir_second_key = OFF;

    if(NVM_GetIrCoding() == OFF) // Alternate
        msg.Code ^= 0x80;

    if(msg.Key != E_KEY_HOLD)
        __ir_hold_cnt = 0;

    if(msg.Key == E_KEY_HOLD) {
        if(ir_hold_func(msg.Code))
            __ir_hold_cnt++;
        else
            return;
    }

    switch(msg.Code) {
        case IR_SHIFT:
            if((UI_GetMenu() == E_MENU_MAIN) || (UI_GetMenu() == E_MENU_PASSTHRU))
                __ir_second_key = ON;
            break;
        case IR_AM_OUT1:
            if(__ir_second_key == ON) {
                UI_Input(C_IR_OUT1_BTN);
                __ir_second_key = OFF;
            }
            break;
        case IR_FM_OUT2:
            if(__ir_second_key == ON) {
                UI_Input(C_IR_OUT2_BTN);
                __ir_second_key = OFF;
            }
            break;
        case IR_PWR_ON:
            if(APP_GetSchedule() == E_SCHED_INACTIVE)
                UI_Input(C_KEY_POWER);
            break;
        case IR_PWR_OFF:
            if(APP_GetSchedule() != E_SCHED_INACTIVE)
                UI_Input(C_KEY_POWER);
            break;
        case IR_PWR_TOGGLE: UI_Input(C_KEY_POWER);   break;
        case IR_TRIM:       UI_Input(C_IR_TRIM_BTN); break;
        case IR_MODE:       UI_Input(C_IR_MODE_BTN); break;
        case IR_LEVEL_UP:
            if(UI_GetMenu() == E_MENU_TRIM) {
                if((UI_TrimGetMenu() == E_TRIM_MC_PHONO) || (UI_TrimGetMenu() == E_TRIM_MM_PHONO)) {
                    if(IOX_PhonoDelay())
                        break;
                }
                UI_Input(C_IR_LEVEL_UP_BTN);
            }
            break;
        case IR_LEVEL_DOWN:
            if(UI_GetMenu() == E_MENU_TRIM) {
                if((UI_TrimGetMenu() == E_TRIM_MC_PHONO) || (UI_TrimGetMenu() == E_TRIM_MM_PHONO)) {
                    if(IOX_PhonoDelay())
                        break;
                }
                UI_Input(C_IR_LEVEL_DOWN_BTN);
            }
            break;
        case IR_VOLUME_UP:
            if(UI_GetMenu() != E_MENU_TRIM) {
                if(UI_GetMenu() == E_MENU_MAIN) {
                    if(__ir_hold_cnt > 10)
                        VOL_Accum(5);
                    else if (__ir_hold_cnt > 5)
                        VOL_Accum(2);
                    else
                        VOL_Accum(1);
                }
                else
                    UI_Input(C_RTY_VOLUME_UP);
            }
            break;
        case IR_VOLUME_DOWN:
            if(UI_GetMenu() != E_MENU_TRIM) {
                if(UI_GetMenu() == E_MENU_MAIN) {
                    if(__ir_hold_cnt > 10)
                        VOL_Accum(-5);
                    else if (__ir_hold_cnt > 5)
                        VOL_Accum(-2);
                    else
                        VOL_Accum(-1);
                }
                else
                    UI_Input(C_RTY_VOLUME_DOWN);
            }
            break;
        case IR_INPUT_UP:
        case IR_INPUT_DOWN:
            if(UI_GetMenu() != E_MENU_TRIM) {
                if(ir_hold_cnt_check(3)) {
                    if (msg.Code == IR_INPUT_UP)
                        UI_Input(C_RTY_INPUT_UP);
                    else
                        UI_Input(C_RTY_INPUT_DOWN);
                }
            }
            break;
        case IR_MUTE:
            if(ir_hold_cnt_check(5))
                UI_Input(C_KEY_MUTE);
            break;
        case IR_UP:
        case IR_DOWN:
            break;
        case IR_DIR_INPUT_1:
            if(NVM_GetSrc() != E_SRC_MM_PHONO)
                SRC_Set(E_SRC_MM_PHONO);
            break;
        case IR_DIR_INPUT_2:
                if(NVM_GetSrc() != E_SRC_MC_PHONO)
                    SRC_Set(E_SRC_MC_PHONO);
                break;
        case IR_DIR_INPUT_3:
            if((NVM_GetSrc() != E_SRC_MCT) && DA_Installed())
                SRC_Set(E_SRC_MCT);
            break;
        case IR_DIR_INPUT_4:
            if((NVM_GetSrc() != E_SRC_OPTI1) && DA_Installed())
                SRC_Set(E_SRC_OPTI1);
            break;
        case IR_DIR_INPUT_5:
            if((NVM_GetSrc() != E_SRC_OPTI2) && DA_Installed())
                SRC_Set(E_SRC_OPTI2);
            break;
        case IR_DIR_INPUT_6:
            if((NVM_GetSrc() != E_SRC_COAX1) && DA_Installed())
                SRC_Set(E_SRC_COAX1);
            break;
        case IR_DIR_INPUT_7:
            if((NVM_GetSrc() != E_SRC_COAX2) && DA_Installed())
                SRC_Set(E_SRC_COAX2);
            break;
        case IR_DIR_INPUT_8:
            if(NVM_GetSrc() != E_SRC_BAL1)
                SRC_Set(E_SRC_BAL1);
            break;
        case IR_DIR_INPUT_9:
//            if(NVM_GetSrc() != E_SRC_BAL2)     // Removed in C80
//                SRC_Set(E_SRC_BAL2);
            break;
        case IR_DIR_INPUT_12:
            if((NVM_GetSrc() != E_SRC_USB) && DA_Installed())
                SRC_Set(E_SRC_USB);
            break;
        case IR_DIR_INPUT_13:
            if(NVM_GetSrc() != E_SRC_UNBAL1)
                SRC_Set(E_SRC_UNBAL1);
            break;
        case IR_DIR_INPUT_14:
            if(NVM_GetSrc() != E_SRC_UNBAL2)
                SRC_Set(E_SRC_UNBAL2);
            break;
        case IR_DIR_INPUT_15:
//            if(NVM_GetSrc() != E_SRC_UNBAL3)    // Removed in C80
//                SRC_Set(E_SRC_UNBAL3);
            break;
        case IR_DIR_INPUT_10:
        case IR_DIR_INPUT_11:
        case IR_DIR_INPUT_16:
            break;
        case IR_DIR_INPUT_17:
            if((NVM_GetSrc() != E_SRC_HDMI) && DA2_Installed())
                SRC_Set(E_SRC_HDMI);
            break;
        case IR_NUMBER_1: case IR_NUMBER_2: case IR_NUMBER_3: case IR_NUMBER_4:
        case IR_NUMBER_5: case IR_NUMBER_6: case IR_NUMBER_7: case IR_NUMBER_8:
        case IR_NUMBER_9: case IR_NUMBER_0:
            if(UI_GetMenu() == E_MENU_SETUP)
                UI_Input(msg.Code);
            break;
        default: break;
    }
}

//===================================================================
// Private Functions
//===================================================================
static bool ir_hold_func(uint8_t code)
{
    bool rc = FALSE;

    switch(code) {
        case IR_VOLUME_UP:
        case IR_VOLUME_DOWN:
        case IR_INPUT_UP:
        case IR_INPUT_DOWN:
        case IR_UP:
        case IR_DOWN:
            rc = TRUE;
        case IR_LEVEL_UP:
        case IR_LEVEL_DOWN:
                if((UI_GetMenu() == E_MENU_TRIM) &&
                  ((UI_TrimGetMenu() == E_TRIM_MC_PHONO) || (UI_TrimGetMenu() == E_TRIM_MM_PHONO) ||
                   (UI_TrimGetMenu() == E_TRIM_BRIGHTNESS)))
                    rc = FALSE;
                else
                    rc = TRUE;
    }

    return rc;
}

static bool ir_hold_cnt_check(uint8_t limit)
{
    return ((__ir_hold_cnt == 0) || ((__ir_hold_cnt % limit) == 0));
}

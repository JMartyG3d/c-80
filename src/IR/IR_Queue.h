//-----------------------------------------------------------------------------|
//	IR_Queue.h
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __IR_QUEUE_H__
#define __IR_QUEUE_H__

#define	C_IR_QUEUE_SIZE  80

//===================================================================
// Public Function Prototypes
//===================================================================
void IR_QueueInit(void);
bool IR_QueuePush(uint8_t code, Ir_Key_Type key);
uint8_t IR_QueuePop(Ir_Msg_Type *pMsg);
bool IR_QueueIsDirty(void);

#endif // __IR_QUEUE_H__
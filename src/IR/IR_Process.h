//-----------------------------------------------------------------------------|
//	IR_Process.h
//	Abstract:
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __IR_PROCESS_H__
#define __IR_PROCESS_H__

#define C_IR_SAMPLING_TIME  100    // 150
#define C_IR_TIMEOUT        450000 //21 ms Timeout during receive

#define IR_100US            38   //57

#define C_IR_LEADER9_MIN    82   // 9ms
#define C_IR_LEADER9_MAX    94
#define C_IR_LEADER45_MIN   42   // 4.5ms
#define C_IR_LEADER45_MAX   47
#define C_IR_DATA_LOW_MIN   4    // 5.6ms
#define C_IR_DATA_LOW_MAX   7
#define	C_IR_DATA_0_MIN     9    // 1.125ms
#define C_IR_DATA_0_MAX     13
#define C_IR_DATA_1_MIN     20   // 2.25ms
#define C_IR_DATA_1_MAX     23
#define C_IR_MIN_HOLD_TIME  1060 // 108ms
#define C_IR_MAX_HOLD_TIME  1160

typedef enum {
    E_KEY_NONE,
    E_KEY_PRESS,
    E_KEY_HOLD_FIRST,
    E_KEY_HOLD,
} Ir_Key_Type;

typedef struct{
    uint8_t Code;
    Ir_Key_Type Key;
}Ir_Msg_Type;

typedef struct {
	enum  {IR_START,IR_LEADER9MS,IR_LEADER45MS,IR_CUSTOMCODE,IR_DATA,IR_LAST,IR_DUPLICATE}State;
	uint32_t EdgeTimer; // Times edges of a Transmission code
    uint32_t HoldTimer; // Times full Transmission code
 	uint32_t CurDataBit;
	uint32_t Code;
	bool KeyHoldFirst;
    bool KeyHold;
} Ir_Rx_Type;

//===================================================================
// Public Function Prototypes
//===================================================================
void IR_Reset(void);
void IR_PioHandlerFront(uint32_t id, uint32_t mask);
void IR_PioHandlerRear(uint32_t id, uint32_t mask);

#endif // __IR_PROCESS_H__

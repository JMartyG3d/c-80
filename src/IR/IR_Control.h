//-----------------------------------------------------------------------------|
//	IR_Control.h
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __IR_CONTROL_H__
#define __IR_CONTROL_H__

//===================================================================
// Public Function Prototypes
//===================================================================
void IR_Initialize(void);
void IR_Periodic(void);

#endif // __IR_CONTROL_H__

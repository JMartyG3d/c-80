//-----------------------------------------------------------------------------|
//	HR085_IRCodes.h
//	Abstract: McIntosh IR Remote Codes
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __HR085_IRCODES_H__
#define __HR085_IRCODES_H__

/* Power */
#define IR_PWR_ON       0x4B
#define IR_PWR_TOGGLE   0x40
#define IR_PWR_OFF      0x5F

/* Playback */
#define IR_PLAY         0x01
#define IR_STOP         0x04
#define IR_RECORD       0x4A

/* TUNER */
#define IR_PRESET_UP    0x02
#define IR_PRESET_DOWN  0x03
#define IR_SEEK_UP      0x5B
#define IR_SEEK_DOWN    0x5A
#define IR_AM_OUT1      0x5D
#define IR_FM_OUT2      0x5E
#define IR_BAND         0x5C

/* Trim/preset */
#define IR_TRIM         0x0E
#define IR_PRESET       0x0F

/* Trim navigation */
#define IR_LEFT         0x18
#define IR_UP           0x19
#define IR_RIGHT        0x1A
#define IR_DOWN         0x1B
#define IR_SELECT       0x1C

/* Volume/Balance/Input */
#define IR_VOLUME_UP    0x42
#define IR_VOLUME_DOWN  0x43
#define IR_LEVEL_UP     0x44
#define IR_LEVEL_DOWN   0x45
#define IR_MUTE         0x46
#define IR_INPUT_UP     0x4E
#define IR_INPUT_DOWN   0x4F

/* Misc */
#define IR_MODE         0x47
#define IR_SHIFT        0x48 // blue button
#define IR_NUMBER_1     0x50
#define IR_NUMBER_2     0x51
#define IR_NUMBER_3     0x52
#define IR_NUMBER_4     0x53
#define IR_NUMBER_5     0x54
#define IR_NUMBER_6     0x55
#define IR_NUMBER_7     0x56
#define IR_NUMBER_8     0x57
#define IR_NUMBER_9     0x58
#define IR_NUMBER_0     0x59

/* Direct input codes */
#define IR_DIR_INPUT_1   0x05  // MM PHONO
#define IR_DIR_INPUT_2   0x06  // MC PHONO
#define IR_DIR_INPUT_3   0x08  // MCT
#define IR_DIR_INPUT_4   0x09  // OPT 1
#define IR_DIR_INPUT_5   0x0A  // OPT 2
#define IR_DIR_INPUT_6   0x0C  // COAX 1
#define IR_DIR_INPUT_7   0x0D  // COAX 2
#define IR_DIR_INPUT_8   0x12  // BAL 1
#define IR_DIR_INPUT_9   0x13  // UNUSED
#define IR_DIR_INPUT_10  0x14  // UNUSED
#define IR_DIR_INPUT_11  0x15  // UNUSED
#define IR_DIR_INPUT_12  0x17  // USB
#define IR_DIR_INPUT_13  0x41  // UNBAL 1
#define IR_DIR_INPUT_14  0x49  // UNBAL 2
#define IR_DIR_INPUT_15  0x4C  // UNUSED
#define IR_DIR_INPUT_16  0x4D  // UNUSED
#define IR_DIR_INPUT_17  0x10  // HDMI(ARC)

#endif // __HR085_IRCODES_H__
//-----------------------------------------------------------------------------|
//	TRIG_Control.c
//	Abstract: Controls the configuration and operation of the unit triggers
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include "TRIG_Control.h"
#include "IOX_Control.h"
#include "NVM_Access.h"
#include "APP_Process.h"
#include "PT_Control.h"

//===================================================================
// Public Function Definitions
//===================================================================
void TRIG_Config(void)
{
    if(APP_GetSchedule() == E_SCHED_ACTIVE || (APP_GetSchedule() == E_SCHED_POWERUP)) {
        for(Trig_Id_Type i=E_TRIG_ID_FIRST;i<=E_TRIG_ID_LAST;i++) {
            switch(NVM_GetTrigger(i)) {
                case E_TRIG_ON:
                    IOX_Trigger(i, ON);
                    break;
                case E_TRIG_OUT1:
                    if(PT_Active())
                        IOX_Trigger(i, NVM_GetPtOutput(E_OUTPUT_1));
                    else
                        IOX_Trigger(i, NVM_GetOutput(E_OUTPUT_1));
                    break;
                case E_TRIG_OUT2:
                    if(PT_Active())
                        IOX_Trigger(i, NVM_GetPtOutput(E_OUTPUT_2));
                    else
                        IOX_Trigger(i, NVM_GetOutput(E_OUTPUT_2));
                    break;
                case E_TRIG_INPUT:
                    IOX_Trigger(i, NVM_GetTriggerInput(i, (Src_Type) NVM_GetSrc()));
                    break;
            }
        }
    }
}

void TRIG_Off(void)
{
    for(Trig_Id_Type i=E_TRIG_ID_FIRST;i<=E_TRIG_ID_LAST;i++)
        IOX_Trigger(i, OFF);

    IOX_TriggerMain(OFF);
    IOX_TrigMeterlight(OFF);
}

void DPORT_Config(void)
{
    Dport_Id_Type last = E_DPORT_ID_LAST;

    for(Dport_Id_Type i=E_DPORT_ID_FIRST; i<=last; i++) {
        // 0xAA == ALL, 0..n = inputs offset by 1
        if((NVM_GetDataport(i) == E_SRC_DP_ALL) || ((NVM_GetDataport(i)) == NVM_GetSrc()))
            IOX_Dataport(i, ON);
        else
            IOX_Dataport(i, OFF);
    }
}

void DPORT_Off(void)
{
    Dport_Id_Type last = E_DPORT_ID_LAST;

    for(Dport_Id_Type i=E_DPORT_ID_FIRST; i<=last; i++)
        IOX_Dataport(i, OFF);
}
//-----------------------------------------------------------------------------|
//	TRIG_Control.h
//	Abstract: Controls the configuration and operation of the unit triggers
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __TRIG_CONFIG_H__
#define __TRIG_CONFIG_H__

#include "Common.h"

//===================================================================
// Public Function Prototypes
//===================================================================
void TRIG_Config(void);
void TRIG_Off(void);
void DPORT_Config(void);
void DPORT_Off(void);

#endif // __TRIG_CONFIG_H__
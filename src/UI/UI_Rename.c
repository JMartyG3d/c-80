//-----------------------------------------------------------------------------|
//	UI_Rename.c
//	Abstract: Source Rename UI module
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include <string.h>
#include "UI_Rename.h"
#include "UI_Control.h"
#include "UI_Common.h"
#include "UI_Function.h"
#include "NVM_Access.h"
#include "NVM_Control.h"
#include "DISP_Control.h"

static Ui_Rename_Type __rename;

//==============================================================================
// Private Function Prototypes
//==============================================================================
static void ui_rename_vfd2(void);
static void ui_rename_keypress(uint32_t code);
static void ui_rename_keyhold(uint32_t code);
static void ui_rename_next_char(void);
static void ui_rename_prev_char(void);
static void ui_rename_save(void);
static void ui_rename_fill(void);
static void ui_rename_trim(void);

//==============================================================================
// Public Functions
//==============================================================================
/* Set the input to be renamed, must be called prior to entering rename menu */
void UI_RenameInput(uint8_t input)
{
    if (input < E_SRC_NUM) {
        __rename.char_pos = 0;
        __rename.blink_timer = 0;
        __rename.space_sub = FALSE;

        __rename.input = input;
        strncpy(__rename.name,NVM_SavedNames[input], C_MAX_INPUT_LEN);
        __rename.blink_char = __rename.name[__rename.char_pos];

        ui_rename_fill();
    }
}

/* process input */
void UI_Rename_Process(uint32_t code)
{
    if ((code & C_KEY_HOLD_MASK) == C_KEY_HOLD_MASK)
        ui_rename_keyhold(code);
    else
        ui_rename_keypress(code);
}

void UI_Rename_Display(void)
{
    char vfd1[20]; vfd1[19]=0;
    char vfd2[13]; vfd2[12]=0;

    DISP_ClearLine(C_DISP_BOTHLINES);

    snprintf(vfd1, 20, "RENAME: %s\0", NVM_SavedNames[__rename.input]);
    DISP_Print(vfd1,C_DISP_LINE1,2);
    snprintf(vfd2, 13, ">%10s<\0", __rename.name);
    DISP_Print(vfd2, C_DISP_LINE2, 5);
}

void UI_Rename_Blink(void)
{
    /* don't blink if actively changing char */
    if(__rename.blink_pause == 0) {
        __rename.blink_timer++;

        if(__rename.blink_timer == 1)
            __rename.name[__rename.char_pos] = __rename.blink_char; // restore character

        if(__rename.blink_timer == 15) {
            __rename.blink_char = __rename.name[__rename.char_pos]; // store character

            /* blink, set _ if a space */
            if (__rename.blink_char == ' ')
                __rename.name[__rename.char_pos] = '_';
            else
                __rename.name[__rename.char_pos] = ' ';
        }

        if (__rename.blink_timer == 21)
            __rename.blink_timer = 0;

        ui_rename_vfd2();
    }
    else
        __rename.blink_pause--;
}

void UI_Rename_Exit(void)
{
    UI_SetMenu(E_MENU_SETUP);
}

//==============================================================================
// Private Functions
//==============================================================================
static void ui_rename_vfd2(void)
{
    DISP_Print(&__rename.name[__rename.char_pos], C_DISP_LINE2, __rename.char_pos+6);
}

static void ui_rename_keypress(uint32_t code)
{
    static uint8_t up_dly, down_dly;

    switch(code) {
        case C_KEY_TRIM: UI_Rename_Exit(); break;
        case C_RTY_INPUT_UP:
            __rename.name[__rename.char_pos] = __rename.blink_char; // restore blinked char
            ui_rename_vfd2();
            /* select next character */
            __rename.char_pos = (__rename.char_pos + 1) % (C_MAX_INPUT_LEN-1);
            /* Store character for blinking */
            __rename.blink_char = __rename.name[__rename.char_pos];
            __rename.blink_pause = 0;
            __rename.space_sub = FALSE;
            ui_rename_vfd2();
            break;
        case C_RTY_INPUT_DOWN:
            __rename.name[__rename.char_pos] = __rename.blink_char; // restore blinked char
            ui_rename_vfd2();
            /* select previous character */
            if (__rename.char_pos > 0)
                __rename.char_pos--;
            else
                __rename.char_pos = C_MAX_INPUT_LEN - 2;
            /* Store character for blinking */
            __rename.blink_char = __rename.name[__rename.char_pos];
            __rename.blink_pause = 0;
            __rename.space_sub = FALSE;
            ui_rename_vfd2();
            break;
        case C_RTY_VOLUME_UP:
            if(up_dly == 0) {
                ui_rename_next_char(); // Change character
                up_dly = 2;
            }
            else
                up_dly--;
            break;
        case C_RTY_VOLUME_DOWN:
            if(down_dly == 0) {
                ui_rename_prev_char(); // Change character
                down_dly = 2;
            }
            else
                down_dly--;
            break;
        default:
            break;
    }
}

static void ui_rename_keyhold(uint32_t code)
{
    if(C_KEY_TRIM == (code ^ C_KEY_HOLD_MASK)) {
        ui_rename_save();   // Exit and save
        UI_Rename_Exit();
    }
}

static void ui_rename_next_char(void)
{
    __rename.blink_pause = 20;
    __rename.name[__rename.char_pos]  = __rename.blink_char + 1;

    if (__rename.name[__rename.char_pos] > ((char)0x7E)) // char > '~'
        __rename.name[__rename.char_pos] = ' ';

    // Check for Prev Space Insertion
    if(!__rename.space_sub) {

        // Insert SPACE
        if(__rename.name[__rename.char_pos] == (char)0x30 ||
           __rename.name[__rename.char_pos] == (char)0x3A ||
           __rename.name[__rename.char_pos] == (char)0x41 ||
           __rename.name[__rename.char_pos] == (char)0x5B ||
           __rename.name[__rename.char_pos] == (char)0x61 ||
           __rename.name[__rename.char_pos] == (char)0x7B ) {
                __rename.space_restore = __rename.name[__rename.char_pos];
                __rename.name[__rename.char_pos] = ' ';
                __rename.space_sub = TRUE;
        }
    }
    else {
        __rename.name[__rename.char_pos] = __rename.space_restore;
        __rename.space_sub = FALSE;
    }

    __rename.blink_char = __rename.name[__rename.char_pos];
    __rename.blink_timer = 0;

    ui_rename_vfd2();
}

static void ui_rename_prev_char(void)
{
    __rename.blink_pause = 20;

    if(!__rename.space_sub) {
        __rename.name[__rename.char_pos] = __rename.blink_char - 1;

        if(__rename.name[__rename.char_pos] < ((char)0x20)) // char > ' '
            __rename.name[__rename.char_pos] = '~';
    }

    // Check for Prev Space Insertion
    if(!__rename.space_sub) {

        // Insert SPACE
        if(__rename.name[__rename.char_pos] == (char)0x2F ||
           __rename.name[__rename.char_pos] == (char)0x3B ||
           __rename.name[__rename.char_pos] == (char)0x40 ||
           __rename.name[__rename.char_pos] == (char)0x5A ||
           __rename.name[__rename.char_pos] == (char)0x60 ||
           __rename.name[__rename.char_pos] == (char)0x7A ) {
                __rename.space_restore = __rename.name[__rename.char_pos];
                __rename.name[__rename.char_pos] = ' ';
                __rename.space_sub = TRUE;
        }
    }
    else {
        __rename.name[__rename.char_pos] = __rename.space_restore;
        __rename.space_sub = FALSE;
    }

    __rename.blink_char = __rename.name[__rename.char_pos];
    __rename.blink_timer = 0;

    ui_rename_vfd2();
}

static void ui_rename_save(void)
{
    __rename.name[__rename.char_pos] = __rename.blink_char; // restore blinked char
    ui_rename_trim();   // trim extra spaces
    strncpy(NVM_SavedNames[__rename.input],__rename.name, C_MAX_INPUT_LEN);
    NVM_SaveName(__rename.input);   // NVM request save
}

/* pad selected input name with spaces */
static void ui_rename_fill(void)
{
    int32_t i = C_MAX_INPUT_LEN - 2; // Back off two, make sure last /0 is left alone
    // cheat because we know max length of an input name string
    while( (__rename.name[i] == '\0') || (__rename.name[i] == ' ')) {
        __rename.name[i] = ' ';
        i--;

        if (i < 0)
            break;
    }
}

static void ui_rename_trim(void)
{
    int32_t i = C_MAX_INPUT_LEN;
    // cheat because we know max length of an input name string
    do {
        __rename.name[i] = '\0';
        i--;

        if (i < 0)
            break;
    } while ( (__rename.name[i] == '\0') || (__rename.name[i] == ' '));
}

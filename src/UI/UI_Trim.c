//-----------------------------------------------------------------------------|
//	UI_Trim.c
//	Abstract: Trim Menu UI module
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include <string.h>
#include "Common.h"
#include "UI_Trim.h"
#include "UI_Control.h"
#include "UI_Common.h"
#include "UI_Function.h"
#include "NVM_Access.h"
#include "DISP_Control.h"
#include "VOL_Control.h"
#include "OUT_Control.h"
#include "IOX_Control.h"
#include "TONE_Control.h"
#include "DA_ProcessTx.h"
#include "APP_Process.h"
#include "RS232_Tx.h"
#include "SRC_Control.h"
#include "PT_Control.h"

static Trim_Menu_Type __ui_trim_menu;
static signed char __ui_trim_prev_balance;

//==============================================================================
// Private Function Prototypes
//==============================================================================
static void ui_trim_keypress(uint32_t code);
static void ui_trim_keyhold(uint32_t code);
static bool ui_trim_menu_validate(Trim_Menu_Type menu);
static void ui_trim_menu_up(void);
static void ui_trim_menu_down(void);
static void ui_trim_rampbal(signed char new_balpos);
static bool ui_trim_balance_adjust(bool dir);
/** handlers -----------------------------------------------------------------*/
static bool ui_trim_balance_handler(uint32_t code);
static bool ui_trim_input_trim_handler(uint32_t code);
static bool ui_trim_tone_handler(uint32_t code);
static bool ui_trim_treble_handler(uint32_t code);
static bool ui_trim_bass_handler(uint32_t code);
static bool ui_trim_mc_phono_handler(uint32_t code);
static bool ui_trim_mm_phono_handler(uint32_t code);
static bool ui_trim_mono_handler(uint32_t code);
static bool ui_trim_trig_meterlight_handler(uint32_t code);
static bool ui_trim_pt_meterlight_handler(uint32_t code);
static bool ui_trim_brightness_handler(uint32_t code);
static bool ui_trim_lipsync_handler(uint32_t code);
static bool ui_trim_hxd_handler(uint32_t code);
/** display routines ---------------------------------------------------------*/
static void ui_trim_balance_display(bool both);
static void ui_trim_input_trim_display(bool both);
static void ui_trim_tone_display(bool both);
static void ui_trim_treble_display(bool both);
static void ui_trim_bass_display(bool both);
static void ui_trim_mc_phono_display(bool both);
static void ui_trim_mm_phono_display(bool both);
static void ui_trim_mono_display(bool both);
static void ui_trim_trig_meterlight_display(bool both);
static void ui_trim_pt_meterlight_display(bool both);
static void ui_trim_brightness_display(bool both);
static void ui_trim_lipsync_display(bool both);
static void ui_trim_hxd_display(bool both);

static bool (* const __trim_handler[E_TRIM_LAST])(uint32_t) = {
    &ui_trim_balance_handler,
    &ui_trim_input_trim_handler,
    &ui_trim_tone_handler,
    &ui_trim_bass_handler,
    &ui_trim_treble_handler,
    &ui_trim_mc_phono_handler,
    &ui_trim_mm_phono_handler,
    &ui_trim_mono_handler,
    &ui_trim_trig_meterlight_handler,
    &ui_trim_pt_meterlight_handler,
    &ui_trim_brightness_handler,
    &ui_trim_lipsync_handler,
    &ui_trim_hxd_handler
};

static void (* const __trim_display[E_TRIM_LAST])(bool) = {
    &ui_trim_balance_display,
    &ui_trim_input_trim_display,
    &ui_trim_tone_display,
    &ui_trim_bass_display,
    &ui_trim_treble_display,
    &ui_trim_mc_phono_display,
    &ui_trim_mm_phono_display,
    &ui_trim_mono_display,
    &ui_trim_trig_meterlight_display,
    &ui_trim_pt_meterlight_display,
    &ui_trim_brightness_display,
    &ui_trim_lipsync_display,
    &ui_trim_hxd_display
};

//==============================================================================
// Public Functions
//==============================================================================
void UI_Trim_Init(void)
{
    if(PT_Active())
        __ui_trim_menu = E_TRIM_METERLIGHT_PT;
    else
        __ui_trim_menu = E_TRIM_FIRST;
}

void UI_Trim_SetMenu(Trim_Menu_Type menu)
{
    __ui_trim_menu = menu;
    __trim_display[__ui_trim_menu](TRUE);
}

Trim_Menu_Type UI_TrimGetMenu(void)
{
    return __ui_trim_menu;
}

void UI_Trim_Process(uint32_t code)
{
    if ((code & C_KEY_HOLD_MASK) == C_KEY_HOLD_MASK)
        ui_trim_keyhold(code);
    else
        ui_trim_keypress(code);
}

void UI_Trim_Display(void)
{
    // Check for invalid menus at Trim re-entry
    if(PT_Active()){
        if((__ui_trim_menu < E_TRIM_METERLIGHT_PT) || (__ui_trim_menu > E_TRIM_BRIGHTNESS))
            __ui_trim_menu = E_TRIM_METERLIGHT_PT;
    }

    if((__ui_trim_menu == E_TRIM_MC_PHONO) && (NVM_GetSrc() != E_SRC_MC_PHONO))
        __ui_trim_menu = E_TRIM_MONO;

    if((__ui_trim_menu == E_TRIM_MM_PHONO) && (NVM_GetSrc() != E_SRC_MM_PHONO))
        __ui_trim_menu = E_TRIM_MONO;

    if((__ui_trim_menu == E_TRIM_HXD) && (OUT_Headphone() != E_HEADPHONE_IN))
        __ui_trim_menu = E_TRIM_BRIGHTNESS;

    if((__ui_trim_menu == E_TRIM_LIPSYNC) && ((NVM_GetLipSyncMode() != E_LIP_SYNC_MANUAL) ||
       (!SRC_IsLipSync(NVM_GetSrc()))))
            __ui_trim_menu = E_TRIM_BRIGHTNESS;

    if((__ui_trim_menu == E_TRIM_METERLIGHT) && PT_Active())
        __ui_trim_menu = E_TRIM_METERLIGHT_PT;

    if((__ui_trim_menu == E_TRIM_METERLIGHT_PT) && !PT_Active())
        __ui_trim_menu = E_TRIM_METERLIGHT;

    __trim_display[__ui_trim_menu](TRUE);
}

void UI_Trim_Exit(void)
{

}

//==============================================================================
// Private Functions
//==============================================================================
static void ui_trim_keypress(uint32_t code)
{
    bool update = FALSE;

    switch(code) {
        case C_KEY_TONE:
            UI_Toggle_Tone();
            if(__ui_trim_menu == E_TRIM_TONE)
                ui_trim_tone_display(FALSE);
            if(__ui_trim_menu == E_TRIM_BASS)
                ui_trim_bass_display(FALSE);
            if(__ui_trim_menu == E_TRIM_TREBLE)
                ui_trim_treble_display(FALSE);
            break;
        case C_IR_MODE_BTN:
        case C_KEY_TRIM: // Exit Trim Menu
            UI_Trim_Exit();
            if(PT_Active())
                UI_SetMenu(E_MENU_PASSTHRU);
            else
                UI_SetMenu(E_MENU_MAIN);
            break;
        case C_IR_TRIM_BTN:
        case C_RTY_INPUT_UP:   ui_trim_menu_up();   break;
        case C_RTY_INPUT_DOWN: ui_trim_menu_down(); break;
        case C_IR_LEVEL_UP_BTN:
            update = __trim_handler[__ui_trim_menu](C_RTY_VOLUME_UP);
            break;
        case C_IR_LEVEL_DOWN_BTN:
            update = __trim_handler[__ui_trim_menu](C_RTY_VOLUME_DOWN);
            break;
        case C_RTY_VOLUME_UP:
        case C_RTY_VOLUME_DOWN:
            update = __trim_handler[__ui_trim_menu](code);
            break;
        default:
            break;
    }

    if(update)
        __trim_display[__ui_trim_menu](FALSE);
}

static void ui_trim_keyhold(uint32_t code)
{
    uint32_t key = (code ^ C_KEY_HOLD_MASK);  // mask out the hold bit

    switch(key) {
        case C_KEY_TRIM: // Transition to setup menu
            UI_Trim_Exit();
            if(!PT_Active())
                UI_SetMenu(E_MENU_SETUP);
            break;
        default:
            break;
    }
}

static bool ui_trim_menu_validate(Trim_Menu_Type menu)
{
    bool valid = FALSE;

    switch (menu) {
        case E_TRIM_MC_PHONO:
            if(NVM_GetSrc() == E_SRC_MC_PHONO)
                valid = TRUE;
            break;
        case E_TRIM_MM_PHONO:
            if(NVM_GetSrc() == E_SRC_MM_PHONO)
                valid = TRUE;
            break;
        case E_TRIM_BALANCE:
        case E_TRIM_INPUT_TRIM:
        case E_TRIM_MONO:
        case E_TRIM_METERLIGHT:
        case E_TRIM_BRIGHTNESS:
        case E_TRIM_TONE:
        case E_TRIM_BASS:
        case E_TRIM_TREBLE:
            valid = TRUE;
            break;
        case E_TRIM_LIPSYNC:
            if((NVM_GetLipSyncMode() == E_LIP_SYNC_MANUAL) && (SRC_IsLipSync(NVM_GetSrc())))
                valid = TRUE;
            break;
        case E_TRIM_HXD:
            if(OUT_Headphone() == E_HEADPHONE_IN)
                valid = TRUE;
            break;
        case E_TRIM_METERLIGHT_PT:
            if(!PT_Active())
                valid = FALSE;
            else
                valid = TRUE;
            break;

        default:
            break;
    }
    return valid;
}

static void ui_trim_menu_up(void)
{
    Trim_Menu_Type prev_menu = __ui_trim_menu;
    Trim_Menu_Type next_menu = (Trim_Menu_Type) (prev_menu + 1);

    /* search for next valid menu */
    while (__ui_trim_menu == prev_menu) {
        if(PT_Active()){
            if (next_menu == E_TRIM_LIPSYNC)
                next_menu = E_TRIM_METERLIGHT_PT;
        }
        else{
            if (next_menu == E_TRIM_LAST)
                next_menu = E_TRIM_FIRST;
        }

        if (ui_trim_menu_validate(next_menu))
            __ui_trim_menu = next_menu;
        else
            next_menu++;
    }

    __trim_display[__ui_trim_menu](TRUE);
}

static void ui_trim_menu_down(void)
{
    Trim_Menu_Type prev_menu = __ui_trim_menu;
    int8_t next_menu = prev_menu - 1;

    /* search for next valid menu */
    while (__ui_trim_menu == prev_menu) {
        if(PT_Active()){
            if (next_menu == E_TRIM_METERLIGHT)
                next_menu = E_TRIM_BRIGHTNESS;
        }
        else{
            if (next_menu < 0)
                next_menu = E_TRIM_LAST - 1;
        }
        if (ui_trim_menu_validate((Trim_Menu_Type)next_menu))
            __ui_trim_menu = (Trim_Menu_Type) next_menu;
        else
            next_menu--;
    }

    __trim_display[__ui_trim_menu](TRUE);
}

//=================================================|
// BALANCE ========================================|
//=================================================|
static void ui_trim_balance_display(bool both)
{
    char DispStr[21]; DispStr[20]=0;
	uint8_t result, rem;
    const char DispStatusBar[6] = {0x00,0x01,0x02,0x03,0x04,0x05};

    if(both) {
        DISP_ClearLine(C_DISP_BOTHLINES);
        DISP_Print("L     BALANCE      R\0", C_DISP_LINE1, 0);

        if(NVM_GetBalance() >= 0)
            DISP_FillCGRAM(E_DISP_CGRAM_BAL_R);
        else
            DISP_FillCGRAM(E_DISP_CGRAM_BAL_L);
	}

    /* dB value to tx on rs232 */
    result = (unsigned)abs(NVM_GetBalance());
    snprintf( DispStr, 21, " %u dB\0",(uint8_t)result);

    /* calculate values for displaying balance on VFD */
	result = (uint8_t)abs(NVM_GetBalance())/5;
	rem = (uint8_t)abs(NVM_GetBalance()) - (result * 5);

	if(NVM_GetBalance() == 0) {
		memset(DispStr, 0x20, 20);
		DispStr[9] = 0x06;
		DispStr[10] = 0x01;
		DispStr[20] = '\0';
		DISP_Print(DispStr,C_DISP_LINE2,0);
	}
	else if(NVM_GetBalance() > 0) {
        // print space over neg single bar
        snprintf( DispStr, 21, "%c",0x20);
        DISP_Print(DispStr,C_DISP_LINE2,9);

		for(uint8_t idx = 0; idx < 10; idx++) {
			if(idx < result)
				DispStr[idx] = 0x05;
			else
				DispStr[idx] = 0x20;
		}
		if(rem > 0)
			DispStr[result] = DispStatusBar[rem];

        DispStr[10] = '\0';
        DISP_Print(DispStr,C_DISP_LINE2,10);
	}
	else {
        // print space over pos single bar
        snprintf( DispStr, 21, "%c",0x20);
		DISP_Print(DispStr,C_DISP_LINE2,10);

		for(uint8_t jdx = 0; jdx < 10; jdx++){
			if(jdx < 10 - result)
				DispStr[jdx] = 0x20;
			else
				DispStr[jdx] = 0x01;
		}
		if(rem > 0)
			DispStr[9 - result] = DispStatusBar[6 - rem];
        DispStr[10] = '\0';
        DISP_Print(DispStr,C_DISP_LINE2,0);
	}
}

static bool ui_trim_balance_handler(uint32_t code)
{
    bool update = FALSE;

    if ((code & C_RTY_VOLUME_DOWN) == C_RTY_VOLUME_DOWN)
        update = ui_trim_balance_adjust(DOWN);
    else
        update = ui_trim_balance_adjust(UP);

    if(update)
        RS232_TxBalance();

    return update;
}

static void ui_trim_rampbal(signed char new_balpos)
{
    int8_t balance = NVM_GetBalance();

    if(__ui_trim_prev_balance < 0 && new_balpos >= 0) {
        DISP_ClearLine(C_DISP_LINE2);
        DISP_FillCGRAM(E_DISP_CGRAM_BAL_R);
    }
    else if(__ui_trim_prev_balance == 0 && new_balpos < 0) {
        DISP_ClearLine(C_DISP_LINE2);
        DISP_FillCGRAM(E_DISP_CGRAM_BAL_L);
    }

    do {
        if(balance != new_balpos) {
       		if(balance < new_balpos)
                NVM_SetBalance(balance + 1);
	      	else
                NVM_SetBalance(balance - 1);
		}
		VOL_ChangeVolume(FALSE,TRUE,pNVM_GetVol());
	}while(NVM_GetBalance() != new_balpos);
}

static bool ui_trim_balance_adjust(bool dir)
{
    int8_t balance = NVM_GetBalance();
    __ui_trim_prev_balance = balance;

    if(dir) {
        if(balance < 50) {
            ui_trim_rampbal(balance < 50 ? balance + 1 : 50);
            return(TRUE);
        }
    }
    else {
        if(balance > -50) {
            ui_trim_rampbal(balance > -50 ? balance - 1 : -50);
            return(TRUE);
        }
    }

    return(FALSE);
}

//=================================================|
// INPUT TRIM =====================================|
//=================================================|
static void ui_trim_input_trim_display(bool both)
{
    char level_str[9];
    int8_t level = NVM_GetLevel();
    uint8_t fullstep = abs(level) / 2;
    uint8_t halfstep = abs(level) % 2;

    if(both)
        DISP_PrintCenter(C_DISP_LINE1,"INPUT TRIM\0");

	if(level == 0)
        DISP_PrintCenter(C_DISP_LINE2, " 0.0 dB\0");
	else if(level > 0) {
        snprintf( level_str, 9, "+%d.%d dB\0",fullstep, halfstep*5);
        DISP_PrintCenter(C_DISP_LINE2,level_str);
	}
	else {
        snprintf( level_str, 9, "-%d.%d dB\0",fullstep, halfstep*5);
        DISP_PrintCenter(C_DISP_LINE2,level_str);
	}
}

static bool ui_trim_input_trim_handler(uint32_t code)
{
    bool update = FALSE;
    int8_t level = NVM_GetLevel(); // current level

    // adjust level as indicated, updating volume ic as required
    if ((code & C_RTY_VOLUME_DOWN) == C_RTY_VOLUME_DOWN) {
        if (level > (int8_t)-12) {
            NVM_SetLevel(level-1);
            VOL_ChangeVolume(FALSE,TRUE,pNVM_GetVol());
            update = TRUE;
        }
    }
    else {
        if (level < (int8_t)12) {
            NVM_SetLevel(level+1);
            VOL_ChangeVolume(FALSE,TRUE,pNVM_GetVol());
            update = TRUE;
        }
    }

    if(update)
        RS232_TxInputTrim();

    return update;
}

//=================================================|
// TONE ===========================================|
//=================================================|
static void ui_trim_tone_display(bool both)
{
    if(both)
        DISP_PrintCenter(C_DISP_LINE1,"TONE CONTROLS\0");

    if (NVM_GetTone(NVM_GetSrc()))
        DISP_PrintCenter(C_DISP_LINE2,"On\0");
    else
        DISP_PrintCenter(C_DISP_LINE2,"Off\0");
}

static bool ui_trim_tone_handler(uint32_t code)
{
    bool update = FALSE;

    if((code & C_RTY_VOLUME_DOWN) == C_RTY_VOLUME_DOWN) {
        if(NVM_GetTone(NVM_GetSrc()) == ON) {
            DISP_PrintCenter(C_DISP_LINE2,"Off\0");
            UI_Toggle_Tone();
            update = TRUE;
        }
    }
    else {
        if(NVM_GetTone(NVM_GetSrc()) == OFF) {
            DISP_PrintCenter(C_DISP_LINE2,"On\0");
            UI_Toggle_Tone();
            update = TRUE;
        }
    }
    return update;
}

static bool ui_trim_treble_handler(uint32_t code)
{
    bool update = FALSE;

    if(NVM_GetTone(NVM_GetSrc()) == ON) {
        if ((code & C_RTY_VOLUME_DOWN) == C_RTY_VOLUME_DOWN)
            update = TONE_TrebleAdjust(DOWN);
        else
            update = TONE_TrebleAdjust(UP);
    }

    if(update)
        RS232_TxTreble();

    return update;
}

static bool ui_trim_bass_handler(uint32_t code)
{
    bool update = FALSE;

    if(NVM_GetTone(NVM_GetSrc()) == ON) {
        if((code & C_RTY_VOLUME_DOWN) == C_RTY_VOLUME_DOWN)
            update = TONE_BassAdjust(DOWN);
        else
            update = TONE_BassAdjust(UP);
    }

    if(update)
        RS232_TxBass();

    return update;
}

static void ui_trim_bass_display(bool both)
{
    char tmp_str[8]; tmp_str[7]=0;

    if(both) {
        DISP_ClearLine(C_DISP_BOTHLINES);
        DISP_PrintCenter(C_DISP_LINE1, "BASS\0");
    }

    if(NVM_GetTone(NVM_GetSrc()) == ON) {
        if(NVM_GetBass(NVM_GetSrc()) > 0) {
            if(NVM_GetBass(NVM_GetSrc()) > 9)
                snprintf(tmp_str, 8, "+%ddB\0", NVM_GetBass(NVM_GetSrc()));
            else
                snprintf(tmp_str, 8, "+%d dB\0", NVM_GetBass(NVM_GetSrc()));

            DISP_PrintCenter(C_DISP_LINE2,tmp_str);
        }
        else if(NVM_GetBass(NVM_GetSrc()) < 0) {
            if(NVM_GetBass(NVM_GetSrc()) > -10)
                snprintf(tmp_str, 8, "%d dB\0", NVM_GetBass(NVM_GetSrc()));
            else
                snprintf(tmp_str, 8, "%ddB\0", NVM_GetBass(NVM_GetSrc()));

            DISP_PrintCenter(C_DISP_LINE2,tmp_str);
        }
        else
            DISP_PrintCenter(C_DISP_LINE2, " 0 dB\0");
    }
    else
        DISP_PrintCenter(C_DISP_LINE2, "Tone Off\0");
}

static void ui_trim_treble_display(bool both)
{
    char tmp_str[8];  tmp_str[7]=0;

    if(both) {
        DISP_ClearLine(C_DISP_BOTHLINES);
        DISP_PrintCenter(C_DISP_LINE1, "TREBLE\0");
    }

    if(NVM_GetTone(NVM_GetSrc()) == ON) {
        if(NVM_GetTreble(NVM_GetSrc()) > 0) {
            if(NVM_GetTreble(NVM_GetSrc()) > 9)
                snprintf(tmp_str, 8, "+%ddB\0", NVM_GetTreble(NVM_GetSrc()));
            else
                snprintf(tmp_str, 8, "+%d dB\0", NVM_GetTreble(NVM_GetSrc()));

            DISP_PrintCenter(C_DISP_LINE2,tmp_str);
        }
        else if(NVM_GetTreble(NVM_GetSrc()) < 0) {

            if(NVM_GetTreble(NVM_GetSrc()) > -10)
                snprintf(tmp_str, 8, "%d dB\0", NVM_GetTreble(NVM_GetSrc()));
            else
                snprintf(tmp_str, 8, "%ddB\0", NVM_GetTreble(NVM_GetSrc()));

            DISP_PrintCenter(C_DISP_LINE2,tmp_str);
        }
        else
            DISP_PrintCenter(C_DISP_LINE2, " 0 dB\0");
    }
    else
        DISP_PrintCenter(C_DISP_LINE2, "Tone Off\0");
}

//=================================================|
// MC PHONO =======================================|
//=================================================|
static void ui_trim_mc_phono_display(bool both)
{
    char ohm25_char[4]  = {0x32,0x35,0x9A,'\0'};
    char ohm50_char[4]  = {0x35,0x30,0x9A,'\0'};
    char ohm100_char[5] = {0x31,0x30,0x30,0x9A,'\0'};
    char ohm200_char[5] = {0x32,0x30,0x30,0x9A,'\0'};
    char ohm400_char[5] = {0x34,0x30,0x30,0x9A,'\0'};
    char ohm1K_char[6]  = {0x31,0x30,0x30,0x30,0x9A,'\0'};

    if(both)
        DISP_PrintCenter(C_DISP_LINE1,"PHONO RESISTANCE\0");

    switch(NVM_GetMcPhonoRes()) {
        case E_PRES_25_OHMS:  DISP_PrintCenter(C_DISP_LINE2,ohm25_char);  break;
        case E_PRES_50_OHMS:  DISP_PrintCenter(C_DISP_LINE2,ohm50_char);  break;
        case E_PRES_100_OHMS: DISP_PrintCenter(C_DISP_LINE2,ohm100_char); break;
        case E_PRES_200_OHMS: DISP_PrintCenter(C_DISP_LINE2,ohm200_char); break;
        case E_PRES_400_OHMS: DISP_PrintCenter(C_DISP_LINE2,ohm400_char); break;
        case E_PRES_1K_OHMS:  DISP_PrintCenter(C_DISP_LINE2,ohm1K_char);  break;
    }
}

static bool ui_trim_mc_phono_handler(uint32_t code)
{
    bool update = FALSE;

    if ((code & C_RTY_VOLUME_DOWN) == C_RTY_VOLUME_DOWN) {
        if(NVM_GetMcPhonoRes() != E_PRES_FIRST) {
            NVM_SetMcPhonoRes((Phono_Res_Type) (NVM_GetMcPhonoRes() - 1));
            IOX_PhonoResistance(NVM_GetMcPhonoRes(), TRUE);
            update = TRUE;
        }
    }
    else {
        if(NVM_GetMcPhonoRes() != E_PRES_LAST) {
            NVM_SetMcPhonoRes((Phono_Res_Type) (NVM_GetMcPhonoRes() + 1));
            IOX_PhonoResistance(NVM_GetMcPhonoRes(), TRUE);
            update = TRUE;
        }
    }

    if(update)
        RS232_TxPhonoRes();

    return update;
}

//=================================================|
// MM PHONO =======================================|
//=================================================|
static void ui_trim_mm_phono_display(bool both)
{
    if(both)
        DISP_PrintCenter(C_DISP_LINE1,"PHONO CAPACITANCE\0");

    switch(NVM_GetMmPhonoCap()) {
        case E_PCAP_50_PF:  DISP_PrintCenter(C_DISP_LINE2,"50 pF");  break;
        case E_PCAP_100_PF: DISP_PrintCenter(C_DISP_LINE2,"100 pF"); break;
        case E_PCAP_150_PF: DISP_PrintCenter(C_DISP_LINE2,"150 pF"); break;
        case E_PCAP_200_PF: DISP_PrintCenter(C_DISP_LINE2,"200 pF"); break;
        case E_PCAP_250_PF: DISP_PrintCenter(C_DISP_LINE2,"250 pF"); break;
        case E_PCAP_300_PF: DISP_PrintCenter(C_DISP_LINE2,"300 pF"); break;
        case E_PCAP_350_PF: DISP_PrintCenter(C_DISP_LINE2,"350 pF"); break;
        case E_PCAP_400_PF: DISP_PrintCenter(C_DISP_LINE2,"400 pF"); break;
        case E_PCAP_450_PF: DISP_PrintCenter(C_DISP_LINE2,"450 pF");  break;
        case E_PCAP_500_PF: DISP_PrintCenter(C_DISP_LINE2,"500 pF"); break;
        case E_PCAP_550_PF: DISP_PrintCenter(C_DISP_LINE2,"550 pF"); break;
        case E_PCAP_600_PF: DISP_PrintCenter(C_DISP_LINE2,"600 pF"); break;
        case E_PCAP_650_PF: DISP_PrintCenter(C_DISP_LINE2,"650 pF"); break;
        case E_PCAP_700_PF: DISP_PrintCenter(C_DISP_LINE2,"700 pF"); break;
        case E_PCAP_750_PF: DISP_PrintCenter(C_DISP_LINE2,"750 pF"); break;
        case E_PCAP_800_PF: DISP_PrintCenter(C_DISP_LINE2,"800 pF"); break;
    }
}

static bool ui_trim_mm_phono_handler(uint32_t code)
{
    bool update = FALSE;

    if ((code & C_RTY_VOLUME_DOWN) == C_RTY_VOLUME_DOWN) {
        if(NVM_GetMmPhonoCap() != E_PCAP_FIRST) {
            NVM_SetMmPhonoCap((Phono_Cap_Type) (NVM_GetMmPhonoCap() - 1));
            IOX_PhonoCapacitance(NVM_GetMmPhonoCap(), TRUE);
            update = TRUE;
        }
    }
    else {
        if(NVM_GetMmPhonoCap() != E_PCAP_LAST) {
            NVM_SetMmPhonoCap((Phono_Cap_Type) (NVM_GetMmPhonoCap() + 1));
            IOX_PhonoCapacitance(NVM_GetMmPhonoCap(), TRUE);
            update = TRUE;
        }
    }

    if(update)
        RS232_TxPhonoCap();

    return update;
}

//=================================================|
// MONO/STEREO ====================================|
//=================================================|
static void ui_trim_mono_display(bool both)
{
    char vfd[20];  vfd[19]=0;

    if(both)
        DISP_PrintCenter(C_DISP_LINE1,"MONO / STEREO\0");

    if(NVM_GetMono(NVM_GetSrc()))
        snprintf(vfd, 20, "----         \0");
    else
        snprintf(vfd, 20, "       ------\0");

    DISP_PrintCenter(C_DISP_LINE2,vfd);
}

static bool ui_trim_mono_handler(uint32_t code)
{
    bool update = FALSE;

    if((code & C_RTY_VOLUME_DOWN) == C_RTY_VOLUME_DOWN) {
        if(NVM_GetMono(NVM_GetSrc()) == OFF) {
            NVM_SetMono(NVM_GetSrc(), ON);
            IOX_Mono(ON);

            update = TRUE;
        }
    }
    else {
        if(NVM_GetMono(NVM_GetSrc()) == ON) {
            NVM_SetMono(NVM_GetSrc(), OFF);
            IOX_Mono(OFF);

            update = TRUE;
        }
    }

    if(update)
        RS232_TxMono();

    return update;
}

//=================================================|
// Trigger Meterlight =============================|
//=================================================|
static void ui_trim_trig_meterlight_display(bool both)
{
    char vfd[20];  vfd[19]=0;

    if(both)
        DISP_PrintCenter(C_DISP_LINE1,"AMP METERLIGHT\0");

    snprintf(vfd, 20, "%3s\0", NVM_GetTrigMeterlight() ? " On" : "Off");
    DISP_PrintCenter(C_DISP_LINE2,vfd);
}

static bool ui_trim_trig_meterlight_handler(uint32_t code)
{
    bool update = FALSE;

    if ((code & C_RTY_VOLUME_DOWN) == C_RTY_VOLUME_DOWN) {
        if(NVM_GetTrigMeterlight() == ON) {
            NVM_SetTrigMeterlight(OFF);
            IOX_TrigMeterlight(OFF);

            update = TRUE;
        }
    }
    else {
        if(NVM_GetTrigMeterlight() == OFF) {
            NVM_SetTrigMeterlight(ON);
            IOX_TrigMeterlight(ON);
            update = TRUE;
        }
    }

    if(update)
        RS232_TxTrigMeterlight();

    return update;
}

//=================================================|
// METERLIGHTS PT =================================|
//=================================================|
static void ui_trim_pt_meterlight_display(bool both)
{

    if(both)
        DISP_PrintCenter(C_DISP_LINE1,"AMP METER LIGHT");

    switch(NVM_GetMeterLightPT()){
        case E_METER_PT_OFF:
            DISP_PrintCenter(C_DISP_LINE2,"Off\0");
            break;
        case E_METER_PT_ON:
            DISP_PrintCenter(C_DISP_LINE2,"On\0");
            break;
        case E_METER_PT_TRIG:
            DISP_PrintCenter(C_DISP_LINE2,"Trigger\0");
            break;
    }
}

static bool ui_trim_pt_meterlight_handler(uint32_t code)
{
    bool update = FALSE;

    if ((code & C_RTY_VOLUME_DOWN) == C_RTY_VOLUME_DOWN) {
        if(NVM_GetMeterLightPT() < E_METER_PT_MAX) { // 3 is minimum brightness
            NVM_SetMeterLightPT((Meter_PT_Type)(NVM_GetMeterLightPT()+1));
            update = TRUE;
        }
    }
    else {
        if(NVM_GetMeterLightPT() > E_METER_PT_MIN) { // 0 is maximum brightness
            NVM_SetMeterLightPT((Meter_PT_Type)(NVM_GetMeterLightPT()-1));
            update = TRUE;
        }
    }

    switch(NVM_GetMeterLightPT()){
        case E_METER_PT_OFF:
            IOX_TrigMeterlight(OFF);
            break;
        case E_METER_PT_ON:
            IOX_TrigMeterlight(ON);
            break;
        case E_METER_PT_TRIG:
            IOX_TrigMeterlight(!(PIOA->PIO_PDSR & PIN13_PASS_LGT_CTL));
            break;
    }

    if(update)
        RS232_TxTrigMeterlight();

    return update;
}

//=================================================|
// BRIGHTNESS =====================================|
//=================================================|
static void ui_trim_brightness_display(bool both)
{
    if(both)
        DISP_PrintCenter(C_DISP_LINE1,"DISPLAY");

    DISP_PrintCenter(C_DISP_LINE2,"BRIGHTNESS");
}

static bool ui_trim_brightness_handler(uint32_t code)
{
    bool update = FALSE;

    if(PT_Active()){
        if ((code & C_RTY_VOLUME_DOWN) == C_RTY_VOLUME_DOWN) {
            if(NVM_GetBrightnessPT() < E_DISP_DUTY_MAX) { // 3 is minimum brightness
                DISP_SetDutyCycle((Disp_Duty_Type)(NVM_GetBrightnessPT()+1));
                NVM_SetBrightnessPT((Disp_Duty_Type)(NVM_GetBrightnessPT()+1));
                update = TRUE;
            }
        }
        else {
            if(NVM_GetBrightnessPT() > E_DISP_DUTY_MIN) { // 0 is maximum brightness
                DISP_SetDutyCycle((Disp_Duty_Type)(NVM_GetBrightnessPT()-1));
                NVM_SetBrightnessPT((Disp_Duty_Type)(NVM_GetBrightnessPT()-1));
                update = TRUE;
            }
        }
    }
    else{
        if ((code & C_RTY_VOLUME_DOWN) == C_RTY_VOLUME_DOWN) {
            if(NVM_GetBrightness() < E_DISP_DUTY_MAX) { // 3 is minimum brightness
                DISP_SetDutyCycle((Disp_Duty_Type)(NVM_GetBrightness()+1));
                NVM_SetBrightness((Disp_Duty_Type)(NVM_GetBrightness()+1));
                update = TRUE;
            }
        }
        else {
            if(NVM_GetBrightness() > E_DISP_DUTY_MIN) { // 0 is maximum brightness
                DISP_SetDutyCycle((Disp_Duty_Type)(NVM_GetBrightness()-1));
                NVM_SetBrightness((Disp_Duty_Type)(NVM_GetBrightness()-1));
                update = TRUE;
            }
        }
    }

    if(update)
        RS232_TxBrightness();

    return update;
}

//=================================================|
// Lip Sync =======================================|
//=================================================|
static void ui_trim_lipsync_display(bool both)
{
    char vfd[20];  vfd[19]=0;

    if(both)
        DISP_PrintCenter(C_DISP_LINE1,"HDMI LIP SYNC DELAY");

    snprintf(vfd, 20, "%3u ms\0", (uint8_t)NVM_GetLipSyncTime());
    DISP_PrintCenter(C_DISP_LINE2,vfd);
}

static bool ui_trim_lipsync_handler(uint32_t code)
{
    bool update = FALSE;

    if ((code & C_RTY_VOLUME_DOWN) == C_RTY_VOLUME_DOWN) {
        if(NVM_GetLipSyncTime() != E_LIP_TIME_FIRST) {
            NVM_SetLipSyncTime((Lip_Time_Type) (NVM_GetLipSyncTime() - 10));
            DA_TxLipSyncDelay(NVM_GetLipSyncTime());
            update = TRUE;
        }
    }
    else {
        if(NVM_GetLipSyncTime() != E_LIP_TIME_LAST) {
            NVM_SetLipSyncTime((Lip_Time_Type) (NVM_GetLipSyncTime() + 10));
            DA_TxLipSyncDelay(NVM_GetLipSyncTime());
            update = TRUE;
        }
    }

    if(update)
        RS232_TxLipSyncDelay();

    return update;
}

//=================================================|
// HXD ============================================|
//=================================================|
static void ui_trim_hxd_display(bool both)
{
    if(both)
        DISP_PrintCenter(C_DISP_LINE1,"HEADPHONE HXD");

    if (NVM_GetHXD() == ON)
        DISP_PrintCenter(C_DISP_LINE2,"On");
    else
        DISP_PrintCenter(C_DISP_LINE2,"Off");
}

static bool ui_trim_hxd_handler(uint32_t code)
{
    bool update = FALSE;

    if ((code & C_RTY_VOLUME_DOWN) == C_RTY_VOLUME_DOWN) {
        if (NVM_GetHXD() == ON) {
            UI_Set_HXD(OFF);
            update = TRUE;
        }
    }
    else {
        if (NVM_GetHXD() == OFF) {
            UI_Set_HXD(ON);
            update = TRUE;
        }
    }

    if(update)
        RS232_TxHpHxd();

    return update;
}

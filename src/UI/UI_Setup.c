//-----------------------------------------------------------------------------|
//	UI_Setup.c
//	Abstract: Setup Menu UI module
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include <string.h>
#include "Common.h"
#include "UI_Setup.h"
#include "UI_Control.h"
#include "UI_Common.h"
#include "UI_Function.h"
#include "UI_Rename.h"
#include "NVM_Access.h"
#include "NVM_Control.h"
#include "DISP_Control.h"
#include "SRC_Control.h"
#include "VOL_Control.h"
#include "APP_Process.h"
#include "PRODUCT_Config.h"
#include "OUT_Control.h"
#include "UART_Driver.h"
#include "IOX_Control.h"
#include "TRIG_Control.h"
#include "IR_Codes.h"
#include "DA_ProcessTx.h"

#define kFirmwareVersionStringMaxLen 16
#define kFW_Code_StringMaxLen 6

static uint32_t __setup_menu;
static uint8_t  __da_fw_mode, __da_fw_idx, __da_fw_hold;

static uint8_t __fw_mode, __fw_idx, __fw_hold;  // fw update hold delay
static char    __fw_code[kFW_Code_StringMaxLen] = "XXXXX\0";
static bool    gFW_code_menu;

static Src_Type __name_idx,__trig_name_idx;

static bool    __inputs_top;
static bool    __outputs_top;
static uint8_t __outputs_id;
static bool    __dig_gain_top;
static uint8_t __dig_gain_id;

static bool    __trig_top, __trig_input_top;
static Trig_Id_Type __trig_id;

static bool    __dataports_top;
static Dport_Id_Type __dataports_id;

static uint8_t __factoryrst_mode;

//==============================================================================
// Private Function Prototypes
//==============================================================================
static void ui_setup_menu_up(void);
static void ui_setup_menu_down(void);

/** Sub menu handler functions -----------------------------------------------*/
static void ui_setup_fw_ver_handler(uint32_t code);
static void ui_setup_da_ver_handler(uint32_t code);
static void ui_setup_inputs_handler(uint32_t code);
static void ui_setup_outputs_handler(uint32_t code);
static void ui_setup_trig_handler(uint32_t code);
static void ui_setup_dataports_handler(uint32_t code);
static void ui_setup_passthru_handler(uint32_t code);
static void ui_setup_usb_automute_handler(uint32_t code);
static void ui_setup_hdmi_cec_handler(uint32_t code);
static void ui_setup_hdmi_cec_pwr_handler(uint32_t code);
static void ui_setup_hdmi_lip_sync_handler(uint32_t code);
static void ui_setup_dig_gain_handler(uint32_t code);
static void ui_setup_rs232_handler(uint32_t code);
static void ui_setup_ircodes_handler(uint32_t code);
static void ui_setup_irdisable_handler(uint32_t code);
static void ui_setup_autooff_handler(uint32_t code);
static void ui_setup_factoryrst_handler(uint32_t code);

static void ui_setup_fw_ver_display(bool both);
static void ui_setup_da_ver_display(bool both);
static void ui_setup_inputs_display(bool both);
static void ui_setup_outputs_display(bool both);
static void ui_setup_trig_display(bool both);
static void ui_setup_dataports_display(bool both);
static void ui_setup_passthru_display(bool both);
static void ui_setup_usb_automute_display(bool both);
static void ui_setup_hdmi_cec_display(bool both);
static void ui_setup_hdmi_cec_pwr_display(bool both);
static void ui_setup_hdmi_lip_sync_display(bool both);
static void ui_setup_dig_gain_display(bool both);
static void ui_setup_rs232_display(bool both);
static void ui_setup_ircodes_display(bool both);
static void ui_setup_irdisable_display(bool both);
static void ui_setup_autooff_display(bool both);
static void ui_setup_factoryrst_display(bool both);

/* array access to individual handlers */
static void (* const __setup_handler[E_SETUP_NUM])(uint32_t) = {
    &ui_setup_fw_ver_handler,
    &ui_setup_da_ver_handler,
    &ui_setup_inputs_handler,
    &ui_setup_outputs_handler,
    &ui_setup_trig_handler,
    &ui_setup_dataports_handler,
    &ui_setup_passthru_handler,
    &ui_setup_usb_automute_handler,
    &ui_setup_hdmi_cec_handler,
    &ui_setup_hdmi_cec_pwr_handler,
    &ui_setup_hdmi_lip_sync_handler,
    &ui_setup_dig_gain_handler,
    &ui_setup_rs232_handler,
    &ui_setup_ircodes_handler,
    &ui_setup_irdisable_handler,
    &ui_setup_autooff_handler,
    &ui_setup_factoryrst_handler
};

/* array access to individual display functions */
static void (* const __setup_display[E_SETUP_NUM])(bool) = {
    &ui_setup_fw_ver_display,
    &ui_setup_da_ver_display,
    &ui_setup_inputs_display,
    &ui_setup_outputs_display,
    &ui_setup_trig_display,
    &ui_setup_dataports_display,
    &ui_setup_passthru_display,
    &ui_setup_usb_automute_display,
    &ui_setup_hdmi_cec_display,
    &ui_setup_hdmi_cec_pwr_display,
    &ui_setup_hdmi_lip_sync_display,
    &ui_setup_dig_gain_display,
    &ui_setup_rs232_display,
    &ui_setup_ircodes_display,
    &ui_setup_irdisable_display,
    &ui_setup_autooff_display,
    &ui_setup_factoryrst_display
};
//==============================================================================
// Public Functions
//==============================================================================
void UI_Setup_Init(void)
{
    __da_fw_mode = __da_fw_idx = __da_fw_hold = 0;
    __fw_mode = __fw_idx = __fw_hold = 0;
    gFW_code_menu = FALSE;
    __name_idx = __trig_name_idx = E_SRC_FIRST;
    __factoryrst_mode = 0;
    __setup_menu = 0;
    __outputs_top = TRUE;
    __outputs_id = 0;
    __dig_gain_top = TRUE;
    __dig_gain_id = 0;
    __inputs_top = TRUE;

    __trig_top = TRUE;
    __trig_input_top = FALSE;
    __trig_id = E_TRIG_ID1;

    __dataports_top = TRUE;
    __dataports_id = E_DPORT_ID1;
}

void UI_Setup_Process(uint32_t code)
{
    static uint8_t opt_skip;

    switch(code) {
        case C_SETUP_OPT1_NEXT:
        case C_SETUP_OPT1_PREV:
            if(opt_skip == 0) {
                opt_skip = 4;
                __setup_handler[__setup_menu](code);
            }
            else
                opt_skip--;
            break;
        case C_KEY_SETUP:
        case C_KEY_MUTE:
        case C_SETUP_MENU_NEXT:
        case C_SETUP_MENU_PREV:
        case C_SETUP_HOLD:
        case C_SETUP_HOLD_LATCH:
        case C_IR_TRIM_BTN:
        case IR_NUMBER_1: case IR_NUMBER_2: case IR_NUMBER_3: case IR_NUMBER_4:
        case IR_NUMBER_5: case IR_NUMBER_6: case IR_NUMBER_7: case IR_NUMBER_8:
        case IR_NUMBER_9: case IR_NUMBER_0:
            __setup_handler[__setup_menu](code);
            break;
        default:            // do nothing
            break;
    }
}

void UI_Setup_Display(void)
{
    DISP_ClearLine(C_DISP_BOTHLINES);
    __setup_display[__setup_menu](TRUE);
}

void UI_SetupExit(void)
{
    // Check if Returning Src is Off
    if(!NVM_SrcIsOn(NVM_GetSrc()))
        SRC_Up(NO,NO); // Set next valid source

    // Check if Returning Src is the PT SRC
    if(NVM_GetPtSrc() == NVM_GetSrc())
        SRC_Up(NO,NO); // Set next valid source

    // re-init setup menu so it is re-entered at the top level
    UI_Setup_Init(); // clear setup menu
    UI_SetMenu(E_MENU_MAIN);
}

bool UI_SetupInFwCodeMenu(void)
{
    return gFW_code_menu;
}

//==============================================================================
// Private Functions
//==============================================================================
static void ui_setup_menu_up(void)
{
    if(__setup_menu == E_SETUP_LAST)
        __setup_menu = E_SETUP_FIRST;
    else
        __setup_menu += 1;

    if(DA_Installed()){
        if(DA1_Installed()){
            if(__setup_menu == E_SETUP_HDMI_CEC)
                __setup_menu = E_SETUP_RS232;
        }
        else{
            if(__setup_menu == E_SETUP_USB_AUTOMUTE)
                __setup_menu = E_SETUP_HDMI_CEC;
        }
    }
    else{
        if(__setup_menu == E_SETUP_DAVER)
            __setup_menu = E_SETUP_INPUTS;
        if(__setup_menu == E_SETUP_USB_AUTOMUTE)
            __setup_menu = E_SETUP_RS232;
    }

    __setup_display[__setup_menu](TRUE);
}

static void ui_setup_menu_down(void)
{
    if(__setup_menu == E_SETUP_FIRST)
        __setup_menu = E_SETUP_LAST;
    else
        __setup_menu -= 1;

    if(DA_Installed()){
        if(DA1_Installed()){
            if(__setup_menu == E_SETUP_DIG_GAIN)
                __setup_menu = E_SETUP_USB_AUTOMUTE;
        }
        else{
            if(__setup_menu == E_SETUP_USB_AUTOMUTE)
                __setup_menu = E_SETUP_PASSTHRU;
        }
    }
    else{
        if(__setup_menu == E_SETUP_DAVER)
            __setup_menu = E_SETUP_FWVER;

        if(__setup_menu == E_SETUP_DIG_GAIN)
            __setup_menu = E_SETUP_PASSTHRU;
    }

    __setup_display[__setup_menu](TRUE);
}

//===============================================|
// FIRMWARE VERSION DISPLAY =====================|
//===============================================|
static void ui_setup_fw_ver_display(bool both)
{
    char str[kFirmwareVersionStringMaxLen+1];  str[kFirmwareVersionStringMaxLen]=0;

    gFW_code_menu = TRUE;

    if(both) {
        if(__fw_mode == 0) {
            snprintf( str, kFirmwareVersionStringMaxLen, " %-6s V%s ", PRODUCT_Name(), PRODUCT_Version());
            DISP_PrintCenter(C_DISP_LINE1, str);
        }
        else if(__fw_mode == 1)
            DISP_PrintCenter(C_DISP_LINE1, "ENTER CODE\0");
        else
            DISP_PrintCenter(C_DISP_LINE1, "FIRMWARE UPDATE\0");
    }

    switch(__fw_mode) {
        case 0:
            snprintf( str, kFirmwareVersionStringMaxLen, "S/N: %s\0", NVM_SerialString);
            DISP_PrintCenter(C_DISP_LINE2, str);
            break;
        case 1:
            snprintf( str, kFirmwareVersionStringMaxLen, "%s\0", __fw_code);
            DISP_PrintCenter(C_DISP_LINE2, str);
            break;
        case 2:
            DISP_PrintCenter(C_DISP_LINE2, "MODE\0");
            break;
        default:
            break;
    }
}

static void ui_setup_fw_ver_handler(uint32_t code)
{
    if(__fw_mode == 0) {
        switch(code) {
            case C_KEY_SETUP: UI_SetupExit(); break;
            case C_SETUP_MENU_NEXT:
                gFW_code_menu = FALSE;
                __fw_hold = 0;
                ui_setup_menu_up();
                break;
            case C_SETUP_MENU_PREV:
                gFW_code_menu = FALSE;
                __fw_hold = 0;
                ui_setup_menu_down();
                break;
            case C_SETUP_HOLD:
                if(++__fw_hold > 5) {
                    __fw_mode = 1;
                    __fw_hold = 0;
                    ui_setup_fw_ver_display(TRUE);
                }
                break;
            default:
                break;
        }
    }
    else {
        switch(code) {
            case C_KEY_SETUP: /* escape to top level setup */
                __fw_mode = __fw_idx = 0;
                strncpy(__fw_code,"XXXXX",5);
                ui_setup_fw_ver_display(TRUE);
                break;
            case C_KEY_MUTE:
                __fw_code[__fw_idx] = 'M';
                __fw_idx++;
                ui_setup_fw_ver_display(FALSE);
                break;
            case IR_NUMBER_1:
                __fw_code[__fw_idx] = '1';
                __fw_idx++;
                ui_setup_fw_ver_display(FALSE);
                break;
            case IR_NUMBER_2:
                __fw_code[__fw_idx] = '2';
                __fw_idx++;
                ui_setup_fw_ver_display(FALSE);
                break;
            case IR_NUMBER_3:
                __fw_code[__fw_idx] = '3';
                __fw_idx++;
                ui_setup_fw_ver_display(FALSE);
                break;
            case IR_NUMBER_4:
                __fw_code[__fw_idx] = '4';
                __fw_idx++;
                ui_setup_fw_ver_display(FALSE);
                break;
            case IR_NUMBER_5:
                __fw_code[__fw_idx] = '5';
                __fw_idx++;
                ui_setup_fw_ver_display(FALSE);
                break;
            case IR_NUMBER_6:
                __fw_code[__fw_idx] = '6';
                __fw_idx++;
                ui_setup_fw_ver_display(FALSE);
                break;
            case IR_NUMBER_7:
                __fw_code[__fw_idx] = '7';
                __fw_idx++;
                ui_setup_fw_ver_display(FALSE);
                break;
            case IR_NUMBER_8:
                __fw_code[__fw_idx] = '8';
                __fw_idx++;
                ui_setup_fw_ver_display(FALSE);
                break;
            case IR_NUMBER_9:
                __fw_code[__fw_idx] = '9';
                __fw_idx++;
                ui_setup_fw_ver_display(FALSE);
                break;
            case IR_NUMBER_0:
                __fw_code[__fw_idx] = '0';
                __fw_idx++;
                ui_setup_fw_ver_display(FALSE);
                break;
            default:
                break;
        }
        if(__fw_idx == 5) {// all digits entered
            if(strncmp(__fw_code,"13903",5) == 0) { // match
                NVM_SetFwPrevVersion(PRODUCT_Version());
                NVM_SetMcuFwDownload(TRUE);
                NVM_WriteBank(E_NVM_STATUS);

                DISP_PrintCenter(C_DISP_LINE1, "Ensure USB Cable\0");
                DISP_PrintCenter(C_DISP_LINE2, "is Disconnected\0");
                delay_s(4);

                DISP_PrintCenter(C_DISP_LINE1, "Connect USB Cable\0");
                DISP_PrintCenter(C_DISP_LINE2, "in 30 Seconds\0");
                delay_s(4);

 //               IOX_UsbSwitchPwr(ON);
                delay_ms(200);
 //               IOX_UsbSwitchSel(E_USB_MCU_UPDATE);
                __fw_mode = 2;
                ui_setup_fw_ver_display(TRUE);
                VOL_Mute();
                pmc_enable_periph_clk(ID_UDP);
                /* setup the system for SAMBA */
                cpu_irq_disable();
//                efc_perform_command(EFC0, EFC_FCMD_CLB, 0);  // Clear Lock Bit
//                efc_perform_command(EFC0, EFC_FCMD_CLB, 1);  // Clear Lock Bit
                efc_perform_command(EFC0, EFC_FCMD_CGPB, 1); // Set GPNVM Bit
                NVIC_SystemReset();
            }
            else{// no match
                __fw_idx = 0;
                strncpy(__fw_code,"XXXXX",5);
                ui_setup_fw_ver_display(FALSE);
            }
        }
    }
}

//===============================================|
// DA VERSION ===================================|
//===============================================|
static void ui_setup_da_ver_display(bool both)
{
    char str[kFirmwareVersionStringMaxLen];
    gFW_code_menu = TRUE;

    if (both) {
        if(__da_fw_mode == 1)
            DISP_PrintCenter(C_DISP_LINE1, "ENTER CODE\0");
        else {
            if(DA_Installed()) {
                snprintf( str, kFirmwareVersionStringMaxLen, "DA%u FIRMWARE\0", DA_GetHwVersion());
                DISP_PrintCenter(C_DISP_LINE1,str);
            }
            else
                DISP_PrintCenter(C_DISP_LINE1, "DA MODULE\0");
        }
    }

    switch(__da_fw_mode) {
        case 0:
            if(DA_Installed()) {
                snprintf( str, kFirmwareVersionStringMaxLen, "V%u.0%u\0", DA_MajorVersion(),DA_MinorVersion());
                DISP_PrintCenter(C_DISP_LINE2,str);
            }
            else
                DISP_PrintCenter(C_DISP_LINE2, "Not Installed\0");
            break;
        case 1:
            snprintf( str, kFirmwareVersionStringMaxLen, "%s\0", __fw_code);
            DISP_PrintCenter(C_DISP_LINE2, str);
            break;
        case 2:
            if(DA1_Installed()) {
                DISP_PrintCenter(C_DISP_LINE1, "DA MODULE\0");
                DISP_PrintCenter(C_DISP_LINE2, "UPDATE MODE\0");
            }

            if(DA2_Installed()) {
                DISP_PrintCenter(C_DISP_LINE1, "DA UPDATE MODE\0");
                DISP_PrintCenter(C_DISP_LINE2, "POWER OFF WHEN DONE\0");
            }

            while(1) { };   // Recycle Power for DA UPDATE
            break;
        default:
            break;
    }
}

static void ui_setup_da_ver_handler(uint32_t code)
{
    char fw_ver[5];  fw_ver[4]=0;
    char str[kFirmwareVersionStringMaxLen+1];    str[kFW_Code_StringMaxLen]=0;
    uint8_t count = 30;

    if(__da_fw_mode == 0) {
        switch (code) {
            case C_KEY_SETUP: UI_SetupExit(); break;
            case C_SETUP_MENU_NEXT:
                gFW_code_menu = FALSE;
                __da_fw_hold = 0;
                ui_setup_menu_up();
                break;
            case C_SETUP_MENU_PREV:
                gFW_code_menu = FALSE;
                __da_fw_hold = 0;
                ui_setup_menu_down();
                break;
            case C_SETUP_HOLD:
                __da_fw_hold++; // need a hold first, ensures f/w update doesn't trigger from user holding trim button to enter setup menu
            case C_SETUP_HOLD_LATCH:
                if(DA_Installed()) {
                    if (__da_fw_hold > 0) {
                        __da_fw_hold++;
                        if (__da_fw_hold > 5) {
                            __da_fw_mode = 1;
                            ui_setup_da_ver_display(TRUE);
                            __da_fw_hold = 0;
                        }
                    }
                }
                break;
            default:
                break;
        }
    }
    else {
        switch (code) {
            case C_KEY_SETUP:
                __da_fw_mode = __da_fw_idx = 0;
                strncpy(__fw_code,"XXXXX",5);
                ui_setup_da_ver_display(TRUE);
                break;
            case C_KEY_MUTE:
                __fw_code[__da_fw_idx] = 'M';
                __da_fw_idx++;
                ui_setup_da_ver_display(FALSE);
                break;
            case IR_NUMBER_1:
                __fw_code[__da_fw_idx] = '1';
                __da_fw_idx++;
                ui_setup_da_ver_display(FALSE);
                break;
            case IR_NUMBER_2:
                __fw_code[__da_fw_idx] = '2';
                __da_fw_idx++;
                ui_setup_da_ver_display(FALSE);
                break;
            case IR_NUMBER_3:
                __fw_code[__da_fw_idx] = '3';
                __da_fw_idx++;
                ui_setup_da_ver_display(FALSE);
                break;
            case IR_NUMBER_4:
                __fw_code[__da_fw_idx] = '4';
                __da_fw_idx++;
                ui_setup_da_ver_display(FALSE);
                break;
            case IR_NUMBER_5:
                __fw_code[__da_fw_idx] = '5';
                __da_fw_idx++;
                ui_setup_da_ver_display(FALSE);
                break;
            case IR_NUMBER_6:
                __fw_code[__da_fw_idx] = '6';
                __da_fw_idx++;
                ui_setup_da_ver_display(FALSE);
                break;
            case IR_NUMBER_7:
                __fw_code[__da_fw_idx] = '7';
                __da_fw_idx++;
                ui_setup_da_ver_display(FALSE);
                break;
            case IR_NUMBER_8:
                __fw_code[__da_fw_idx] = '8';
                __da_fw_idx++;
                ui_setup_da_ver_display(FALSE);
                break;
            case IR_NUMBER_9:
                __fw_code[__da_fw_idx] = '9';
                __da_fw_idx++;
                ui_setup_da_ver_display(FALSE);
                break;
            case IR_NUMBER_0:
                __fw_code[__da_fw_idx] = '0';
                __da_fw_idx++;
                ui_setup_da_ver_display(FALSE);
                break;
            default:
                break;
        }
        if (__da_fw_idx == 5) { // all digits entered
            if (strncmp(__fw_code,"13903",5) == 0) { // match
                snprintf(fw_ver, 5, "%u.0%u\0", DA_MajorVersion(),DA_MinorVersion());
                NVM_SetFwPrevVersion(fw_ver);
                NVM_SetDaFwDownload(TRUE);
                NVM_WriteBank(E_NVM_STATUS);

 //               IOX_UsbSwitchPwr(ON);
                delay_ms(200);
 //               IOX_UsbSwitchSel(E_USB_DA_UPDATE);
                delay_ms(200);
                DA_TxSambaMode();
                __da_fw_mode = 2;
                VOL_Mute();

                if(DA1_Installed()) {
                    DISP_PrintCenter(C_DISP_LINE1, "Ensure USB Cable\0");
                    DISP_PrintCenter(C_DISP_LINE2, "is Disconnected\0");
                    delay_s(4);

                    DISP_PrintCenter(C_DISP_LINE1, "Connect USB Cable\0");
                    while(count > 0) {
                        snprintf( str, kFirmwareVersionStringMaxLen, "in %u seconds\0", count);
                        DISP_PrintCenter(C_DISP_LINE2, str);
                        delay_s(1);
                        count--;
                    }
                }

                ui_setup_da_ver_display(TRUE);
            }
            else { // no match
                __da_fw_idx = 0;
                strncpy(__fw_code,"XXXXX",5);
                ui_setup_da_ver_display(FALSE);
            }
        }
    }
}

//===============================================|
// INPUTS =======================================|
//===============================================|
static void ui_setup_inputs_display(bool both)
{
    char vfd20[20]; vfd20[19]=0;

    if (__inputs_top) {
        DISP_ClearLine(C_DISP_BOTHLINES);
        DISP_PrintCenter(C_DISP_LINE1, "SETUP: Inputs\0");
        DISP_PrintCenter(C_DISP_LINE2, "(Hold INPUT)\0");
    }
    else {
        snprintf( vfd20, 20, "SETUP: %s\0", NVM_SavedNames[__name_idx]);
        DISP_PrintCenter(C_DISP_LINE1, vfd20);

        if(NVM_SrcIsOn(__name_idx))
            DISP_PrintCenter(C_DISP_LINE2, "On/Name (Hold INPUT)\0");
        else
            DISP_PrintCenter(C_DISP_LINE2, "Off\0");
    }
}

static void ui_setup_inputs_handler(uint32_t code)
{
    Src_Type src_last = DA_Installed() ? E_SRC_LAST : E_SRC_AE_LAST;

    if(__inputs_top) {
        switch (code) {
            case C_KEY_SETUP:         UI_SetupExit();       break;
            case C_SETUP_MENU_NEXT: ui_setup_menu_up();   break;
            case C_SETUP_MENU_PREV: ui_setup_menu_down(); break;
            case C_SETUP_HOLD: // switch to secondary Menu
                __inputs_top = FALSE;
                ui_setup_inputs_display(TRUE);
                break;
            default:
                break;
        }
    }
    else {
        switch (code) {
            case C_KEY_SETUP: // Exit to top Menu
                __inputs_top = TRUE;
                __name_idx = E_SRC_FIRST;
                ui_setup_inputs_display(TRUE);
                break;
            case C_SETUP_OPT1_NEXT: // Toggle On/Off
                if(NVM_SrcIsOn(__name_idx) != ON) {
                    NVM_ToggleSrc(__name_idx);
                    ui_setup_inputs_display(FALSE);
                }
                break;
            case C_SETUP_OPT1_PREV: // Toggle On/Off
                if(NVM_SrcIsOn(__name_idx) != OFF) {
                    NVM_ToggleSrc(__name_idx);
                    ui_setup_inputs_display(FALSE);
                }
                break;
            case C_SETUP_MENU_NEXT: // next input name
                if(__name_idx != src_last)
                    __name_idx = SRC_NextUp(__name_idx,YES);
                ui_setup_inputs_display(FALSE);
                break;
            case C_SETUP_MENU_PREV: // prev input name
                if(__name_idx != E_SRC_FIRST)
                    __name_idx = SRC_NextDown(__name_idx,YES);
                ui_setup_inputs_display(FALSE);
                break;
            case C_SETUP_HOLD: // Enter Rename Menu
                UI_RenameInput(__name_idx);
                UI_SetMenu(E_MENU_RENAME);
                break;
            default:
                break;
        }
    }
}

//===============================================|
// OUTPUTS ======================================|
//===============================================|
static void ui_setup_outputs_display(bool both) {

    if (__outputs_top) {
        DISP_ClearLine(C_DISP_BOTHLINES);
        DISP_PrintCenter(C_DISP_LINE1, "SETUP: Outputs\0");
        DISP_PrintCenter(C_DISP_LINE2, "(Hold INPUT)\0");
    }
    else {
        switch(__outputs_id) {
            case 0:
                DISP_PrintCenter(C_DISP_LINE1, "SETUP: OUTPUT 1\0");
                if(NVM_GetOutputSw(E_OUTPUT_1))
                    DISP_PrintCenter(C_DISP_LINE2, "Unswitched\0");
                else
                    DISP_PrintCenter(C_DISP_LINE2, "Switched\0");
                break;
            case 1:
                DISP_PrintCenter(C_DISP_LINE1, "SETUP: OUTPUT 2\0");
                if(NVM_GetOutputSw(E_OUTPUT_2))
                    DISP_PrintCenter(C_DISP_LINE2, "Unswitched\0");
                else
                    DISP_PrintCenter(C_DISP_LINE2, "Switched\0");
                break;
            case 2:
                DISP_PrintCenter(C_DISP_LINE1, "SETUP: HEADPHONES\0");
                switch(NVM_GetHpOut()) {
                    case 0: DISP_PrintCenter(C_DISP_LINE2, "Mute All Outputs\0"); break;
                    case 1: DISP_PrintCenter(C_DISP_LINE2, "Mute No Outputs\0");  break;
                }
                break;
            default:
                break;
        }
    }
}

static void ui_setup_outputs_handler(uint32_t code) {

    switch (code) {
        case C_KEY_SETUP: // exit setup menu
            __outputs_id = 0;

            if(__outputs_top)
                UI_SetupExit();
            else {
                __outputs_top = TRUE;
                ui_setup_outputs_display(TRUE);
            }
            break;
        case C_SETUP_MENU_NEXT:
            if(__outputs_top) {
                __outputs_id = 0;
                ui_setup_menu_up();
            }
            else {
                switch(__outputs_id) {
                    case 0: // OUTPUT1
                        __outputs_id += 1;
                        ui_setup_outputs_display(TRUE);
                        break;
                    case 1: // OUTPUT2
                        if(OUT_Headphone() != E_HEADPHONE_CHINA) {
                            __outputs_id += 1;
                            ui_setup_outputs_display(TRUE);
                        }
                        break;
                    case 2: // HEADPHONES
                        break;
                }
            }
            break;
        case C_SETUP_MENU_PREV:
            if(__outputs_top) {
                __trig_id = E_TRIG_ID1;
                ui_setup_menu_down();
            }
            else if(__outputs_id != 0) {
                __outputs_id -= 1;
                ui_setup_outputs_display(TRUE);
            }
            break;
        case C_SETUP_HOLD:
            if(__outputs_top) {
                __outputs_top = FALSE;
                ui_setup_outputs_display(TRUE);
            }
            break;
        case C_SETUP_OPT1_NEXT:
            switch(__outputs_id) {
                case 0:
                    if(NVM_GetOutputSw(E_OUTPUT_1) == E_OUTPUT_SWITCHED) {
                        IOX_Output(E_OUTPUT_1, ON);
//                        IOX_OutputLedSet(E_OUTPUT_1, ON);   Removed for C80
                        NVM_SetOutput(E_OUTPUT_1, ON);
                        NVM_SetOutputSw(E_OUTPUT_1,E_OUTPUT_UNSWITCHED);
                    }
                    break;
                case 1:
                    if(NVM_GetOutputSw(E_OUTPUT_2) == E_OUTPUT_SWITCHED) {
                        IOX_Output(E_OUTPUT_2, ON);
//                        IOX_OutputLedSet(E_OUTPUT_2, ON);    Removed for C80
                        NVM_SetOutput(E_OUTPUT_2, ON);
                        NVM_SetOutputSw(E_OUTPUT_2,E_OUTPUT_UNSWITCHED);
                    }
                    break;
                case 2:
                    if(NVM_GetHpOut() == E_HP_OUT_MUTE_ALL)
                        NVM_SetHpOut(E_HP_OUT_MUTE_NONE);
                    break;
            }
            ui_setup_outputs_display(TRUE);
            break;
        case C_SETUP_OPT1_PREV:
            switch(__outputs_id) {
                case 0:
                    if(NVM_GetOutputSw(E_OUTPUT_1) == E_OUTPUT_UNSWITCHED)
                        NVM_SetOutputSw(E_OUTPUT_1,E_OUTPUT_SWITCHED);
                    break;
                case 1:
                    if(NVM_GetOutputSw(E_OUTPUT_2) == E_OUTPUT_UNSWITCHED)
                        NVM_SetOutputSw(E_OUTPUT_2,E_OUTPUT_SWITCHED);
                    break;
                case 2:
                    if(NVM_GetHpOut() == E_HP_OUT_MUTE_NONE)
                        NVM_SetHpOut(E_HP_OUT_MUTE_ALL);
                    break;
            }
            ui_setup_outputs_display(TRUE);
            break;
        default:
            break;
    }
}

//===============================================|
// TRIGGERS =====================================|
//===============================================|
static void ui_setup_trig_display(bool both) {

    char vfd[20]; vfd[19]=0;

    while(!NVM_SrcIsOn(__trig_name_idx))
        __trig_name_idx = SRC_NextUp(__trig_name_idx,NO);

    if (__trig_top) {
        DISP_ClearLine(C_DISP_BOTHLINES);
        DISP_PrintCenter(C_DISP_LINE1, "SETUP: Triggers\0");
        DISP_PrintCenter(C_DISP_LINE2, "(Hold INPUT)\0");
    }
    else {
        switch(__trig_id) {
            case E_TRIG_ID1: DISP_PrintCenter(C_DISP_LINE1, "SETUP: TRIGGER 1\0");  break;
            case E_TRIG_ID2: DISP_PrintCenter(C_DISP_LINE1, "SETUP: TRIGGER 2\0");  break;
            default: break;
        }

        if(__trig_input_top) {
            if(NVM_GetTriggerInput(__trig_id, (Src_Type) __trig_name_idx))
                snprintf( vfd, 20, "%s: ON\0", NVM_SavedNames[__trig_name_idx]);
            else
                snprintf( vfd, 20, "%s: OFF\0", NVM_SavedNames[__trig_name_idx]);

            DISP_PrintCenter(C_DISP_LINE2, vfd);
        }
        else {
            switch(NVM_GetTrigger(__trig_id)) {
                case E_TRIG_ON:    DISP_PrintCenter(C_DISP_LINE2, "Main\0");               break;
                case E_TRIG_OUT1:  DISP_PrintCenter(C_DISP_LINE2, "Output 1\0");           break;
                case E_TRIG_OUT2:  DISP_PrintCenter(C_DISP_LINE2, "Output 2\0");           break;
                case E_TRIG_INPUT: DISP_PrintCenter(C_DISP_LINE2, "Input (Hold INPUT)\0"); break;
                default: break;
            }
        }
    }
}

static void ui_setup_trig_handler(uint32_t code) {
    switch (code) {
        case C_KEY_SETUP: // exit setup menu
            __trig_id = E_TRIG_ID1;

            if(__trig_top)
                UI_SetupExit();
            else if(__trig_input_top) {
                __trig_top = FALSE;
                __trig_input_top = FALSE;
                ui_setup_trig_display(TRUE);
            }
            else {
                __trig_top = TRUE;
                ui_setup_trig_display(TRUE);
            }
            break;
        case C_SETUP_MENU_NEXT:
            if(__trig_top) {
                __trig_id = E_TRIG_ID1;
                ui_setup_menu_up();
            }
            else if(__trig_input_top) {
                __trig_name_idx = SRC_NextUp((Src_Type)__trig_name_idx,NO);
                ui_setup_trig_display(TRUE);
            }
            else if(__trig_id != E_TRIG_ID_LAST) {
                __trig_id += 1;
                ui_setup_trig_display(TRUE);
            }
            break;
        case C_SETUP_MENU_PREV:
            if(__trig_top) {
                __trig_id = E_TRIG_ID1;
                ui_setup_menu_down();
            }
            else if(__trig_input_top) {
                __trig_name_idx = SRC_NextDown((Src_Type)__trig_name_idx,NO);
                ui_setup_trig_display(TRUE);
            }
            else if (__trig_id != E_TRIG_ID_FIRST) {
                __trig_id -= 1;
                ui_setup_trig_display(TRUE);
            }
            break;
        case C_SETUP_HOLD:
            if(__trig_top) {
                __trig_top = FALSE;
                ui_setup_trig_display(TRUE);
            }
            else if(NVM_GetTrigger(__trig_id) == E_TRIG_INPUT) {
                __trig_input_top = TRUE;
                ui_setup_trig_display(TRUE);
            }
            break;
        case C_SETUP_OPT1_NEXT:
            if(__trig_input_top) {
                if(!NVM_GetTriggerInput(__trig_id, (Src_Type) __trig_name_idx)) {
                    NVM_SetTriggerInput(__trig_id, (Src_Type) __trig_name_idx, ON);
                    ui_setup_trig_display(TRUE);
                    TRIG_Config();
                }
            }
            else if(NVM_GetTrigger(__trig_id) < E_TRIG_LAST) {
                NVM_SetTrigger(__trig_id, (Trig_Type) (NVM_GetTrigger(__trig_id) + 1));
                ui_setup_trig_display(TRUE);
                TRIG_Config();
            }
            break;
        case C_SETUP_OPT1_PREV:
            if(__trig_input_top) {
                if(NVM_GetTriggerInput(__trig_id, (Src_Type) __trig_name_idx)) {
                    NVM_SetTriggerInput(__trig_id,(Src_Type) __trig_name_idx, OFF);
                    ui_setup_trig_display(TRUE);
                    TRIG_Config();
                }
            }
            else if(NVM_GetTrigger(__trig_id) > E_TRIG_FIRST) {
                NVM_SetTrigger(__trig_id, (Trig_Type) (NVM_GetTrigger(__trig_id) - 1));
                ui_setup_trig_display(TRUE);
                TRIG_Config();
            }
            break;
        default: break;
    }
}

//===============================================|
// DATAPORTS ====================================|
//===============================================|
static void ui_setup_dataports_display(bool both) {

    char vfd[20]; vfd[19]=0;
    Src_Type dport_src = NVM_GetDataport(__dataports_id);

    if(dport_src != E_SRC_DP_ALL) {
        while(!NVM_SrcIsOn(dport_src))
            dport_src = SRC_NextUp(dport_src,NO);
    }

    if (__dataports_top) {
        DISP_ClearLine(C_DISP_BOTHLINES);
        DISP_PrintCenter(C_DISP_LINE1, "SETUP: Data Ports\0");
        DISP_PrintCenter(C_DISP_LINE2, "(Hold INPUT)\0");
    }
    else {
        switch(__dataports_id) {
            case E_DPORT_ID1: DISP_PrintCenter(C_DISP_LINE1, "SETUP: DATA PORT 1\0");  break;
            case E_DPORT_ID2: DISP_PrintCenter(C_DISP_LINE1, "SETUP: DATA PORT 2\0");  break;
            case E_DPORT_ID3: DISP_PrintCenter(C_DISP_LINE1, "SETUP: DATA PORT 3\0");  break;
            case E_DPORT_ID4: DISP_PrintCenter(C_DISP_LINE1, "SETUP: DATA PORT 4\0");  break;
            default: break;
        }

        // Display User Saved Names
        if(dport_src == E_SRC_DP_ALL)
            snprintf( vfd, 20, "All Data\0");
        else
            snprintf( vfd, 20, "%s\0", NVM_SavedNames[dport_src]);

        DISP_PrintCenter(C_DISP_LINE2, vfd);
    }
}

static void ui_setup_dataports_handler(uint32_t code) {

    Src_Type dp_next;

    Dport_Id_Type last = E_DPORT_ID_LAST;

    switch (code) {
        case C_KEY_SETUP: /* exit setup menu */
            __dataports_id = E_DPORT_ID1;

            if(__dataports_top)
                UI_SetupExit();
            else {
                __dataports_top = TRUE;
                ui_setup_dataports_display(TRUE);
            }
            break;
        case C_SETUP_MENU_NEXT:
            if(__dataports_top) {
                __dataports_id = E_DPORT_ID1;
                ui_setup_menu_up();
            }
            else if(__dataports_id != last) {
                __dataports_id += 1;
                ui_setup_dataports_display(TRUE);
            }
            break;
        case C_SETUP_MENU_PREV:
            if(__dataports_top) {
                __dataports_id = E_DPORT_ID1;
                ui_setup_menu_down();
            }
            else if(__dataports_id != E_DPORT_ID_FIRST) {
                __dataports_id -= 1;
                ui_setup_dataports_display(TRUE);
            }
            break;
        case C_SETUP_HOLD:
            if(__dataports_top) {
                __dataports_top = FALSE;
                ui_setup_dataports_display(TRUE);
            }
            break;
        case C_SETUP_OPT1_NEXT:
            dp_next = NVM_GetDataport(__dataports_id);

            if(dp_next == E_SRC_DP_ALL) {
                dp_next = E_SRC_FIRST;

                // Special Case: check if FIRST is ON
                if(!NVM_SrcIsOn(E_SRC_FIRST)) {
                    do{ // Skip Non-Dport and OFF Sources
                        dp_next = SRC_NextUp(dp_next,NO);
                    } while(dp_next == E_SRC_MM_PHONO || dp_next == E_SRC_MC_PHONO ||
                            dp_next == E_SRC_USB || dp_next == E_SRC_HDMI);
                }

                NVM_SetDataport(__dataports_id, dp_next);
                DPORT_Config();
                ui_setup_dataports_display(TRUE);
            }
            else {
                do{ // Skip Non-Dport and OFF Sources
                    dp_next = SRC_NextUp(dp_next,NO);
                    } while(dp_next == E_SRC_MM_PHONO || dp_next == E_SRC_MC_PHONO ||
                            dp_next == E_SRC_USB || dp_next == E_SRC_HDMI);

                // Bookended - check if at last Dport
                if(dp_next > NVM_GetDataport(__dataports_id)) {
                    NVM_SetDataport(__dataports_id, dp_next);
                    DPORT_Config();
                    ui_setup_dataports_display(TRUE);
                }
            }
            break;
        case C_SETUP_OPT1_PREV:
            dp_next = NVM_GetDataport(__dataports_id);

            if(dp_next == E_SRC_DP_ALL)
                break;

            do{ // Skip Non-Dport and OFF Sources
                dp_next = SRC_NextDown(dp_next,NO);
            } while(dp_next == E_SRC_MM_PHONO || dp_next == E_SRC_MC_PHONO ||
                    dp_next == E_SRC_USB || dp_next == E_SRC_HDMI);

            // Bookended - check if at first Dport
            if(dp_next < NVM_GetDataport(__dataports_id))
                NVM_SetDataport(__dataports_id, dp_next);
            else
                NVM_SetDataport(__dataports_id, E_SRC_DP_ALL);

            DPORT_Config();
            ui_setup_dataports_display(TRUE);

            break;
        default: break;
    }
}

//===============================================|
// PASSTHRU =====================================|
//===============================================|
static void ui_setup_passthru_display(bool both)
{
    char vfd[20]; vfd[19]=0;
    Src_Type pt_src = NVM_GetPtSrc();

    if(both)
        DISP_PrintCenter(C_DISP_LINE1, "SETUP: PASSTHRU\0");

    if(pt_src == E_SRC_PT_OFF)
        snprintf( vfd, 20, " OFF\0");
    else
        snprintf( vfd, 20, " %s\0", NVM_SavedNames[pt_src]);

    DISP_PrintCenter(C_DISP_LINE2, vfd);
}

static void ui_setup_passthru_handler(uint32_t code)
{
    bool update = FALSE;
    Src_Type pt_next;

    switch (code) {
        case C_KEY_SETUP:         UI_SetupExit();       break;
        case C_SETUP_MENU_NEXT: ui_setup_menu_up();   break;
        case C_SETUP_MENU_PREV: ui_setup_menu_down(); break;
        case C_SETUP_OPT1_NEXT:
            pt_next = NVM_GetPtSrc();

            if(pt_next == E_SRC_PT_OFF) {
                pt_next = E_SRC_FIRST;

                // Special Case: check if FIRST is ON
                if(!NVM_SrcIsOn(E_SRC_FIRST)) {
                    do{ // Skip Non-PT and OFF Sources
                        pt_next = SRC_NextUp(pt_next,YES);
                    } while(SRC_IsDigital(pt_next) || SRC_IsPhono(pt_next));
                }

                NVM_SetPtSrc(pt_next);
                update = TRUE;
            }
            else {
                do{ // Skip Non-PT and OFF Sources
                    pt_next = SRC_NextUp(pt_next,YES);
                } while(SRC_IsDigital(pt_next) || SRC_IsPhono(pt_next));

                // Bookended - check if at last PT Src
                if(pt_next > NVM_GetPtSrc()) {
                    NVM_SetPtSrc(pt_next);
                    update = TRUE;
                }
            }
            break;
        case C_SETUP_OPT1_PREV:
            pt_next = NVM_GetPtSrc();

            if(pt_next == E_SRC_PT_OFF)
                break;

            do{ // Skip Non-PT and OFF Sources
                pt_next = SRC_NextDown(pt_next,YES);
            } while(SRC_IsDigital(pt_next) || SRC_IsPhono(pt_next));

            // Bookended - check if at first PT
            if(pt_next < NVM_GetPtSrc())
                NVM_SetPtSrc(pt_next);
            else
                NVM_SetPtSrc(E_SRC_PT_OFF);

            update = TRUE;
        default:
            break;
    }

    if(update)
        ui_setup_passthru_display(FALSE);
}

//===============================================|
// USB AUTOMUTE =================================|
//===============================================|
static void ui_setup_usb_automute_display(bool both) {
    if(both)
        DISP_PrintCenter(C_DISP_LINE1, "SETUP: USB Automute\0");

    DISP_ClearLine(C_DISP_LINE2);

    if(NVM_GetUsbAutomute() == ON)
        DISP_PrintCenter(C_DISP_LINE2, "ON\0");
    else
        DISP_PrintCenter(C_DISP_LINE2, "OFF\0");

}

static void ui_setup_usb_automute_handler(uint32_t code) {

    bool update = FALSE;

    switch (code) {
        case C_KEY_SETUP:         UI_SetupExit();       break;
        case C_SETUP_MENU_NEXT: ui_setup_menu_up();   break;
        case C_SETUP_MENU_PREV: ui_setup_menu_down(); break;
        case C_SETUP_OPT1_NEXT: // transition to next option
            if (NVM_GetUsbAutomute() == OFF) {
                NVM_SetUsbAutomute(ON);
                DA_TxUsbAutomute(ON);
                update = TRUE;
            }
            break;
        case C_SETUP_OPT1_PREV: // transition to next option
            if (NVM_GetUsbAutomute() == ON) {
                NVM_SetUsbAutomute(OFF);
                DA_TxUsbAutomute(OFF);
                update = TRUE;
            }
            break;
        default:
            break;
    }

    if(update)
        ui_setup_usb_automute_display(FALSE);
}

//===============================================|
// RS232 BAUD RATE ==============================|
//===============================================|
static void ui_setup_rs232_display(bool both) {

    if(both)
        DISP_PrintCenter(C_DISP_LINE1, "SETUP: RS232\0");

    DISP_ClearLine(C_DISP_LINE2);

    switch(NVM_GetRS232Baud()) {
        case E_BAUD_9600:   DISP_PrintCenter(C_DISP_LINE2, " 9600 Baud");  break;
        case E_BAUD_19200:  DISP_PrintCenter(C_DISP_LINE2, "19200 Baud");  break;
        case E_BAUD_38400:  DISP_PrintCenter(C_DISP_LINE2, "38400 Baud");  break;
        case E_BAUD_57600:  DISP_PrintCenter(C_DISP_LINE2, "57600 Baud");  break;
        case E_BAUD_115200: DISP_PrintCenter(C_DISP_LINE2, "115200 Baud"); break;
    }
}

static void ui_setup_rs232_handler(uint32_t code) {
    bool update = FALSE;

    switch (code) {
        case C_KEY_SETUP:         UI_SetupExit();       break;
        case C_SETUP_MENU_NEXT: ui_setup_menu_up();   break;
        case C_SETUP_MENU_PREV: ui_setup_menu_down(); break;
        case C_SETUP_OPT1_NEXT: // transition to next option
            if(NVM_GetRS232Baud() < E_BAUD_LAST) {
                NVM_SetRS232Baud((Baud_Type) (NVM_GetRS232Baud() + 1));
                UART_Init(3);
                update = TRUE;
            }
            break;
        case C_SETUP_OPT1_PREV: // transition to next option
            if(NVM_GetRS232Baud() > E_BAUD_FIRST) {
                NVM_SetRS232Baud((Baud_Type) (NVM_GetRS232Baud() - 1));
                UART_Init(3);
                update = TRUE;
            }
            break;
        default:
            break;
    }

    if(update)
        ui_setup_rs232_display(FALSE);
}

//===============================================|
// IR CODES (Norm/Alt) ==========================|
//===============================================|
static void ui_setup_ircodes_display(bool both) {
    if(both)
        DISP_PrintCenter(C_DISP_LINE1, "SETUP: IR Codes\0");

    DISP_ClearLine(C_DISP_LINE2);

    if(NVM_GetIrCoding() == ON)
        DISP_PrintCenter(C_DISP_LINE2, "Normal\0");
    else
        DISP_PrintCenter(C_DISP_LINE2, "Alternate\0");

}

static void ui_setup_ircodes_handler(uint32_t code) {

    bool update = FALSE;

    switch (code) {
        case C_KEY_SETUP: UI_SetupExit(); break;
        case C_SETUP_MENU_NEXT: ui_setup_menu_up();   break;
        case C_SETUP_MENU_PREV: ui_setup_menu_down(); break;
        case C_SETUP_OPT1_NEXT: // transition to next option
            if (NVM_GetIrCoding() == OFF) {
                NVM_SetIrCoding(ON);
                update = TRUE;
            }
            break;
        case C_SETUP_OPT1_PREV: // transition to next option
            if (NVM_GetIrCoding() == ON) {
                NVM_SetIrCoding(OFF);
                update = TRUE;
            }
            break;
        default:
            break;
    }

    if(update)
        ui_setup_ircodes_display(FALSE);
}

//===============================================|
// FRONT IR (Enable/Disable) ====================|
//===============================================|
static void ui_setup_irdisable_display(bool both)
{
    if(both)
        DISP_PrintCenter(C_DISP_LINE1, "SETUP: Front IR\0");

    DISP_ClearLine(C_DISP_LINE2);

    if(NVM_GetIrFront() == ON)
        DISP_PrintCenter(C_DISP_LINE2, "Enabled\0");
    else
        DISP_PrintCenter(C_DISP_LINE2, "Disabled\0");
}

static void ui_setup_irdisable_handler(uint32_t code)
{
    bool update = FALSE;
    switch (code) {
        case C_KEY_SETUP:       UI_SetupExit();       break;
        case C_SETUP_MENU_NEXT: ui_setup_menu_up();   break;
        case C_SETUP_MENU_PREV: ui_setup_menu_down(); break;
        case C_SETUP_OPT1_NEXT:
            if(NVM_GetIrFront() == OFF) {
                NVM_SetIrFront(ON);
                update = TRUE;
            }
            break;
        case C_SETUP_OPT1_PREV:
            if(NVM_GetIrFront() == ON) {
                NVM_SetIrFront(OFF);
                update = TRUE;
            }
            break;
        default:
            break;
    }

    if(update)
        ui_setup_irdisable_display(FALSE);
}

//===============================================|
// AUTO POWER OFF (Enable/Disable) ==============|
//===============================================|
static void ui_setup_autooff_display(bool both)
{
    if(both)
        DISP_PrintCenter(C_DISP_LINE1, "SETUP: Auto-Off\0");

    if(NVM_GetPwrAutoOff() == OFF)
        DISP_PrintCenter(C_DISP_LINE2, "Disabled\0");
    else
        DISP_PrintCenter(C_DISP_LINE2, "Enabled\0");
}

static void ui_setup_autooff_handler(uint32_t code)
{
    switch (code) {
        case C_KEY_SETUP: UI_SetupExit(); break;
        case C_SETUP_MENU_NEXT: ui_setup_menu_up();   break;
        case C_SETUP_MENU_PREV: ui_setup_menu_down(); break;
        case C_SETUP_OPT1_NEXT: // transition to next option
            if(NVM_GetPwrAutoOff() == OFF) {
                NVM_SetPwrAutoOff(ON);
                ui_setup_autooff_display(FALSE);
            }
            break;
        case C_SETUP_OPT1_PREV:
            if(NVM_GetPwrAutoOff() == ON) {
                NVM_SetPwrAutoOff(OFF);
                ui_setup_autooff_display(FALSE);
            }
            break;
        default:
            break;
    }
}

//===============================================|
// FACTORY RESET ================================|
//===============================================|
static void ui_setup_factoryrst_display(bool both)
{
    if(both)
        DISP_PrintCenter(C_DISP_LINE1, "FACTORY RESET\0");

    if (__factoryrst_mode == 0)
        DISP_PrintCenter(C_DISP_LINE2,"(Hold INPUT)\0");
    else if ((__factoryrst_mode > 6) && (__factoryrst_mode != 0xD))
        DISP_PrintCenter(C_DISP_LINE2,"In Progress\0");
    else
        DISP_PrintCenter(C_DISP_LINE2,"Completed!\0");
}

static void ui_setup_factoryrst_handler(uint32_t code)
{
    switch (code) {
        case C_KEY_SETUP: UI_SetupExit(); break;
        case C_SETUP_MENU_NEXT:
            __factoryrst_mode = 0;
            ui_setup_menu_up();
            break;
        case C_SETUP_MENU_PREV:
            __factoryrst_mode = 0;
            ui_setup_menu_down();
            break;
        case C_SETUP_HOLD:
        case C_SETUP_HOLD_LATCH:
            __factoryrst_mode++;

            if (__factoryrst_mode> 2) {
                __factoryrst_mode = 0xA;
                ui_setup_factoryrst_display(FALSE);
                NVM_FactoryResetAll();

                delay_s(2); // artificial delay
                __factoryrst_mode = 0xD;
                ui_setup_factoryrst_display(FALSE);

                NVM_Startup(); // ensure the local values are retrieved from the new NVM values
                delay_s(2); // artificial delay before going into standby

                APP_SetSchedule(E_SCHED_POWERDOWN);
                UI_SetupExit();
            }
            break;
        default:
            break;
    }
}
//===============================================|
// HDMI CEC =====================================|
//===============================================|
static void ui_setup_hdmi_cec_display(bool both) {
    if(both)
        DISP_PrintCenter(C_DISP_LINE1, "SETUP: HDMI CEC\0");

    DISP_ClearLine(C_DISP_LINE2);

    if(NVM_GetHdmiCec() == ON)
        DISP_PrintCenter(C_DISP_LINE2, "ON\0");
    else
        DISP_PrintCenter(C_DISP_LINE2, "OFF\0");

}

static void ui_setup_hdmi_cec_handler(uint32_t code) {

    bool update = FALSE;

    switch (code) {
        case C_KEY_SETUP:       UI_SetupExit();       break;
        case C_SETUP_MENU_NEXT: ui_setup_menu_up();   break;
        case C_SETUP_MENU_PREV: ui_setup_menu_down(); break;
        case C_SETUP_OPT1_NEXT: // transition to next option
            if (NVM_GetHdmiCec() == OFF) {
                NVM_SetHdmiCec(ON);
                DA_TxHdmiCecState(ON);
                update = TRUE;
            }
            break;
        case C_SETUP_OPT1_PREV: // transition to next option
            if (NVM_GetHdmiCec() == ON) {
                NVM_SetHdmiCec(OFF);
                DA_TxHdmiCecState(OFF);
                update = TRUE;
            }
            break;
        default:
            break;
    }

    if(update)
        ui_setup_hdmi_cec_display(FALSE);
}

//===============================================|
// HDMI CEC POWER ===============================|
//===============================================|
static void ui_setup_hdmi_cec_pwr_display(bool both) {
    if(both)
        DISP_PrintCenter(C_DISP_LINE1, "SETUP: HDMI CEC PWR");

    DISP_ClearLine(C_DISP_LINE2);

    if(NVM_GetHdmiCecPwr() == ON)
        DISP_PrintCenter(C_DISP_LINE2, "ON\0");
    else
        DISP_PrintCenter(C_DISP_LINE2, "OFF\0");
}

static void ui_setup_hdmi_cec_pwr_handler(uint32_t code) {

    bool update = FALSE;

    switch (code) {
        case C_KEY_SETUP:       UI_SetupExit();       break;
        case C_SETUP_MENU_NEXT: ui_setup_menu_up();   break;
        case C_SETUP_MENU_PREV: ui_setup_menu_down(); break;
        case C_SETUP_OPT1_NEXT: // transition to next option
            if (NVM_GetHdmiCecPwr() == OFF) {
                NVM_SetHdmiCecPwr(ON);
                DA_TxHdmiCecPower(ON);
                update = TRUE;
            }
            break;
        case C_SETUP_OPT1_PREV: // transition to next option
            if (NVM_GetHdmiCecPwr() == ON) {
                NVM_SetHdmiCecPwr(OFF);
                DA_TxHdmiCecPower(OFF);
                update = TRUE;
            }
            break;
        default:
            break;
    }

    if(update)
        ui_setup_hdmi_cec_pwr_display(FALSE);
}

//===============================================|
// HDMI Lip sync ================================|
//===============================================|
static void ui_setup_hdmi_lip_sync_display(bool both) {
    if(both)
        DISP_PrintCenter(C_DISP_LINE1, "SETUP: Lip Sync Mode\0");

    DISP_ClearLine(C_DISP_LINE2);

    if(NVM_GetLipSyncMode() == E_LIP_SYNC_AUTO)
        DISP_PrintCenter(C_DISP_LINE2, "Auto\0");
    else
        DISP_PrintCenter(C_DISP_LINE2, "Manual\0");
}

static void ui_setup_hdmi_lip_sync_handler(uint32_t code) {

    bool update = FALSE;

    switch (code) {
        case C_KEY_SETUP:       UI_SetupExit();       break;
        case C_SETUP_MENU_NEXT: ui_setup_menu_up();   break;
        case C_SETUP_MENU_PREV: ui_setup_menu_down(); break;
        case C_SETUP_OPT1_NEXT: // transition to next option
            if (NVM_GetLipSyncMode() == E_LIP_SYNC_MANUAL) {
                NVM_SetLipSyncMode(E_LIP_SYNC_AUTO);
                DA_TxLipSyncMode(E_LIP_SYNC_AUTO);
                update = TRUE;
            }
            break;
        case C_SETUP_OPT1_PREV: // transition to next option
            if (NVM_GetLipSyncMode() == E_LIP_SYNC_AUTO) {
                NVM_SetLipSyncMode(E_LIP_SYNC_MANUAL);
                DA_TxLipSyncMode(E_LIP_SYNC_MANUAL);
                update = TRUE;
            }
            break;
        default:
            break;
    }

    if(update)
        ui_setup_hdmi_lip_sync_display(FALSE);
}

//===============================================|
// DIG GAIN =====================================|
//===============================================|
static void ui_setup_dig_gain_display(bool both) {

    char vfd[20]; vfd[19]=0;

    if (__dig_gain_top) {
        DISP_ClearLine(C_DISP_BOTHLINES);
        DISP_PrintCenter(C_DISP_LINE1, "SETUP: Digital Gain\0");
        DISP_PrintCenter(C_DISP_LINE2, "(Hold INPUT)\0");
    }
    else {
        snprintf( vfd, 20, " %2u dB\0", 0);

        switch(__dig_gain_id) {
            case 0:
                DISP_PrintCenter(C_DISP_LINE1, "SETUP: HDMI Gain\0");
                if(NVM_GetDigGain(E_DIG_GAIN_HDMI) != 0)
                    snprintf( vfd, 20, "+%2u dB\0", NVM_GetDigGain(E_DIG_GAIN_HDMI));
                break;
            case 1:
                DISP_PrintCenter(C_DISP_LINE1, "SETUP: OPTI 1 Gain\0");
                if(NVM_GetDigGain(E_DIG_GAIN_OPT1) != 0)
                    snprintf( vfd, 20, "+%2u dB\0", NVM_GetDigGain(E_DIG_GAIN_OPT1));
                break;
            case 2:
                DISP_PrintCenter(C_DISP_LINE1, "SETUP: OPTI 2 Gain\0");
                if(NVM_GetDigGain(E_DIG_GAIN_OPT2) != 0)
                    snprintf( vfd, 20, "+%2u dB\0", NVM_GetDigGain(E_DIG_GAIN_OPT2));
                break;
            default:
                break;
        }
        DISP_PrintCenter(C_DISP_LINE2,vfd);
    }
}

static void ui_setup_dig_gain_handler(uint32_t code) {

    switch (code) {
        case C_KEY_SETUP: // exit setup menu
            __dig_gain_id = 0;

            if(__dig_gain_top)
                UI_SetupExit();
            else {
                __dig_gain_top = TRUE;
                ui_setup_dig_gain_display(TRUE);
            }
            break;
        case C_SETUP_MENU_NEXT:
            if(__dig_gain_top) {
                __dig_gain_id = 0;
                ui_setup_menu_up();
            }
            else if(__dig_gain_id != E_DIG_GAIN_OPT2) {
                __dig_gain_id += 1;
                ui_setup_dig_gain_display(TRUE);
            }
            break;
        case C_SETUP_MENU_PREV:
            if(__dig_gain_top) {
                __trig_id = E_TRIG_ID1;
                ui_setup_menu_down();
            }
            else if(__dig_gain_id != 0) {
                __dig_gain_id -= 1;
                ui_setup_dig_gain_display(TRUE);
            }
            break;
        case C_SETUP_HOLD:
            if(__dig_gain_top) {
                __dig_gain_top = FALSE;
                ui_setup_dig_gain_display(TRUE);
            }
            break;
        case C_SETUP_OPT1_NEXT:
            switch(__dig_gain_id) {
                case 0: // HDMI
                    if(NVM_GetDigGain(E_DIG_GAIN_HDMI) != 15) {
                        NVM_SetDigGain(E_DIG_GAIN_HDMI, (NVM_GetDigGain(E_DIG_GAIN_HDMI) + 1));
                        DA_TxDigGain(E_DIG_GAIN_HDMI, NVM_GetDigGain(E_DIG_GAIN_HDMI));
                    }
                    break;
                case 1: // OPT1
                    if(NVM_GetDigGain(E_DIG_GAIN_OPT1) != 15) {
                        NVM_SetDigGain(E_DIG_GAIN_OPT1, (NVM_GetDigGain(E_DIG_GAIN_OPT1) + 1));
                        DA_TxDigGain(E_DIG_GAIN_OPT1, NVM_GetDigGain(E_DIG_GAIN_OPT1));
                    }
                    break;
                case 2: // OPT2
                    if(NVM_GetDigGain(E_DIG_GAIN_OPT2) != 15) {
                        NVM_SetDigGain(E_DIG_GAIN_OPT2, (NVM_GetDigGain(E_DIG_GAIN_OPT2) + 1));
                        DA_TxDigGain(E_DIG_GAIN_OPT2, NVM_GetDigGain(E_DIG_GAIN_OPT2));
                    }
                    break;
            }
            ui_setup_dig_gain_display(TRUE);
            break;
        case C_SETUP_OPT1_PREV:
            switch(__dig_gain_id) {
                case 0: // HDMI
                    if(NVM_GetDigGain(E_DIG_GAIN_HDMI) != 0) {
                        NVM_SetDigGain(E_DIG_GAIN_HDMI, (NVM_GetDigGain(E_DIG_GAIN_HDMI) - 1));
                        DA_TxDigGain(E_DIG_GAIN_HDMI, NVM_GetDigGain(E_DIG_GAIN_HDMI));
                    }
                    break;
                case 1: // OPT1
                    if(NVM_GetDigGain(E_DIG_GAIN_OPT1) != 0) {
                        NVM_SetDigGain(E_DIG_GAIN_OPT1, (NVM_GetDigGain(E_DIG_GAIN_OPT1) - 1));
                        DA_TxDigGain(E_DIG_GAIN_OPT1, NVM_GetDigGain(E_DIG_GAIN_OPT1));
                    }
                    break;
                case 2: // OPT2
                    if(NVM_GetDigGain(E_DIG_GAIN_OPT2) != 0) {
                        NVM_SetDigGain(E_DIG_GAIN_OPT2, (NVM_GetDigGain(E_DIG_GAIN_OPT2) - 1));
                        DA_TxDigGain(E_DIG_GAIN_OPT2, NVM_GetDigGain(E_DIG_GAIN_OPT2));
                    }
                    break;
            }
            ui_setup_dig_gain_display(TRUE);
            break;
        default:
            break;
    }
}

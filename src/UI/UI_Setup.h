//-----------------------------------------------------------------------------|
//	UI_Setup.h
//	Abstract: Setup Menu UI module
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __UI_SETUP_H__
#define __UI_SETUP_H__

#define C_SETUP_MENU_NEXT   C_RTY_INPUT_UP     // cycle to next menu
#define C_SETUP_MENU_PREV   C_RTY_INPUT_DOWN   // cycle to prev menu
#define C_SETUP_OPT1_NEXT   C_RTY_VOLUME_UP    // change to next option (primary) on the current menu
#define C_SETUP_OPT1_PREV   C_RTY_VOLUME_DOWN  // change to prev option (primary) on the current menu
#define C_SETUP_HOLD        (C_KEY_HOLD_MASK | C_KEY_SETUP) // "hold key"
#define C_SETUP_HOLD_LATCH  (C_SETUP_HOLD | C_KEY_LATCH_MASK) // repeated holds

typedef enum {
    E_SETUP_FIRST        = 0,
    E_SETUP_FWVER        = 0,
    E_SETUP_DAVER        = 1,
    E_SETUP_INPUTS       = 2,
    E_SETUP_OUTPUTS      = 3,
    E_SETUP_TRIGGERS     = 4,
    E_SETUP_DATAPORTS    = 5,
    E_SETUP_PASSTHRU     = 6,
    E_SETUP_USB_AUTOMUTE = 7,
    E_SETUP_HDMI_CEC      = 8,
    E_SETUP_HDMI_CEC_PWR  = 9,
    E_SETUP_HDMI_LIP_SYNC = 10,
    E_SETUP_DIG_GAIN      = 11,
    E_SETUP_RS232        = 12,
    E_SETUP_IRCODES      = 13,
    E_SETUP_IR_DISABLE   = 14,
    E_SETUP_AUTOOFF      = 15,
    E_SETUP_FACTORY_RST  = 16,
    E_SETUP_LAST         = 16,
    E_SETUP_NUM          = 17
} Ui_Setup_Menu_Type;

//===================================================================
// Public Function Prototypes
//===================================================================
void UI_Setup_Init(void);
void UI_Setup_Process(uint32_t code);
void UI_Setup_Display(void);
void UI_SetupExit(void);
bool UI_SetupInFwCodeMenu(void);

#endif // __UI_SETUP_H__
//-----------------------------------------------------------------------------|
//	UI_Control.c
//	Abstract: Top level UI control module
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include "UI_Control.h"
#include "UI_Common.h"
#include "UI_Main.h"
#include "UI_Demo.h"
#include "UI_Trim.h"
#include "UI_Setup.h"
#include "UI_Rename.h"
#include "UI_Passthru.h"
#include "APP_Process.h"
#include "PWR_Control.h"
#include "DISP_Control.h"
#include "VOL_Control.h"
#include "NVM_Control.h"
#include "PT_Control.h"

static uint8_t          __ui_display;           // 0x1 = display ON, 0x0 = Display OFF
static Ui_Timer_Type    __ui_active_timer;      // active timer
static int32_t          __ui_timer;             // timer value
static const int32_t    __ui_timeouts[] = {     // timeout values for each timer
    (-1*C_UI_PERIOD_SEC),     // E_UI_TMR_NONE, never "times out"
    (10*C_UI_PERIOD_SEC),     // E_UI_TMR_TRIM, 10 seconds
};

static Ui_Menu_Type __menu;

static void (*__menu_ptr[E_MENU_NUM])(uint32_t) = { // pointer to menu entry
    &UI_Main_Process,
    &UI_Trim_Process,
    &UI_Setup_Process,
    &UI_Passthru_Process,
    &UI_Rename_Process,
    &UI_Demo_Process
};

static void (*__menu_exit_ptr[E_MENU_NUM])(void) = { // pointer to menu init
    &UI_Main_Exit,
    &UI_Trim_Exit,
    &UI_SetupExit,
    &UI_Passthru_Exit,
    &UI_Rename_Exit,
    &UI_Demo_Exit
};

static void (*__display_ptr[E_MENU_NUM])(void) = { // pointer to menu display
    &UI_Main_Display,
    &UI_Trim_Display,
    &UI_Setup_Display,
    &UI_Passthru_Display,
    &UI_Rename_Display,
    &UI_Demo_Display
};

// queue
static uint32_t __ui_q[C_UI_QUEUE_SIZE];        // ui queue
static uint32_t __ui_q_head, __ui_q_tail;       // ui queue head andtail

//==============================================================================
// Private Function Prototypes
//==============================================================================
static void ui_timeouts(void);
static void ui_init_queue(void);
static void ui_queue(uint32_t code);
static uint32_t ui_dequeue(void);

//==============================================================================
// Public Functions
//==============================================================================
void UI_Init(void)
{
    __menu = E_MENU_MAIN;
    __ui_active_timer = E_UI_TMR_NONE;
    __ui_timer = 0;
    __ui_display = ON;

    ui_init_queue();
    UI_Setup_Init();
    UI_Trim_Init();
}

// Called periodically by ACTIVE scheduler (~20msec), enforces menu timeouts
void UI_Periodic(void)
{
    /* dispatch outstanding commands */
    uint32_t code = ui_dequeue();
    while(code != C_KEY_NOKEY)
    {
        // first input will turn the display back on if it is off
        if(__ui_display == ON) {
            __menu_ptr[__menu](code);

            if((code == (C_KEY_HOLD_MASK | C_KEY_SETUP)) && !UI_SetupInFwCodeMenu())
                __ui_display = OFF;
        }
        else {
            __ui_display = ON;
            __ui_active_timer = E_UI_TMR_DISP;
            DISP_SetState(ON);
        }

        // dequeue code, reset timer
        code = ui_dequeue();
        __ui_timer = 0;
        PWR_ResetAutoOff(); // reset auto-off timer
    }

    ui_timeouts();

    if(__menu == E_MENU_RENAME)
        UI_Rename_Blink();

    __menu_ptr[__menu](C_UI_PERIODIC);
}

void UI_SetMenu(Ui_Menu_Type menu)
{
    __menu = menu;  // Set Menu

    if(menu != E_MENU_MAIN)
        VOL_SetAbort();

    /* if the menu is trim, activate trim timer */
    if (__menu == E_MENU_TRIM) {
        __ui_active_timer = E_UI_TMR_TRIM;
        __ui_timer = 0;
    }
    else if (__menu != E_MENU_MAIN)
        __ui_active_timer = E_UI_TMR_NONE; // clear timer - setup, rename and profile menus will stay up indefinitely

    __display_ptr[__menu]();
}

void UI_Input(uint32_t code)
{
    // power key is always serviced
    if(code == C_KEY_POWER) {
        if(!PT_Active())
            PWR_Command();
    }
    else if(APP_GetSchedule() == E_SCHED_ACTIVE) {
        __ui_timer = 0; // reset timer
        ui_queue(code); // queue input
    }
}

Ui_Menu_Type UI_GetMenu(void)
{
    return __menu;
}

//==============================================================================
// Private Functions
//==============================================================================
static void ui_timeouts(void)
{
    if (__ui_active_timer != E_UI_TMR_NONE)
        __ui_timer++;

    if (__ui_timer >= __ui_timeouts[__ui_active_timer]) {
        switch (__ui_active_timer) {
            case E_UI_TMR_TRIM:
                __menu_exit_ptr[__menu]();
                if(PT_Active())
                    __menu = E_MENU_PASSTHRU;
                else
                    __menu = E_MENU_MAIN;
                __display_ptr[__menu]();
                __ui_active_timer = E_UI_TMR_NONE;
                break;
            case E_UI_TMR_NONE:
            default:
                break;
        }
    }
}

static void ui_init_queue(void)
{
    __ui_q_head = __ui_q_tail = 0;

    for(uint16_t i=0;i<C_UI_QUEUE_SIZE;i++)
        __ui_q[i] = C_KEY_NOKEY;
}

static void ui_queue(uint32_t code)
{
    __ui_q[__ui_q_head] = code;
    __ui_q_head = (__ui_q_head + 1) % C_UI_QUEUE_SIZE;
}

static uint32_t ui_dequeue(void)
{
    uint32_t rc = C_KEY_NOKEY;

    if (__ui_q_tail != __ui_q_head) {
        rc = __ui_q[__ui_q_tail];
        __ui_q[__ui_q_tail] = C_KEY_NOKEY;
        __ui_q_tail = (__ui_q_tail + 1) % C_UI_QUEUE_SIZE;
    }
    return rc;
}
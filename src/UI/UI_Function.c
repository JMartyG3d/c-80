//-----------------------------------------------------------------------------|
//	UI_Function.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include "UI_Function.h"
#include "IOX_control.h"
#include "PWR_Control.h"
#include "NVM_Access.h"
#include "I2C_Control.h"
#include "TRIG_Control.h"
#include "SRC_Control.h"
#include "DISP_Control.h"
#include "VOL_Control.h"
#include "UI_Trim.h"
#include "OUT_Control.h"
#include "PT_Control.h"
#include "APP_Process.h"
#include "TONE_Control.h"
#include "RS232_Tx.h"
#include "KEY_Process.h"
#include "ADC_Process.h"
#include "DA_ProcessTx.h"

static bool __ui_waiting, __ui_keypressed;

//==============================================================================
// Private Function Prototypes
//==============================================================================
static void ui_set_tone(bool state);

//===================================================================
// Public Functions
//===================================================================
bool UI_WaitforKeyPress(void)
{
    uint16_t loop = 0;
    __ui_waiting = TRUE;

    while(loop < 600) {
        ADC_Start();
        delay_ms(10);
        KEY_Periodic();
        loop++;
        if(__ui_keypressed) {
            __ui_keypressed = __ui_waiting = FALSE;
            KEY_Init(); // clear pending key presses
            return TRUE;
        }
    }
    __ui_waiting = FALSE;
    KEY_Init(); // clear pending key presses
    return FALSE;
}

bool UI_WaitKeyInProgress(void)
{
    return __ui_waiting;
}

void UI_WaitKeyPressed(void)
{
    __ui_keypressed = TRUE;
}

void UI_Toggle_HXD(void)
{
    if(NVM_GetHXD())
        UI_Set_HXD(OFF);
    else
        UI_Set_HXD(ON);
}

void UI_Set_HXD(uint8_t state)
{
   NVM_SetHXD(state);
   IOX_Hxd(state);
}

void UI_Toggle_Tone(void)
{
    if(NVM_GetTone(NVM_GetSrc()))
        ui_set_tone(OFF);
    else
        ui_set_tone(ON);

    RS232_TxTone(NVM_GetTone(NVM_GetSrc()));
}

void UI_Toggle_Mute(void)
{
    if(VOL_GetMute()) {
        UI_SetMute(OFF);
        RS232_TxMute(OFF);
    }
    else {
        UI_SetMute(ON);
        RS232_TxMute(ON);
    }
}

void UI_SetMute(bool state)
{
    if(state)
        VOL_Mute();
    else {
        VOL_ChangeVolume(TRUE,FALSE,pNVM_GetVol());
        VOL_UnMute(0);
    }
    DA_TxMute(state);
    DISP_Vol();
}

void UI_Toggle_Output(Output_Type output)
{
    if(NVM_GetOutputSw(output) == E_OUTPUT_SWITCHED) {
        if(PT_Active())
            UI_SetOutput(output, !NVM_GetPtOutput(output));
        else
            UI_SetOutput(output, !NVM_GetOutput(output));
    }
}

void UI_SetOutput(Output_Type output, bool state)
{
    IOX_Output(output, state);
    // IOX_OutputLedSet(output, state);   removed in C80

    if(PT_Active())
        NVM_SetPtOutput(output, state);
    else
        NVM_SetOutput(output, state);

    RS232_TxOutput(output, state);
    TRIG_Config();
}

//==============================================================================
// Private Functions
//==============================================================================
static void ui_set_tone(bool state)
{
//    IOX_ToneLed(state);  Removed for C80
    IOX_Tone(state);
    NVM_SetTone(NVM_GetSrc(), state);
}

//-----------------------------------------------------------------------------|
//	UI_Rename.h
//	Abstract: Source Rename UI module
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __UI_RENAME_H__
#define __UI_RENAME_H__

#include "Common.h"

typedef struct {
    uint32_t    input;                  /* input selected for rename */
    uint32_t    char_pos;               /* position being modified */
    uint32_t    blink_timer;            /* time for blinks */
    uint32_t    blink_pause;            /* pause blinks while actively changing */
    char        blink_char;
    char        name[C_MAX_INPUT_LEN];  /* temp storage for rename */
    bool        space_sub;
    char        space_restore;
} Ui_Rename_Type;

//===================================================================
// Public Function Prototypes
//===================================================================
void UI_RenameInput(uint8_t input); // call prior to entering rename menu
void UI_Rename_Process(uint32_t code);
void UI_Rename_Display(void);
void UI_Rename_Blink(void);
void UI_Rename_Exit(void);

#endif // __UI_RENAME_H__
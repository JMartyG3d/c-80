//-----------------------------------------------------------------------------|
//	UI_Trim.h
//	Abstract: Trim Menu UI module
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __UI_TRIM_H__
#define __UI_TRIM_H__

typedef enum {
    E_TRIM_FIRST   = 0,
    E_TRIM_BALANCE = 0,
    E_TRIM_INPUT_TRIM,
    E_TRIM_TONE,
    E_TRIM_BASS,
    E_TRIM_TREBLE,
    E_TRIM_MC_PHONO,
    E_TRIM_MM_PHONO,
    E_TRIM_MONO,
    E_TRIM_METERLIGHT,
    E_TRIM_METERLIGHT_PT,
    E_TRIM_BRIGHTNESS,
    E_TRIM_LIPSYNC,
    E_TRIM_HXD,
    E_TRIM_LAST
} Trim_Menu_Type;

//===================================================================
// Public Function Prototypes
//===================================================================
void UI_Trim_Init(void);
void UI_Trim_SetMenu(Trim_Menu_Type menu);
Trim_Menu_Type UI_TrimGetMenu(void);
void UI_Trim_Process(uint32_t code);
void UI_Trim_Display(void);
void UI_Trim_Exit(void);

#endif // __UI_TRIM_H__
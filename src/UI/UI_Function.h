//-----------------------------------------------------------------------------|
//	UI_Control.h
//	Abstract: Top level UI control module
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __UI_FUNCTION_H__
#define __UI_FUNCTION_H__

#include "Common.h"

//===================================================================
// Public Function Prototypes
//===================================================================
bool UI_WaitforKeyPress(void);
bool UI_WaitKeyInProgress(void);
void UI_WaitKeyPressed(void);
void UI_Toggle_HXD(void);
void UI_Set_HXD(uint8_t state);
void UI_Toggle_Tone(void);
void UI_Toggle_Mute(void);
void UI_SetMute(bool state);
void UI_Toggle_Output(Output_Type output);
void UI_SetOutput(Output_Type output, bool state);

#endif // __UI_FUNCTION_H__
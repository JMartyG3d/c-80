//-----------------------------------------------------------------------------|
//	UI_Passthru.h
//	Abstract: Passthru UI module
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include "UI_Passthru.h"
#include "UI_Control.h"
#include "UI_Common.h"
#include "UI_Function.h"
#include "PT_Control.h"
#include "DISP_Control.h"

//==============================================================================
// Private Function Prototypes
//==============================================================================
static void ui_passthru_keypress(uint32_t code);
static void ui_passthru_keyhold(uint32_t code);

//==============================================================================
// Public Functions
//==============================================================================
void UI_Passthru_Process(uint32_t code)
{
    if ((code & C_KEY_HOLD_MASK) == C_KEY_HOLD_MASK)
        ui_passthru_keyhold(code);
    else
        ui_passthru_keypress(code);
}

void UI_Passthru_Display(void)
{
	DISP_ClearLine(C_DISP_BOTHLINES);
	DISP_PrintCenter(C_DISP_LINE1,"PASSTHRU");
}

void UI_Passthru_Exit(void)
{
    PT_RestoreState();
    DISP_ClearLine(C_DISP_BOTHLINES);
    UI_SetMenu(E_MENU_MAIN);
}

//==============================================================================
// Private Functions
//==============================================================================
static void ui_passthru_keypress(uint32_t code)
{
    switch(code) {
        case C_KEY_TRIM:
        case C_IR_TRIM_BTN:
            if(UI_GetMenu() == E_MENU_PASSTHRU)
                UI_SetMenu(E_MENU_TRIM);
            break;
        case C_IR_OUT2_BTN:
        case C_KEY_OUT2:
            UI_Toggle_Output(E_OUTPUT_2);
            break;
        case C_IR_OUT1_BTN:
        case C_KEY_OUT1:
            UI_Toggle_Output(E_OUTPUT_1);
            break;
        case C_KEY_TONE:
        case C_KEY_MUTE:
        case C_RTY_INPUT_UP:
        case C_RTY_INPUT_DOWN:
        case C_RTY_VOLUME_UP:
        case C_RTY_VOLUME_DOWN:
        default:
            break;
    }
}

static void ui_passthru_keyhold(uint32_t code)
{
    uint32_t key = (code ^ C_KEY_HOLD_MASK);  // Mask out hold bit

    switch(key) {
        case C_KEY_TRIM:
        case C_KEY_OUT2:
        case C_KEY_OUT1:
        case C_KEY_TONE:
        case C_KEY_MUTE:
        default:    // do nothing
            break;
    }
}
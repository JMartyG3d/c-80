//-----------------------------------------------------------------------------|
//	UI_Main.c
//	Abstract: Main Menu UI module
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include "UI_Main.h"
#include "UI_Control.h"
#include "UI_Common.h"
#include "UI_Function.h"
#include "APP_Process.h"
#include "DISP_Control.h"
#include "SRC_Control.h"
#include "OUT_Control.h"
#include "IOX_Control.h"
#include "NVM_Access.h"
#include "VOL_Control.h"

//==============================================================================
// Private Function Prototypes
//==============================================================================
static void ui_main_keypress(uint32_t code);
static void ui_main_keyhold(uint32_t code);

//==============================================================================
// Public Functions
//==============================================================================
void UI_Main_Process(uint32_t code)
{
    if ((code & C_KEY_HOLD_MASK) == C_KEY_HOLD_MASK)
        ui_main_keyhold(code);
    else
        ui_main_keypress(code);
}

void UI_Main_Display(void)
{
    DISP_ClearLine(C_DISP_BOTHLINES);

    DISP_Src();
}

void UI_Main_Exit(void)
{

}

//==============================================================================
// Private Functions
//==============================================================================
static void ui_main_keypress(uint32_t code)
{
    switch(code) {
        case C_KEY_TONE: UI_Toggle_Tone(); break;
        case C_KEY_MUTE:    UI_Toggle_Mute();   break;
        case C_KEY_TRIM:
        case C_IR_TRIM_BTN:
            if(UI_GetMenu() == E_MENU_MAIN)
                UI_SetMenu(E_MENU_TRIM);
            break;
        case C_IR_OUT2_BTN:
//        case C_KEY_OUT2:
            if(OUT_Headphone() == E_HEADPHONE_IN) {
                bool out2 = NVM_GetPtOutput(E_OUTPUT_2);
                IOX_Output(E_OUTPUT_2, out2);  // was !IOX_OutputLedGet(E_OUTPUT_2)); removed LEDs in C80
//                IOX_OutputLedSet(E_OUTPUT_2, !IOX_OutputLedGet(E_OUTPUT_2));  ToDo: Save state of outputs selection for Headphones since no more OUTPUT LEDs in C80
            }
            else
                UI_Toggle_Output(E_OUTPUT_2);
            break;
        case C_IR_OUT1_BTN:
//        case C_KEY_OUT1:
            if(OUT_Headphone() == E_HEADPHONE_IN) {
                bool out1 = NVM_GetPtOutput(E_OUTPUT_1);  // ToDo:  Is this the right variable to read?  removed LEDs in C80
                IOX_Output(E_OUTPUT_1, out1);   // was !IOX_OutputLedGet(E_OUTPUT_1));
//                IOX_OutputLedSet(E_OUTPUT_1, !IOX_OutputLedGet(E_OUTPUT_1));  ToDo: Save state of outputs selection for Headphones since no more OUTPUT LEDs in C80
            }
            else
                UI_Toggle_Output(E_OUTPUT_1);
            break;
        case C_RTY_INPUT_UP:      SRC_Up(YES,NO);     break;
        case C_RTY_INPUT_DOWN:    SRC_Down(YES,NO);   break;
        case C_RTY_VOLUME_UP:
        case C_RTY_VOLUME_DOWN:
        default:
            break;
    }
}

static void ui_main_keyhold(uint32_t code)
{
    uint32_t key = (code ^ C_KEY_HOLD_MASK); // mask out the hold bit

    switch(key) {
        case C_KEY_TRIM:
            UI_SetMenu(E_MENU_SETUP);
            break;
        case C_KEY_MUTE:
//            UI_SetMenu(E_MENU_DEMO);    // DISABLE DEMO MODE
//            break;
        default:
            break;
    }
}
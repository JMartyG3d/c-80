//-----------------------------------------------------------------------------|
//	UI_Demo.h
//	Abstract: Demo Menu UI module
//            Used for canned screens for photography
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __UI_DEMO_H__
#define __UI_DEMO_H__

//===================================================================
// Public Function Prototypes
//===================================================================
void UI_Demo_Init(void);
void UI_Demo_Process(uint32_t code);
void UI_Demo_Display(void);
void UI_Demo_Exit(void);

#endif /* __UI_DEMO_H__ */
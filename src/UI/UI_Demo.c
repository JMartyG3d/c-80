//-----------------------------------------------------------------------------|
//	UI_Demo.c
//	Abstract: Demo Menu UI module
//            Used for canned screens for photography
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include "UI_Demo.h"
#include "UI_Common.h"
#include "UI_Control.h"
#include "DISP_Control.h"

#define C_UI_DEMO_OPTIONS 10

static uint32_t __ui_demo_option; // what demo option is selected

//==============================================================================
// Public Functions
//==============================================================================
void UI_Demo_Init(void)
{
    __ui_demo_option = 0;
}

void UI_Demo_Process(uint32_t code)
{
    switch(code) {
        case C_KEY_TRIM:  // return to main menu
            UI_Demo_Exit();
            break;
        case C_RTY_INPUT_DOWN: // toggle option
            if(__ui_demo_option == 0)
                __ui_demo_option = C_UI_DEMO_OPTIONS;
            else
                __ui_demo_option--;
            UI_Demo_Display();
            break;
        case C_RTY_INPUT_UP:
            __ui_demo_option = (__ui_demo_option + 1) % C_UI_DEMO_OPTIONS;
            UI_Demo_Display();
            break;
        default:        // do nothing
            break;
    }
}

void UI_Demo_Display(void)
{
    char ohm200_char[5] = {0x32,0x30,0x30,0x9A,'\0'};
    char disp[2];
    disp[1] = '\0';

    switch (__ui_demo_option) {
        case 0:
            DISP_PrintCenter(C_DISP_LINE1,"MC PHONO         60%");
            DISP_PrintCenter(C_DISP_LINE2,ohm200_char);
            break;
        case 1:
            DISP_PrintCenter(C_DISP_LINE1,"MM PHONO         60%");
            DISP_PrintCenter(C_DISP_LINE2,"MONO          300 pF");
            break;
        case 2:
            DISP_PrintCenter(C_DISP_LINE1,"MM PHONO         60%");
            DISP_PrintCenter(C_DISP_LINE2,"300 pF");
            break;
        case 3:
            DISP_PrintCenter(C_DISP_LINE1,"USB              60%");
            DISP_PrintCenter(C_DISP_LINE2,"DSD256\0");
            break;
        case 4:
            DISP_PrintCenter(C_DISP_LINE1,"BALANCED 1       60%");
            DISP_ClearLine(C_DISP_LINE2);
            break;
        case 5:
            DISP_PrintCenter(C_DISP_LINE1,"BALANCED 1       60%");
            DISP_PrintCenter(C_DISP_LINE2,"MONO");
            break;
        case 6:
            DISP_PrintCenter(C_DISP_LINE1,"MCT              60%");
            DISP_PrintCenter(C_DISP_LINE2,"SACD\0");
            break;
        case 7:
            disp[0] = 0x03;
            DISP_PrintCenter(C_DISP_LINE1,"P1   103.9 MHz  ST  ");
            DISP_Print(disp,C_DISP_LINE1,19);
            DISP_PrintCenter(C_DISP_LINE2,"Binghamton's Hits\0");
            break;
        case 8:
            disp[0] = 0x03;
            DISP_PrintCenter(C_DISP_LINE1,"P1   103.9 MHz  ST  ");
            DISP_Print(disp,C_DISP_LINE1,19);
            DISP_PrintCenter(C_DISP_LINE2,"Binghamton's Radio\0");
            break;
        case 9:
            disp[0] = 0x03;
            DISP_PrintCenter(C_DISP_LINE1,"P1   103.9 MHz  ST  ");
            DISP_Print(disp,C_DISP_LINE1,19);
            DISP_ClearLine(C_DISP_LINE2);
            break;
        default:
            break;
    }
}


void UI_Demo_Exit(void)
{
    UI_SetMenu(E_MENU_MAIN);
}

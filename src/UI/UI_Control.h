//-----------------------------------------------------------------------------|
//	UI_Control.h
//	Abstract: Top level UI control module
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __UI_CONTROL_H__
#define __UI_CONTROL_H__
#include "common.h"

#define C_UI_PERIOD_SEC 50  // how many times UI_Periodic is called per second
#define C_UI_QUEUE_SIZE 10  // number of inputs able to be queued

typedef enum {
    E_MENU_MAIN,
    E_MENU_TRIM,
    E_MENU_SETUP,
    E_MENU_PASSTHRU,
    E_MENU_RENAME,
    E_MENU_DEMO,
    E_MENU_NUM
} Ui_Menu_Type;

typedef enum {
    E_UI_TMR_NONE = 0,
    E_UI_TMR_TRIM,
    E_UI_TMR_DISP,
} Ui_Timer_Type;

//===================================================================
// Public Function Prototypes
//===================================================================
void UI_Init(void);
void UI_Periodic(void);
void UI_SetMenu(Ui_Menu_Type menu);
void UI_Input(uint32_t code);
Ui_Menu_Type UI_GetMenu(void);

#endif // __UI_CONTROL_H__
//-----------------------------------------------------------------------------|
//	UI_Main.h
//	Abstract: Main Menu UI module
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __UI_MAIN_H__
#define __UI_MAIN_H__

//===================================================================
// Public Function Prototypes
//===================================================================
void UI_Main_Process(uint32_t code);
void UI_Main_Display(void);
void UI_Main_Exit(void);

#endif // __UI_MAIN_H__
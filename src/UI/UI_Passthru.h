//-----------------------------------------------------------------------------|
//	UI_Passthru.h
//	Abstract: Passthru UI module
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __UI_PASSTHRU_H__
#define __UI_PASSTHRU_H__

//===================================================================
// Public Function Prototypes
//===================================================================
void UI_Passthru_Process(uint32_t code);
void UI_Passthru_Display(void);
void UI_Passthru_Exit(void);

#endif // __UI_PASSTHRU_H__
//-----------------------------------------------------------------------------|
//	UI_Common.h
//	Abstract: Common definitions and types used by UI functionality
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __UI_COMMON_H__
#define __UI_COMMON_H__

#define C_UI_PERIODIC       0xA5A5A5A5u // Periodic process
#define C_KEY_HOLD_MASK     0x40000000  // Bitwise OR with a key to indicate key hold
#define C_KEY_LATCH_MASK    0x80000000  // Bitwise OR with a key to indicate key latch

/** Front panel buttons ------------------------------------------------------*/
#define C_KEY_NOKEY         0x00
#define C_KEY_POWER		    0x01
#define C_KEY_MUTE		    0x02
#define C_KEY_TONE	        0x05
#define C_KEY_OUT2          0x04
#define C_KEY_OUT1          0x08
#define C_KEY_TRIM          0x20        // this is the input knob push button
#define C_KEY_SETUP         C_KEY_TRIM  // generally this shares same button as trim
#define C_KEY_SETUP_PB      0x40        // sometimes setup gets its own key

/** Rotary knobs -------------------------------------------------------------*/
#define C_RTY_TRIM_UP       0x100   // clockwise
#define C_RTY_TRIM_DOWN     0x200   // counter-clockwise
#define C_RTY_INPUT_UP      0x1000  // clockwise
#define C_RTY_INPUT_DOWN    0x2000  // counter-clockwise
#define C_RTY_VOLUME_UP     0x10000 // clockwise
#define C_RTY_VOLUME_DOWN   0x20000 // counter-clockwise

/** RIR Keys -----------------------------------------------------------------*/
#define C_IR_LEVEL_UP_BTN   0x100000
#define C_IR_LEVEL_DOWN_BTN 0x200000
#define C_IR_TRIM_BTN       0x400000
#define C_IR_MODE_BTN       0x800000
#define C_IR_OUT1_BTN       0x1000000
#define C_IR_OUT2_BTN       0x2000000

/** Timing and delays --------------------------------------------------------*/
#define C_HOLD_THRESHOLD    50

#endif // __UI_COMMON_H__

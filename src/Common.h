/*
//-----------------------------------------------------------------------------|
//	common.h
//	Abstract: Generic common defines and types
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
*/
#ifndef __COMMON_H__
#define __COMMON_H__

#define C_MAX_INPUT_LEN     11      // max length of input name (10 chars + NULL char)

typedef enum {
    E_SRC_FIRST    = 0,
    E_SRC_BAL1     = 0,
//    E_SRC_BAL2     = 1,
    E_SRC_UNBAL1   = 1,     // was 2
    E_SRC_UNBAL2   = 2,     // was 3
//    E_SRC_UNBAL3   = 4,
//    E_SRC_UNBAL4   = 4,  // ????  why no increment to 5?
    E_SRC_MM_PHONO = 3,     // was 5
    E_SRC_MC_PHONO = 4,     // was 6
    E_SRC_AE_LAST  = 4,     // DA NOT installed  // was 6
    E_SRC_AE_NUM   = 5,     // DA NOT installed  // was 7
    E_SRC_COAX1    = 5,                       // was 7
    E_SRC_COAX2    = 6,                       // was 8
    E_SRC_OPTI1    = 7,                       // was 9
    E_SRC_OPTI2    = 8,                      // was 10
    E_SRC_USB      = 9,                      // was 11
    E_SRC_MCT      = 10,                      // was 12
    E_SRC_HDMI     = 11,    // DA2 Only       // was 13
    E_SRC_LAST     = 11,                      // was 13
    E_SRC_NUM      = 12,                      // was 14
    E_SRC_DP_ALL   = 0xAA,
    E_SRC_PT_OFF   = 0xFF
} Src_Type;

typedef enum {
    E_SCHED_INIT = 0,
    E_SCHED_INACTIVE,
    E_SCHED_ACTIVE,
    E_SCHED_POWERUP,
    E_SCHED_WARMUP,
    E_SCHED_POWERDOWN,
} Schedule_Type;

typedef enum {
    E_BAUD_FIRST  = 0,
    E_BAUD_9600   = 0,
    E_BAUD_19200  = 1,
    E_BAUD_38400  = 2,
    E_BAUD_57600  = 3,
    E_BAUD_115200 = 4,
    E_BAUD_LAST   = 4,
} Baud_Type;

typedef enum {
    E_TRIG_ID_FIRST = 0,
    E_TRIG_ID1      = 0,
    E_TRIG_ID2      = 1,
    E_TRIG_ID_LAST  = 1,
    E_TRIG_ID_NUM   = 2
} Trig_Id_Type;

typedef enum {
    E_TRIG_FIRST = 0,
    E_TRIG_ON    = 0,
    E_TRIG_OUT1  = 1,
    E_TRIG_OUT2  = 2,
    E_TRIG_INPUT = 3,
    E_TRIG_LAST  = 3
} Trig_Type;

typedef enum {
    E_DPORT_ID_FIRST = 0,
    E_DPORT_ID1      = 0,
    E_DPORT_ID2      = 1,
    E_DPORT_ID3      = 2,
    E_DPORT_ID4      = 3,
    E_DPORT_ID_LAST  = 3,
    E_DPORT_ID_NUM   = 4,
    E_DPORT_ID_ALL   = 4
} Dport_Id_Type;

typedef enum {
    E_USB_MCU_UPDATE,
    E_USB_DA_UPDATE,
} Usb_Select_Type;

typedef enum {
    E_DA_RESET,
    E_DA_RUN,
} Da_Reset_Type;

typedef enum {
    E_OUTPUT_1 = 0,
    E_OUTPUT_2 = 1,
} Output_Type;

typedef enum {
    E_OUTPUT_SWITCHED,
    E_OUTPUT_UNSWITCHED,
} Output_Sw_Type;

typedef enum {
    E_METER_PT_MIN  = 0,
    E_METER_PT_OFF  = 0,
    E_METER_PT_ON   = 1,
    E_METER_PT_TRIG = 2,
    E_METER_PT_MAX  = 2,
} Meter_PT_Type;


typedef enum {
    E_HEADPHONE_IN    = 0,
    E_HEADPHONE_OUT   = 1,
    E_HEADPHONE_CHINA = 0xFF,
} HP_Type;

typedef enum {
    E_HP_OUT_MUTE_ALL  = 0,
    E_HP_OUT_MUTE_NONE = 1,
} Hp_Out_Type;

typedef enum { // (0=100%,1=75%,2=50%,3=25%)
    E_DISP_DUTY_MIN = 0,
    E_DISP_DUTY0    = 0,    // Brightest
    E_DISP_DUTY1    = 1,    // Default
    E_DISP_DUTY2    = 2,
    E_DISP_DUTY3    = 3,    // Dimmest
    E_DISP_DUTY_MAX = 3,
} Disp_Duty_Type;

typedef enum {
    E_PRES_FIRST    = 0,
    E_PRES_25_OHMS  = 0,
    E_PRES_50_OHMS  = 1,
    E_PRES_100_OHMS = 2,
    E_PRES_200_OHMS = 3,    // Default value
    E_PRES_400_OHMS = 4,
    E_PRES_1K_OHMS  = 5,
    E_PRES_LAST     = 5,
} Phono_Res_Type;

typedef enum {
    E_PCAP_FIRST    = 0,
    E_PCAP_50_PF    = 0,    // Default value
    E_PCAP_100_PF   = 1,
    E_PCAP_150_PF   = 2,
    E_PCAP_200_PF   = 3,
    E_PCAP_250_PF   = 4,
    E_PCAP_300_PF   = 5,
    E_PCAP_350_PF   = 6,
    E_PCAP_400_PF   = 7,
    E_PCAP_450_PF   = 8,
    E_PCAP_500_PF   = 9,
    E_PCAP_550_PF   = 10,
    E_PCAP_600_PF   = 11,
    E_PCAP_650_PF   = 12,
    E_PCAP_700_PF   = 13,
    E_PCAP_750_PF   = 14,
    E_PCAP_800_PF   = 15,
    E_PCAP_LAST     = 15
} Phono_Cap_Type;

typedef enum {
    E_VOL_TX,
    E_TONE_TX,
} Vol_Tx_Type;

typedef enum {
    E_LIP_SYNC_MANUAL,
    E_LIP_SYNC_AUTO
} Lip_Mode_Type;

typedef enum {
    E_LIP_TIME_FIRST = 0,
    E_LIP_TIME_0MS   = 0,
    E_LIP_TIME_10MS  = 10,
    E_LIP_TIME_20MS  = 20,
    E_LIP_TIME_30MS  = 30,
    E_LIP_TIME_40MS  = 40,
    E_LIP_TIME_50MS  = 50,
    E_LIP_TIME_60MS  = 60,
    E_LIP_TIME_70MS  = 70,
    E_LIP_TIME_80MS  = 80,
    E_LIP_TIME_90MS  = 90,
    E_LIP_TIME_100MS = 100,
    E_LIP_TIME_110MS = 110,
    E_LIP_TIME_120MS = 120,
    E_LIP_TIME_130MS = 130,
    E_LIP_TIME_140MS = 140,
    E_LIP_TIME_150MS = 150,
    E_LIP_TIME_LAST  = 150
} Lip_Time_Type;

typedef enum {
    E_DIG_GAIN_HDMI,
    E_DIG_GAIN_OPT1,
    E_DIG_GAIN_OPT2
} Dig_Gain_Type;
#endif /* __COMMON_H__ */
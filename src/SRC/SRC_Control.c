//-----------------------------------------------------------------------------|
//	SRC_Control.c
//	Abstract:
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include <string.h>
#include "APP_Process.h"
#include "SRC_Control.h"
#include "TMR_Control.h"
#include "IOX_Control.h"
#include "NVM_Access.h"
#include "NVM_Control.h"
#include "VOL_Control.h"
#include "DISP_Control.h"
#include "TRIG_Control.h"
#include "PT_Control.h"
#include "OUT_Control.h"
#include "DA_Config.h"
#include "RS232_Tx.h"
#include "UI_Setup.h"

typedef enum {
    E_SRC_OK = 0, // no work to be done
    E_SRC_BREAK,
    E_SRC_MAKE
} src_status_t;

static const char * __src_default_names[E_SRC_NUM] = {
    "BAL 1\0",
    "UNBAL 1\0",
    "UNBAL 2\0",
    "MM PHONO\0",
    "MC PHONO\0",
    "COAX 1\0",
    "COAX 2\0",
    "OPTI 1\0",
    "OPTI 2\0",
    "USB\0",
    "MCT\0",
    "HDMI(ARC)\0",
};

static src_status_t __src_status;
static Src_Type __src_input;    // mirror value in NVM_Status.src
static bool __changeinprogress;

bool __delay_start = FALSE, __delay_done = FALSE;
vuint16_t __delay_time = 0;

//==============================================================================
// Private Function Prototypes
//==============================================================================
static void src_break(void);
static void src_make(void);

//==============================================================================
// Public Functions
//==============================================================================
void SRC_Powerup(void)
{
    if(SRC_IsDigital(NVM_GetSrc()) && !DA_Installed())
        NVM_SetSrc(E_SRC_FIRST);

    __src_input = NVM_GetSrc();

    // validate current source
    if(!NVM_SrcIsOn(__src_input))
        SRC_Up(NO,NO);
    else
        src_break();    // perform the break
}

bool SRC_Set(Src_Type src)
{
    if(src < E_SRC_NUM && (NVM_SrcIsOn(src) || PT_Active())) {
        __src_input = src;
        NVM_SetSrc(src);
        src_break();
        DISP_Src();
        return TRUE;
    }
    return FALSE;
}

void SRC_Periodic(void)
{
    if(__src_status == E_SRC_BREAK) {
        if(TMR_Timeout(E_TMR_SRCMAKE))
            src_make();
    }
}

void SRC_PeriodicDelay(void)
{
    if(__delay_start) {
        if(__delay_time++ > 5) {
            __delay_done = TRUE;
            __delay_start = FALSE;
            __delay_time = 0;
        }
    }

    if(__delay_done) {
        __delay_done = FALSE;
        SRC_Set((Src_Type) NVM_GetSrc());
        RS232_TxSrc();
    }
}

void SRC_Up(bool delay, bool all_src)
{
    char disp[13]; disp[12]=0;
    
    __changeinprogress = TRUE;
    Src_Type next_src = SRC_NextUp(NVM_GetSrc(), all_src);

    if(delay) {
        __delay_start = TRUE;
        __delay_time = 0;
        DISP_ClearLine(C_DISP_LINE2);
        NVM_SetSrc(next_src);
        snprintf(disp, 13, "%-11s \0", NVM_SavedNames[next_src]);
        DISP_PrintLine(C_DISP_LINE1, disp);
        DISP_Vol();
    }
    else if(next_src != NVM_GetSrc()) {
        __src_input = next_src;
        NVM_SetSrc(__src_input);
        src_break();
    }
}

void SRC_Down(bool delay, bool all_src)
{
    char disp[13]; disp[12]=0;
    
    __changeinprogress = TRUE;
    Src_Type prev_src = SRC_NextDown(NVM_GetSrc(), all_src);

    if(delay) {
        __delay_start = TRUE;
        __delay_time = 0;
        DISP_ClearLine(C_DISP_LINE2);
        NVM_SetSrc(prev_src);
        snprintf(disp, 13, "%-11s \0", NVM_SavedNames[prev_src]);
        DISP_PrintLine(C_DISP_LINE1, disp);
        DISP_Vol();
    }
    else if(prev_src != NVM_GetSrc()) {
        __src_input = prev_src;
        NVM_SetSrc(__src_input);
        src_break();
    }
}

Src_Type SRC_NextUp(Src_Type src, bool all_src)
{
    Src_Type rtsrc;

    do{
        switch(src) {
            case E_SRC_AE_LAST:
                rtsrc = DA_Installed() ? E_SRC_COAX1 : E_SRC_BAL1;
                break;
            case E_SRC_MCT:
                if(DA_Installed()) {
                    if(DA1_Installed())
                        rtsrc = E_SRC_FIRST;
                    else if(DA2_Installed())
                        rtsrc = E_SRC_HDMI;
                }
                break;
            case E_SRC_LAST:    rtsrc = E_SRC_FIRST;         break;
            default:            rtsrc = (Src_Type)(src + 1); break;
        }

        if(all_src)
            break;

        src = rtsrc;
    } while(!NVM_SrcIsOn(rtsrc) || rtsrc == NVM_GetPtSrc());

    return rtsrc;
}

Src_Type SRC_NextDown(Src_Type src, bool all_src)
{
    Src_Type rtsrc;

    do{
        switch(src) {
            case E_SRC_COAX1: rtsrc = E_SRC_MC_PHONO;      break;
            case E_SRC_FIRST:
                if(DA_Installed()) {
                    if(DA1_Installed())
                        rtsrc = E_SRC_MCT;
                    else if(DA2_Installed())
                        rtsrc = E_SRC_HDMI;
                }
                else
                    rtsrc = E_SRC_MC_PHONO;
                break;
            default:          rtsrc = (Src_Type)(src - 1); break;
        }

        if(all_src)
            break;

        src = rtsrc;
    } while(!NVM_SrcIsOn(rtsrc) || rtsrc == NVM_GetPtSrc());

    return rtsrc;
}

bool SRC_IsDigital(Src_Type src)
{
    return(src == E_SRC_USB   || src == E_SRC_MCT   || src == E_SRC_OPTI1 ||
           src == E_SRC_OPTI2 || src == E_SRC_COAX1 || src == E_SRC_COAX2 ||
           src == E_SRC_HDMI );
}

bool SRC_IsLipSync(Src_Type src)
{
    return(src == E_SRC_OPTI1 || src == E_SRC_OPTI2 || src == E_SRC_COAX1 ||
           src == E_SRC_COAX2 || src == E_SRC_HDMI );
}

bool SRC_IsPhono(Src_Type src)
{
    return(src == E_SRC_MM_PHONO || src == E_SRC_MC_PHONO);
}

bool SRC_ChangeInProgress(void)
{
    return __changeinprogress;
}

bool SRC_GetDefaultName(Src_Type src, char * name)
{
    if(src < E_SRC_NUM) {
        strncpy(name, __src_default_names[src], 10);

        return TRUE;
    }
    else
        return FALSE;
}

//==============================================================================
// Private Functions
//==============================================================================
static void src_break(void)
{
    TMR_Reset(E_TMR_SRCMAKE);    // reset timer
    __changeinprogress = TRUE;

    if (__src_status != E_SRC_BREAK) {
        __src_status = E_SRC_BREAK;         // set status

        if(!VOL_GetMute())
            VOL_ChangeVolume(FALSE, TRUE, 0);   // set volume to 0

        IOX_InputClear();
    }

    if(APP_GetSchedule() == E_SCHED_ACTIVE) {
        DPORT_Config();
        IOX_Output(E_OUTPUT_1, OFF);
        IOX_Output(E_OUTPUT_2, OFF);
        IOX_Headphones(OFF);
    }
}

static void src_make(void)
{
    bool output_1_ToSet = FALSE; 
    bool output_2_ToSet = FALSE;

    __src_status = E_SRC_MAKE;    // set status

    IOX_InputSet(__src_input);

    if(APP_GetSchedule() == E_SCHED_ACTIVE) { // don't unmute volume during power up
        delay_ms(500);

        if((OUT_Headphone() == E_HEADPHONE_IN) && !NVM_GetHpOut() && (FALSE == PT_Active()))
            IOX_Headphones(ON);
        else {
            IOX_Headphones(OFF);

            if (FALSE == PT_Active()){
                output_1_ToSet = NVM_GetOutput(E_OUTPUT_1);
                output_2_ToSet = NVM_GetOutput(E_OUTPUT_2);
            }
            else{
                output_1_ToSet = NVM_GetPtOutput(E_OUTPUT_1);
                output_2_ToSet = NVM_GetPtOutput(E_OUTPUT_2);
            }
            IOX_Output(E_OUTPUT_1, output_1_ToSet);
            delay_ms(20);
            IOX_Output(E_OUTPUT_2, output_2_ToSet);
            delay_ms(20);
            
//            IOX_OutputLedSet(E_OUTPUT_1, output_1_ToSet);  Removed for C80
            delay_ms(20);
//            IOX_OutputLedSet(E_OUTPUT_2, output_2_ToSet);  Removed for C80
        }

        // Unmute and set volume
        if(!VOL_GetMute()) {
            VOL_UnMute(1);

            VOL_ChangeVolume(TRUE, FALSE, pNVM_GetVol());
        }
    }

    TRIG_Config();

    if(APP_GetSchedule() == E_SCHED_ACTIVE)
        DISP_MainMenu();

    __changeinprogress = FALSE;
    __src_status = E_SRC_OK;    // set status
}

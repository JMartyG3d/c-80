//-----------------------------------------------------------------------------|
//	SRC_Control.h
//	Abstract:
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __SRC_CONTROL_H__
#define __SRC_CONTROL_H__

#include "Common.h"

//==============================================================================
// Public Function Prototypes
//==============================================================================
void SRC_Powerup(void);     // sets initial source and does break
void SRC_Up(bool delay, bool all_src);
void SRC_Down(bool delay, bool all_src);
bool SRC_Set(Src_Type src); // sets selected source and does break
void SRC_Periodic(void);    // performs make when required
void SRC_PeriodicDelay(void);
Src_Type SRC_NextUp(Src_Type src, bool all_src);
Src_Type SRC_NextDown(Src_Type src, bool all_src);
bool SRC_IsDigital(Src_Type src);
bool SRC_IsLipSync(Src_Type src);
bool SRC_IsPhono(Src_Type src);
bool SRC_ChangeInProgress(void);
bool SRC_GetDefaultName(Src_Type src, char * name);

#endif // __SRC_CONTROL_H__
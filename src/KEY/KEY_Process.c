//-----------------------------------------------------------------------------|
//	KEY_Process.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include "KEY_Process.h"
#include "ADC_Process.h"
#include "APP_Process.h"
#include "UI_Common.h"
#include "UI_Control.h"
#include "PWR_Control.h"
#include "UI_Function.h"
#include "PT_Control.h"
#include "IOX_Control.h"
#include "NVM_Access.h"

// the translation table to the key code used by the UI module (found in UI_Common.h)
// match the order of Key_eType
static const uint32_t KEY_CODE[E_KEY_LAST] = {
    C_KEY_POWER,
    C_KEY_TRIM,
    C_KEY_MUTE,
    C_KEY_OUT1,
    C_KEY_OUT2,
    C_KEY_TONE
};

// Only initialize the types - match the order of Key_eType
static Key_Status_Type __keys[E_KEY_LAST] = {
    {.type=C_KEY_PIOTYPE},      // Power button
    {.type=C_KEY_ADCTYPE_2},    // Trim/Setup button (input knob)
    {.type=C_KEY_ADCTYPE_1},    // Mute button
    {.type=C_KEY_ADCTYPE_1},    // Output 1 button
    {.type=C_KEY_ADCTYPE_1},    // Output 2 button
    {.type=C_KEY_ADCTYPE_1},    // Tone button
};

static uint8_t  __key_adc_current;       // adc key currently pressed
static bool     __first_pwr_on;

//===================================================================
// Private Function Prototypes
//===================================================================
static void     key_pio_process(void);              // process PIO keys
static void     key_pio_service(Key_eType key); // service key from periodic
static void     key_pio_zero(Key_eType key);    // zero key from periodic
static void     key_adc_process(void);              // process ADC keys
static uint8_t  key_adc_identify(uint32_t data);    // identify the ADC key pressed
static void     key_adc_set(uint8_t key);           // sets the ADC key press based on current state
static void     key_adc_debounce_handler(uint8_t key); // handles key in the debounce state
static void     key_adc_press_handler(uint8_t key);    // handles key in the press state
static void     key_adc_hold_handler(uint8_t key);     // handles key in the hold state
static void     key_adc_clear(uint8_t key);            // clear the key

//===================================================================
// Public Functions
//===================================================================;
void KEY_Init(void)
{
    for(uint16_t i=0;i<E_KEY_LAST;i++)
        key_pio_zero((Key_eType)i);
}

// id == ?, mask == pin(s) that triggered interrupt
void PIO_PwrKeyHandler(uint32_t id, uint32_t mask)
{
    vuint32_t status = PIOA->PIO_PDSR;

    if(PT_Active())
        return;

    if((APP_GetSchedule() == E_SCHED_POWERUP) || (APP_GetSchedule() == E_SCHED_WARMUP) || (APP_GetSchedule() == E_SCHED_POWERDOWN))
        return;

    // Pwr Key Generated Interrupt
    if((mask & PIN20_POWER_KEY) == PIN20_POWER_KEY) {

        // Pwr Key pressed
        if((status & PIN20_POWER_KEY) == PIN20_POWER_KEY) {
            if(__first_pwr_on) {
                pio_disable_interrupt(PIOA, PIN20_POWER_KEY);
                PWR_Command();
                __first_pwr_on = FALSE;
                PWR_SetSleepState(NO);
            }
            else {
                if(NVM_GetSrc() != E_SRC_HDMI || NVM_GetHdmiCecPwr() == OFF) {
                    pio_disable_interrupt(PIOA, PIN20_POWER_KEY);
                    PWR_Command();
                    PWR_SetSleepState(NO);
                }
                else {
                    if(PWR_GetSleepState()) {   // PWR: Sleeping
                        IOX_SleepExit();
                        PWR_SetSleepState(FALSE);
                    }
                    else {  // PWR: ON
                        UI_Init();
                        IOX_SleepEnter();
                        PWR_SetSleepState(TRUE);
                    }
                }
            }
        }
    }
}

void KEY_FirstPwrOn(void)
{
    __first_pwr_on = TRUE;
}
void KEY_PwrEnable(void)
{
    pio_enable_interrupt(PIOA, PIN20_POWER_KEY);
}

void KEY_Periodic(void)
{
    key_pio_process();

    if(PWR_GetSleepState() == YES)
        return;

    if((APP_GetSchedule() == E_SCHED_ACTIVE) || UI_WaitKeyInProgress())
        key_adc_process();
}

//===================================================================
// Private Functions
//===================================================================
static void key_pio_process(void)
{
    for(uint32_t i=0;i<E_KEY_LAST;i++) { // loop over all keys
        // handle only PIO Type keys
        if (__keys[i].type == C_KEY_PIOTYPE) {
            // if the key is not inactive, increment hold count
            if (__keys[i].status.pio.state > 0)
                __keys[i].status.pio.count++;

// NOTE
// D1100 has no PIO keys that have a "HOLD" function.
// The power button has a hardware hold implementation that triggers a micro reset

            // if key is a multiple of hold count, set key to service
            if ((__keys[i].status.pio.count > 0) && ((__keys[i].status.pio.count % C_HOLD_THRESHOLD) == 0))
                __keys[i].status.pio.service = TRUE;

            // if key is set for service, service
            if (__keys[i].status.pio.service)
                key_pio_service((Key_eType)i);

            // zero any inactive key
            if (__keys[i].status.pio.state == 0)
                key_pio_zero((Key_eType)i);
        }
    }
}

static void key_pio_service(Key_eType key)
{
    // because of the sleep state, reduce the hold count by 50, to a minumum of 5
    if (APP_GetSchedule() == E_SCHED_INACTIVE)
        __keys[key].status.pio.count = (__keys[key].status.pio.count > 55) ? (__keys[key].status.pio.count - 50) : 5;

    // Filter States -------------------------------
    // if hold count less than hold threshold
    if (__keys[key].status.pio.count < C_HOLD_THRESHOLD)
        UI_Input(KEY_CODE[key]);    // process normal

    // if hold count equal to hold threshold
    if (__keys[key].status.pio.count == C_HOLD_THRESHOLD)
        UI_Input((KEY_CODE[key] | C_KEY_HOLD_MASK));  // process hold

    // if hold count multiple to hold threshold
    if ((__keys[key].status.pio.count % C_HOLD_THRESHOLD) == 0)
        UI_Input((KEY_CODE[key] | C_KEY_HOLD_MASK | C_KEY_LATCH_MASK)); // process latch
}

static void key_pio_zero(Key_eType key)
{
    __keys[key].status.pio.state = 0;
    __keys[key].status.pio.count = 0;
    __keys[key].status.pio.service = FALSE;
}

// -----------------------------------------------------------------------------
// -- ADC Section --------------------------------------------------------------
// -----------------------------------------------------------------------------
static void key_adc_process(void)
{
    uint8_t key;
    uint32_t adc = ADC_GetChan(E_ADC_KEYS); // retrieve last adc data

    // process any new key press
    if(adc != C_ADC_NO_DATA) {

        __key_adc_current = key = key_adc_identify(adc); // id the key

        if(UI_WaitKeyInProgress()) {

            if(key == E_KEY_TRIM)
                UI_WaitKeyPressed();

            __key_adc_current = 0xFF;
            return;
        }

        key_adc_set(key); // set the key
    }
    else
        __key_adc_current = 0xFF;

    // dispatch keys to respective state handlers
    for(uint8_t i=0;i<E_KEY_LAST;i++) { // loop over all keys
        // handle only ADC Type keys
        if (__keys[i].type != C_KEY_PIOTYPE) {
            // check to see active keys are currently set, otherwise increment zero counter
            if ((__keys[i].status.adc.state != E_KEY_ADC_INACTIVE) && (__key_adc_current != key))
                __keys[i].status.adc.zero_cnt++;

            // dispatch to handlers
            switch(__keys[i].status.adc.state) {
                case E_KEY_ADC_DEBOUNCE: key_adc_debounce_handler(i); break;
                case E_KEY_ADC_PRESS:    key_adc_press_handler(i);    break;
                case E_KEY_ADC_HOLD:     key_adc_hold_handler(i);     break;
                case E_KEY_ADC_INACTIVE:
                default: break;
            }
        }
    }
}

static uint8_t key_adc_identify(uint32_t data)
{
    // Current ADC key handling ocde assumes keys are tied to single ADC channel
	uint8_t keycode =  (data < C_ADC_0_00V ? E_KEY_TRIM :
                        data < C_ADC_0_67V ? E_KEY_OUT1 :
                        data < C_ADC_1_36V ? E_KEY_OUT2 :
                        data < C_ADC_1_94V ? E_KEY_EQ   :
                        data < C_ADC_2_62V ? E_KEY_MUTE : 0xFF); // should never report 0xFF

    return keycode;
}

static void key_adc_set(uint8_t key)
{
    // validate key before setting
    if ((key != 0xFF) && (__keys[key].type != C_KEY_PIOTYPE)) {
        // set the key based on its current state
        switch (__keys[key].status.adc.state) {
            case E_KEY_ADC_INACTIVE: // key not previously set
                // clear key, then set state to debounce
                key_adc_clear(key);
                __keys[key].status.adc.state = E_KEY_ADC_DEBOUNCE;
                break;
             case E_KEY_ADC_DEBOUNCE: // key previously set
             case E_KEY_ADC_PRESS:
             case E_KEY_ADC_HOLD:
             default: // increment hold count
                __keys[key].status.adc.hold_cnt++;
                break;
        }
    }
}

static void key_adc_debounce_handler(uint8_t key)
{
    // assume by this point key has been validated as adc key
    // 3 cases for state of key: (in order of precedence)
    // ---------------------------------------------------
    // 1. hold count > press threshold -> transition to pressed state (and run the handler)
    // 2. Zero count > zero threshold   -> clear key (consider as no key press)
    // 3. Neither of the above are true, do nothing

    if (__keys[key].status.adc.hold_cnt > C_KEY_ADC_PRESS_THRESHOLD) {
        __keys[key].status.adc.state = E_KEY_ADC_PRESS;
        key_adc_press_handler(key);
    }
    else {
        if(__keys[key].status.adc.zero_cnt > C_KEY_ADC_ZERO_THRESHOLD)
            key_adc_clear(key);
    }
}

static void key_adc_press_handler(uint8_t key)
{
    // assume by this point key has been validated as adc key
    // States flow chart
    // -------------------------------------------------------------------------
    // 1. ADC Key type 1
    //    -- hold count > press theshold?
    //    ---- Y: Service key if proc_cnt == 0
    //    ---- N: Should not occur here
    //    -- zero count > zero threshold?
    //    ---- Y: Clear key
    //    ---- N : do nothing
    //
    // 2. ADC Key type 2
    //    -- zero count > zero threshold?
    //    ---- Y: service key, then clear key
    //    ---- N: hold count > hold threshold ?
    //    ------ Y: Transition key to hold and call handler
    //    ------ N: Do nothing

    if(__keys[key].type == C_KEY_ADCTYPE_1) {
        if(__keys[key].status.adc.proc_cnt == 0) {
            __keys[key].status.adc.proc_cnt++;  // Increment proc count
            UI_Input(KEY_CODE[key]);            // Pass to UI module for service
        }

        if(__keys[key].status.adc.zero_cnt > C_KEY_ADC_ZERO_THRESHOLD)
            key_adc_clear(key);
    }
    else {// C_KEY_ADCTYPE_2
        if(__keys[key].status.adc.zero_cnt > C_KEY_ADC_ZERO_THRESHOLD) {
            __keys[key].status.adc.proc_cnt++;  // Increment proc count
            UI_Input(KEY_CODE[key]);            // Pass to UI module for service
            key_adc_clear(key);                 // clear key
        }
        else {
            if(__keys[key].status.adc.hold_cnt > C_KEY_ADC_HOLD_THRESHOLD) {
                // mark as hold and pass to hold handler
                __keys[key].status.adc.state = E_KEY_ADC_HOLD;
                key_adc_hold_handler(key);
            }
        }
    }
}

static void key_adc_hold_handler(uint8_t key)
{
    // assume by this point key has been validated as adc key
    // States flow chart
    // -------------------------------------------------------------------------
    // 1. ADC Key type 1
    //    -- Do nothing, currently type 1 keys do not hold or recurr
    //    -- if in the future need a type 1 keyhold, they should behave similar, if not identically to type 2
    //
    // 2. ADC Key type 2
    //    -- hold count > hold threshold * proc cnt? (this handles key recurrence)
    //    ---- Y: Increment proc cnt, service key
    //    ---- N: zero count > zero threshold?
    //    ------ Y: If  proc cnt is 0, service key.  Clear key in both cases.
    //    ------ N: Do nothing
    if (__keys[key].type != C_KEY_ADCTYPE_1) {
        if (__keys[key].status.adc.hold_cnt > (C_KEY_ADC_HOLD_THRESHOLD * __keys[key].status.adc.proc_cnt)) {
            // increment proc cnt
            __keys[key].status.adc.proc_cnt++;

            // Pass to UI module for service
            UI_Input(KEY_CODE[key] | C_KEY_HOLD_MASK);
        }
        else {
            if (__keys[key].status.adc.zero_cnt > C_KEY_ADC_ZERO_THRESHOLD) {
                // make sure the key has been serviced at least once
                if (__keys[key].status.adc.proc_cnt == 0)
                    UI_Input(KEY_CODE[key] | C_KEY_HOLD_MASK);    // Pass to UI module for service

                key_adc_clear(key);
            }
        }
    }
}

static void key_adc_clear(uint8_t key)
{   // assume by this point key has been validated as adc key
    __keys[key].status.adc.state = E_KEY_ADC_INACTIVE;
    __keys[key].status.adc.hold_cnt = 0;
    __keys[key].status.adc.zero_cnt = 0;
    __keys[key].status.adc.proc_cnt = 0;
}

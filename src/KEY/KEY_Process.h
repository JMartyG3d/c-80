//-----------------------------------------------------------------------------|
//	KEY_Process.h
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __KEY_PROCESS_H__
#define __KEY_PROCESS_H__

#include "Common.h"

// NOTES ///////////////////////////////////////////////////////////////////////
// 1. PIO and ADC keys could probable be integrated more seemlessly into a single
//      module, and might be in the future.  However due to time constraints I have
//      chosen to have completely seperate functions and handling for each.
//
// 2. It would be worth investigating using ADC interrupts in the same manner that
//      the PIO interrupt is used to set a key
////////////////////////////////////////////////////////////////////////////////

// defines for the various key types that exist
#define C_KEY_PIOTYPE       0x00
#define C_KEY_ADCTYPE_1     0x01
#define C_KEY_ADCTYPE_2     0x02
// Descriptions:
// C_KEY_PIOTYPE: Key hooked to PIO and each key press is caught/debounced by the MCU PIO IRQ handler
// C_KEY_ADCTYPE_1: Key hooked to an ADC, key will only be serviced by initial key press (no hold, no repeat)
// C_KEY_ADCTYPE_2: Key hooked to an ADC, key will be serviced when it is released, or after a hold is registered. (no repeats)

// misc adc defines
#define C_KEY_ADC_PRESS_THRESHOLD   2
#define C_KEY_ADC_HOLD_THRESHOLD   50
#define C_KEY_ADC_ZERO_THRESHOLD    2

// the keys that exist in this unit
typedef enum {
    E_KEY_PWR = 0,
    E_KEY_TRIM,
    E_KEY_MUTE,
    E_KEY_OUT1,
    E_KEY_OUT2,
    E_KEY_EQ,
    E_KEY_LAST
} Key_eType;

// state types for adc keys, required because we manually debounce each key
typedef enum {
    E_KEY_ADC_INACTIVE = 0, // key is not pressed
    E_KEY_ADC_DEBOUNCE,     // key is signaled, currently debouncing
    E_KEY_ADC_PRESS,        // key has debounced and is pressed
    E_KEY_ADC_HOLD,         // key has been held long enough to register a hold
} Key_Adc_State_eType;

typedef struct {
    const uint8_t type; // type determines how each key is processed
    union {
       struct {
            uint32_t state;   // 0->inactive, 1->active, 2->latched
            uint32_t count;   // held/pressed count (incremented by periodic function)
            bool     service; // indicates key should be serviced
        } pio;
       struct {
            uint32_t state;     // tracked with Key_Adc_State_eType
            uint32_t hold_cnt;  // cycles this key has been held
            uint32_t zero_cnt;  // cycles this key has been clear
            uint32_t proc_cnt;  // times this key has been processed while be pressed/held
        } adc;
    } status;
} Key_Status_Type;

//===================================================================
// Public Function Prototypes
//===================================================================
void KEY_Init(void);
void PIO_PwrKeyHandler(uint32_t id, uint32_t mask);
void KEY_FirstPwrOn(void);
void KEY_PwrEnable(void);
void KEY_Periodic(void);

#endif // __KEY_PROCESS_H__

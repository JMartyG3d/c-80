//-----------------------------------------------------------------------------|
//	RS232_Process.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include <string.h>
#include "VOL_Control.h"
#include "SRC_Control.h"
#include "RS232_Control.h"
#include "RS232_Queue.h"
#include "UART_Driver.h"
#include "RS232_Commands.h"

//===================================================================
// Public Functions
//===================================================================
void RS232_Init(void)
{
    UART_Init(3);
    RS232_QueueInit();
    RS232_ResetStatusDefault();
}

void RS232_Periodic(void)
{
    if(SRC_ChangeInProgress() || VOL_GetWakeup())
        return;

    RS232_RxMsg_Type msg;
    bool rc = RS232_QueuePopRxMsg(&msg); // attempt to retrieve a message

    while(rc) {
        RS232_ParseCmd(msg);
        rc = RS232_QueuePopRxMsg(&msg); // attempt to retrieve next message
    }
}

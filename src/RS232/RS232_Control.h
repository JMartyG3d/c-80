//-----------------------------------------------------------------------------|
//	RS232_Process.h
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __RS232_PROCESS_H__
#define __RS232_PROCESS_H__

typedef struct {
    uint8_t params;
    char cmd[3];
    char param1[4];
    char param2[4];
    char param3[4];
} RS232_RxMsg_Type;

//===================================================================
// Public Function Prototypes
//===================================================================
void RS232_Init(void);
void RS232_Periodic(void);

#endif // __RS232_PROCESS_H__


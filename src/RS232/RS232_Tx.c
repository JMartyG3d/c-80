//-----------------------------------------------------------------------------|
//	RS232_Tx.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include <string.h>
#include <stdio.h>
#include "RS232_Tx.h"
#include "RS232_Commands.h"
#include "UART_Driver.h"
#include "RS232_Queue.h"
#include "DISP_Control.h"
#include "SRC_Control.h"
#include "APP_Process.h"
#include "VOL_Control.h"
#include "NVM_Access.h"
#include "OUT_Control.h"

#define kMaxStringLen 20
char str[kMaxStringLen]; 

//===================================================================
// Public Functions
//===================================================================
void RS232_TxPwr(bool state)
{
    if(!RS232_StatusShow())
        return;

    snprintf( str, kMaxStringLen,  "(%s %d)\0", "PWR", state);
    UART_TxBuf(str, strlen(str));
}

void RS232_TxVol(uint8_t vol)
{
    if(!RS232_StatusShow())
        return;

    snprintf( str, kMaxStringLen,  "(%s %d)\0", "VOL", vol);
    UART_TxBuf(str, strlen(str));
}

void RS232_TxMute(bool state)
{
    if(!RS232_StatusShow())
        return;

    snprintf( str, kMaxStringLen,  "(%s %d)\0", "MUT", state);
    UART_TxBuf(str, strlen(str));
}

void RS232_TxOutput(Output_Type out, bool state)
{
    if(!RS232_StatusShow())
        return;

    if(out == E_OUTPUT_1)
        snprintf( str, kMaxStringLen,  "(%s %d)\0", "OP1", state);
    else
        snprintf( str, kMaxStringLen,  "(%s %d)\0", "OP2", state);

    UART_TxBuf(str, strlen(str));
}

void RS232_TxSrc(void)
{
    if(!RS232_StatusShow())
        return;

    snprintf( str, kMaxStringLen,  "(%s %d)\0", "INP", NVM_GetSrc()+1);
    UART_TxBuf(str, strlen(str));
}

void RS232_TxBalance(void)
{
    if(!RS232_StatusShow())
        return;

    snprintf( str, kMaxStringLen,  "(%s %d)\0", "TBA", NVM_GetBalance());
    UART_TxBuf(str, strlen(str));
}

void RS232_TxInputTrim(void)
{
    if(!RS232_StatusShow())
        return;

    snprintf( str, kMaxStringLen,  "(%s %d)\0", "TIN", NVM_GetLevel());
    UART_TxBuf(str, strlen(str));
}

void RS232_TxTone(bool state)
{
    if(!RS232_StatusShow())
        return;

    snprintf( str, kMaxStringLen,  "(%s %d)\0", "TTN", state);
    UART_TxBuf(str, strlen(str));
}

void RS232_TxBass(void)
{
    if(!RS232_StatusShow())
        return;

    if(NVM_GetTone(NVM_GetSrc()) == ON)
        snprintf( str, kMaxStringLen,  "(%s %d)\0", "TTB", NVM_GetBass(NVM_GetSrc()));
    else
        snprintf( str, kMaxStringLen,  "(%s %d)\0", "TTB", 0);

    UART_TxBuf(str, strlen(str));
}

void RS232_TxTreble(void)
{
    if(!RS232_StatusShow())
        return;

    if(NVM_GetTone(NVM_GetSrc()) == ON)
        snprintf( str, kMaxStringLen,  "(%s %d)\0", "TTT", NVM_GetTreble(NVM_GetSrc()));
    else
        snprintf( str, kMaxStringLen,  "(%s %d)\0", "TTT", 0);

    UART_TxBuf(str, strlen(str));
}

void RS232_TxPhonoRes(void)
{
    if(!RS232_StatusShow())
        return;

    snprintf( str, kMaxStringLen,  "(%s %d)\0", "TPR", NVM_GetMcPhonoRes()+1);
    UART_TxBuf(str, strlen(str));
}

void RS232_TxPhonoCap(void)
{
    if(!RS232_StatusShow())
        return;

    snprintf( str, kMaxStringLen,  "(%s %d)\0", "TPC", NVM_GetMmPhonoCap()+1);
    UART_TxBuf(str, strlen(str));
}


void RS232_TxMono(void)
{
    if(!RS232_StatusShow())
        return;

    snprintf( str, kMaxStringLen,  "(%s %d)\0", "TMO", NVM_GetMono(NVM_GetSrc()));
    UART_TxBuf(str, strlen(str));
}

void RS232_TxTrigMeterlight(void)
{
    if(!RS232_StatusShow())
        return;

    snprintf( str, kMaxStringLen,  "(%s %d)\0", "TML", NVM_GetTrigMeterlight());
    UART_TxBuf(str, strlen(str));
}

void RS232_TxBrightness(void)
{
    if(!RS232_StatusShow())
        return;

    switch(NVM_GetBrightness()) {
        case E_DISP_DUTY0:  snprintf( str, kMaxStringLen,  "(%s %d)\0", "TDB", 4); break;
        case E_DISP_DUTY1:  snprintf( str, kMaxStringLen,  "(%s %d)\0", "TDB", 3); break;
        case E_DISP_DUTY2:  snprintf( str, kMaxStringLen,  "(%s %d)\0", "TDB", 2); break;
        case E_DISP_DUTY3:  snprintf( str, kMaxStringLen,  "(%s %d)\0", "TDB", 1); break;
    }

    UART_TxBuf(str, strlen(str));
}

void RS232_TxLipSyncDelay(void)
{
    if(!RS232_StatusShow())
        return;

    snprintf( str, kMaxStringLen,  "(%s %d)\0", "TLS", NVM_GetLipSyncTime());
    UART_TxBuf(str, strlen(str));
}


void RS232_TxHp(void)
{
    if(!RS232_StatusShow())
        return;

    switch(OUT_Headphone()) {
        case E_HEADPHONE_OUT:   snprintf( str, kMaxStringLen,  "(%s %d)\0","HPS", 0); break;
        case E_HEADPHONE_IN:    snprintf( str, kMaxStringLen,  "(%s %d)\0","HPS", 1); break;
        case E_HEADPHONE_CHINA: snprintf( str, kMaxStringLen,  "(%s %d)\0","HPS", 2); break;
    }

    UART_TxBuf(str, strlen(str));
}

void RS232_TxHpHxd(void)
{
    if(!RS232_StatusShow())
        return;

    snprintf( str, kMaxStringLen,  "(%s %d)\0", "THH", NVM_GetHXD());
    UART_TxBuf(str, strlen(str));
}

//-----------------------------------------------------------------------------|
//	RS232_Commands.c
//	Abstract: RS232 command parsing and processing
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include <string.h>
#include "RS232_Commands.h"
#include "RS232_Queue.h"
#include "RS232_Tx.h"
#include "UART_Driver.h"
#include "UI_Common.h"
#include "UI_Control.h"
#include "UI_Function.h"
#include "UI_Trim.h"
#include "APP_Process.h"
#include "VOL_Control.h"
#include "SRC_Control.h"
#include "OUT_Control.h"
#include "NVM_Access.h"
#include "NVM_Control.h"
#include "TST_Control.h"
#include "IOX_Control.h"
#include "DISP_Control.h"
#include "IR_Process.h"
#include "UI_Setup.h"
#include "PRODUCT_Config.h"
#include "PWR_Control.h"

#define kXferBufMaxChars 20

static bool gShow_status;

//==============================================================================
// Private Function Prototypes
//==============================================================================
static uint32_t rs232_parse_cmd_text(char * cmd);

void RS232_SendProductInfo(void);

//==============================================================================
// Public Function Definitions
//==============================================================================
void RS232_ParseCmd(RS232_RxMsg_Type rx_cmd)
{
    int32_t param1, param2, param3;
    int32_t percentage = 0;
    char    direction = ' ';
    uint8_t trimidx;
    bool status = FALSE;
    static bool power;

    switch (rx_cmd.params) {
        case 3: param3 = atoi(rx_cmd.param3);
        case 2: param2 = atoi(rx_cmd.param2);
        case 1: param1 = atoi(rx_cmd.param1);
            if(rx_cmd.param1[0] == 'U' || rx_cmd.param1[0] == 'D' ||
               rx_cmd.param1[0] == 'L' || rx_cmd.param1[0] == 'R')
                direction = rx_cmd.param1[0];
            else
                percentage = atoi(rx_cmd.param1);
        default: break;
    }

    uint32_t cmd = rs232_parse_cmd_text(rx_cmd.cmd);

    if(((APP_GetSchedule() == E_SCHED_INACTIVE) && (cmd != C_RS232_CMD_PWR)) ||
       ((PWR_GetSleepState() == YES) && (cmd != C_RS232_CMD_PWR))) {

        if(cmd == C_RS232_CMD_QRY)
            RS232_QueryPwrOff();
        else
            RS232_TxError(C_RS232_ERROR_COMMAND);

        return;
    }

    switch(cmd) {
        // --------------------------------------------------------------------|
        // STATUS -------------------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_STA:
            if (rx_cmd.params > 0) {
                if ((param1 == 0) || (param1 == 1))
                    gShow_status = param1;
                else
                    RS232_TxError(C_RS232_ERROR_PARAM);
            }
            else
                RS232_TxCmd1("STA", gShow_status);
            break;

        // --------------------------------------------------------------------|
        // POWER --------------------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_PWR:

            power = (APP_GetSchedule() != E_SCHED_INACTIVE);

            if (rx_cmd.params > 0) {
                if((param1 == 0) || (param1 == 1)) {
                    if((param1 == 0) && (APP_GetSchedule() == E_SCHED_ACTIVE)) {
                        UI_Input(C_KEY_POWER);
                        power = OFF;
                    }
                    else if((param1 == 1) && (APP_GetSchedule() == E_SCHED_INACTIVE)) {
                        UI_Input(C_KEY_POWER);
                        power = ON;
                    }
                }
                else
                    RS232_TxError(C_RS232_ERROR_PARAM);
            }
            else
                RS232_TxCmd1("PWR", power);

            break;

        // --------------------------------------------------------------------|
        // VOLUME -------------------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_VOL:
            if (rx_cmd.params > 0) {
                if('U' == direction) {
                    if(pNVM_GetVol() != 100)
                        VOL_Set(pNVM_GetVol()+1);   // Reports Status
                    else
                        RS232_TxError(C_RS232_ERROR_PARAM);
                }
                else if('D' == direction) {
                    if(pNVM_GetVol() > 0)
                        VOL_Set(pNVM_GetVol()-1);   // Reports Status
                    else
                        RS232_TxError(C_RS232_ERROR_PARAM);
                }
                else if((percentage >= 0) && (percentage <= 100))
                    VOL_Set(percentage);                // Reports Status
                else
                    RS232_TxError(C_RS232_ERROR_PARAM);
            }
            else
                RS232_TxCmd1("VOL", pNVM_GetVol());
            break;

        // --------------------------------------------------------------------|
        // MUTE ---------------------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_MUT:
            if (rx_cmd.params > 0) {
                if ((param1 == 0) || (param1 == 1)) {
                    UI_SetMute(param1);
                    if(gShow_status)
                        RS232_TxCmd1("MUT", param1);
                }
                else
                    RS232_TxError(C_RS232_ERROR_PARAM);
            }
            else
                RS232_TxCmd1("MUT", VOL_GetMute());

            break;

        // --------------------------------------------------------------------|
        // OUTPUT 1 -----------------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_OP1:
            if (rx_cmd.params > 0) {
                if ((param1 == 0) || (param1 == 1)) {
                    if(!NVM_GetOutputSw(E_OUTPUT_1)) {
                        IOX_Output(E_OUTPUT_1, param1);
                        //  IOX_OutputLedSet(E_OUTPUT_1, param1);  removed in C80
                        NVM_SetOutput(E_OUTPUT_1, param1);
                    }
                    else
                        RS232_TxError(C_RS232_ERROR_COMMAND);
                }
                else
                    RS232_TxError(C_RS232_ERROR_PARAM);
            }
            else
                status = TRUE;

            if(status || gShow_status)
                RS232_TxCmd1("OP1", NVM_GetOutput(E_OUTPUT_1));
            break;

        // --------------------------------------------------------------------|
        // OUTPUT 2 -----------------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_OP2:
            if (rx_cmd.params > 0) {
                if ((param1 == 0) || (param1 == 1)) {
                    if(!NVM_GetOutputSw(E_OUTPUT_2)) {
                        IOX_Output(E_OUTPUT_2, param1);
                        // IOX_OutputLedSet(E_OUTPUT_2, param1);   removed in C80
                        NVM_SetOutput(E_OUTPUT_2, param1);
                    }
                    else
                        RS232_TxError(C_RS232_ERROR_COMMAND);
                }
                else
                    RS232_TxError(C_RS232_ERROR_PARAM);
            }
            else
                status = TRUE;

            if(status || gShow_status)
                RS232_TxCmd1("OP2", NVM_GetOutput(E_OUTPUT_2));
            break;

        // --------------------------------------------------------------------|
        // OUTPUT 2 -----------------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_HPS:
            if(rx_cmd.params > 0)
                RS232_TxError(C_RS232_ERROR_PARAM);
            else {
                switch(OUT_Headphone()) {
                    case E_HEADPHONE_OUT:   RS232_TxCmd1("HPS", 0); break;
                    case E_HEADPHONE_IN:    RS232_TxCmd1("HPS", 1); break;
                    case E_HEADPHONE_CHINA: RS232_TxCmd1("HPS", 2); break;
                }
            }
            break;

        // --------------------------------------------------------------------|
        // INPUT --------------------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_INP:
            if (rx_cmd.params > 0) {
                if('U' == direction) {
                    SRC_Up(NO,NO);
                    DISP_Src();
                }
                else if('D' == direction) {
                    SRC_Down(NO,NO);
                    DISP_Src();
                }
                else if(param1 > 0 && param1 <= E_SRC_NUM) {
                     Src_Type inputNum = (Src_Type) (param1-1);  // convert from 1-based HR list to 0-based array
                    if(!SRC_Set(inputNum) )
                        RS232_TxError(C_RS232_ERROR_PARAM);
                }
                else
                    RS232_TxError(C_RS232_ERROR_PARAM);
            }
            else
                status = TRUE;

            if(status || gShow_status)
            {
                Src_Type inputNum = NVM_GetSrc() + 1;  // convert back to 1-based input number from 0-based array
                RS232_TxCmd1("INP", inputNum);   
            }
            break;

        // --------------------------------------------------------------------|
        // TRIM BALANCE -------------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_TBA: // Balance Set -> (TBA x)
            if (rx_cmd.params > 0) {
                if('L' == direction) {
                    if(NVM_GetBalance() != -50) {
                        UI_SetMenu(E_MENU_TRIM);
                        UI_Trim_SetMenu(E_TRIM_BALANCE);
                        UI_Trim_Process(C_IR_LEVEL_DOWN_BTN);   // Reports Status
                    }
                    else
                        RS232_TxError(C_RS232_ERROR_PARAM);
                }
                else if('R' == direction) {
                    if(NVM_GetBalance() != 50) {
                        UI_SetMenu(E_MENU_TRIM);
                        UI_Trim_SetMenu(E_TRIM_BALANCE);
                        UI_Trim_Process(C_IR_LEVEL_UP_BTN);   // Reports Status
                    }
                    else
                        RS232_TxError(C_RS232_ERROR_PARAM);
                }
                else if((param1 >= -50) && (param1 <= 50)) {
                    UI_SetMenu(E_MENU_TRIM);
                    UI_Trim_SetMenu(E_TRIM_BALANCE);

                    // convert range to [0-100]
                    trimidx = NVM_GetBalance() + 50;
                    param1 = param1 + 50;

                    if(param1 > trimidx) {
                        for(uint8_t i=0; i<(param1-trimidx); i++)
                            UI_Trim_Process(C_IR_LEVEL_UP_BTN);    // Reports Status
                    }
                    else if(param1 < trimidx) {
                        for(uint8_t i=0; i<(trimidx-param1); i++)
                            UI_Trim_Process(C_IR_LEVEL_DOWN_BTN);   // Reports Status
                    }
                }
                else
                    RS232_TxError(C_RS232_ERROR_PARAM);
            }
            else
                RS232_TxCmd1("TBA", NVM_GetBalance());
            break;

        // --------------------------------------------------------------------|
        // TRIM: INPUT TRIM ---------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_TIN:
            if (rx_cmd.params > 0) {
                if('U' == direction) {
                    UI_SetMenu(E_MENU_TRIM);
                    UI_Trim_SetMenu(E_TRIM_INPUT_TRIM);
                    UI_Trim_Process(C_IR_LEVEL_UP_BTN);   // Reports Status
                }
                else if('D' == direction) {
                    UI_SetMenu(E_MENU_TRIM);
                    UI_Trim_SetMenu(E_TRIM_INPUT_TRIM);
                    UI_Trim_Process(C_IR_LEVEL_DOWN_BTN);   // Reports Status
                }
                else if(param1 >= -12 && param1 <= 12) {
                    UI_SetMenu(E_MENU_TRIM);
                    UI_Trim_SetMenu(E_TRIM_INPUT_TRIM);

                    // convert range to [0-12]
                    trimidx = NVM_GetLevel() + 12;
                    param1 = param1 + 12;

                    if(param1 > trimidx) {
                        for(uint8_t i=0; i<(param1-trimidx); i++)
                            UI_Trim_Process(C_IR_LEVEL_UP_BTN);   // Reports Status
                    }
                    else if(param1 < trimidx) {
                        for(uint8_t i=0; i<(trimidx-param1); i++)
                            UI_Trim_Process(C_IR_LEVEL_DOWN_BTN);  // Reports Status
                    }
                }
                else
                    RS232_TxError(C_RS232_ERROR_PARAM); // error
            }
            else
                RS232_TxCmd1("TIN", NVM_GetLevel());   // current input setting
            break;

        // --------------------------------------------------------------------|
        // TRIM: TONE ---------------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_TTN:

            if (rx_cmd.params > 0) {
                if((param1 == 0) || (param1 == 1)) {
                    UI_SetMenu(E_MENU_TRIM);
                    UI_Trim_SetMenu(E_TRIM_TONE);

                    if ((param1 == 0) && NVM_GetTone(NVM_GetSrc()))
                        UI_Trim_Process(C_RTY_VOLUME_DOWN);  // Reports Status
                    else if ((param1 == 1) && !NVM_GetTone(NVM_GetSrc()))
                        UI_Trim_Process(C_RTY_VOLUME_UP);   // Reports Status
                }
                else
                    RS232_TxError(C_RS232_ERROR_PARAM);
            }
            else
                RS232_TxCmd1("TTN", NVM_GetTone(NVM_GetSrc()));
            break;

        // --------------------------------------------------------------------|
        // TRIM: TONE BASS ----------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_TTB:
            if(rx_cmd.params > 0) {
                if('U' == direction) {
                    if(NVM_GetBass(NVM_GetSrc()) != -12) {
                        UI_SetMenu(E_MENU_TRIM);
                        UI_Trim_SetMenu(E_TRIM_BASS);
                        UI_Trim_Process(C_IR_LEVEL_UP_BTN);   // Reports Status
                    }
                    else
                        RS232_TxError(C_RS232_ERROR_PARAM);
                }
                else if('D' == direction) {
                    if(NVM_GetBass(NVM_GetSrc()) != 12) {
                        UI_SetMenu(E_MENU_TRIM);
                        UI_Trim_SetMenu(E_TRIM_BASS);
                        UI_Trim_Process(C_IR_LEVEL_DOWN_BTN);   // Reports Status
                    }
                    else
                        RS232_TxError(C_RS232_ERROR_PARAM);
                }
                else if((param1 >= -12) && (param1 <= 12)) {
                    UI_SetMenu(E_MENU_TRIM);
                    UI_Trim_SetMenu(E_TRIM_BASS);

                    // convert range to [0-24]
                    trimidx = NVM_GetBass(NVM_GetSrc()) + 12;
                    param1 = param1 + 12;

                    if(param1 > trimidx) {
                        for(uint8_t i=0; i<(param1-trimidx); i++)
                            UI_Trim_Process(C_IR_LEVEL_UP_BTN);    // Reports Status
                    }
                    else if(param1 < trimidx) {
                        for(uint8_t i=0; i<(trimidx-param1); i++)
                            UI_Trim_Process(C_IR_LEVEL_DOWN_BTN);   // Reports Status
                    }
                }
                else
                    RS232_TxError(C_RS232_ERROR_PARAM);
            }
            else
                RS232_TxCmd1("TTB", NVM_GetBass(NVM_GetSrc()));
            break;

        // --------------------------------------------------------------------|
        // TRIM: TONE TREBLE --------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_TTT:
            if(rx_cmd.params > 0) {
                if('U' == direction) {
                    if(NVM_GetTreble(NVM_GetSrc()) != -12) {
                        UI_SetMenu(E_MENU_TRIM);
                        UI_Trim_SetMenu(E_TRIM_TREBLE);
                        UI_Trim_Process(C_IR_LEVEL_UP_BTN);   // Reports Status
                    }
                    else
                        RS232_TxError(C_RS232_ERROR_PARAM);
                }
                else if('D' == direction) {
                    if(NVM_GetTreble(NVM_GetSrc()) != 12) {
                        UI_SetMenu(E_MENU_TRIM);
                        UI_Trim_SetMenu(E_TRIM_TREBLE);
                        UI_Trim_Process(C_IR_LEVEL_DOWN_BTN);   // Reports Status
                    }
                    else
                        RS232_TxError(C_RS232_ERROR_PARAM);
                }
                else if((param1 >= -12) && (param1 <= 12)) {
                    UI_SetMenu(E_MENU_TRIM);
                    UI_Trim_SetMenu(E_TRIM_TREBLE);

                    // convert range to [0-24]
                    trimidx = NVM_GetTreble(NVM_GetSrc()) + 12;
                    param1 = param1 + 12;

                    if(param1 > trimidx) {
                        for(uint8_t i=0; i<(param1-trimidx); i++)
                            UI_Trim_Process(C_IR_LEVEL_UP_BTN);    // Reports Status
                    }
                    else if(param1 < trimidx) {
                        for(uint8_t i=0; i<(trimidx-param1); i++)
                            UI_Trim_Process(C_IR_LEVEL_DOWN_BTN);   // Reports Status
                    }
                }
                else
                    RS232_TxError(C_RS232_ERROR_PARAM);
            }
            else
                RS232_TxCmd1("TTT", NVM_GetTreble(NVM_GetSrc()));
            break;

        // --------------------------------------------------------------------|
        // TRIM: PHONO RESISTANCE ---------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_TPR: // Phono Resistance Set -> (TPR x) where X is (1..5)
            if(NVM_GetSrc() != E_SRC_MC_PHONO) {
                RS232_TxError(C_RS232_ERROR_INPUT);
                break;
            }

            if (rx_cmd.params > 0) {
                if('U' == direction) {
                    if(NVM_GetMcPhonoRes() == E_PRES_LAST) {
                        RS232_TxError(C_RS232_ERROR_PARAM);
                        break;
                    }
                    UI_SetMenu(E_MENU_TRIM);
                    UI_Trim_SetMenu(E_TRIM_MC_PHONO);
                    UI_Trim_Process(C_IR_LEVEL_UP_BTN);  // Reports Status
                }
                else if('D' == direction) {
                    if(NVM_GetMcPhonoRes() == E_PRES_FIRST) {
                        RS232_TxError(C_RS232_ERROR_PARAM);
                        break;
                    }
                    UI_SetMenu(E_MENU_TRIM);
                    UI_Trim_SetMenu(E_TRIM_MC_PHONO);
                    UI_Trim_Process(C_IR_LEVEL_DOWN_BTN);  // Reports Status
                }
                else if(param1 >= 1 && param1 <= 5) {
                    UI_SetMenu(E_MENU_TRIM);
                    UI_Trim_SetMenu(E_TRIM_MC_PHONO);

                    // convert range to [1-5]
                    trimidx = NVM_GetMcPhonoRes() + 1;

                    if(param1 > trimidx) {
                        for(uint8_t i=0; i<(param1-trimidx); i++)
                            UI_Trim_Process(C_IR_LEVEL_UP_BTN);  // Reports Status
                    }
                    else if(param1 < trimidx) {
                        for(uint8_t i=0; i<(trimidx-param1); i++)
                            UI_Trim_Process(C_IR_LEVEL_DOWN_BTN);  // Reports Status
                    }
                }
                else
                    RS232_TxError(C_RS232_ERROR_PARAM); // error
            }
            else
                RS232_TxCmd1("TPR", NVM_GetMcPhonoRes() + 1);
            break;

        // --------------------------------------------------------------------|
        // TRIM: PHONO CAPACITANCE --------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_TPC:
            if(NVM_GetSrc() != E_SRC_MM_PHONO) {
                RS232_TxError(C_RS232_ERROR_INPUT); // error
                break;
            }

            if (rx_cmd.params > 0) {
                if('U' == direction) {
                    if(NVM_GetMmPhonoCap() == E_PCAP_LAST) {
                        RS232_TxError(C_RS232_ERROR_PARAM);
                        break;
                    }
                    UI_SetMenu(E_MENU_TRIM);
                    UI_Trim_SetMenu(E_TRIM_MM_PHONO);
                    UI_Trim_Process(C_IR_LEVEL_UP_BTN);  // Reports Status
                }
                else if('D' == direction) {
                    if(NVM_GetMmPhonoCap() == E_PCAP_FIRST) {
                        RS232_TxError(C_RS232_ERROR_PARAM);
                        break;
                    }
                    UI_SetMenu(E_MENU_TRIM);
                    UI_Trim_SetMenu(E_TRIM_MM_PHONO);
                    UI_Trim_Process(C_IR_LEVEL_DOWN_BTN);  // Reports Status
                }
                else if(param1 >= 1 && param1 <= 8) {
                    UI_SetMenu(E_MENU_TRIM);
                    UI_Trim_SetMenu(E_TRIM_MM_PHONO);

                    // convert range to [1-8]
                    trimidx = NVM_GetMmPhonoCap() + 1;

                    if(param1 > trimidx) {
                        for(uint8_t i=0; i<(param1-trimidx); i++)
                            UI_Trim_Process(C_IR_LEVEL_UP_BTN);  // Reports Status
                    }
                    else if(param1 < trimidx) {
                        for(uint8_t i=0; i<(trimidx-param1); i++)
                            UI_Trim_Process(C_IR_LEVEL_DOWN_BTN);  // Reports Status
                    }
                }
                else
                    RS232_TxError(C_RS232_ERROR_PARAM);
            }
            else
                RS232_TxCmd1("TPC", NVM_GetMmPhonoCap() + 1);
            break;

        // --------------------------------------------------------------------|
        // TRIM: MONO ---------------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_TMO: // Trim Mono -> (TMO 0) or (TMO 1)
            if (rx_cmd.params > 0) {
                if((param1 == 0) || (param1 == 1)) {
                    UI_SetMenu(E_MENU_TRIM);
                    UI_Trim_SetMenu(E_TRIM_MONO);

                    if ((param1 == 0) && NVM_GetMono(NVM_GetSrc()))
                        UI_Trim_Process(C_RTY_VOLUME_UP);   // Reports Status
                    else if ((param1 == 1) && !NVM_GetMono(NVM_GetSrc()))
                        UI_Trim_Process(C_RTY_VOLUME_DOWN);  // Reports Status
                }
                else
                    RS232_TxError(C_RS232_ERROR_PARAM);
            }
            else
                RS232_TxCmd1("TMO", NVM_GetMono(NVM_GetSrc()));
            break;

        // --------------------------------------------------------------------|
        // TRIM: Trigger Meterlight -------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_TTM: // Trigger Meterlight -> (TTM 0) or (TTM 1)
            if (rx_cmd.params > 0) {
                if((param1 == 0) || (param1 == 1)) {
                    UI_SetMenu(E_MENU_TRIM);
                    UI_Trim_SetMenu(E_TRIM_METERLIGHT);

                    if ((param1 == 0) && NVM_GetTrigMeterlight())
                        UI_Trim_Process(C_RTY_VOLUME_DOWN); // Reports Status
                    else if ((param1 == 1) && !NVM_GetTrigMeterlight())
                        UI_Trim_Process(C_RTY_VOLUME_UP);  // Reports Status
                }
                else
                    RS232_TxError(C_RS232_ERROR_PARAM);
            }
            else
                RS232_TxCmd1("TTM", NVM_GetTrigMeterlight());
            break;


        // --------------------------------------------------------------------|
        // TRIM: DISPLAY BRIGHTNESS -------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_TDB: // Display Brightness -> (TDB X) where X is desired Brightness setting
            if (rx_cmd.params > 0) {
                UI_SetMenu(E_MENU_TRIM);
                UI_Trim_SetMenu(E_TRIM_BRIGHTNESS);

                switch(param1) {
                    case 1:
                        DISP_SetDutyCycle(E_DISP_DUTY3);
                        NVM_SetBrightness(E_DISP_DUTY3);
                        break;
                    case 2:
                        DISP_SetDutyCycle(E_DISP_DUTY2);
                        NVM_SetBrightness(E_DISP_DUTY2);
                        break;
                    case 3:
                        DISP_SetDutyCycle(E_DISP_DUTY1);
                        NVM_SetBrightness(E_DISP_DUTY1);
                        break;
                    case 4:
                        DISP_SetDutyCycle(E_DISP_DUTY0);
                        NVM_SetBrightness(E_DISP_DUTY0);
                        break;
                    default:
                        RS232_TxError(C_RS232_ERROR_PARAM);
                        break;
                }
            }
            else
                status = TRUE;

            if(status || gShow_status) {
                switch(NVM_GetBrightness()) {
                    case E_DISP_DUTY0:  RS232_TxCmd1("TDB", 4); break;
                    case E_DISP_DUTY1:  RS232_TxCmd1("TDB", 3); break;
                    case E_DISP_DUTY2:  RS232_TxCmd1("TDB", 2); break;
                    case E_DISP_DUTY3:  RS232_TxCmd1("TDB", 1); break;
                }
            }
            break;

        // --------------------------------------------------------------------|
        // TRIM: HEADPHONE HXD ------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_THH: // Headphone HXD -> (THH 0) or (THH 1)
            if(OUT_Headphone() == E_HEADPHONE_IN) {
                if (rx_cmd.params > 0) {
                    if((param1 == 0) || (param1 == 1)) {
                        UI_SetMenu(E_MENU_TRIM);
                        UI_Trim_SetMenu(E_TRIM_HXD);

                        if ((param1 == OFF) && (NVM_GetHXD() == ON))
                            UI_Trim_Process(C_RTY_VOLUME_DOWN); // Reports Status
                        else if ((param1 == ON) && (NVM_GetHXD() == OFF))
                            UI_Trim_Process(C_RTY_VOLUME_UP);   // Reports Status
                    }
                    else
                        RS232_TxError(C_RS232_ERROR_PARAM);
                }
                else
                    RS232_TxCmd1("THH", NVM_GetHXD());
            }
            else
                RS232_TxError(C_RS232_ERROR_COMMAND);
            break;

        // --------------------------------------------------------------------|
        // TRIM: EXIT ---------------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_TOF:
            if(UI_GetMenu() == E_MENU_TRIM)
                UI_SetMenu(E_MENU_MAIN);
            break;

        // --------------------------------------------------------------------|
        // TRIM: QUERY --------------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_QRY:
            if(rx_cmd.params == 0)
                RS232_Query();
            else
                RS232_TxError(C_RS232_ERROR_PARAM);
            break;

        // --------------------------------------------------------------------|
        // TEST: MODE ---------------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_TST:
            if (rx_cmd.params > 0) {
                if (rx_cmd.params > 1) {
                    if ((param1 == 1) && (param2 == 132)) // turn on test mode
                        TST_SetMode(ON);
                    else
                        RS232_TxError(C_RS232_ERROR_PARAM);
                }
                else {
                    if(param1 == 0) // turn off test mode
                        TST_SetMode(OFF);
                    else
                        RS232_TxError(C_RS232_ERROR_PARAM);
                }
            }
            else
                RS232_TxCmd1("TST", TST_GetMode());
            break;

        // --------------------------------------------------------------------|
        // TEST: SERIAL NUMBER ------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_TSN:
            if(TST_GetMode()) {
                if (rx_cmd.params > 0) {
                    if(rx_cmd.params == 2) {
                        TST_SetSerialNum(rx_cmd.param1, rx_cmd.param2);
                        UI_Setup_Display();
                    }
                    else
                        RS232_TxError(C_RS232_ERROR_PARAM);
                }
                else
                    TST_TxSerialNum();
            }
            else
                RS232_TxError(C_RS232_ERROR_COMMAND);
            break;

        // --------------------------------------------------------------------|
        // TEST: FACTORY RESET ------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_TFR:
            if(TST_GetMode()) {
                if (rx_cmd.params > 0)
                    RS232_TxError(C_RS232_ERROR_PARAM);
                else
                    TST_FactoryReset();
            }
            else
                RS232_TxError(C_RS232_ERROR_COMMAND);
            break;

        // --------------------------------------------------------------------|
        // TEST: IOX ----------------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_IOX:
            if(TST_GetMode()) {
                if (rx_cmd.params == 3)
                    TST_SetIOX_Pin((Iox_Id_Type)param1, (Iox_Pin_Id_Type)param2, param3); //set pin on iox
                else if (rx_cmd.params == 2)
                    TST_SetIOX_Reg((Iox_Id_Type)param1, param2); // set iox to value
                else if (rx_cmd.params == 1)
                    TST_GetIOX_Reg((Iox_Id_Type)param1);  // tx value of iox
                else
                    RS232_TxError(C_RS232_ERROR_PARAM);
            }
            else
                RS232_TxError(C_RS232_ERROR_COMMAND);
            break;

        // --------------------------------------------------------------------|
        // TEST: PAS ----------------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_PAS:
            if(TST_GetMode()) {
                if (rx_cmd.params > 0) {
                    if (rx_cmd.params > 1)
                        TST_SetPIO('A', param1, param2);
                    else
                        TST_TxPIO('A', param1);
                }
                else
                    RS232_TxError(C_RS232_ERROR_PARAM);
            }
            else
                RS232_TxError(C_RS232_ERROR_COMMAND);
            break;

        // --------------------------------------------------------------------|
        // TEST: PBS ----------------------------------------------------------|
        // --------------------------------------------------------------------|
        case C_RS232_CMD_PBS:
            if(TST_GetMode()) {
                if(rx_cmd.params > 0) {
                    if (rx_cmd.params > 1)
                        TST_SetPIO('B', param1, param2);
                    else
                        TST_TxPIO('B', param1);
                }
                else
                    RS232_TxError(C_RS232_ERROR_PARAM);
            }
            else
                RS232_TxError(C_RS232_ERROR_COMMAND);
            break;

        case C_RS232_CMD_ERR:
        default: RS232_TxError(C_RS232_ERROR_COMMAND); break;
    }
}

void RS232_TxCmd0(char * cmd)
{
    char str[kXferBufMaxChars]; str[kXferBufMaxChars-1]=0;
    snprintf( str, kXferBufMaxChars, "(%s)\0", cmd);    // create buffer
    UART_TxBuf(str, strlen(str));    // pass to UART
}

void RS232_TxCmd1(char * cmd, int8_t param1)
{
    char str[kXferBufMaxChars]; str[kXferBufMaxChars-1]=0;
    snprintf( str, kXferBufMaxChars, "(%s %d)\0", cmd, param1);    // create buffer
    UART_TxBuf(str, strlen(str));    // pass to UART
}

void RS232_TxCmd2(char * cmd, uint8_t param1, uint8_t param2)
{
    char str[kXferBufMaxChars]; str[kXferBufMaxChars-1]=0;
    snprintf( str, kXferBufMaxChars, "(%s %d %d)\0", cmd, param1, param2);    // create buffer
    UART_TxBuf(str, strlen(str));    // pass to UART
}

void RS232_TxString(char * string, uint8_t len)
{
    UART_TxBuf(string, len);
}

void RS232_TxError(uint8_t error)
{
#define kMaxErrStringBuf 30    
    char str[kMaxErrStringBuf];  str[kMaxErrStringBuf-1]=0;

    switch (error) {
        case C_RS232_ERROR_COMMAND: snprintf( str, kMaxErrStringBuf, "(ERROR - Invalid Command)\0");   break;
        case C_RS232_ERROR_PARAM:   snprintf( str, kMaxErrStringBuf, "(ERROR - Invalid Parameter)\0"); break;
        case C_RS232_ERROR_INPUT:   snprintf( str, kMaxErrStringBuf, "(ERROR - Invalid Input)\0");     break;
        case C_RS232_ERROR_UNKNOWN:
        default: snprintf( str, kMaxErrStringBuf, "(ERROR - Unknown Error)\0");     break;
    }
    UART_TxBuf(str, strlen(str));
}

void RS232_SendProductInfo(void)
{
#define kMaxInfoStringBuf 25
    char str[kMaxInfoStringBuf]; str[kMaxInfoStringBuf-1]=0;
    char * ver = PRODUCT_Version();

    RS232_TxString("\r\n", strlen("\r\n"));

    snprintf( str, kMaxInfoStringBuf, "(%s)\0", PRODUCT_Name());
    RS232_TxString(str, strlen(str));

    snprintf( str, kMaxInfoStringBuf, "(Serial Number: %s)\0", NVM_SerialString);
    RS232_TxString(str, strlen(str));

    snprintf( str, kMaxInfoStringBuf, "(FW Version: %s)\0", ver);
    RS232_TxString(str, strlen(str));
}

void RS232_Query(void)
{
    char str[kXferBufMaxChars]; str[kXferBufMaxChars-1]=0;

    RS232_SendProductInfo();
    RS232_TxCmd1("PWR", APP_GetSchedule() != E_SCHED_INACTIVE);

    // Check for Volume Guard
    if((APP_GetSchedule() != E_SCHED_ACTIVE) && (pNVM_GetVol() > 85)) {
        snprintf( str, kXferBufMaxChars, "(VOL 85)\0");
        RS232_TxString(str, strlen(str));
    }
    else
        RS232_TxCmd1("VOL", pNVM_GetVol());

    RS232_TxCmd1("MUT", VOL_GetMute());

    RS232_TxCmd1("OP1", NVM_GetOutput(E_OUTPUT_1));
    RS232_TxCmd1("OP2", NVM_GetOutput(E_OUTPUT_2));

    RS232_TxCmd1("INP", NVM_GetSrc()+1);
    RS232_TxCmd1("STA", gShow_status);
    RS232_TxCmd1("TBA", NVM_GetBalance());
    RS232_TxCmd1("TIN", NVM_GetLevel());  // TODO: PARSE FOR INPUT
    RS232_TxCmd1("TTN", NVM_GetTone(NVM_GetSrc()));
    RS232_TxCmd1("TTB", NVM_GetBass(NVM_GetSrc()));
    RS232_TxCmd1("TTT", NVM_GetTreble(NVM_GetSrc()));
    RS232_TxCmd1("TPR", NVM_GetMcPhonoRes()+1);
    RS232_TxCmd1("TPC", NVM_GetMmPhonoCap()+1);
    RS232_TxCmd1("TMO", NVM_GetMono(NVM_GetSrc()));
    RS232_TxCmd1("TTM", NVM_GetTrigMeterlight());

    switch(NVM_GetBrightness()) {
        case E_DISP_DUTY0:  RS232_TxCmd1("TDB", 4); break;
        case E_DISP_DUTY1:  RS232_TxCmd1("TDB", 3); break;
        case E_DISP_DUTY2:  RS232_TxCmd1("TDB", 2); break;
        case E_DISP_DUTY3:  RS232_TxCmd1("TDB", 1); break;
    }

    RS232_TxCmd1("THH", NVM_GetHXD());
    switch(OUT_Headphone()) {
        case E_HEADPHONE_OUT:   RS232_TxCmd1("HPS", 0); break;
        case E_HEADPHONE_IN:    RS232_TxCmd1("HPS", 1); break;
        case E_HEADPHONE_CHINA: RS232_TxCmd1("HPS", 2); break;
    }
    RS232_TxString("\r\n", strlen("\r\n"));
}


void RS232_QueryPwrOff(void)
{
    RS232_SendProductInfo();
    RS232_TxCmd1("PWR", APP_GetSchedule() != E_SCHED_INACTIVE);
}

void RS232_AcPwrStatus(void)
{
    RS232_SendProductInfo();
}

void RS232_ResetStatusDefault(void)
{
    gShow_status = TRUE;
}

bool RS232_StatusShow(void)
{
    return gShow_status;
}

//==============================================================================
// Private Function Definitions
//==============================================================================
static uint32_t rs232_parse_cmd_text(char * cmd)
{
    uint32_t i, j = 2, rc = 0;

    for (i=0;i<3;i++) {
        rc |= cmd[i] << (j*8);
        j--;
    }
    return rc;
}

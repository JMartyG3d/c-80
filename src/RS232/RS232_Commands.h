//-----------------------------------------------------------------------------|
//	RS232_Commands.h
//	Abstract: RS232 command parsing and processing
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __RS232_COMMANDS_H__
#define __RS232_COMMANDS_H__

#include "RS232_Queue.h"

// Commands list
#define C_RS232_CMD_ERR 0x455252 // "ERR" // Error
#define C_RS232_CMD_STA 0x535441 // "STA" // Enable RS232 Status Feedback
#define C_RS232_CMD_PWR 0x505752 // "PWR" // Power -> (PWR x)
#define C_RS232_CMD_VOL 0x564F4C // "VOL" // Volume Set -> (VOL X) where X is percentage volume (0..100)
#define C_RS232_CMD_MUT 0x4D5554 // "MUT" // Mute set -> (MUT 0) or (MUT 1)
#define C_RS232_CMD_OP1 0x4F5031 // "OP1" // Output1 set -> (OP1 0) or (OP1 1)
#define C_RS232_CMD_OP2 0x4F5032 // "OP2" // Output2 set -> (OP2 0) or (OP2 1)
#define C_RS232_CMD_HPS 0x485053 // "HPS" // Headphone State -> No Parameters
#define C_RS232_CMD_INP 0x494E50 // "INP" // Input set -> (INP X) where X is Indexed Value

// Trim commands
#define C_RS232_CMD_TBA 0x544241 // "TBA" // Balance Set -> (TBA x) where X is (-50..50)
#define C_RS232_CMD_TIN 0x54494E // "TIN" // Input Trim Set -> (TIN x) where X is (-6..6)
#define C_RS232_CMD_TTN 0x54544E // "TTN" // Tone Set -> (TTN 0) or (TTN 1)
#define C_RS232_CMD_TTB 0x545442 // "TTB" // Tone Bass Set
#define C_RS232_CMD_TTT 0x545454 // "TTT" // Tone Treble Set
#define C_RS232_CMD_TPR 0x545052 // "TPR" // Phono Resistance Set -> (TPR x) where X is (1..5)
#define C_RS232_CMD_TPC 0x545043 // "TPC" // Phono Capacitance Set -> (TPC x) where X is (1..8)
#define C_RS232_CMD_TMO 0x544D4F // "TMO" // Mono Set -> 1: Mono, 0: Stereo
#define C_RS232_CMD_TTM 0x54544D // "TTM" // Trigger Meter lights -> (TTM 0) or (TTM 1)
#define C_RS232_CMD_TDB 0x544442 // "TDB" // Display Brightness -> (TDB X) where X is desired Brightness setting (1..4)
#define C_RS232_CMD_TLS 0x544C53 // "TLS" // Lip Sync Delay -> (TLS 0) to (TLS 150)
#define C_RS232_CMD_THH 0x544848 // "THH" // Headphone HXD -> (THH 0) or (THH 1)
#define C_RS232_CMD_TOF 0x544F46 // "TOF" // Trim menu Exit
#define C_RS232_CMD_QRY 0x515259 // "QRY" // system query -> (QRY)

// Test commands
#define C_RS232_CMD_TST 0x545354 // "TST" // Test Enter Mode -> (TST 1 132) or (TST 0)
#define C_RS232_CMD_TSN 0x54534E // "TSN" // Test Set Serial -> (TSN XXX XXXX)
#define C_RS232_CMD_TFR 0x544652 // "TFR" // Test Factory Reset -> (TFR)
#define C_RS232_CMD_IOX 0x494F58 // "IOX" // Set IO extender pin -> (IOX A Q S) where is IOX Address, Q is the pin, S is the state; (IOX N) will transmit state of iox N
#define C_RS232_CMD_PAS 0x504153 // "PAS" // Set PIOA pin -> (PAS P S) where P is the PIOA pin, S is the state; (PAS X) will transmit state of pin X
#define C_RS232_CMD_PBS 0x504253 // "PBS" // Set PIOB pin -> (PBS P S) where P is the PIOB pin, S is the state; (PBS X) will transmit state of pin X

#define C_RS232_ERROR_UNKNOWN 0
#define C_RS232_ERROR_COMMAND 1
#define C_RS232_ERROR_PARAM   2
#define C_RS232_ERROR_INPUT   3

//==============================================================================
// Public Function Prototypes
//==============================================================================
void RS232_ParseCmd(RS232_RxMsg_Type rx_cmd);
bool RS232_QueueTxCmd(uint8_t cmd, uint8_t param1, uint8_t param2);
void RS232_TxCmd0(char * cmd);
void RS232_TxCmd1(char * cmd, int8_t param1);
void RS232_TxCmd2(char * cmd, uint8_t param1, uint8_t param2);
void RS232_TxString(char * string, uint8_t len);
void RS232_TxError(uint8_t error);
void RS232_Query(void);
void RS232_QueryPwrOff(void);
void RS232_AcPwrStatus(void);
uint8_t RS232_ModelGetSrcIdx(void);
void RS232_ResetStatusDefault(void);
bool RS232_StatusShow(void);
void RS232_SendProductInfo(void);
#endif // __RS232_COMMANDS_H__
//-----------------------------------------------------------------------------|
//	RS232_Tx.h
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __RS232_TX_H__
#define __RS232_TX_H__

#include "Common.h"

//===================================================================
// Public Function Prototypes
//===================================================================
void RS232_TxPwr(bool state);
void RS232_TxVol(uint8_t vol);
void RS232_TxMute(bool state);
void RS232_TxOutput(Output_Type out, bool state);
void RS232_TxSrc(void);
void RS232_TxBalance(void);
void RS232_TxInputTrim(void);
void RS232_TxTone(bool state);
void RS232_TxBass(void);
void RS232_TxTreble(void);
void RS232_TxPhonoRes(void);
void RS232_TxPhonoCap(void);
void RS232_TxMono(void);
void RS232_TxTrigMeterlight(void);
void RS232_TxBrightness(void);
void RS232_TxLipSyncDelay(void);
void RS232_TxHp(void);
void RS232_TxHpHxd(void);

#endif // __RS232_TX_H__
//-----------------------------------------------------------------------------|
//	UART_Driver.h
//	Abstract: SAM4S8B Uart driver implementation
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __UART_DRIVER_H__
#define __UART_DRIVER_H__

//===================================================================
// Public Function Prototypes
//===================================================================
void UART_Init(uint8_t irq_priority);
void UART_TxBuf(const char * buffer, uint8_t length);

#endif // __UART_DRIVER_H__
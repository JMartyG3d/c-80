//-----------------------------------------------------------------------------|
//	UART_Driver.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include <ctype.h>
#include <string.h>
#include "UART_Driver.h"
#include "Common.h"
#include "RS232_Queue.h"
#include "NVM_Access.h"

typedef enum {
    E_RS232_PREFIX = 0, // waiting for command prefix
    E_RS232_CMD,        // reading command characters
    E_RS232_PARAM1,     // reading parameter 1 characters
    E_RS232_PARAM2,     // reading parameter 2 characters
    E_RS232_PARAM3      // reading parameter 3 characters
} rs232_state_t;

static RS232_RxMsg_Type __uart_buf;
static uint8_t          __uart_subcnt;   // subcount of characters read (used for each buffer triplet)
static rs232_state_t    __uart_state;

//===================================================================
// Private Function Prototypes
//===================================================================
static void uart_reset_state(void);

//===================================================================
// Public Functions
//===================================================================
void UART_Init(uint8_t irq_priority)
{
    uint32_t baud;

    switch(NVM_GetRS232Baud()) {
        case E_BAUD_9600:   baud = 9600; break;
        case E_BAUD_19200:  baud = 19200; break;
        case E_BAUD_38400:  baud = 38400; break;
        case E_BAUD_57600:  baud = 57600; break;
        case E_BAUD_115200:
        default: baud = 115200; break;
    }

	const usart_serial_options_t uart_config = {
		.baudrate = baud,
        .charlength = US_MR_CHRL_8_BIT,
		.paritytype = UART_MR_PAR_NO,
        .stopbits   = US_MR_NBSTOP_1_BIT
	};

    // Enables Periph clock, rx and tx, disables PDC
	stdio_serial_init(UART0, &uart_config);
    uart_enable_interrupt(UART0, US_IER_RXRDY);
    UART0->UART_PTCR = UART_PTCR_TXTEN;  //Enable PDC for UART (TX only) TODO
	NVIC_DisableIRQ((IRQn_Type)UART0_IRQn);
	NVIC_ClearPendingIRQ((IRQn_Type)UART0_IRQn);
	NVIC_SetPriority((IRQn_Type)UART0_IRQn, irq_priority);
	NVIC_EnableIRQ((IRQn_Type)UART0_IRQn);
}

void UART0_Handler(void)
{
    char c;

	if(UART0->UART_SR & UART_SR_RXRDY) {
        c = UART0->UART_RHR;
        switch (__uart_state) {
            case E_RS232_PREFIX:
                if(c == '(') { // transition to CMD
                    __uart_state = E_RS232_CMD;
                    __uart_subcnt = 0;
                }
                else {
                    if(c == ')')
                        uart_reset_state(); // tx queue error
                    // silently ignore all other characters while waiting for a PREFIX
                }
                break;
            case E_RS232_CMD:
                if(__uart_subcnt < 3) {
                    __uart_buf.cmd[__uart_subcnt] = c;
                    __uart_subcnt++;
                }
                else {
                    if(c == ' ') { // transition to param1
                        __uart_state = E_RS232_PARAM1;
                        __uart_subcnt = 0;
                    }
                    else if(c == ')') { // add to queue
                        __uart_buf.params = 0;
                        RS232_QueuePushRxMsg(__uart_buf);
                        uart_reset_state();
                    }
                    else
                        uart_reset_state(); // tx queue error
                }
                break;
            case E_RS232_PARAM1:
                if(__uart_subcnt < 4) {
                    if(c == ')') { // add to queue
                        if(__uart_subcnt > 0)
                            __uart_buf.params = 1;
                        RS232_QueuePushRxMsg(__uart_buf);
                        uart_reset_state();
                    }
                    else {
                        if(c != ' ') { //ignore a blank
                            __uart_buf.param1[__uart_subcnt] = c;
                            __uart_subcnt++;
                        }
                        else if(__uart_subcnt > 0) { // transition to param2
                            __uart_buf.params = 1;
                            __uart_state = E_RS232_PARAM2;
                            __uart_subcnt = 0;
                        }
                    }
                }
                else {
                    if(c == ' ') { // transition to param2
                        __uart_buf.params = 1;
                        __uart_state = E_RS232_PARAM2;
                        __uart_subcnt = 0;
                    }
                    else if(c == ')'){ // add to rx queue
                        if(__uart_subcnt > 0)
                            __uart_buf.params = 1;
                        RS232_QueuePushRxMsg(__uart_buf);
                        uart_reset_state();
                    }
                    else
                        uart_reset_state(); // tx queue error
                }
                break;
            case E_RS232_PARAM2:
                if(__uart_subcnt < 4) {
                    if(c == ')') { // add to rx queue
                        if(__uart_subcnt > 0)
                            __uart_buf.params = 2;
                        RS232_QueuePushRxMsg(__uart_buf);
                        uart_reset_state();
                    }
                    else {
                        if(c != ' ') { //ignore a blank
                            __uart_buf.param2[__uart_subcnt] = c;
                            __uart_subcnt++;
                        }
                        else if(__uart_subcnt > 0) { // transition to param2
                            __uart_buf.params = 2;
                            __uart_state = E_RS232_PARAM3;
                            __uart_subcnt = 0;
                        }
                    }
                }
                else {
                    if(c == ')') {
                        if(__uart_subcnt > 0) // add to rx queue
                            __uart_buf.params = 2;

                        RS232_QueuePushRxMsg(__uart_buf);
                        uart_reset_state();
                    }
                    else
                        uart_reset_state(); // transmit error
                }

                break;
            case E_RS232_PARAM3:
                if(__uart_subcnt < 4) {
                    if(c == ')') {
                        if(__uart_subcnt > 0) // add to rx queue
                            __uart_buf.params = 3;
                        RS232_QueuePushRxMsg(__uart_buf);
                        uart_reset_state();
                    }
                    else if(c != ' ') { //ignore a blank
                        __uart_buf.param3[__uart_subcnt] = c;
                        __uart_subcnt++;
                    }
                }
                else {
                    if(c == ')') {
                        if(__uart_subcnt > 0) // add to rx queue
                            __uart_buf.params = 3;

                        RS232_QueuePushRxMsg(__uart_buf);
                        uart_reset_state();
                    }
                    else
                        uart_reset_state(); // transmit error
                }
                break;
            default:
                break;
        }
    }
}

void UART_TxBuf(const char * buffer, uint8_t length)
{
    UART0->UART_TPR = (uint32_t)buffer;
    UART0->UART_TCR = length;
    while (!(UART0->UART_SR & UART_SR_TXBUFE));
}

//===================================================================
// Private Functions
//===================================================================
static void uart_reset_state(void)
{
    __uart_state = E_RS232_PREFIX;
    __uart_subcnt = 0;
    memset(&__uart_buf, 0, sizeof(__uart_buf));
}
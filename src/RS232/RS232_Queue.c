//-----------------------------------------------------------------------------|
//	RS232_Queue.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include <string.h>
#include "RS232_Queue.h"

static RS232_RxMsg_Type __rx_queue[C_RS232_QUEUE_SIZE];
static uint8_t __rx_head, __rx_tail;

//==============================================================================
// Private Function Prototypes
//==============================================================================
static bool rs232_rx_queue_full(void);

//==============================================================================
// Public Function Definitions
//==============================================================================
void RS232_QueueInit(void)
{
	__rx_head = __rx_tail = 0;
    memset(__rx_queue, 0, sizeof(__rx_queue));
}

bool RS232_QueuePushRxMsg(RS232_RxMsg_Type rx_msg)
{
    bool rc = FALSE;

    if(!rs232_rx_queue_full()) {// if queue isn't full, copy next element
        memcpy(&__rx_queue[__rx_tail], &rx_msg, sizeof(RS232_RxMsg_Type));
        __rx_tail = (__rx_tail + 1) % C_RS232_QUEUE_SIZE;
        rc = TRUE;
    }
    return rc;
}

bool RS232_QueuePopRxMsg(RS232_RxMsg_Type* msg)
{
    bool rc = FALSE;

	if(__rx_head != __rx_tail) {
        memcpy(msg, &__rx_queue[__rx_head], sizeof(RS232_RxMsg_Type));
        __rx_head = (__rx_head + 1) % C_RS232_QUEUE_SIZE;
        rc = TRUE;
    }
    return rc;
}

//==============================================================================
// Private Function Definitions
//==============================================================================
static bool rs232_rx_queue_full(void)
{
    bool rc = FALSE;

    if (((__rx_tail+1) == __rx_head)
        || (((__rx_tail+1) == C_RS232_QUEUE_SIZE) && (__rx_head == 0)) )
        rc = TRUE;

    return rc;
}

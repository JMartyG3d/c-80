//-----------------------------------------------------------------------------|
//	RS232_Queue.h
//	Abstract: Queue structure for RS232 packets
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __RS232_QUEUE_H__
#define __RS232_QUEUE_H__

#include "RS232_Control.h"

#define C_RS232_QUEUE_SIZE  20

//===================================================================
// Public Function Prototypes
//===================================================================
void RS232_QueueInit(void);
bool RS232_QueuePushRxMsg(RS232_RxMsg_Type rx_msg);
bool RS232_QueuePopRxMsg(RS232_RxMsg_Type* msg);

#endif // __RS232_QUEUE_H__

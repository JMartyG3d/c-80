//-----------------------------------------------------------------------------|
//	OUT_Control.c
//	Abstract:
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include "common.h"
#include "OUT_Control.h"
#include "NVM_Access.h"
#include "IOX_Control.h"
#include "VOL_Control.h"
#include "ADC_Process.h"
#include "APP_Process.h"
#include "PT_Control.h"
#include "RS232_Tx.h"
#include "UI_Control.h"

static HP_Type __hp_state;
static bool __first_call;

//==============================================================================
// Private Function Prototypes
//==============================================================================
static void out_set_relays(void);
static HP_Type out_hp_check(void);    // check the state of the headphone
static void out_hp_set_relays(void);  // sets relays when headphone is plugged in or removed

//==============================================================================
// Public Functions
//==============================================================================
void OUT_Init(void)
{
    __first_call = TRUE;
    __hp_state = out_hp_check();    // Get Headphone connection

    if((__hp_state != E_HEADPHONE_IN) && !PT_Active()){
        //IOX_OutputLedSet(E_OUTPUT_1, NVM_GetOutput(E_OUTPUT_1));      Removed for C80
        //IOX_OutputLedSet(E_OUTPUT_2, NVM_GetOutput(E_OUTPUT_2));      Removed for C80
    }
}

// Called when warm up timer expires to enable configured outputs
void OUT_Warmup(void)
{
    if(PT_Active())
        return;

    __hp_state = out_hp_check();    // Get Headphone connection

    if(__hp_state == E_HEADPHONE_IN)
        out_hp_set_relays();
    else
        out_set_relays();
}

// performs periodic output functionality (for now - just checking headphone)
void OUT_Periodic(void)
{
    HP_Type hp = out_hp_check();    // Get Headphone connection

    if((hp == E_HEADPHONE_CHINA) || PT_Active())
        return;

    // process if hdph connection changed
    if(hp != __hp_state) {
        __hp_state = hp;
        RS232_TxHp();
        out_hp_set_relays();
    }
}

HP_Type OUT_Headphone(void)
{
    return __hp_state;
}

//==============================================================================
// Private Functions
//==============================================================================
static void out_set_relays(void)
{
    IOX_FixedOut(ON);

    IOX_Output(E_OUTPUT_1, NVM_GetOutput(E_OUTPUT_1));
    //IOX_OutputLedSet(E_OUTPUT_1, NVM_GetOutput(E_OUTPUT_1));  Removed for C80

    IOX_Output(E_OUTPUT_2, NVM_GetOutput(E_OUTPUT_2));
    //IOX_OutputLedSet(E_OUTPUT_2, NVM_GetOutput(E_OUTPUT_2));  Removed for C80
}

static HP_Type out_hp_check(void)
{
    uint8_t val = ADC_GetChan(E_ADC_HDPH);
    HP_Type hp = E_HEADPHONE_CHINA;
    static HP_Type hp_prev;
    Hp_Out_Type hp_output = NVM_GetHpOut();

    if(val < C_ADC_0_00V)
        hp = E_HEADPHONE_IN;
    else if(val < C_ADC_1_94V)
        hp = E_HEADPHONE_OUT;

    if(PT_Active()) {
        if(hp == E_HEADPHONE_IN) {
            pNVM_SetVol = &NVM_SetHpVolume;
            pNVM_GetVol = &NVM_GetHpVolume;
        }
        else {
            pNVM_SetVol = &NVM_SetVolume;
            pNVM_GetVol = &NVM_GetVolume;
        }
        return hp;
    }

    if(__first_call || (hp_prev != hp)) {

        if(!VOL_GetMute() && !__first_call)
            VOL_ChangeVolume(FALSE, TRUE, 0); // set volume to 0

        if(hp == E_HEADPHONE_IN) {
            if(!hp_output){
                if(!__first_call)
                    pNVM_SetVol(pNVM_GetVol(), NO); // Save Current Value
                pNVM_SetVol = &NVM_SetHpVolume;
                pNVM_GetVol = &NVM_GetHpVolume;

                if(!__first_call) {
                    RS232_TxVol(pNVM_GetVol());
                    delay_ms(100);
                }
            }
        }
        else {
            if(!__first_call)
                pNVM_SetVol(pNVM_GetVol(), NO); // Save Current Value
            pNVM_SetVol = &NVM_SetVolume;
            pNVM_GetVol = &NVM_GetVolume;

            if(!__first_call) {
                RS232_TxVol(pNVM_GetVol());
                delay_ms(100);
            }
        }

        if(!__first_call) {
            // Unmute and set volume
            if(!VOL_GetMute()) {
                VOL_UnMute(1);
                VOL_ChangeVolume((UI_GetMenu() == E_MENU_MAIN), FALSE, pNVM_GetVol());
            }
        }
        __first_call = FALSE;
    }
    hp_prev = hp;
    return hp;
}

// sets relays when headphone is plugged in or removed, Called 1x at power on
static void out_hp_set_relays(void)
{
    Hp_Out_Type hp_output = NVM_GetHpOut();

    if(__hp_state == E_HEADPHONE_IN) {
        IOX_Headphones(ON);

        if(hp_output == E_HP_OUT_MUTE_ALL) {
            IOX_Output(E_OUTPUT_1, OFF);
//            IOX_OutputLedSet(E_OUTPUT_1, OFF);
            IOX_Output(E_OUTPUT_2, OFF);
//            IOX_OutputLedSet(E_OUTPUT_2, OFF);
            IOX_FixedOut(OFF);
        }
        else
            out_set_relays();
    }
    else { // Headphones Unplugged
        IOX_Headphones(OFF);

        if(hp_output == E_HP_OUT_MUTE_NONE) {       // ToDo:  Using OUTPUT LEDs to store state of Output Selection needs a replacement, since no more Output LEDs in C80
//            NVM_SetOutput(E_OUTPUT_1, IOX_OutputLedGet(E_OUTPUT_1));
//            NVM_SetOutput(E_OUTPUT_2, IOX_OutputLedGet(E_OUTPUT_2));
            delay_ms(50);
        }

        out_set_relays();
    }
}

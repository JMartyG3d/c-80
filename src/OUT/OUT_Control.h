//-----------------------------------------------------------------------------|
//	OUT_Control.h
//	Abstract:
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __OUT_CONTROL_H__
#define __OUT_CONTROL_H__

//==============================================================================
// Public Function Prototypes
//==============================================================================
void    OUT_Init(void);
void    OUT_Warmup(void);    // called when warm up timer expires to enable configured outputs
void    OUT_Periodic(void);  // performs periodic output functionality
HP_Type OUT_Headphone(void); // Returns // 0=No headphone, 1=Headphone, FF=Chinese

#endif // __OUT_CONTROL_H__
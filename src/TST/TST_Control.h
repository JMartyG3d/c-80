//-----------------------------------------------------------------------------|
//	TST_Control.h
//	Abstract: Factory test module
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __TST_CONTROL_H__
#define __TST_CONTROL_H__

#include "IOX_Control.h"
#include "Common.h"

//===================================================================
// Public Function Prototypes
//===================================================================
void TST_SetMode(bool state);
bool TST_GetMode(void);
void TST_SetSerialNum(char * prefix, char * number);
void TST_TxSerialNum(void); // return null terminated string
void TST_FactoryReset(void);
void TST_SetIOX_Reg(Iox_Id_Type io, uint8_t state);
void TST_SetIOX_Pin(Iox_Id_Type io, Iox_Pin_Id_Type pin, uint8_t state);
void TST_GetIOX_Reg(Iox_Id_Type io);
void TST_SetPIO(char periph, uint8_t pin, uint8_t state);
void TST_TxPIO(char periph, uint8_t pin);

#endif // __TST_CONTROL_H__

//-----------------------------------------------------------------------------|
//	TST_Control.c
//	Abstract: Factory test module
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include <string.h>
#include "TST_Control.h"
#include "NVM_Access.h"
#include "NVM_Control.h"
#include "RS232_Commands.h"
#include "IOX_Control.h"
#include "APP_Process.h"
#include "PRODUCT_Config.h"

static bool __tst_mode;

//==============================================================================
// Public Function Definitions
//==============================================================================
void TST_SetMode(bool state)
{
    __tst_mode = state;
    RS232_TxCmd1("TST", __tst_mode);
}

bool TST_GetMode(void)
{
    return __tst_mode;
}

// ----------------------------------------------------|
// Serial Number  -------------------------------------|
// ----------------------------------------------------|
void TST_SetSerialNum(char * prefix, char * number)
{
    for(uint8_t i=0;i<3;i++)
        NVM_SerialString[i] = prefix[i];

    for(uint8_t i=0;i<4;i++)
        NVM_SerialString[i+3] = number[i];

    NVM_SerialString[7] = '\0';
    NVM_SaveSerial();
    TST_TxSerialNum();
}

void TST_TxSerialNum(void)
{
    char str[15]; str[14]=0;
    snprintf(str, 15, "(TSN %s)\0", NVM_SerialString);
    RS232_TxString(str, strlen(str));
}

// ----------------------------------------------------|
// Factory Reset --------------------------------------|
// ----------------------------------------------------|
void TST_FactoryReset(void)
{
    NVM_FactoryResetAll();
    RS232_TxCmd0("TFR");
    NVM_Startup();
    APP_SetSchedule(E_SCHED_POWERDOWN);
}

// ----------------------------------------------------|
// IOX ------------------------------------------------|
// ----------------------------------------------------|
void TST_SetIOX_Reg(Iox_Id_Type io, uint8_t state)
{
    if (io < E_IOX_NUM) {
        IOX_SetReg(io, state);
        TST_GetIOX_Reg(io);
    }
}

void TST_SetIOX_Pin(Iox_Id_Type io, Iox_Pin_Id_Type pin, uint8_t state)
{
    if (io < E_IOX_NUM) {
        IOX_SetPin(io, pin, state);
        TST_GetIOX_Reg(io);
    }
}

void TST_GetIOX_Reg(Iox_Id_Type io)
{
    if(io < E_IOX_NUM)
        RS232_TxCmd2("IOX", io, IOX_GetReg(io));
}

// ----------------------------------------------------|
// PIO ------------------------------------------------|
// ----------------------------------------------------|
void TST_SetPIO(char periph, uint8_t pin, uint8_t state)
{
    Pio * pioptr;

    if(pin > 31)
        return;

    if (periph == 'A')
        pioptr = PIOA;
    else if (periph == 'B')
        pioptr = PIOB;
    else
        pioptr = NULL;

    if(pioptr != NULL) {
        pioptr->PIO_PER  = (1 << pin); // will leave pin enabled permanently, should be no harmful effects
        pioptr->PIO_OER  = (1 << pin);
        pioptr->PIO_PUER = (1 << pin);
        pioptr->PIO_MDER = (1 << pin);

        if (state == 1)
            pioptr->PIO_SODR = (1 << pin);
        else
            pioptr->PIO_CODR = (1 << pin);

        // TX pin state
        TST_TxPIO(periph, pin);
    }
}

void TST_TxPIO(char periph, uint8_t pin)
{
    uint8_t value;

    if(periph == 'A') {
        if(pin < 32) {
            value = (PIOA->PIO_PDSR & (1 << pin)) >> pin;
            RS232_TxCmd1("PAS", value);
        }
    }
    else {
        if(periph == 'B') {
            if(pin < 32) {
                value = (PIOA->PIO_PDSR & (1 << pin)) >> pin;
                RS232_TxCmd1("PBS", value);
            }
        }
    }
}

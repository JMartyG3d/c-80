//-----------------------------------------------------------------------------|
//	ADC_Process.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include "ADC_Process.h"

static uint32_t __adc_chan4 = C_ADC_NO_DATA;
static uint32_t __adc_chan5 = C_ADC_NO_DATA;
static uint32_t __adc_chan6 = C_ADC_NO_DATA;

//===================================================================
// Public Functions
//===================================================================
// ADC Handler called aperiodically when a new A2D conversion is triggered
void ADC_Handler(void)
{
    uint32_t dummy;

	__adc_chan4 = (ADC->ADC_CDR[4]) >> 2;
	__adc_chan5 = (ADC->ADC_CDR[5]) >> 2;
	__adc_chan6 = (ADC->ADC_CDR[6]) >> 2;

    dummy = ADC->ADC_LCDR; // clears ADC_ISR bits
	dummy = dummy;

    if (__adc_chan4 > C_ADC_THRESHOLD)
        __adc_chan4 = C_ADC_NO_DATA;

    if (__adc_chan5 > C_ADC_THRESHOLD)
        __adc_chan5 = C_ADC_NO_DATA;

    if (__adc_chan6 > C_ADC_THRESHOLD)
        __adc_chan6 = C_ADC_NO_DATA;
}

// initialize ADC periph
void ADC_Init(void)
{
	pmc_enable_periph_clk(ID_ADC);

	adc_init(ADC, sysclk_get_main_hz(), C_ADC_CLOCK, ADC_STARTUP_TIME_0);

	adc_configure_timing(ADC, 5, ADC_SETTLING_TIME_0, 2);

	adc_set_resolution(ADC, ADC_10_BITS);
    adc_configure_power_save(ADC, ADC_MR_SLEEP_SLEEP, ADC_MR_FWUP_OFF);
	adc_enable_channel(ADC, ADC_CHANNEL_4);
	adc_enable_channel(ADC, ADC_CHANNEL_5);
	adc_enable_channel(ADC, ADC_CHANNEL_6);
	adc_enable_interrupt(ADC, ADC_IER_DRDY);

    NVIC_EnableIRQ((IRQn_Type)ADC_IRQn );
    NVIC_SetPriority((IRQn_Type)ADC_IRQn,1);

    __adc_chan4 = C_ADC_NO_DATA;
    __adc_chan5 = C_ADC_NO_DATA;
    __adc_chan6 = C_ADC_NO_DATA;

    adc_start(ADC);
}

void ADC_Start(void)
{
    adc_start(ADC);
}

// return last value for specified channel, then clear it
uint32_t ADC_GetChan(ADC_Channel_T channel)
{
    uint32_t rc = C_ADC_NO_DATA;

    switch (channel) {
        case E_ADC_KEYS:
            rc = __adc_chan5;
            __adc_chan5 = C_ADC_NO_DATA;
            break;
        case E_ADC_HDPH:
            rc = __adc_chan4;
            __adc_chan4 = C_ADC_NO_DATA;
            break;
        case E_ADC_UNIT_ID:
            rc = __adc_chan6;
            __adc_chan6 = C_ADC_NO_DATA;
            break;
        default:
            break;
    }

    return rc;
}

// clear last value for specified channel
void ADC_ClearChan(ADC_Channel_T channel)
{
    switch (channel) {
        case E_ADC_KEYS:
            __adc_chan5 = C_ADC_NO_DATA;
            break;
        case E_ADC_HDPH:
            __adc_chan4 = C_ADC_NO_DATA;
            break;
        case E_ADC_UNIT_ID:
            __adc_chan6 = C_ADC_NO_DATA;
            break;
        default:
            break;
    }
}

//-----------------------------------------------------------------------------|
//	ADC_Process.h
//	Abstract: Simple ADC driver - polls adc channels and stores values
//              No processing is done by this unit, all data will be
//               periodically requested by other modules to be processed
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __ADC_PROCESS_H__
#define __ADC_PROCESS_H__

#define C_ADC_THRESHOLD 240

// no data reported/detected
#define C_ADC_NO_DATA (0xBADC0FEEu)

// voltage conversions
// -- These may require tweaking product to product
// -- Define the reported value with some slack (add 5-10)
#define C_ADC_3_30V     254
#define C_ADC_2_62V     210
#define C_ADC_1_94V     160
#define C_ADC_1_36V     112
#define C_ADC_0_67V     60
#define C_ADC_0_00V     10

// adc channnels
typedef enum {
    E_ADC_KEYS = 0,
    E_ADC_HDPH,
    E_ADC_UNIT_ID,
} ADC_Channel_T;

//===================================================================
// Public Function Prototypes
//===================================================================
void ADC_Init(void);
void ADC_Start(void);
uint32_t ADC_GetChan(ADC_Channel_T channel);
void ADC_ClearChan(ADC_Channel_T channel);

#endif // __ADC_PROCESS_H__
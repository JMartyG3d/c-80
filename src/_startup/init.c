//-----------------------------------------------------------------------------|
//	init.c
//	Abstract: Pin initialization
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include "board.h"
#include "conf_board.h"
#include "PIO_Config.h"

void board_init(void)
{
#ifdef CONF_BOARD_KEEP_WATCHDOG_AT_INIT
//	WDT->WDT_MR = WDT_MR_WDDIS;	/* Disable the watchdog */
#endif

// PIO ----------------------------------------------------------|
#ifdef CONF_BOARD_PIO
    pmc_enable_periph_clk(ID_PIOA);
    pmc_enable_periph_clk(ID_PIOB);
    PIO_Config();
#endif

// TWI0 ----------------------------------------------------------|
#ifdef CONF_BOARD_TWI0
    pmc_enable_periph_clk(ID_TWI0);
	pio_configure_pin_group(PIOA, PINS_TWI0_DATA, PINS_TWI0_DATA_FLAGS);
	pio_configure_pin_group(PIOA, PINS_TWI0_CLK, PINS_TWI0_CLK_FLAGS);
#endif

// UART ----------------------------------------------------------|
#ifdef CONF_BOARD_UART_CONSOLE
    pio_configure_pin_group(PINS_UART0_PIO, PINS_UART0, PINS_UART0_FLAGS);
#endif

// ADC -----------------------------------------------------------|
#ifdef CONF_BOARD_ADC
    pio_configure_pin_group(PINS_ADC_PIO, PINS_ADC, PINS_ADC_FLAG);
#endif

// USART1 DA -----------------------------------------------------|
#ifdef CONF_BOARD_USART
    pmc_enable_periph_clk(ID_USART1);

    #ifdef CONF_BOARD_USART_RXD
	pio_configure_pin_group(PIOA, PINS_USART1_RXD, PINS_USART1_RXD_FLAGS);
    #endif
    #ifdef CONF_BOARD_USART_TXD
	pio_configure_pin_group(PIOA, PINS_USART1_TXD, PINS_USART1_TXD_FLAGS);
    #endif
#endif

// SPI -----------------------------------------------------------|
#ifdef CONF_BOARD_SPI
    pmc_enable_periph_clk(ID_SPI);
    pio_configure_pin_group(PINS_SPI_PIO, PINS_SPI_MOSI, PINS_SPI_MOSI_FLAGS);
    pio_configure_pin_group(PINS_SPI_PIO, PINS_SPI_SPCK, PINS_SPI_SPCK_FLAGS);
#endif
#ifdef CONF_BOARD_SPI_NPCS0
    pio_configure_pin_group(PINS_SPI_PIO, PINS_SPI_NPCS0, PINS_SPI_NPCS0_FLAGS);
#endif

}

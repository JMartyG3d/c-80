//-----------------------------------------------------------------------------|
//	I2C_Control.h
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __I2C_CONTROL_H__
#define __I2C_CONTROL_H__

//===================================================================
// Public Function Prototypes
//===================================================================
void I2C_Config(void);
void I2C_Send(uint8_t sladdr, uint8_t* data);
void I2C_Tx(Twi *p_twi, uint8_t baddr, uint8_t saddr, uint8_t iaddr, uint8_t ilen, uint8_t dlen, uint8_t* regdata);
void I2C_Rx(Twi *p_twi, uint8_t baddr, uint8_t saddr, uint8_t iaddr, uint8_t ilen, uint8_t dlen, uint8_t* regdata);
void I2C_BusClear(void);
uint32_t I2C_GetReadNackCount(void);
uint32_t I2C_GetWriteNackCount(void);
uint32_t I2C_GetTimeoutCount(void);

#endif // __I2C_CONTROL_H__

//-----------------------------------------------------------------------------|
//	I2C_Control.c
//	Abstract: TBD
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include "I2C_Control.h"

static uint32_t __i2c_read_nack_cnt;
static uint32_t __i2c_write_nack_cnt;
static uint32_t __i2c_read_timeout_cnt;
static uint32_t __i2c_write_timeout_cnt; // timeout error

//==============================================================================
// Public Functions
//==============================================================================
void I2C_Config(void)
{
	twi_master_options_t opt_twi0;
	opt_twi0.speed = TWI_CLOCK;

    /* reset before setup */
    twi_reset(TWI0);
	twi_master_setup(TWI0, &opt_twi0);
}

void I2C_Tx(Twi *p_twi, uint8_t baddr, uint8_t saddr, uint8_t iaddr, uint8_t ilen, uint8_t dlen, uint8_t* regdata)
{
    uint32_t twi_status;

	twi_package_t i2c_write = {
        .addr_length  = 0,
		.chip         = baddr | saddr,  // TWI slave bus address
	    .buffer       = regdata,        // transfer data source buffer
	    .length       = dlen            // transfer data size (num bytes)
	};

    if(ilen != 0) {
        i2c_write.addr_length  = ilen;      // Length of iaddr (1-3 bytes)
        if(ilen == 1) {
            i2c_write.addr[0] = (uint8_t)iaddr;      // Internal address
        }
        else if (ilen == 2) {
            i2c_write.addr[0] = (uint8_t)iaddr>>8;   // Internal address
            i2c_write.addr[1] = (uint8_t)iaddr;      // Internal address
        }
        else if (ilen == 3) {
            i2c_write.addr[0] = (uint8_t)iaddr>>16;  // Internal address
            i2c_write.addr[1] = (uint8_t)iaddr>>8;   // Internal address
            i2c_write.addr[2] = (uint8_t)iaddr;      // Internal address
        }
        else
            return;
    }

    twi_status = twi_master_write(p_twi, &i2c_write);

    switch(twi_status)
    {
        case TWI_RECEIVE_NACK:
            I2C_BusClear();
            __i2c_write_nack_cnt++;
            twi_master_write(p_twi, &i2c_write);    // one time re-transmit
            break;
        case TWI_ERROR_TIMEOUT:
            I2C_BusClear();
            __i2c_write_timeout_cnt++;
            twi_master_write(p_twi, &i2c_write);    // one time re-transmit
            break;
        default:
            break;
    }
}

void I2C_Rx(Twi *p_twi, uint8_t baddr, uint8_t saddr, uint8_t iaddr, uint8_t ilen, uint8_t dlen, uint8_t* regdata)
{
    uint32_t twi_status;

	twi_package_t i2c_read = {
        .addr_length  = 0,
		.chip         = baddr | saddr,  // TWI slave bus address
	    .buffer       = regdata,        // transfer data source buffer
	    .length       = dlen            // transfer data size (num bytes)
	};

    if(ilen != 0)
    {
        i2c_read.addr_length  = ilen;      // Length of iaddr (1-3 bytes)
        if(ilen == 1) {
            i2c_read.addr[0] = (uint8_t)iaddr;      // Internal address
        }
        else if (ilen == 2) {
            i2c_read.addr[0] = (uint8_t)iaddr>>8;   // Internal address
            i2c_read.addr[1] = (uint8_t)iaddr;      // Internal address
        }
        else if (ilen == 3) {
            i2c_read.addr[0] = (uint8_t)iaddr>>16;  // Internal address
            i2c_read.addr[1] = (uint8_t)iaddr>>8;   // Internal address
            i2c_read.addr[2] = (uint8_t)iaddr;      // Internal address
        }
        else
            return;
    }

	twi_status = twi_master_read(p_twi, &i2c_read);

    if(twi_status == TWI_RECEIVE_NACK)
        __i2c_read_nack_cnt++;

    if(twi_status == TWI_ERROR_TIMEOUT)
        __i2c_read_timeout_cnt++;
}

void I2C_BusClear(void)
{
    cpu_irq_disable();
    pmc_disable_periph_clk(ID_TWI0);

    pio_set_output(PIOA, PIN36_TWI0_SCL, LOW, FALSE, TRUE);

    // Tx 9 Clock Pulses to force slave release of Data Line
    for(uint8_t i = 0; i < 9; i++) {
        pio_set(PIOA, PIN36_TWI0_SCL);
        delay_us(5);
        pio_clear(PIOA, PIN36_TWI0_SCL);
        delay_us(5);
    }

    // Re-Configure TWI Pin Setting
    pmc_enable_periph_clk(ID_TWI0);
    pio_configure_pin_group(PIOA, PINS_TWI0_CLK,  PINS_TWI0_CLK_FLAGS);
    pio_configure_pin_group(PIOA, PINS_TWI0_DATA, PINS_TWI0_DATA_FLAGS);
    I2C_Config();
    cpu_irq_enable();
}

uint32_t I2C_GetReadNackCount(void)
{
    return __i2c_read_nack_cnt;
}
uint32_t I2C_GetWriteNackCount(void)
{
    return __i2c_write_nack_cnt;
}
uint32_t I2C_GetTimeoutCount(void)
{
    return __i2c_read_timeout_cnt;
}

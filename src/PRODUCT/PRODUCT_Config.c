//-----------------------------------------------------------------------------|
//	PRODUCT_Information.c
//	Abstract: Contains information about the current product:
//              Name, FW Version, etc
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#include <asf.h>
#include <string.h>
#include "PRODUCT_Config.h"
#include "NVM_Control.h"
#include "APP_Process.h"

// Handles to the constants placed in a fixed location
extern const char MANUID[TARGET_DEVICE_ID_SIZE];
extern const char MANUNAME[TARGET_DEVICE_ID_SIZE];
extern const char PRODUCTID[TARGET_DEVICE_ID_SIZE];
extern const char PRODNAME[TARGET_DEVICE_ID_SIZE];
extern const char FIRMVER[TARGET_DEVICE_ID_SIZE];

static char gProd_name[TARGET_NAME_SIZE];
static char gProd_ver[TARGET_VERSION_SIZE];

//static char pilot[8] = "DEV7\0";
//static char demo[6]  = "DEMO \0";

//==============================================================================
// Public Function Defintions
//==============================================================================
// called during power up to do a one time generation
// of null terminated strings for the Name() and Vesion() functions
void PRODUCT_Init(void)
{
    snprintf(gProd_name, TARGET_NAME_SIZE, "C80\0");

    // set firmware version
    snprintf(gProd_ver, TARGET_VERSION_SIZE, "%c.%c%c\0", FIRMVER[0], FIRMVER[1], FIRMVER[2]);
}

// return null terminated string of the product name
char * PRODUCT_Name(void)
{
    return gProd_name;
}

// return null terminates string of the product version
char * PRODUCT_Version(void)
{
    return gProd_ver;
}

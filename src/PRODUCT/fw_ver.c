#include "PRODUCT_Config.h"

// Places product ID data at fixed locations in Flash that the downloader can read
#pragma section = ".fw_version"
__root const char MANUID[TARGET_DEVICE_ID_SIZE]    @ ".fw_version" = {TARGET_DEVICE_MANUFACTURER_ID};
__root const char MANUNAME[TARGET_DEVICE_ID_SIZE]  @ ".fw_version" = {TARGET_DEVICE_MANUFACTURER_NAME};
__root const char PRODUCTID[TARGET_DEVICE_ID_SIZE] @ ".fw_version" = {TARGET_DEVICE_PRODUCT_ID};
__root const char PRODNAME[TARGET_DEVICE_ID_SIZE]  @ ".fw_version" = {TARGET_DEVICE_PRODUCT_NAME};
__root const char FIRMVER[TARGET_DEVICE_ID_SIZE]   @ ".fw_version" = {TARGET_DEVICE_FIRMWARE_VERSION};
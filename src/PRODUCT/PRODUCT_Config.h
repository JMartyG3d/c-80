//-----------------------------------------------------------------------------|
//	PRODUCT_Config.h
//	Abstract: Contains information about the current product:
//              Name, FW Version, etc
//
//	McIntosh Laboratories Inc.
//-----------------------------------------------------------------------------|
#ifndef __PRODUCT_CONFIG_H__
#define __PRODUCT_CONFIG_H__

#define TARGET_DEVICE_ID_SIZE   16 // size of the product ID strings
#define TARGET_NAME_SIZE        9  // max size of name string "MHA150\0"
#define TARGET_VERSION_SIZE     5  // max size of version string "N.nn\0"

#define TARGET_DEVICE_PRODUCT_ID         "Pxxxx           "   // 16 CHARS
#define TARGET_DEVICE_PRODUCT_NAME       "C80             "   // 16 CHARS
#define TARGET_DEVICE_MANUFACTURER_ID    "XXX             "   // 16 CHARS
#define TARGET_DEVICE_MANUFACTURER_NAME  "McIntosh        "   // 16 CHARS
#define TARGET_DEVICE_FIRMWARE_VERSION   "00a             "   // 16 CHARS

#define TARGET_DEVICE_FORCE_FACTORY_RESET             TRUE // Evaluate need for this for each update

//==============================================================================
// Public Function Prototypes
//==============================================================================
void   PRODUCT_Init(void);      // called during power up to do a one time generation
                                // of null terminated strings for the Name() and Vesion() functions

char * PRODUCT_Name(void);      // return null terminated string of the product name
char * PRODUCT_Version(void);   // return null terminates string of the product version

#endif // __PRODUCT_CONFIG_H__
